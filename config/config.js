const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
let env;

try {
	env = require('./env');
} catch (e) {
	env = 'local';
}

console.log('Se carga ' + env);

const config = {
	development: {
		secret: 'genit',
		base_url: 'http://localhost:5000/',
		api_url: 'http://localhost:5000/app/',
		root: rootPath,
		app: {
			name: 'Fuss'
		},
		port: process.env.PORT || 4000,
		db: 'mongodb://192.168.3.8:27017/sia',
		nodeMailerConfig: {
			host: 'mail.genit.com.ar',
			port: 26,
			secure: false,
			requireTLS: true, //Force TLS
			tls: {
				rejectUnauthorized: false
			},
			auth: {
				user: 'gestion@genit.com.ar',
				pass: 'Genit-2016'
			}
		}
	},
	gendesa: {
		secret: 'genit',
		base_url: 'http://localhost:5000/app/',
		root: rootPath,
		app: {
			name: 'Sia-Sueldos'
		},
		port: process.env.PORT || 5000,
		db: 'mongodb://192.168.3.8:27017/sia_gendesa',
		nodeMailerConfig: {
			host: 'mail.genit.com.ar',
			port: 26,
			secure: false,
			requireTLS: true, //Force TLS
			tls: {
				rejectUnauthorized: false
			},
			auth: {
				user: 'gestion@genit.com.ar',
				pass: 'Genit-2016'
			}
		}
	},
	local: {
		secret: 'genit',
		base_url: 'http://localhost:5000/',
		api_url: 'http://localhost:5000/app/',
		root: rootPath,
		app: {
			name: 'flus'
		},
		port: process.env.PORT || 4000,
		db: 'mongodb://127.0.0.1:27017/flus',
		nodeMailerConfig: {
			host: 'mail.genit.com.ar',
			port: 26,
			secure: false,
			requireTLS: true, //Force TLS
			tls: {
				rejectUnauthorized: false
			},
			auth: {
				user: 'gestion@genit.com.ar',
				pass: 'Genit-2016'
			}
		}
	},

	production: {
		secret: 'genit',
		root: rootPath,
		app: {
			name: 'node'
		},
		port: process.env.PORT || 3000,
		db: 'mongodb://192.168.3.8:27017/agrobox'
	}
};

module.exports = config[env];

