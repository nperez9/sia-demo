var express = require('express');
var glob = require('glob');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');
const session = require('express-session');

module.exports = function (app, config) {
    var env = process.env.NODE_ENV || 'development';
    app.locals.ENV = env;
    app.locals.ENV_DEVELOPMENT = env == 'development';

    // In this file you should put your angular  
    app.use(express.static('../'));
    app.use('/uploads', express.static('public'));
    // Set the enjine to HTML (if you use angular)
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'ejs');

    // Add extended parses to Express
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(cookieParser());
    app.use(compress());
    app.use(methodOverride());

    //security 
    app.use(session({
        saveUninitialized: true, // saved new sessions
        resave: false,
        secret: config.secret,
        cookie: { httpOnly: true, maxAge: 2419200000 } // configure when sessions expires
    }));

    return app;
};