var app = angular.module('index', ['ui.router','ui.bootstrap', 'ngCookies','ngMaterial','sia.empresas', 'sia.padron','sia.categorias', 'sia.ganancias','sia.valor' ,'sia.cargo', 'sia.login', 'sia.liquidacion','sia.conceptos', 'sia.subEmpresas', 'sia.periodos','angularMoment', 'sia.antiguedad', 'sia.usuarios', 'sia.variables', 'bw.paging', 'angular.filter', 'sia.bancos']);

app.config(function ($stateProvider, $mdIconProvider, $mdThemingProvider, $httpProvider, $mdDateLocaleProvider, $provide) {
    $httpProvider.defaults.headers.delete = { "Content-Type": "application/json;charset=utf-8" };

    $mdThemingProvider.theme('docs-dark', 'default')
      .primaryPalette('indigo')
      .dark();

    $mdDateLocaleProvider.parseDate = function(dateString) {
        var m = moment(dateString, 'DD/MM/YYYY', true);       
        return m.isValid() ? m.toDate() : new Date(NaN);
    };

    $mdDateLocaleProvider.formatDate = function (date) {
        if(date === undefined || !moment(date).isValid()) return null; 
        return moment.utc(date).format('DD/MM/YYYY');
    };

    $mdDateLocaleProvider.months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 
    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    
    $mdDateLocaleProvider.shortMonths = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 
        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
    
    $mdDateLocaleProvider.days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'];
    
    $mdDateLocaleProvider.shortDays = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'];

    $mdDateLocaleProvider.firstDayOfWeek = 1;

    $mdDateLocaleProvider.msgCalendar = 'Calendario';
    
    $mdDateLocaleProvider.msgOpenCalendar = 'Abrir calendario';

    var DEFAULT_TIMEZONE = 'UTC';

     $provide.decorator('dateFilter', ['$delegate', '$injector', function($delegate, $injector) {
       var oldDelegate = $delegate;

       var standardDateFilterInterceptor = function(date, format, timezone) {
         return oldDelegate.apply(this, [date, format, timezone]);
       };

       return standardDateFilterInterceptor;
    }]);


}).run(function($rootScope, $state, $cookies) {
    $state.go('login');
});

app.controller('indexController', function ($rootScope, moment,  $cookies) {
    
    $rootScope.user = {};
    if($cookies.get('nombre')){
        $rootScope.user.loggued = true;
        $rootScope.user.isAdmin = $cookies.get('tipo') == "admin" ? true : false; 
        $rootScope.user.nombre = $cookies.get('nombre');
    }

    $rootScope.formatMyDate = function (myDate) {
        var r = moment(myDate).format('DD/MM/YYYY');
        return r;
    }
});

app.filter('startFrom', function () {
    return function(input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
    }
});

app.directive('shortcut', function() {
    return {
      restrict: 'E',
      replace: true,
      scope: true,
      link:    function postLink(scope, iElement, iAttrs){
        jQuery(document).on('keyup', function(e){
           scope.$apply(scope.keyPressed(e));
         });
      }
    };
  });
