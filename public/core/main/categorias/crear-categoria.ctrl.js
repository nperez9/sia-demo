categorias.controller('crearCategoriasController', ['$scope', '$rootScope', '$state', '$mdDialog', 'apiService',
function($scope, $rootScope, $state, $mdDialog, apiService) {
  $rootScope.sideBar = true;  
 
  $scope.showAdvanced = function(ev) {
    $mdDialog.show({
      controller: confirmarController,
      templateUrl: 'core/main/categorias/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
  };

  function init () {
  
  };

  $scope.save = function(){
    apiService.post('categorias', $scope.categoria) 
    .then(function successCallback(response) {
      $state.go('categorias.listado');          
    });
  }

  init();
}]);

