categorias.controller('verCategoriasController', ['$rootScope', '$scope', '$filter', '$stateParams', '$state', '$mdDialog', 'apiService', 'categoriasService',
function($rootScope, $scope, $filter, $stateParams, $state, $mdDialog, apiService, categoriasService) {
  $rootScope.sideBar = true;
	//$scope.token = $cookies.get('token');
  $scope.searchText = $stateParams.search;
  $scope.id = $stateParams.id;
  $scope.ver = $stateParams.ver;
  
  $scope.showAdvanced = function(ev) {
    $mdDialog.show({
      controller: confirmarController,
      templateUrl: 'core/main/categorias/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
  };
  
  $scope.confirmModal = function(ev, item) {
    $mdDialog.show({
      controller: categoriaDeleteController,
      templateUrl: 'core/main/categorias/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function(data) {
      if (data) {
        categoriasService.delete(item._id).then(
          function successCallback(response) {
            $state.go('categorias.listado', {}, {reload: true});
          }, 
          function errorCallback(response) {
            console.log(response)
        });
      }
    });
  };


  function init () {
    apiService.get('categorias/' + $scope.id) 
      .then(function successCallback(response) {
        $scope.item = response;
      });
  };

  init();

}]);