categorias.controller('categoriasController', ['$scope', '$rootScope', '$filter', '$state', '$stateParams', '$mdDialog', 'apiService', 'categoriasService', '$cookies', 'Notification',
function ($scope, $rootScope, $filter, $state, $stateParams, $mdDialog, apiService, categoriasService, $cookies, Notification) {
  $rootScope.sideBar = true;
  $scope.currentPage = 1;
  $scope.pageSize = 20;
  $scope.searchText = $stateParams.search;

	function init () {
    if(!$cookies.get('token')){
      Notification.error({message: 'Debe iniciar sesión', delay: 3000});
      $state.go('login')
    }else{
      if($cookies.get('tipo') != "admin"){
        Notification.error({message: 'Acceso denegado.', delay: 3000});
        $state.go('liquidacion')
      }else{
        $scope.search(true);
      }
    }

  }

  $scope.confirmModal = function (ev, item) {
    $mdDialog.show({
      controller: categoriaDeleteController,
      templateUrl: 'core/main/categorias/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function (data) {
      if (data !== true) 
        return false; 
        
      categoriasService.delete(item._id).then(function (r) {
        $scope.search();
      });      
    });
  };

  window.onkeyup = function(e) {
    var key = e.keyCode ? e.keyCode : e.which;
    if (key == 13) {
        $scope.search();
    }
  }

  $scope.search = function () {
    apiService.get('categorias/?search='+$scope.searchText+'&page='+$scope.currentPage)
    .then(function (response) {
      $scope.items = response.items;
      $scope.total = response.total;
    });
  }
    
  init();
}]);


function categoriaDeleteController($scope, $mdDialog) {
    $scope.hide = function() {
      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.answer = function(boolean) {
      $mdDialog.hide(boolean);
    };

    $scope.init = function(){
    };
};