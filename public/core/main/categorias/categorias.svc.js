categorias.service('categoriasService', ['apiService', 
function (apiService) {
    this.url = 'categorias/'; 
    this.categorias = {}; 
    
    function errorCallback (r) {
        console.log(r); 
    }
    
    this.scopeCategoria = function ($scope, id) {
        var url = this.url + id; 
        return apiService.request(
            url, 
            'GET'
        ).then(function (response) {
            $scope.categoria = response; 
        }, errorCallback);
    }

    this.insertCategoria = function (categoria) {
        return apiService.request(
            this.url, 
            'POST', 
            categoria
        ); 
    }

    this.editCategoria = function (categoria, id) {
        return apiService.request(
            this.url + id, 
            'PUT', 
            categoria
        ); 
    }

    this.delete = function (id) {
        return apiService.request(
            this.url + id, 
            'DELETE',
            null, 
            false
        ); 
    }
}]); 