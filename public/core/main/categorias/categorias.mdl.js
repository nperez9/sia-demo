var categorias = angular.module( 'sia.categorias', ['ui.router','ui.bootstrap','bw.paging', 'ngCookies', 'ui-notification']).config(function config($stateProvider, $urlRouterProvider) {
	
	$urlRouterProvider.when('/categorias', '/categorias/listado');

	$stateProvider
	    .state('categorias',{
	    	url:'/categorias',
	    	abstract:true,
	    }) 
	    .state('categorias.listado',{
	    	url:'/listado/:search',
	    	templateUrl:'core/main/categorias/categorias.tpl.html',
	    	controller:'categoriasController',
	    	params: { 
    		   search: ""         
    		}
	    })
	    .state('categorias.crear',{
	    	url:'/crear',
	    	templateUrl:'core/main/categorias/crear-categoria.tpl.html',
	    	controller:'crearCategoriasController',
	    })
	    .state('categorias.edit',{
	    	url:'/editar/:id/:search/:ver',
	    	templateUrl:'core/main/categorias/edit-categoria.tpl.html',
	    	controller:'editCategoriasController',
	    	params: { 
    		   search: ""         
    		}
	    })
	    .state('categorias.ver',{
	    	url:'ver/:id/:search',
	    	templateUrl:'core/main/categorias/ver-categoria.tpl.html',
	    	controller:'verCategoriasController',
	    	params: { 
    		   search: ""         
    		}
	    })
});
