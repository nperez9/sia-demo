categorias.controller('editCategoriasController', ['$scope', '$rootScope', '$state', '$mdDialog', 'apiService', '$stateParams', 'categoriasService',
function($scope, $rootScope, $state, $mdDialog, apiService, $stateParams, categoriasService) {
  $rootScope.sideBar = true;  
  $scope.id = $stateParams.id;
  $scope.searchText = $stateParams.search;
  $scope.ver = $stateParams.ver;
  
  $scope.showAdvanced = function (ev) {
    $mdDialog.show({
      controller: confirmarController,
      templateUrl: 'core/main/categorias/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
  };

  $scope.cancelar = function () {
    if ($scope.ver == 1) {
      $state.go('categorias.ver', { id: $scope._id, search: $scope.searchText});
    } else {
      $state.go('categorias.listado', { search: $scope.searchText});
    }
  }

  
  function init () {
    apiService.get('categorias/' + $scope.id) 
    .then(function successCallback (response) {
      $scope.categoria = response;
    });
  }

  $scope.save = function () {
    apiService.request(
      'categorias/' + $scope._id, 
      'PUT',
      $scope.categoria)
    .then(function successCallback(response) {
      $state.go('categorias.listado', { expired:true });
    });
  }

  init();
}]);

