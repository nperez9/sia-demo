function columnaDeleteController($scope, $mdDialog) {
    $scope.hide = function() {
      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.answer = function(boolean) {
      $mdDialog.hide(boolean);
    };

  };
