function formColumnaEditController($scope, $mdDialog, apiService, $state, banco, archivo, item) {
    $scope.hide = function () {
        $mdDialog.hide(false);
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    function init () {
        if(item != null){
            $scope.columna = banco[archivo][item];
        }
    };

    $scope.save = function () {

        banco[archivo][item] = $scope.columna

        apiService.request(
            'bancos/edit/columna',
            'POST',
            { banco: banco })
            .then(function successCallback(response) {
                banco = response
                $mdDialog.hide();
            });
    };

    init();
};
