function formColumnaController($scope, $mdDialog, apiService, $state, banco, archivo, item) {
    $scope.hide = function () {
        $mdDialog.hide(false);
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    function init() {
    };

    $scope.save = function () {

        switch (archivo) {
            case 'head':
                banco.head.push($scope.columna);
                break;
            case 'body':
                banco.body.push($scope.columna);
                break;
            case 'footer':
                banco.footer.push($scope.columna);
                break;
        }

        apiService.request(
            'bancos/columna',
            'POST',
            { banco: banco, archivo: archivo, columna: $scope.columna.columna })
            .then(function successCallback(response) {
                if (response.success == false) {
                    switch (archivo) {
                        case 'head':
                            banco.head.pop()
                            break;
                        case 'body':

                            break;
                        case 'footer':

                            break;
                    }
                }

                banco = response
                $mdDialog.hide();
            });
    };

    init();
};
