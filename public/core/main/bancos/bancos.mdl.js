var bancos = angular.module( 'sia.bancos', ['ui.router','ui.bootstrap','bw.paging', 'ngCookies', 'ui-notification']).config(function config($stateProvider, $urlRouterProvider) {
	
	$urlRouterProvider.when('/bancos', '/bancos/listado');

	$stateProvider
	    .state('bancos',{
	    	url:'/bancos',
	    	abstract:true,
	    }) 
	    .state('bancos.listado',{
	    	url:'/listado/:search',
	    	templateUrl:'core/main/bancos/reporte-bancos.tpl.html',
	    	controller:'reporteBancosController',
	    	params: { 
    		   search: ""         
    		}
		})
		.state('bancos.columnas',{
	    	url:'/columnas/:id',
	    	templateUrl:'core/main/bancos/columnas-banco.tpl.html',
	    	controller:'columnasBancoController',
	    	params: { 
    		}
		})
});
