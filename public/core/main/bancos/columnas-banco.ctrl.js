bancos.controller('columnasBancoController', ['$scope', '$rootScope', '$state', '$mdDialog', '$stateParams', 'apiService',
  function ($scope, $rootScope, $state, $mdDialog, $stateParams, apiService) {
    $rootScope.sideBar = true;
    var bancoId = $stateParams.id;

    function init() {

      apiService.request(
        'bancos/columna/' + bancoId,
        'GET')
        .then(function successCallback(response) {
          $scope.banco = response;
        });
    };

    $scope.agregarColumna = function (archivo) {
      $mdDialog.show({
        controller: formColumnaController,
        templateUrl: 'core/main/bancos/form-columnas.tpl.html',
        parent: angular.element(document.body),
        locals: {
          banco: $scope.banco,
          archivo: archivo,
          item: null
        }
      }).then(function (data) {
      });
    };

    $scope.editarColumna = function (archivo, item) {
      $mdDialog.show({
        controller: formColumnaEditController,
        templateUrl: 'core/main/bancos/form-columnas.tpl.html',
        parent: angular.element(document.body),
        locals: {
          banco: $scope.banco,
          archivo: archivo,
          item: item
        }
      }).then(function (data) {
      });
    };

    $scope.eliminarColumna = function (archivo, index) {

      $mdDialog.show({
        controller: columnaDeleteController,
        templateUrl: 'core/main/bancos/confirm-modal.tpl.html',
        parent: angular.element(document.body),
        locals: {
        }
      }).then(function (data) {
        if (!data)
          return false;

        $scope.banco[archivo].splice(index, 1);

        apiService.request(
          'bancos/columna/delete',
          'POST',
          {
            banco : $scope.banco
          })
          .then(function successCallback(response) {
            $scope.banco = response;
          });
      });

    };

    init();
  }]);