function nombreBancoController($scope, $mdDialog, apiService, $state) {
    $scope.hide = function () {
        $mdDialog.hide(false);
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    function init () {
        apiService.request(
            'bancos',
            'GET')
            .then(function successCallback(response) {
                $scope.bancos = response
            });
    };

    $scope.save = function () {
        console.log($scope.banco)
        apiService.request(
            'bancos/init',
            'POST',
            { 
                id : $scope.banco._id,
                nombre : $scope.banco.nombre
            })
            .then(function successCallback(response) {
                $state.go('bancos.columnas', { id: response.result._id });
                $mdDialog.hide();
            });
    };

    init();
};