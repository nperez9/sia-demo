bancos.controller('reporteBancosController', ['$scope', '$rootScope', '$state', '$mdDialog', '$stateParams', 'apiService',
  function ($scope, $rootScope, $state, $mdDialog, $stateParams, apiService) {
    $rootScope.sideBar = true;
    $scope.searchText = $stateParams.search;

    function init() {
      apiService.request(
        'bancos/reportes',
        'GET')
        .then(function successCallback(response) {
          $scope.bancos = response
        });
    };

    $scope.crearBanco = function (ev) {
      $mdDialog.show({
        controller: nombreBancoController,
        templateUrl: 'core/main/bancos/nombre-banco.tpl.html',
        parent: angular.element(document.body),
        locals: {
        }
      }).then(function (data) {
      });
    };

    $scope.eliminarBanco = function (item) {
      $mdDialog.show({
        controller: columnaDeleteController,
        templateUrl: 'core/main/bancos/confirm-modal.tpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose: true
      }).then(function (data) {
        if (!data)
          return false;

        apiService.request(
          'bancos/delete/' + item._id,
          'GET')
          .then(function successCallback(response) {
            $scope.bancos = response
          });
      });
    };

    init();
  }]);