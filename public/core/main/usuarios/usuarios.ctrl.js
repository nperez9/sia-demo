usuarios.controller('usuariosController', ['$scope', '$rootScope', '$filter', '$state', '$stateParams', '$mdDialog', 'apiService', 'usuariosService',
function ($scope, $rootScope, $filter, $state, $stateParams, $mdDialog, apiService, usuariosService) {
    $rootScope.sideBar = true;
    $scope.currentPage = 1;
    $scope.pageSize = 20;
    $scope.total = 1;
    $scope.searchText = $stateParams.search;

    window.onkeyup = function (e) {
        var key = e.keyCode ? e.keyCode : e.which;
        if (key == 13) {
            $scope.search();
        }
    }

    $scope.search = function () {
        apiService.get('usuarios/listado/?page=' + $scope.currentPage + '&search=' + $scope.searchText)
        .then(function successCallback(response) {
            $scope.usuarios = response.items;
            $scope.total = response.total;
        });
    }

    $scope.eliminarUsuario = function (ev, item) {
        $mdDialog.show({
            controller: conceptoDeleteController,
            templateUrl: 'core/main/usuarios/confirm-modal.tpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        }).then(function (data) {
            if (!data)
                return false;

            usuariosService.eliminarUsuario(item._id)
            .then(function (response) {
                $scope.search();
            });
        });
    };

    function init() {
        $scope.search();
    }

    $scope.paging = function (pagina) {
        $scope.currentPage = pagina;
        $scope.search();
    }

    init();
}]);
