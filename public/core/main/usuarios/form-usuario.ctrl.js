usuarios.controller('formUsuarioController', ['$scope', '$rootScope', '$stateParams', '$state', '$mdDialog', 'apiService', 'Notification', 'usuariosService',
    function ($scope, $rootScope, $stateParams, $state, $mdDialog, apiService, Notification, usuariosService) {
        var id = $stateParams.id == 'undefined' ? 0 : $stateParams.id;
        var ver = $stateParams.ver == 'undefined' ? 0 : $stateParams.ver;
        $rootScope.sideBar = true;
        $scope.title = 'Ingresar Usuario';
        $scope.usuario = {};
        $scope.usuario.permiso_empresas = [];
        
        $scope.save = function () {
            if (id == 0) {
                usuariosService.guardarUsuario($scope.usuario)
                    .then(function (response) {
                        $state.go('usuarios.listado');
                    });
            }else{
                usuariosService.editarUsuario($scope.usuario, $scope.usuario._id)
                    .then(function successCallback(response) {
                        if (response.success)
                            $state.go('usuarios.listado');
                    });
            }
            
        };

        $scope.goBack = function () {
            if(ver == "1")
                $state.go('usuarios.ver',{id: $scope.usuario._id});
            else
                $state.go('usuarios.listado');
        }

        if(id != 0){
            usuariosService.getDatosUsuario($scope, id)
                .then(function (usuario) {
                    $scope.usuario = usuario;
                });
        }

        $scope.asignarEmpresa = function (empresa) {
            var check = $scope.usuario.permiso_empresas.indexOf(empresa);
            if(check != 0){
                $scope.usuario.permiso_empresas.push(empresa);
            }
        }

        $scope.eliminarEmpresa = function (empresa) {
            Array.prototype.removeItem = function (a) {
                for (var i = 0; i < this.length; i++) {
                    if (this[i] == a) {
                        for (var i2 = i; i2 < this.length - 1; i2++) {
                            this[i2] = this[i2 + 1];
                        }
                        this.length = this.length - 1;
                        return;
                    }
                }
            };

            $scope.usuario.permiso_empresas.removeItem(empresa);
        }

        function getEmpresas() {
            apiService.request(
                'empresas/all',
                'GET'
            ).then(function (empresas) {
                $scope.empresas = empresas;
            });
        }

        function init() {
            getEmpresas();
        }

        init();
    }]);
