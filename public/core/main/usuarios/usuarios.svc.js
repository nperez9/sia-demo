usuarios.service('usuariosService', ['apiService',
function (apiService) {
    this.antiguedad = {};

    function errorCallback (r) {
        console.log(r);
    }

    this.getUsuarios = function ($scope) {
        return apiService.request(
            'usuarios/',
            'GET'
        ).then(function (response) {
            $scope.usuarios = response;
        }, errorCallback);
    }

    this.guardarUsuario = function (usuario) {
        return apiService.request(
            'usuarios/',
            'POST',
            usuario
        );
    }

    this.getDatosUsuario = function ($scope, id) {
        return apiService.request(
            'usuarios/' + id,
            'GET'
        ).then(function (response) {
            return response;
        }, errorCallback);
    }

    this.editarUsuario = function (usuario, id) {
        return apiService.request(
            'usuarios/' + id,
            'PUT',
            usuario
        );
    }

    this.eliminarUsuario = function (id) {
        return apiService.request(
            'usuarios/' + id,
            'DELETE',
            null,
            false
        );
    }

}]);
