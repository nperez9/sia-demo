var usuarios = angular.module('sia.usuarios', ['ui.router', 'ui.bootstrap', 'bw.paging', 'ngCookies', 'ui-notification']).config(function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when('/usuarios', '/usuarios/listado');

    $stateProvider
        .state('usuarios', {
            url: '/usuarios',
            abstract: true,
        })
        .state('usuarios.listado', {
            url: '/listado/:search',
            templateUrl: 'core/main/usuarios/usuarios.tpl.html',
            controller: 'usuariosController',
            params: {
                search: ""
            }
        })
        .state('usuarios.crear', {
            url: '/crear',
            templateUrl: 'core/main/usuarios/form-usuario.tpl.html',
            controller: 'formUsuarioController',
            params:{
                id: 0
            }
        })
        .state('usuarios.ver', {
            url: '/:id/ver',
            templateUrl: 'core/main/usuarios/ver-usuario.tpl.html',
            controller: 'verUsuarioController'
        })
        .state('usuarios.editar', {
            url: 'editar/:id/:ver/:search',
            templateUrl: 'core/main/usuarios/form-usuario.tpl.html',
            controller: 'formUsuarioController',
            params: {
                search: ""
            }
        })
});
