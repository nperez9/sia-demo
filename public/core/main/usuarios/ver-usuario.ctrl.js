usuarios.controller('verUsuarioController', ['$scope', '$rootScope', '$stateParams', '$state', '$mdDialog', 'apiService',
function ($scope, $rootScope, $stateParams, $state, $mdDialog, apiService) {
  $scope.usuario = {};
  $scope.id = $stateParams.id;
  $rootScope.sideBar = true;

  
  $scope.confirmModal = function(ev, item) {
    $mdDialog.show({
      controller: confirmarController,
      templateUrl: 'core/main/usuarios/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function (data) {
      if (data) {
        apiService.request(
          'usuarios/' + item._id,
          'DELETE',
          null, 
          false
        ).then(function successCallback (response) {
            $state.go('usuarios.listado', {reload:true});
        }, function errorCallback(response) { 
        });
      }
    });
  };

  $scope.goBack = function () {
		$state.go('usuarios.listado');
  }

  function init () {
    apiService.get('usuarios/' + $scope.id)
    .then(function (usuario) {
      $scope.usuario = usuario;
    });
  }

  init();
}]);

