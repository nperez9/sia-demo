conceptos.controller('editConceptoController', ['$scope', '$rootScope', '$state', '$mdDialog', '$stateParams', 'apiService', 'conceptosService',
function ($scope, $rootScope, $state, $mdDialog, $stateParams, apiService, conceptosService) {
		$scope.searchText = $stateParams.search;
		$scope.title = 'Editar Concepto';
		$scope.ver = $stateParams.ver;

		var id = $stateParams.id;
		var empresaid = $stateParams.empresaid;
		$rootScope.sideBar = (empresaid == '0') ? true : false;

		function getConcepto() {
			conceptosService.scopeConcepto($scope, id);
		}

		function init() {
			getConcepto();
			apiService.get('categorias/all')
				.then(function successCallback(response) {
					$scope.categorias = response;
				});
		}

		$scope.cancelar = function () {
			if ($scope.ver == 1) {
				$state.go('conceptos.ver', { id: id, search: $scope.searchText });
			} else {
				$state.go('conceptos.listado', { search: $scope.searchText });
			}
		}

		$scope.goBack = function () {
			if (empresaid == '0')
				if($scope.ver == '1')
				$state.go('conceptos.ver', {id:id, ver:1, search:$scope.searchText, empresaid:empresaid})
				else
				$state.go('conceptos.listado');
			else
				if($scope.ver == '1')
					$state.go('conceptos.ver', {id:id, ver:1, search:$scope.searchText, empresaid:empresaid})
				else
				$state.go('liquidacion', { estado: 'concepto' });
		}


		$scope.save = function () {
			apiService.request(
				'conceptos/' + $scope._id,
				'PUT',
				$scope.concepto)
				.then(function successCallback(response) {
					if (response.success)
						$scope.goBack();
				});
		}

		init();
	}]);
