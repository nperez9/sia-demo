conceptos.service('conceptosService', ['apiService', 
function (apiService) {
    this.url = 'conceptos/'; 
    this.conceptos = {}; 
    
    function errorCallback (r) {
        console.log(r); 
    }
    
    this.scopeConcepto = function ($scope, id) {
        var url = this.url + id; 
        return apiService.request(
            url, 
            'GET'
        ).then(function (response) {
            $scope.concepto = response; 
        }, errorCallback);
    }

    this.getConceptosEmpresa = function (empID) {
        return apiService.request(
            this.url + 'empresa/' + empID, 
            'GET'
        ); 
    }

    this.insertConcepto = function (concepto) {
        return apiService.request(
            this.url, 
            'POST', 
            concepto
        ); 
    }

    this.editConcepto = function (concepto, id) {
        return apiService.request(
            this.url + id, 
            'PUT', 
            concepto
        ); 
    }

    this.deleteConcepto = function (id) {
        return apiService.request(
            this.url + id, 
            'DELETE'
        ); 
    }
}]); 