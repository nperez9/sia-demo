conceptos.controller('conceptosController', ['$scope', '$rootScope', '$filter', '$state', '$stateParams', '$mdDialog', 'apiService', '$cookies', 'Notification',
function($scope, $rootScope, $filter, $state, $stateParams, $mdDialog, apiService, $cookies, Notification) {
  $rootScope.sideBar = true;
  $scope.currentPage = 1;
  $scope.pageSize = 20;
  $scope.total = 1;
  $scope.searchText = $stateParams.search;

  $scope.confirmModal = function(ev, item) {
    $mdDialog.show({
      controller: conceptoDeleteController,
      templateUrl: 'core/main/conceptos/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function(data) {
      if (!data)
        return false;

      apiService.request(
        'conceptos/' + item._id,
        'DELETE',
        null,
        false
      ).then(function (response) {
        $scope.search();
      });

    });
  };

  window.onkeyup = function(e) {
      var key = e.keyCode ? e.keyCode : e.which;
      if (key == 13) {
        $scope.search();
      }
  }

  $scope.search = function () {
      apiService.get('conceptos/?page='+$scope.currentPage+'&search='+$scope.searchText)
      .then(function successCallback(response) {
          $scope.items = response.items;
          $scope.total = response.total;
      });
  }

	function init () {
    if(!$cookies.get('token')){
      Notification.error({message: 'Debe iniciar sesión', delay: 3000});
      $state.go('login')
    }else{
      if($cookies.get('tipo') != "admin"){
        Notification.error({message: 'Acceso denegado.', delay: 3000});
        $state.go('liquidacion')
      }else{
        $scope.search(true);
      }
    }
  }

  $scope.paging = function (pagina) {
    $scope.currentPage = pagina;
    $scope.search();
  }

  $scope.verRemuneracion = function (idRemuneracion) {
    var r = '';

    switch (idRemuneracion) {
      case 1:
        r = 'Remunerativo'
        break;
      case 2:
        r = 'No remunerativo'
        break;
      case 3:
        r = 'Deduccion'
        break;
      default:
        break;
    }

    return r;
  }

  init();
}]);

function conceptoDeleteController($scope, $mdDialog) {
    $scope.hide = function() {
      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.answer = function(boolean) {
      $mdDialog.hide(boolean);
    };

    $scope.init = function(){
    };

};
