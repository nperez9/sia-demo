conceptos.controller('verConceptoController', ['$scope', '$rootScope', '$state', '$mdDialog', '$stateParams', 'apiService', 'conceptosService',
function($scope, $rootScope, $state, $mdDialog, $stateParams, apiService, conceptosService) {
  $scope.esEmpresa = false;
  $scope.searchText = $stateParams.search;
  var id = $stateParams.id; 
  var empresaid = $stateParams.empresaid; 

  if(empresaid != '0'){
    $scope.esEmpresa = true;
  }
  
  $rootScope.sideBar = !$scope.esEmpresa;

  $scope.confirmModal = function (ev, item) {
    $mdDialog.show({
      controller: conceptoDeleteController,
      templateUrl: 'core/main/conceptos/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function(data) {
      if (data) {
        apiService.request(
          'conceptos/' + item._id,
          'DELETE',
          $scope.item
        ) 
        .then(function successCallback(response) {
          $scope.goBack();
        });
      }
    });
  };

  $scope.goBack = function () {
		if ($scope.esEmpresa)
			$state.go('liquidacion', {estado: 'concepto'});
		else 
			$state.go('conceptos.listado');
  }

  function getConcepto () {
    conceptosService.scopeConcepto($scope, id); 
  }

  function init () {
    getConcepto(); 
  };

  init();
}]);