conceptos.controller('crearConceptoController', ['$scope', '$rootScope', '$filter', '$stateParams', '$state', '$mdDialog', 'apiService', 'Notification', 'conceptosService',
function($scope, $rootScope, $filter, $stateParams, $state, $mdDialog, apiService, Notification, conceptosService) {
  $rootScope.sideBar = true;
  $scope.title = 'Crear Concepto';

  $scope.confirmModal = function(ev) {
    $mdDialog.show({
      controller: confirmController,
      templateUrl: 'core/main/conceptos/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
  };
  //Notification.error('Hubo un error en el servidor')
  $scope.goBack = function () {
    $state.go('conceptos.listado');
  }

  $scope.save = function () {
    conceptosService.insertConcepto($scope.concepto)
    .then(function (response) {
      $scope.goBack();
    });
  };

  function getCategorias () {
    apiService.get('categorias/all')
    .then(function successCallback(response) {
      $scope.categorias = response;
    });
  }

  function init () {
    getCategorias();
  };

  init();
}]);
