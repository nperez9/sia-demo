var conceptos = angular.module( 'sia.conceptos', ['ui.router','ui.bootstrap','bw.paging', 'ngCookies', 'ui-notification']).config(function config($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.when('/conceptos', '/conceptos/listado');

	$stateProvider
	    .state('conceptos',{
	    	url:'/conceptos',
	    	abstract:true,
	    }) 
	    .state('conceptos.listado',{
	    	url:'/listado/:search',
	    	templateUrl:'core/main/conceptos/conceptos.tpl.html',
	    	controller:'conceptosController',
	    	params: { 
    		   search: ""         
    		}	    	
	    })
	    .state('conceptos.crear',{ 
	    	url:'/crear', 
	    	templateUrl:'core/main/conceptos/concepto-form.tpl.html',
	    	controller:'crearConceptoController',
		})
		.state('conceptos.crear-empresa',{ 
	    	url:'/crear/:empresaid', 
	    	templateUrl:'core/main/conceptos/concepto-form.tpl.html',
	    	controller:'crearConceptoEmpresaController',
	    })
	    .state('conceptos.ver',{
	    	url:'/:id/ver/:empresaid',
	    	templateUrl:'core/main/conceptos/concepto-form.tpl.html',
	    	controller:'verConceptoController',
		})
	    .state('conceptos.editar',{
	    	url:'editar/:id/:ver/:search/:empresaid',
	    	templateUrl:'core/main/conceptos/concepto-form.tpl.html',
	    	controller:'editConceptoController',	    	
	    	params: { 
    		   search: ""         
    		}
		})
});
