conceptos.controller('crearConceptoEmpresaController', ['$scope', '$rootScope', '$filter', '$stateParams', '$state', '$mdDialog', 'apiService', 'Notification', 'conceptosService',
function($scope, $rootScope, $filter, $stateParams, $state, $mdDialog, apiService, Notification, conceptosService) {
  $rootScope.sideBar = false;
  $scope.title = 'Crear Concepto';
  $scope.concepto = {};
  var empresaId = $stateParams.empresaid;

  $scope.confirmModal = function(ev) {
    $mdDialog.show({
      controller: confirmController,
      templateUrl: 'core/main/conceptos/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
  };
  //Notification.error('Hubo un error en el servidor')

  $scope.goBack = function () {
    $state.go('liquidacion', {estado: 'concepto'});
  }

  $scope.save = function () { 
    conceptosService.insertConcepto($scope.concepto)
    .then(function (response) {
      $scope.goBack();
    });
  };

  function getCategorias () {
    apiService.get('categorias/all') 
    .then(function successCallback(response) {
      $scope.categorias = response;
    }); 
  }

  function getEmpresa () {
    apiService.request('empresas/' + empresaId, 'GET')
    .then(function (response) {
      $scope.concepto.empresa = response;
    });
  }

  function init () {
    getCategorias();
    getEmpresa();
  };

  init();
}]);
