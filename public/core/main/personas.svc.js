padron.service('personaService', ['apiService', 
function (apiService) {
    this.url = 'personas/'; 
    this.padron = {}; 
    
    function errorCallback (r) {
        console.log(r); 
    }
    
    this.scopePersona = function ($scope, id) {
        var url = this.url + 'one/' + id; 
        return apiService.request(
            url, 
            'GET'
        ).then(function (response) {
            $scope.persona = response; 
        }, errorCallback);
    }

    this.insertPersona = function (padron) {
        return apiService.request(
            this.url, 
            'POST', 
            padron
        ); 
    }

    this.editPersona = function (persona, id) {
        return apiService.request(
            this.url + id, 
            'PUT', 
            persona
        ); 
    }

    this.deletePersona = function (id) {
        return apiService.request(
            this.url + id, 
            'DELETE',
            null, 
            false
        ); 
    }
}]); 