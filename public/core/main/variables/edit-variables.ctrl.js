variables.controller('editarVariableController', ['$scope', '$rootScope', '$state', '$mdDialog', '$stateParams', 'apiService', 'conceptosService',
	function ($scope, $rootScope, $state, $mdDialog, $stateParams, apiService, conceptosService) {
		$scope.title = 'Editar variable';

		var id = $stateParams.id;
		var empresaid = $stateParams.empresaid;
		$rootScope.sideBar = false;

		function init() {
			apiService.get('variables/' + id)
				.then(function successCallback(response) {
					$scope.variable = response;
				});
		}

		$scope.cancelar = function () {
			$state.go('liquidacion', {estado:'variables'})
		}

		$scope.save = function () {
			apiService.request(
				'variables/' + $scope.variable._id,
				'PUT',
				$scope.variable)
				.then(function successCallback(response) {
					$state.go('liquidacion', { estado: 'variables' });
				}, function errorCallback(response) {
					console.log(response);
				});
		}

		init();
	}]);
