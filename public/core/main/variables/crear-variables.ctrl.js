variables.controller('crearVariableController', ['$scope', '$rootScope', '$filter', '$stateParams', '$state', '$mdDialog', 'apiService', 'Notification', 'apiService',
function($scope, $rootScope, $filter, $stateParams, $state, $mdDialog, apiService, Notification, apiService) {
  $rootScope.sideBar = false;
  $scope.title = 'Crear valores';
  var empresaid = $stateParams.empresaid;
  var periodoid = $stateParams.periodoid;

  $scope.confirmModal = function(ev) {
    $mdDialog.show({
      controller: confirmController,
      templateUrl: 'core/main/conceptos/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
  };

  $scope.save = function () {
    $scope.variable.empresa = empresaid;
    $scope.variable.periodo = periodoid;

    apiService.post('variables/', $scope.variable)
    .then(function successCallback(response) {
      $state.go('liquidacion',{ estado: 'variables' })
    });
  };

  $scope.cancelar = function () {
    $state.go('liquidacion', {estado:'variables'})
  }

  function init () {
  };

  init();
}]);
