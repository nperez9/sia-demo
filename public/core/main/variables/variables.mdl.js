var variables = angular.module( 'sia.variables', ['ui.router','ui.bootstrap','bw.paging', 'ngCookies', 'ui-notification']).config(function config($stateProvider, $urlRouterProvider) {

	// $urlRouterProvider.when('/valores', '/valores/listado');

	$stateProvider
	    .state('varaibles',{
	    	url:'/variables',
	    	abstract:true,
	    }) 
	    .state('crear',{ 
	    	url:'variables/:empresaid/:periodoid',
	    	templateUrl:'core/main/variables/variable-form.tpl.html',
	    	controller:'crearVariableController',
		})
	    .state('editar',{
	    	url:'editar/:id',
	    	templateUrl:'core/main/variables/variable-form.tpl.html',
	    	controller:'editarVariableController',	    	
		})
});
