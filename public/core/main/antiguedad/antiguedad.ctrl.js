antiguedad.controller('antiguedadController', ['$scope', '$rootScope', '$filter', '$state', '$stateParams', '$mdDialog', 'apiService', 'antiguedadService', '$cookies', 'Notification',
function ($scope, $rootScope, $filter, $state, $stateParams, $mdDialog, apiService, antiguedadService, $cookies, Notification) {
    $rootScope.sideBar = true;
    $scope.currentPage = 1;
    $scope.pageSize = 20;
    $scope.total = 1;
    $scope.searchText = $stateParams.search;

    window.onkeyup = function (e) {
        var key = e.keyCode ? e.keyCode : e.which;
        if (key == 13) {
            $scope.search();
        }
    }

    $scope.search = function () {
        apiService.get('antiguedad/listado/?page=' + $scope.currentPage + '&search=' + $scope.searchText)
        .then(function successCallback(response) {
            $scope.antiguedad = response.items;
            $scope.total = response.total;
        });
    }

    $scope.eliminarAntiguedad = function (ev, item) {
        $mdDialog.show({
            controller: conceptoDeleteController,
            templateUrl: 'core/main/antiguedad/confirm-modal.tpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        }).then(function (data) {
            if (!data)
                return false;

            antiguedadService.deleteAntiguedad(item._id)
            .then(function (response) {
                $scope.search();
            });
        });
    };

    function init() {
        if(!$cookies.get('token')){
            Notification.error({message: 'Debe iniciar sesión', delay: 3000});
            $state.go('login')
          }else{
            if($cookies.get('tipo') != "admin"){
              Notification.error({message: 'Acceso denegado.', delay: 3000});
              $state.go('liquidacion')
            }else{
              $scope.search(true);
            }
          }
    }

    $scope.paging = function (pagina) {
        $scope.currentPage = pagina;
        $scope.search();
    }

    init();
}]);
