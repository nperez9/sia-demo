antiguedad.service('antiguedadService', ['apiService',
function (apiService) {
    this.url = 'antiguedad/';
    this.antiguedad = {};

    function errorCallback (r) {
        console.log(r);
    }

    this.scopeAntiguedad = function ($scope, id) {
        var url = this.url + id;
        return apiService.request(
            url,
            'GET'
        ).then(function (response) {
            $scope.antiguedad = response;
        }, errorCallback);
    }

    this.getAntiguedadEmpresa = function (empID) {
        return apiService.request(
            this.url + 'empresa/' + empID,
            'GET'
        );
    }

    this.insertAntiguedad = function (antiguedad) {
        return apiService.request(
            this.url,
            'POST',
            antiguedad
        );
    }

    this.editAntiguedad = function (antiguedad, id) {
        return apiService.request(
            this.url + id,
            'PUT',
            antiguedad
        );
    }

    this.deleteAntiguedad = function (id) {
        return apiService.request(
            this.url + id,
            'DELETE',
            null,
            false
        );
    }


}]);
