var antiguedad = angular.module( 'sia.antiguedad', ['ui.router','ui.bootstrap','bw.paging', 'ngCookies', 'ui-notification']).config(function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when('/antiguedad', '/antiguedad/listado');

        $stateProvider
            .state('antiguedad',{
                url:'/antiguedad',
                abstract:true,
            })
            .state('antiguedad.listado',{
                url:'/listado/:search',
                templateUrl:'core/main/antiguedad/antiguedad.tpl.html',
                controller:'antiguedadController',
                params: {
                   search: ""
                }
            })
            .state('antiguedad.crear',{
                url:'/crear/:empresaid',
                templateUrl:'core/main/antiguedad/form-antiguedad.tpl.html',
                controller:'crearAntiguedadController'
            })
            .state('antiguedad.ver',{
                url:'/:id/ver/:empresaid',
                templateUrl:'core/main/antiguedad/ver-antiguedad.tpl.html',
                controller:'verAntiguedadController'
            })
            .state('antiguedad.editar',{
                url:'/editar/:id/:ver/:search/:empresaid',
                templateUrl:'core/main/antiguedad/form-antiguedad.tpl.html',
                controller:'editAntiguedadController',
                params: {
                   search: ""
                }
            })
    });
