antiguedad.controller('editAntiguedadController', ['$scope', '$rootScope', '$state', '$mdDialog', '$stateParams', 'apiService', 'antiguedadService',
function ($scope, $rootScope, $state, $mdDialog, $stateParams, apiService, antiguedadService) {

    $scope.title = 'Editar Antiguedad';
    $scope.ver = $stateParams.ver;
    var id = $stateParams.id;
    var idempresa = $stateParams.empresaid;
    var empresa = $stateParams.empresaid.toString();

    function getAntiguedad () {
        antiguedadService.scopeAntiguedad($scope, id);
    }

    function getEmpresa () {
        apiService.request(
            'empresas/' + idempresa,
            'GET'
        ).then(function (empresa) {
            $scope.empresa = empresa;
        });
    }

    function getCategorias () {
        apiService.request(
            'categorias/',
            'GET'
        ).then(function (categorias) {
            $scope.categorias = categorias.items;
        });
    }

    $scope.goBack = function () {
        history.back();    
    }

    function init () {
        getAntiguedad();
        getCategorias();
        getEmpresa();
    }

    $scope.cancelar = function () {
        if ($scope.ver == 1) {
            $state.go('antiguedad.ver', { id: id});
        } else {
            $state.go('liquidacion.antiguedad');
        }
    }

    $scope.save = function () {
        antiguedadService.editAntiguedad($scope.antiguedad, $scope.antiguedad._id)
            .then(function successCallback(response) {
                if (response.success)
                    $state.go('liquidacion');
            });
    }

    $scope.goBack = function () {
        if (idempresa == '0') {
            if ($scope.ver == '1') {
                $state.go('antiguedad.ver', { id: id, ver: 1, empresaid: idempresa });
            }else{
                $state.go('antiguedad.listado');
            }
        }
        else {
            if ($scope.ver == '1') {
                $state.go('antiguedad.ver', { id: id, ver: 1, empresaid: idempresa });
            }else{
                $state.go('liquidacion', { estado: 'antiguedad' });
            }
        }
    }

    init();
}]);
