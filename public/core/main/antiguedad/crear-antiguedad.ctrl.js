antiguedad.controller('crearAntiguedadController', ['$scope', '$rootScope', '$stateParams', '$state', '$mdDialog', 'apiService', 'Notification', 'antiguedadService',
    function ($scope, $rootScope, $stateParams, $state, $mdDialog, apiService, Notification, antiguedadService) {
        var empresaid = $stateParams.empresaid;
        $scope.title = 'Ingresar antiguedad';
        $scope.antiguedad = {};

        if (empresaid == '0') {
            $rootScope.sideBar = true;
        } else {
            var empresaid = $stateParams.empresaid;
            $rootScope.sideBar = false;
        }

        $scope.goBack = function () {
            if (empresaid == '0')
                $state.go('antiguedad.listado');
            else
                $state.go('liquidacion', { estado: 'antiguedad' });
        }

        function getEmpresa() {
            apiService.request(
                'empresas/' + empresaid,
                'GET'
            ).then(function (empresa) {
                $scope.empresa = empresa;
            });
        }

        function getCategorias() {
            apiService.request(
                'categorias/',
                'GET'
            ).then(function (categorias) {
                $scope.categorias = categorias.items;
            });
        }

        $scope.save = function () {
            $scope.antiguedad.empresa = $scope.empresa;
            antiguedadService.insertAntiguedad($scope.antiguedad)
                .then(function (response) {
                    if (empresaid == '0')
                        $state.go('antiguedad.listado');
                    else
                        $state.go('liquidacion', { estado: 'antiguedad' });
                });
        };

        function init() {
            getEmpresa();
            getCategorias();
        }

        init();
    }]);
