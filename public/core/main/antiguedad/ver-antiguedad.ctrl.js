padron.controller('verAntiguedadController', ['$scope', '$rootScope', '$state', '$mdDialog', '$stateParams', 'apiService', 'antiguedadService',
    function ($scope, $rootScope, $state, $mdDialog, $stateParams, apiService, antiguedadService) {
        $rootScope.sideBar = false;
        $scope.searchText = $stateParams.search;
        $scope.empresaid = ($stateParams.empresaid);
        var id = $stateParams.id;
        var tipolistado = $stateParams.tipo;

        if ($scope.empresaid == 0) {
            $rootScope.sideBar = true;
        }


        $scope.confirmModal = function (ev, item) {
            $mdDialog.show({
                controller: antiguedadDeleteController,
                templateUrl: 'core/main/antiguedad/confirm-modal.tpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
            }).then(function (data) {
                antiguedadService.deleteAntiguedad(item._id)
                    .then(function successCallback(response) {
                        $state.go('antiguedad.listado', {}, { reload: true });
                    }, function errorCallback(response) {
                        console.log(response)
                    });
            });
        };

        $scope.eliminarAntiguedad = function (ev, item) {
            $mdDialog.show({
                controller: conceptoDeleteController,
                templateUrl: 'core/main/antiguedad/confirm-modal.tpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            }).then(function (data) {
                if (!data)
                    return false;

                antiguedadService.deleteAntiguedad(item._id)
                    .then(function (response) {
                        if (tipolistado == "general") {
                            $state.go('antiguedad.listado', {}, { reload: true });
                        }
                        else {
                            $state.go('antiguedad', {}, { reload: true });
                        }

                    });
            });
        };

        $scope.goBack = function () {
            if ($scope.empresaid != 0)
                $state.go('liquidacion', { estado: 'antiguedad' });
            else
                $state.go('antiguedad.listado');
        }

        function getAntiguedad() {
            antiguedadService.scopeAntiguedad($scope, id);
        }

        function init() {
            getAntiguedad();
        };

        init();
    }]);
