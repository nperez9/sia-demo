ganancias.controller('datosGananciasController', ['$scope', '$stateParams', 'apiService', '$rootScope', '$state', '$mdDialog', 'moneyService',
function ($scope, $stateParams, apiService, $rootScope, $state, $mdDialog, moneyService) {
    $rootScope.sideBar = false;
    $scope.currentPage = 1;
    $scope.pageSize = 20;
    $scope.total = 1;

    var empresaid = $stateParams.empresaid;
    var personaid = $stateParams.personaid;
    var anio = $stateParams.anio;

    $scope.anio = anio;

    function getPersona () {
        apiService.request(
            'personas/persona_id/'+personaid,
            'GET'
        ).then(function (params) {
            $scope.persona = params;
        });
    }

    function getGanancias () {
        apiService.request(
            'ganancias/datos-ganancias/'+ empresaid + '/' + personaid + '/' + anio,
            'GET'
        ).then(function name(params) {
          $scope.ganancias = params;
          calcularTotales($scope.ganancias);
        }); 
    }

    function calcularTotales (ganancias) {
        $scope.totalGan = {};
        $scope.totalOS = {};
        var totalGan = {};
        var totalOS = {};

        for (let i = 0; i < ganancias.datos_ganancias.length; i++) {
            var datoGan = ganancias.datos_ganancias[i];
            for (var key in datoGan) {
                if (!totalGan.hasOwnProperty(key)) totalGan[key] = 0;
                totalGan[key] += datoGan[key];
            }            
        }

        for (let i = 0; i < ganancias.otros_sueldos.length; i++) {
            var datoOS = ganancias.otros_sueldos[i];
            if (!totalOS.hasOwnProperty('neto')) totalOS['neto'] = 0;
            totalOS['neto'] += datoOS.neto;
        }

        $scope.totalGan = totalGan;
        $scope.totalOS = totalOS;
    }

    $scope.agregarDeducciones = function () {
        $mdDialog.show({
            controller: agregarDeduccionesController,
            templateUrl: 'core/main/ganancias/carga-ganancias.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            locals: {
                anio: anio,
                datos: null
            }
        }).then(function (datos) {
            if (datos === false) {
                return false;
            }

            apiService.request(
                'ganancias/datos-ganancias/'+ empresaid + '/' + personaid + '/' + anio,
                'POST',
                datos
            ).then(function (r) {
                getGanancias(); 
            });
        });
    }


    $scope.editarDatosGanancias = function (gan) {
        $mdDialog.show({
            controller: agregarDeduccionesController,
            templateUrl: 'core/main/ganancias/carga-ganancias.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            locals: {
                anio: anio,
                datos: gan
            }
        }).then(function (datos) {
            if (datos === false) {
                return false;
            }

            apiService.request(
                'ganancias/datos-ganancias/'+ empresaid + '/' + personaid + '/' + anio + '/' + gan._id,
                'PUT',
                datos
            ).then(function (r) {
                getGanancias(); 
            });
        });
    }


    $scope.eliminarDatosGanancias = function (gan) {
        $mdDialog.show({
            controller: confirmController,
            templateUrl: 'core/main/conceptos/confirm-modal.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            locals: {
                anio: anio,
                datos: gan
            }
        }).then(function (datos) {
            if (datos === false || typeof(datos) === 'undefined') {
                return false;
            }

            apiService.request(
                'ganancias/datos-ganancias/'+ empresaid + '/' + personaid + '/' + anio + '/' + gan._id,
                'DELETE'
            ).then(function (r) {
                getGanancias(); 
            });
        });
    }


    $scope.agregarOtrosSueldos = function () {
        $mdDialog.show({
            controller: agregarDeduccionesController,
            templateUrl: 'core/main/ganancias/otros-sueldos.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            locals: {
                anio: anio,
                datos: null
            }
        }).then(function (datos) {
            if (datos === false) {
                return false;
            }

            apiService.request(
                'ganancias/otros-sueldos/'+ empresaid + '/' + personaid + '/' + anio,
                'POST',
                datos
            ).then(function (r) {
                getGanancias(); 
            });
        });
    }


    $scope.editarOtrosSueldos = function (gan) {
        $mdDialog.show({
            controller: agregarDeduccionesController,
            templateUrl: 'core/main/ganancias/otros-sueldos.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            locals: {
                anio: anio,
                datos: gan
            }
        }).then(function (datos) {
            if (datos === false) {
                return false;
            }

            apiService.request(
                'ganancias/otros-sueldos/'+ empresaid + '/' + personaid + '/' + anio + '/' + gan._id,
                'PUT',
                datos
            ).then(function (r) {
                getGanancias(); 
            });
        });
    }


    $scope.eliminarOtrosSueldos = function (gan) {
        $mdDialog.show({
            controller: confirmController,
            templateUrl: 'core/main/conceptos/confirm-modal.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            locals: {
                anio: anio,
                datos: gan
            }
        }).then(function (datos) {
            if (datos === false || typeof(datos) === 'undefined') {
                return false;
            }

            apiService.request(
                'ganancias/otros-sueldos/'+ empresaid + '/' + personaid + '/' + anio + '/' + gan._id,
                'DELETE'
            ).then(function (r) {
                getGanancias(); 
            });
        });
    }

    function init() {
        getPersona();
        getGanancias();
    }

    $scope.goBack = function () {
        $state.go('liquidacion', {estado: "ganancias"});
    }

    init();
}]);


function agregarDeduccionesController ($scope, $mdDialog, anio, datos) {
    if (datos === null)
        $scope.dato = {anio: anio};
    else 
        $scope.dato = datos;

    $scope.hide = function () {
        $mdDialog.hide(false);
    };
  
    $scope.cancel = function () {
        $mdDialog.hide(false);
    };

    $scope.guardar = function () {
        $mdDialog.hide($scope.dato);
    }
    
}