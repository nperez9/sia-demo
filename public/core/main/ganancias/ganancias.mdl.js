var ganancias = angular.module( 'sia.ganancias', ['ui.bootstrap']).config(function config($stateProvider, $urlRouterProvider) {
    
        $stateProvider
            .state('ganancias',{
                url:'/ganancias',
                templateUrl:'core/main/ganancias/ganancias.tpl.html',
                controller:'gananciasController'
            })
            .state('datos-ganancias',{
                url:'/datos-ganancias/:empresaid/:personaid/:anio',
                templateUrl:'core/main/ganancias/datos-ganancias.tpl.html',
                controller:'datosGananciasController'
            });
    });
    