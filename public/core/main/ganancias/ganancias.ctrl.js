ganancias.controller('gananciasController', ['$scope', 'apiService', '$rootScope', '$state', '$mdDialog', 'moneyService', 
function($scope, apiService, $rootScope, $state, $mdDialog, moneyService) {
    $rootScope.sideBar = false;
    $scope.currentPage = 1;
	$scope.pageSize = 20;
	$scope.total = 1;

    function init () {
        findPersonas();
    }

    function findPersonas () {
        apiService.get('personas/?page=' + $scope.currentPage)
        .then(function (result) {
           $scope.items = result.items;
           $scope.total = result.total;
        });
    }

    $scope.uploadFile = function () {
        var filename = event.target.files[0].name;
    	var personaId = event.target.dataset.persona;
        var reader = new FileReader();
        
        reader.onload = function() {
            //var parsed = new DOMParser().parseFromString(this.result, "text/xml");
            var data = {
                xml: this.result
            }
            apiService.post('ganancias/xml/' + personaId, data).then(function (r) {
                console.log(r);
            });
        };
        
        reader.readAsText(event.target.files[0]);
    };

    init();
}]);

// Directiva para subir el file
ganancias.directive('customOnChange', function() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var onChangeFunc = scope.$eval(attrs.customOnChange);
            element.bind('change', onChangeFunc);
        }
    };
});
