var liquidacion = angular.module( 'sia.liquidacion', ['ui.bootstrap'])
.config(function ($stateProvider, $urlRouterProvider) {

	$stateProvider.state('liquidacion',{
	    	url:'/liquidacion',
	    	templateUrl:'core/main/liquidacion/liquidacion.tpl.html',
			controller:'liquidacionController',
			params: {
				estado: 'legajo',
				legajo: null,
				periodo: null
			}
	    })
	    .state('crearLegajo',{
	    	url:'/crear-legajo/:id/:periodoid',
	    	templateUrl:'core/main/liquidacion/form-legajo.tpl.html',
	    	controller:'crearLegajoController',
	    })
	    .state('editarLegajo',{
	    	url:'/editarLegajo/:id/:periodoid',
	    	templateUrl:'core/main/liquidacion/form-legajo.tpl.html',
	    	controller:'editarLegajoController',
		})
		.state('periodo-edit',{
	    	url:'/periodo/:empresaid',
	    	templateUrl:'core/main/liquidacion/form-legajo.tpl.html',
	    	controller:'editarLegajoController',
	    })

});
