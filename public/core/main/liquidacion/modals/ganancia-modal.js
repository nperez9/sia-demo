function gananciaModalController ($scope, $mdDialog, ganancia) {
  $scope.ganancia = ganancia;

  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.answer = function(boolean) {
    $mdDialog.hide();
  };
}