/*
  Aca estan todas las funciones de los modales
 */
function agregarConceptoController($scope, $filter, $mdDialog, conceptos) {
  $scope.currentPage = 0;
  $scope.pageSize = 5;
  $scope.data = [];
  $scope.q = '';

  $scope.resetPage = function () {
    $scope.currentPage = 0;
  }

  $scope.numberOfPages = function () {
    return Math.ceil($scope.getData().length / $scope.pageSize);
  }

  $scope.getData = function () {
    var superArray = $filter('filter')($scope.conceptos, { descripcion: $scope.searchText });
    return superArray.concat($filter('filter')($scope.conceptos, { codigo: $scope.searchText }));
  };

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  $scope.pickColegio = function (item) {
    var cantidad = new RegExp(/\b[Q]\b/g);
    var importe = new RegExp(/\b[I]\b/g);
    var datos = { concepto: item };

    if (cantidad.test(item.formula)) {
      datos.modal = 'cantidad';
    } else if (importe.test(item.formula)) {
      datos.modal = 'importe';
    }
    $mdDialog.hide(datos);
  }

};


function cantidadController($scope, $mdDialog, Notification, concepto) {
  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {
    $scope.titulo = concepto.descripcion;
    $scope.concepto = {};
    $scope.concepto = concepto;
  }

  $scope.save = function () {
    if ($scope.concepto.cantidad == '') {
      Notification.warning('No puede guardar una cantidad vacia');
      return false;
    }
    if ($scope.concepto.descripcion != '') {
      $scope.concepto.descripcionCustom = $scope.concepto.descripcion;
    }

    $mdDialog.hide($scope.concepto);
  }
  init();
};


function importeController($scope, $mdDialog, Notification, concepto) {
  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {
    $scope.concepto = concepto;

  }

  $scope.save = function () {
    if ($scope.concepto.cantidad == '') {
      Notification.warning('No puede guardar una cantidad vacia');
      return false;
    }
    if ($scope.concepto.importe == '') {
      Notification.warning('No puede guardar una cantidad vacia');
      return false;
    }

    if ($scope.concepto.descripcion != '') {
      $scope.concepto.descripcionCustom = $scope.concepto.descripcion;
    }

    $mdDialog.hide($scope.concepto);
  }

  init();
};


function textoController($scope, $mdDialog, Notification, concepto) {
  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {
    $scope.titulo = concepto.descripcion;
    $scope.concepto = concepto;
  }

  $scope.save = function () {
    if ($scope.concepto.descripcion != '') {
      $scope.concepto.descripcionCustom = $scope.concepto.descripcion;
    }

    $mdDialog.hide($scope.concepto);
  }

  init();
};


function liquidacionDeleteController($scope, $mdDialog) {
  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.cancel();
  };

  $scope.answer = function (boolean) {
    $mdDialog.hide(boolean);
  };
}

function conceptoDeleteController($scope, $mdDialog) {
  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.cancel();
  };

  $scope.answer = function (boolean) {
    $mdDialog.hide(boolean);
  };
};

function cambioColegioController($scope, $mdDialog, colegios, $filter) {
  $scope.currentPage = 0;
  $scope.pageSize = 5;
  $scope.data = [];
  $scope.q = '';

  $scope.resetPage = function () {
    $scope.currentPage = 0;
  }

  $scope.numberOfPages = function () {
    return Math.ceil($scope.getData().length / $scope.pageSize);
  }

  $scope.getData = function () {
    return $filter('filter')($scope.colegios, { razon_social: $scope.searchText });
  };

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  $scope.pickColegio = function (item) {
    $mdDialog.hide(item);
  }

  $scope.init = function () {
    $scope.colegios = colegios;
  };

  $scope.init();
};


function conceptoDeleteController($scope, $mdDialog) {
  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.cancel();
  };

  $scope.answer = function (boolean) {
    $mdDialog.hide(boolean);
  };

  $scope.init = function () {

  };
};

function memoController($scope, $mdDialog, apiService, datosLiquidacion, Notification) {
  $scope.memo = datosLiquidacion.memo;

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.cancel();
  };

  $scope.save = function () {
    apiService.request(
      'liquidaciones/memo/' + datosLiquidacion._id,
      'PUT',
      { memo: $scope.memo }

    ).then(function (response) {
      $mdDialog.hide();
    });
  }
};


function mesesFijoController($scope, $mdDialog, Notification, concepto, apiService) {
  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {
    $scope.concepto = concepto;
  }

  $scope.save = function () {
    apiService.request(
      'liquidaciones/concepto/' + concepto._id,
      'PUT',
      { concepto: concepto }
    ).then(function (response) {
      $mdDialog.hide();
    });
  }


  init();
};

function reportesModalController($scope, $mdDialog, Notification, apiService, downloadService, reporte, periodo) {

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {
    $scope.neto = reporte == "netos" ? true : false;

    apiService.get('subempresas/empresa/0/' + periodo)
      .then(function successCallback(response) {
        $scope.subempresas = response;
      });

    $scope.selected = [1];
    $scope.check = true;
  }

  $scope.save = function () {

    var subempresasIds = [];

    for (let a = 0; a < $scope.selected.length; a++) {
      subempresasIds.push($scope.selected[a].subempresa_id)
    }

    var data = {
      periodo: periodo,
      empresa: localStorage.getItem('empresaId'),
      subempresas: subempresasIds,
      agrupar: $scope.agrupaLegajos
    };

    switch (reporte) {
      case "netos":
        data.cta = $scope.cta;
        downloadService.downloadFile('reportes/sueldos-neto', data, 'sueldos-neto.xlsx');
        break;

      case "brutos":
        downloadService.downloadFile('reportes/sueldos-bruto', data, 'sueldos-bruto.xlsx');
        break;
    }

    $mdDialog.hide();
  }

  $scope.toggle = function (item, list) {
    var idx = list.indexOf(item);
    if (idx > -1) {
      list.splice(idx, 1);
    }
    else {
      list.push(item);
    }
  };

  $scope.exists = function (item, list) {
    return list.indexOf(item) > -1;
  };

  $scope.isIndeterminate = function () {
    return ($scope.selected.length !== 0 &&
      $scope.selected.length !== $scope.subempresas.length);
  };

  $scope.isChecked = function () {
    return $scope.selected.length === $scope.subempresas.length;
  };

  $scope.toggleAll = function () {
    if ($scope.selected.length === $scope.subempresas.length) {
      $scope.selected = [];
    } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
      $scope.selected = $scope.subempresas.slice(0);
    }
  };

  $scope.checkSearchText = function () {
    if ($scope.searchText == "") {
      $scope.check = true;
    } else {
      $scope.check = false;
    }
  }

  init();
};

function recibosBlancosModalController($scope, $mdDialog, Notification, apiService, downloadService, tipo, periodo) {

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {

    apiService.get('subempresas/empresa/0/' + periodo)
      .then(function successCallback(response) {
        $scope.subempresas = response;
      });

    $scope.selected = [1];
    $scope.check = true;
  }

  $scope.save = function () {

    var subempresasIds = [];

    for (let a = 0; a < $scope.selected.length; a++) {
      subempresasIds.push($scope.selected[a].subempresa_id)
    }

    var data = {
      periodo: periodo,
      empresa: localStorage.getItem('empresaId'),
      subempresas: subempresasIds,
      banco: $scope.banco,
      fecha_pago: $scope.fecha_pago,
      fecha_deposito: $scope.fecha_deposito
    };

    downloadService.downloadFile('recibos/a4/vertical', data, 'recibos_blancos.pdf');

    $mdDialog.hide();
  }

  $scope.toggle = function (item, list) {
    var idx = list.indexOf(item);
    if (idx > -1) {
      list.splice(idx, 1);
    }
    else {
      list.push(item);
    }
  };

  $scope.exists = function (item, list) {
    return list.indexOf(item) > -1;
  };

  $scope.isIndeterminate = function () {
    return ($scope.selected.length !== 0 &&
      $scope.selected.length !== $scope.subempresas.length);
  };

  $scope.isChecked = function () {
    return $scope.selected.length === $scope.subempresas.length;
  };

  $scope.toggleAll = function () {
    if ($scope.selected.length === $scope.subempresas.length) {
      $scope.selected = [];
    } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
      $scope.selected = $scope.subempresas.slice(0);
    }
  };

  $scope.checkSearchText = function () {
    if ($scope.searchText == "") {
      $scope.check = true;
    } else {
      $scope.check = false;
    }
  }

  init();
};

function recibosModalController($scope, $mdDialog, Notification, apiService, downloadService, tipo, periodo) {

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {

    apiService.get('subempresas/empresa/0/' + periodo)
      .then(function successCallback(response) {
        $scope.subempresas = response;
      });

    $scope.selected = [1];
    $scope.check = true;
    $scope.soloSubempresas = true;
    $scope.imagen = false;

    if (tipo == "a4.horizontal") {
      $scope.imagen = true;
    }
  }

  $scope.save = function () {

    var subempresasIds = [];

    for (let a = 0; a < $scope.selected.length; a++) {
      subempresasIds.push($scope.selected[a].subempresa_id)
    }


    var data = {
      periodo: periodo,
      empresa: localStorage.getItem('empresaId'),
      subempresas: subempresasIds,
      lugar_pago: $scope.lugar_pago,
      fecha_pago: $scope.fecha_pago,
      fecha_deposito: $scope.fecha_deposito,
      orden: $scope.orden,
      fondo: $scope.fondo,
      path: window.location.host
    };

    console.log(data);

    if (tipo == "a4.horizontal") {
      downloadService.downloadFile('recibos/a4/horizontal', data, 'recibo_a4_horizontal.pdf');
    }
    if (tipo == "a4.azul") {
      downloadService.downloadFile('recibos/a4/azul', data, 'recibo_a4_azul.pdf');
    }

    $mdDialog.hide();
  }

  $scope.toggle = function (item, list) {
    var idx = list.indexOf(item);
    if (idx > -1) {
      list.splice(idx, 1);
    }
    else {
      list.push(item);
    }
  };

  $scope.exists = function (item, list) {
    return list.indexOf(item) > -1;
  };

  $scope.isIndeterminate = function () {
    return ($scope.selected.length !== 0 &&
      $scope.selected.length !== $scope.subempresas.length);
  };

  $scope.isChecked = function () {
    return $scope.selected.length === $scope.subempresas.length;
  };

  $scope.toggleAll = function () {
    if ($scope.selected.length === $scope.subempresas.length) {
      $scope.selected = [];
    } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
      $scope.selected = $scope.subempresas.slice(0);
    }
  };

  $scope.checkSearchText = function () {
    if ($scope.searchText == "") {
      $scope.check = true;
    } else {
      $scope.check = false;
    }
  }

  init();
};

function recibosOficioModalController($scope, $mdDialog, Notification, apiService, downloadService, tipo, periodo) {

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {
    apiService.get('subempresas/empresa/0/' + periodo)
      .then(function successCallback(response) {
        $scope.subempresas = response;
      });

    $scope.selected = [1];
    $scope.check = true;
    $scope.soloSubempresas = false;
  }

  $scope.save = function () {

    var subempresasIds = [];

    for (let a = 0; a < $scope.selected.length; a++) {
      subempresasIds.push($scope.selected[a].subempresa_id)
    }

    var data = {
      periodo: periodo,
      empresa: localStorage.getItem('empresaId'),
      subempresas: subempresasIds
    };

    downloadService.downloadFile('recibos/oficio', data, 'planilla_liquidacion_haberes.pdf');

    $mdDialog.hide();
  }

  $scope.toggle = function (item, list) {
    var idx = list.indexOf(item);
    if (idx > -1) {
      list.splice(idx, 1);
    }
    else {
      list.push(item);
    }
  };

  $scope.exists = function (item, list) {
    return list.indexOf(item) > -1;
  };

  $scope.isIndeterminate = function () {
    return ($scope.selected.length !== 0 &&
      $scope.selected.length !== $scope.subempresas.length);
  };

  $scope.isChecked = function () {
    return $scope.selected.length === $scope.subempresas.length;
  };

  $scope.toggleAll = function () {
    if ($scope.selected.length === $scope.subempresas.length) {
      $scope.selected = [];
    } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
      $scope.selected = $scope.subempresas.slice(0);
    }
  };

  $scope.checkSearchText = function () {
    if ($scope.searchText == "") {
      $scope.check = true;
    } else {
      $scope.check = false;
    }
  }

  init();
};

function afipAlertaController($scope, $mdDialog, personasAlert, downloadService) {
  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {
    $scope.personasAlert = personasAlert;
  }

  $scope.guardar = function () {
    var data = {
      personas: personasAlert
    };
    downloadService.downloadFile('reportes/afip_perosonas_alert', data, 'alerta_de_personas.txt');
  }

  init();
};

function importarPadronController($scope, $mdDialog, apiService, $state, empresa, periodo, downloadService) {
  var inProcess = false;

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {
  }

  $scope.uploadFile = function () {
    if (inProcess)
      return false;

    inProcess = true;
    var filename = event.target.files[0].name;
    var reader = new FileReader();

    reader.onload = function () {

      var data = {
        nombre_archivo: filename,
        csv: this.result,
        periodo: periodo,
        empresa: empresa
      }

      apiService.post('personas/csv', data)
        .then(function (r) {
          inProcess = false;
          $state.go('liquidacion', { estado: 'padron' }, { reload: true });
          $mdDialog.hide();
        });
    };
    reader.readAsText(event.target.files[0]);
  };

  $scope.descargarCSVejemplo = function () {
    downloadService.downloadFile('personas/csvejemplo', null, 'alerta_de_personas.csv');
  }

  init();
};

function dgegpModalController($scope, $mdDialog, $window, Notification, apiService, downloadService, tipo, periodo, empresa) {
  $scope.selected = [];
  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {

    apiService.get('subempresas/empresa/0/' + periodo._id)
      .then(function successCallback(response) {
        $scope.subempresas = response;
        for (let i = 0; i < response.length; i++) {
          if (response[i].subvencion != 0) {
            $scope.selected.push(response[i]);
          }
        }
      });
  }

  $scope.save = function () {

    var subempresasIds = [];

    for (let a = 0; a < $scope.selected.length; a++) {
      console.log($scope.selected[a])
      subempresasIds.push($scope.selected[a].subempresa_id)
    }

    var data = {
      periodo: periodo._id,
      empresa: empresa._id,
      subempresas: subempresasIds
    };

    apiService.get('/empresas/' + empresa._id)
      .then(function successCallback(empresa) {

        if (periodo.sac == true) {
          downloadService.downloadFile('reportes/dgegp/0', data, 'SAC_' + empresa.registro + '.csv');
        }
        else {
          downloadService.downloadFile('reportes/dgegp/0', data, 'Rendición_' + empresa.registro + '.csv');
        }

        apiService.post('reportes/dgegp/1', data).then(function (r) {

          if (r.alertaNegativos == true) {
            Notification.error("HAY  IMPORTES  NEGATIVOS  EN  GANANCIAS");
          }
          $mdDialog.show({
            controller: dgegpResultadoModalController,
            templateUrl: 'core/main/liquidacion/modals/resultado-dgegp.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            locals: {
              cantidad: r.cantLegajos
            },
          }).then(function (r) {

          });
        });

      });

    $mdDialog.hide();
  }

  $scope.toggle = function (item, list) {
    var idx = list.indexOf(item);
    if (idx > -1) {
      list.splice(idx, 1);
    }
    else {
      list.push(item);
    }
  };

  $scope.exists = function (item, list) {
    return list.indexOf(item) > -1;
  };

  $scope.isIndeterminate = function () {
    return ($scope.selected.length !== 0 && $scope.selected.length !== $scope.subempresas.length);
  };

  $scope.isChecked = function () {
    return $scope.selected.length === $scope.subempresas.length;
  };

  $scope.toggleAll = function () {
    if ($scope.selected.length === $scope.subempresas.length) {
      $scope.selected = [];
    } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
      $scope.selected = $scope.subempresas.slice(0);
    }
  };

  $scope.checkSearchText = function () {
    if ($scope.searchText == "") {
      $scope.check = true;
    } else {
      $scope.check = false;
    }
  }

  init();
};

function dgegpResultadoModalController($scope, $mdDialog, Notification, cantidad) {
  console.log(cantidad)
  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {
    console.log(cantidad)
    $scope.cantidadLegajos = cantidad;
    $scope.scvo = cantidad * 9.09;

  }

  init();
};

function personascuilModalController($scope, $mdDialog, Notification, apiService, downloadService, periodo, listado) {

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {

    apiService.get('subempresas/empresa/0/' + periodo)
      .then(function successCallback(response) {
        $scope.subempresas = response;
      });

    $scope.selected = [1];
    $scope.check = true;
  }

  $scope.save = function () {

    var subempresasIds = [];

    for (let a = 0; a < $scope.selected.length; a++) {
      subempresasIds.push($scope.selected[a].subempresa_id)
    }

    var data = {
      periodo: periodo,
      empresa: localStorage.getItem('empresaId'),
      subempresas: subempresasIds,
    };

    switch (listado) {
      case "personasCuil":
        downloadService.downloadFile('listados/personas_cuil', data, 'personas_cuil.xlsx');
        break;

      case "suplentes":
        downloadService.downloadFile('listados/suplentes', data, 'listado_suplentes.xlsx');
        break;

      case "novedades":
        downloadService.downloadFile('listados/novedades', data, 'listado_novedades.xlsx');
        break;

      case "caja_complementaria":
        downloadService.downloadFile('listados/caja_complementaria', data, 'listado_caja_complementaria.xlsx');
        break;

      case "resumen_caja_complementaria":
        downloadService.downloadFile('listados/resumen_caja_complementaria', data, 'resumen_caja_complementaria.pdf');
        break;

      case "estadistica_persona":
        data.personas = true;
        downloadService.downloadFile('listados/estadistica', data, 'estadistica_conceptos.xlsx');
        break;

      case "estadistica_subempresa":
        data.personas = false;
        downloadService.downloadFile('listados/estadistica', data, 'estadistica_conceptos.xlsx');
        break;

      default:
        break;
    }


    $mdDialog.hide();
  }

  $scope.toggle = function (item, list) {
    var idx = list.indexOf(item);
    if (idx > -1) {
      list.splice(idx, 1);
    }
    else {
      list.push(item);
    }
  };

  $scope.exists = function (item, list) {
    return list.indexOf(item) > -1;
  };

  $scope.isIndeterminate = function () {
    return ($scope.selected.length !== 0 &&
      $scope.selected.length !== $scope.subempresas.length);
  };

  $scope.isChecked = function () {
    return $scope.selected.length === $scope.subempresas.length;
  };

  $scope.toggleAll = function () {
    if ($scope.selected.length === $scope.subempresas.length) {
      $scope.selected = [];
    } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
      $scope.selected = $scope.subempresas.slice(0);
    }
  };

  $scope.checkSearchText = function () {
    if ($scope.searchText == "") {
      $scope.check = true;
    } else {
      $scope.check = false;
    }
  }

  init();
};

function valorModalController($scope, $mdDialog, apiService, $state, valorFind, valorNew) {

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {
    $scope.valor = valorFind
  }

  $scope.guardar = function () {

    apiService.post('valores', valorNew)
      .then(function successCallback(response) {
        $state.go('valor.listado');
      });

    $mdDialog.hide();
  }

  init();

};

function conceptosModalController($scope, $mdDialog, Notification, apiService, downloadService, periodo, listado) {

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {

    apiService.get('conceptos/modal/' + localStorage.getItem('empresaId') + "/" + periodo)
      .then(function successCallback(response) {
        $scope.conceptos = response;
      });

    $scope.selected = [1];
    $scope.check = true;
  }

  $scope.save = function () {

    var conceptosIds = [];

    for (let a = 0; a < $scope.selected.length; a++) {
      conceptosIds.push($scope.selected[a].codigo)
    }

    var data = {
      periodo: periodo,
      empresa: localStorage.getItem('empresaId'),
      conceptos: conceptosIds,
    };

    switch (listado) {
      case "conceptos":
        downloadService.downloadFile('listados/conceptos', data, 'listado_conceptos.xlsx');
        break;

      case "forzados":
        downloadService.downloadFile('listados/forzados', data, 'listado_conceptos_forzados.xlsx');
        break;

      default:
        break;
    }


    $mdDialog.hide();
  }

  $scope.toggle = function (item, list) {
    var idx = list.indexOf(item);
    if (idx > -1) {
      list.splice(idx, 1);
    }
    else {
      list.push(item);
    }
  };

  $scope.exists = function (item, list) {
    return list.indexOf(item) > -1;
  };

  $scope.isIndeterminate = function () {
    return ($scope.selected.length !== 0 &&
      $scope.selected.length !== $scope.conceptos.length);
  };

  $scope.isChecked = function () {
    return $scope.selected.length === $scope.conceptos.length;
  };

  $scope.toggleAll = function () {
    if ($scope.selected.length === $scope.conceptos.length) {
      $scope.selected = [];
    } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
      $scope.selected = $scope.conceptos.slice(0);
    }
  };

  $scope.checkSearchText = function () {
    if ($scope.searchText == "") {
      $scope.check = true;
    } else {
      $scope.check = false;
    }
  }

  init();
};

function configCajaModalController($scope, $mdDialog, Notification, apiService, downloadService) {

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {
    $scope.conceptos = [];

    apiService.get('conceptos/caja')
      .then(function successCallback(conceptosTotales) {

        apiService.get('listados/caja_complementaria/get_configuracion/' + localStorage.getItem('empresaId'))
          .then(function successCallback(conceptosConfig) {

            $scope.selected = [];
            $scope.check = true;

            for (let i = 0; i < conceptosTotales.length; i++) {
              var elem = {
                codigo: conceptosTotales[i].codigo,
                descripcion: conceptosTotales[i].descripcion
              }

              $scope.conceptos.push(elem);

              for (let i = 0; i < conceptosConfig[0].conceptos.length; i++) {
                if (elem.codigo == conceptosConfig[0].conceptos[i].codigo) {
                  $scope.selected.push(elem);
                }
              }
            }
          });
      });
  }

  $scope.save = function () {

    var conceptos = [];

    for (let a = 0; a < $scope.selected.length; a++) {
      conceptos.push($scope.selected[a]);
    }

    var data = {
      empresa: localStorage.getItem('empresaId'),
      conceptos: conceptos,
    };

    apiService.post('listados/caja_complementaria/save_configuracion', data)
      .then(function successCallback(response) {
        $scope.conceptos = response;
      });

    $mdDialog.hide();
  }

  $scope.toggle = function (item, list) {
    var idx = list.indexOf(item);
    if (idx > -1) {
      list.splice(idx, 1);
    }
    else {
      list.push(item);
    }
  };

  $scope.exists = function (item, list) {
    return list.indexOf(item) > -1;
  };

  $scope.isIndeterminate = function () {
    return ($scope.selected.length !== 0 &&
      $scope.selected.length !== $scope.conceptos.length);
  };

  $scope.isChecked = function () {
    return $scope.selected.length === $scope.conceptos.length;
  };

  $scope.toggleAll = function () {
    if ($scope.selected.length === $scope.conceptos.length) {
      $scope.selected = [];
    } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
      $scope.selected = $scope.conceptos.slice(0);
    }
  };

  $scope.checkSearchText = function () {
    if ($scope.searchText == "") {
      $scope.check = true;
    } else {
      $scope.check = false;
    }
  }

  init();
};


function tablaSubsidiosModalController($scope, $mdDialog, Notification, apiService) {

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {

    apiService.get('subempresas/empresa/0/' + localStorage.getItem('periodo'))
      .then(function successCallback(subempresas) {

        apiService.get('subempresas/tabla_subsidios/' + localStorage.getItem('colegio') + "/" + localStorage.getItem('periodo'))
          .then(function successCallback(response) {

            $scope.subempresas = response;

            if (response.length == 0) {
              for (let i = 0; i < subempresas.length; i++) {
                var obj = {
                  empresa: localStorage.getItem('colegio'),
                  periodo: localStorage.getItem('periodo'),
                  subempresa: subempresas[i]
                }
                $scope.subempresas.push(obj);
              }
            } else {

              for (let i = 0; i < subempresas.length; i++) {
                var aux = false;

                for (let e = 0; e < response.length; e++) {
                  if (subempresas._id == response.subempresa._id) {
                    aux = true;
                  }
                }

                if (aux == false) {
                  var obj = {
                    empresa: localStorage.getItem('colegio'),
                    periodo: localStorage.getItem('periodo'),
                    subempresa: subempresas[i]
                  }
                  $scope.subempresas.push(obj);
                }
              }
            }

          });

      });
  }

  $scope.save = function () {

    apiService.post('subempresas/tabla_subsidios', $scope.subempresas)
      .then(function successCallback(response) {
        // $scope.subempresas = response;
      });

    $mdDialog.hide();
  }

  init();
};

function reporteSueldosModalController($scope, $mdDialog, Notification, apiService, downloadService) {

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {
  }

  $scope.save = function () {

    console.log($scope.type)

    var data = {
      periodo: localStorage.getItem('periodo'),
      empresa: localStorage.getItem('empresaId'),
      type: $scope.type,
    };

    downloadService.downloadFile('reportes/sueldos', data, 'reporte_sueldos.pdf');

    $mdDialog.hide();
  }

  init();
};

function legajosActivosController($scope, $mdDialog, Notification, apiService) {

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.hide();
  };

  function init() {
    apiService.get('liquidaciones/legajos/activos/' + localStorage.getItem('periodo') + "/" + localStorage.getItem('empresaId'))
      .then(function successCallback(response) {
        $scope.legajos_activos = response;
      });
  }

  init();
};