function cambioPeriodoController ($scope, $mdDialog, periodos, moment) {
    $scope.currentPage = 0;
    $scope.pageSize = 5;
    $scope.data = [];
    $scope.q = ''; 
  
    // $scope.resetPage = function () {
    //     $scope.currentPage = 0;
    // }
  
    // $scope.numberOfPages = function () {
    //     return Math.ceil($scope.getData().length/$scope.pageSize);
    // } 
    
    // $scope.getData = function () {
    //     return $scope.periodos;
    // };
  
    $scope.hide = function () {
        $mdDialog.hide();
    };
  
    $scope.cancel = function () {
        $mdDialog.hide();
    };
  
    $scope.pickPeriodo = function (item) {
        $mdDialog.hide(item);
    }
  
    $scope.init = function () {
        $scope.periodos = periodos; 
    };

    $scope.formatDate = function (date) {
        return moment.utc(date).format('DD/MM/YYYY');
    }
  
    $scope.init();
}