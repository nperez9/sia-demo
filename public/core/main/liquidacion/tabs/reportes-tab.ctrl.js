liquidacion.controller('reportesTabController', ['$scope', 'apiService', 'downloadService', '$mdDialog',
  function ($scope, apiService, downloadService, $mdDialog) {

    $scope.asientosContables = function () {
      var data = {
        periodo: $scope.periodo,
        empresa: $scope.colegio
      };
      downloadService.downloadFile('reportes/asientos-contables', data, 'asiento-contable.xlsx');
    }

    $scope.obrasSociales = function () {
      downloadService.downloadFile('reportes/obras-sociales', null, 'obras-sociales.xlsx');
    }

    $scope.reporteModal = function (reporte) {
      $mdDialog.show({
        controller: reportesModalController,
        templateUrl: 'core/main/liquidacion/modals/reporte-modal.tpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        locals: {
          reporte: reporte,
          periodo: $scope.periodo._id
        },
      }).then(function (r) {

      });
    }

    $scope.reciboModal = function (tipo_recibo) {
      $mdDialog.show({
        controller: recibosModalController,
        templateUrl: 'core/main/liquidacion/modals/recibo-modal.tpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        locals: {
          tipo: tipo_recibo,
          periodo: $scope.periodo._id
        },
      }).then(function (r) {

      });
    }

    $scope.reciboBlancoModal = function (tipo_recibo) {
      $mdDialog.show({
        controller: recibosBlancosModalController,
        templateUrl: 'core/main/liquidacion/modals/recibo-blanco-modal.tpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        locals: {
          tipo: tipo_recibo,
          periodo: $scope.periodo._id
        },
      }).then(function (r) {

      });
    }

    $scope.reciboOficioModal = function (tipo_recibo) {
      $mdDialog.show({
        controller: recibosOficioModalController,
        templateUrl: 'core/main/liquidacion/modals/recibo-modal.tpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        locals: {
          tipo: tipo_recibo,
          periodo: $scope.periodo._id
        },
      }).then(function (r) {

      });
    }

    $scope.reporteConceprosNoRemu = function(){
      var data = {
        periodo: $scope.periodo._id,
        empresa: $scope.colegio._id
      };
  
      downloadService.downloadFile('reportes/conceptos-no-remu', data, 'conceptos_no_remu.xlsx');
    }

    $scope.afipCargasSociales = function(){
      var data = {
        periodo: $scope.periodo._id,
        empresa: $scope.colegio._id,
      };

      downloadService.downloadFile('reportes/afip_cargas_sociales', data, 'test.txt');

      apiService.get('reportes/afip_validaciones/' + $scope.colegio._id + '/' + $scope.periodo._id).then(function (r) {
        if(r.length > 0){
          $mdDialog.show({
            controller: afipAlertaController,
            templateUrl: 'core/main/liquidacion/modals/afip-alerta.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            locals: {
              personasAlert: r
            },
          }).then(function (o) {

          });
        }
      });
  
    }

    $scope.modalDGEGP = function () {
      $mdDialog.show({
        controller: dgegpModalController,
        templateUrl: 'core/main/liquidacion/modals/rendicion-dgepg.tpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        locals: {
          tipo: "DGEGP",
          periodo: $scope.periodo,
          empresa: $scope.colegio
        },
      }).then(function (r) {

      });
    }

    $scope.reporteDeBajas = function(){
      var data = {
        periodo: $scope.periodo._id,
        empresa: $scope.colegio._id
      };
  
      downloadService.downloadFile('reportes/bajas', data, 'reporte_de_bajas.pdf');
    }

    $scope.reporteCambioAntiguedad = function(){
      var data = {
        periodo: $scope.periodo._id,
        empresa: $scope.colegio._id
      };
  
      downloadService.downloadFile('reportes/cambio-antiguedad', data, 'reporte_cambio_antiguedad.pdf');
    }

    $scope.padronSubempresa = function(){
      var data = {
        periodo: $scope.periodo._id,
        empresa: $scope.colegio._id
      };
  
      downloadService.downloadFile('reportes/padron-subempresas', data, 'personal_por_subempresa.xlsx');
    }

    $scope.seleccionSubEmpresa = function (listado) {
      $mdDialog.show({
        controller: personascuilModalController,
        templateUrl: 'core/main/liquidacion/modals/personas_cuil-modal.tpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        locals: {
          listado: listado,
          periodo: $scope.periodo._id
        },
      }).then(function (r) {

      });
    }

    $scope.seleccionConceptos = function (listado) {
      $mdDialog.show({
        controller: conceptosModalController,
        templateUrl: 'core/main/liquidacion/modals/conceptos-modal.tpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        locals: {
          listado: listado,
          periodo: $scope.periodo._id
        },
      }).then(function (r) {

      });
    }

    $scope.configCajaComplementaria = function (listado) {
      $mdDialog.show({
        controller: configCajaModalController,
        templateUrl: 'core/main/liquidacion/modals/config_caja-modal.tpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        locals: {
        },
      }).then(function (r) {

      });
    }


    $scope.tablaSubsidios = function () {
      $mdDialog.show({
        controller: tablaSubsidiosModalController,
        templateUrl: 'core/main/liquidacion/modals/tabla-subsidios-modal.tpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        locals: {
        },
      }).then(function (r) {

      });
    }


    $scope.reporteSueldos = function () {
      $mdDialog.show({
        controller: reporteSueldosModalController,
        templateUrl: 'core/main/liquidacion/modals/reporte-sueldos-modal.tpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        locals: {
        },
      }).then(function (r) {

      });
    }

    $scope.listadoPersonal = function (){
      var data = {
        periodo: $scope.periodo._id,
        empresa: $scope.colegio._id
      };
  
      downloadService.downloadFile('listados/personal', data, 'listado_personal.xlsx');
    }

  }]);