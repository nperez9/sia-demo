liquidacion.controller('antiguedadTabController', ['$scope', '$rootScope', '$state', '$filter', 'conceptosService', '$mdDialog', 'apiService', 'antiguedadService',
function ($scope, $rootScope, $state, $filter, conceptosService, $mdDialog, apiService, antiguedadService) {
    $rootScope.sideBar = false;
    $scope.conceptoPager = {};
    $scope.conceptoPager.currentPage = 0;
    $scope.conceptoPager.pageSize = 10;

    $scope.empresaId = localStorage.getItem('empresaId');

    function init () {

    }

    $scope.eliminarAntiguedad = function (ev, item) {
        $mdDialog.show({
            controller: conceptoDeleteController,
            templateUrl: 'core/main/antiguedad/confirm-modal.tpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        }).then(function (data) {
            if (!data)
                return false;

            antiguedadService.deleteAntiguedad(item._id)
            .then(function (response) {
                $state.go('liquidacion', {estado: 'antiguedad'}, {reload:true});
                $scope.search();
            });
        });
    };

    init();
}]);
