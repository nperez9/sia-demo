liquidacion.controller('personasTabController', ['$scope', 'apiService', '$mdDialog', 'personaService', '$state', 'downloadService',
function ($scope, apiService, $mdDialog, personaService, $state, downloadService) {
    var inProcess = false;
    $scope.paginaPadron = 1;
    $scope.totalPadron = 0;
    var limite = 20;
    
    $scope.$on('periodos-legajos-cargados', function (params) {
        getPersonas();
    });

    $scope.eliminarPersona = function (ev, item) {
        $mdDialog.show({
            controller: conceptoDeleteController,
            templateUrl: 'core/main/padron/confirm-modal.tpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        }).then(function (data) {
            if (!data)
                return false;

            personaService.deletePersona(item._id, $scope.periodo._id)
            .then(function (response) {
                $state.go('liquidacion', { estado: 'personas' }, { reload: true });
                $scope.search();
            });
        });
    };

    $scope.getSearchPersonas = function(search) {
        getPersonas(search);
    }

    function getPersonas (busqueda) {
        var url = 'personas/' + $scope.colegio._id + '/' + $scope.periodo._id + '/?page=' + $scope.paginaPadron;
        if (busqueda !== undefined && busqueda !== '') url += '&search=' + busqueda;

        apiService.request(url, 'GET').then(function (r) {
            $scope.personas = r.items;
            $scope.totalPadron = r.total;
            console.log(r.total)
        });
    }

    // SI NO SE NECESITA LA PROMESA NO PONER EL THEN 
    $scope.modalImportar = function () {
        $mdDialog.show({
            controller: importarPadronController,
            templateUrl: 'core/main/liquidacion/modals/importar-padron.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            locals: {
                empresa: $scope.colegio,
                periodo: $scope.periodo
            }
        });
    };

    $scope.cambiarPagina = function (numeroPagina) {
        $scope.paginaPadron += numeroPagina;
        $scope.getSearchPersonas($scope.searchTextPersonas);
    }

    $scope.paginasPadron = function (total) {
        return Math.ceil($scope.totalPadron / limite);
    };

}]);