liquidacion.controller('legajoTabController',
['$scope', '$rootScope', '$state', '$filter', 'conceptosService', '$mdDialog', 'apiService', 'liquidacionesService', 'moneyService', 'Notification', '$stateParams', '$timeout',
function ($scope, $rootScope, $state, $filter, conceptosService, $mdDialog, apiService, liquidacionesService, moneyService, Notification, $stateParams, $timeout) {
    var cantidad = new RegExp(/[Q]/);
    var importe = new RegExp(/[I]/);
    var patt = new RegExp("Vl");
    var patt2 = new RegExp("!");
    var patt3 = new RegExp("IF");
    var patt4 = new RegExp("#");
    var remPat = new RegExp(/\$1000/);
    var anioPat = /\b(legajo->anti_anios)\b/g;
    var bajaPat = /\b(legajo->baja)\b/g;
    var altaPat = /\b(legajo->alta)\b/g;
    var patObraSocial = new RegExp('U200');
    var func = new RegExp("variable");
    
    var valores = [];
    
    $scope.paginaConceptos = 0;

    var patronesRegex = {
        cantidad,
        importe,
        patt,
        patt2,
        patt3,
        patt4,
        remPat,
        anioPat,
        bajaPat,
        altaPat,
        func
    };

    $scope.$on('periodos-legajos-cargados', function () {

        apiService.get('valores/activos')
        .then(function (response) {
            valores = response;
            if($stateParams.legajo != null  && $stateParams.periodo != null){
                var item = {_id: $stateParams.legajo};
                $scope.periodo._id = $stateParams.periodo;
                $scope.changeCollapse(item, 1);
            }
        });
    });
    
    //// Liquidaciones
    $scope.collapseFuc = function () {
        if ($scope.collapse) $scope.collapse = !$scope.collapse;
    }

    $scope.pintarRow = function (tipoRemuneracion, sac) {
        var clase = '';
        switch (tipoRemuneracion) {
            case 1:
                clase = 'row-remuneracion';
                break;
            case 2:
                clase = 'row-no-renumerativo';
                break;
            case 3:
                clase = 'row-deduccion';
                break;
        }
        if (sac) clase += ' concepto-sac'; 
        return clase;
    }

    $scope.editLiquidacion = function (liquidacion) {
        if (cantidad.test(liquidacion.formula)) {
            modalCantidad(liquidacion);
        }
        else if (importe.test(liquidacion.formula)) {
            $scope.noEdit = false;
            modalImporte(liquidacion);
        }
        else {
            modalTexto(liquidacion);
        }
    }

    function loadLiquidaciones (liquidaciones, complete) {
        if( typeof(complete) === 'undefined') {
            $scope.datosLiquidacion = liquidaciones;
        }
        
        var totalLegajo = [];
        
        $scope.totales = {};
        $scope.totales.remunerativo = 0;
        $scope.totales.remunerativo_sac = 0;
        $scope.totales.no_remunerativo = 0;
        $scope.totales.deducciones = 0;
        //  Ordenamos por codigo
        $scope.liquidacion = $filter('orderBy') (liquidaciones.conceptos, 'codigo');
        
        for (var i = 0; i < $scope.liquidacion.length; i++) {
          var originalFormula = $scope.liquidacion[i].formula;
          var formula = $scope.formularFuc($scope.liquidacion[i].formula, $scope.liquidacion[i]);
          try {
              var intFormatTotal = math.eval(formula);
              var total = $scope.numberToMoney(intFormatTotal);
          }
          catch (e) {
              console.log('-----------ERROR---------------');
              console.log('error: ' + i);
              console.log(formula);
              console.log(originalFormula);
              var total = $scope.numberToMoney(0);
              var intFormatTotal = 0;
          }
          $scope.liquidacion[i].calculado = intFormatTotal;

          switch ($scope.liquidacion[i].tipo_remuneracion) {
            case 1:
                $scope.liquidacion[i].renumerativo = total;
                $scope.totales.remunerativo += intFormatTotal;
                if ($scope.liquidacion[i].sac) $scope.totales.remunerativo_sac += intFormatTotal;
                break;
            case 2:
                $scope.liquidacion[i].no_renumerativo = total;
                //($scope.liquidacion[i].activo) ? $scope.totales.no_remunerativo += intFormatTotal : $scope.totales.no_remunerativo += 0;
                $scope.totales.no_remunerativo += intFormatTotal
                break;
            case 3:
                $scope.liquidacion[i].deduccion = total;
                //($scope.liquidacion[i].activo) ? $scope.totales.deduccion += intFormatTotal : $scope.totales.deduccion += 0;
                $scope.totales.deducciones += intFormatTotal
                break;
            default:
                break;
          }
        }

        liquidacionesService.scopeOtrosLegajos(
            $scope,
            liquidaciones.legajo.persona.persona_id,
            liquidaciones.legajo.subempresa.empresa._id,
            $scope.periodo._id,
            liquidaciones._id
        ).then(function (r) {
            calcularTotales(totalLegajo);
        });
    }


    function calcularTotales () {
        try {
            $scope.totales.bruto = $scope.totales.remunerativo + $scope.totales.no_remunerativo;
            $scope.totales.neto = $scope.totales.bruto - $scope.totales.deducciones;

            // datos de todos los legajos de la misma persona 
            $scope.totales.total_bruto = $scope.totales.bruto;
            $scope.totales.total_neto = $scope.totales.neto;

            $scope.otrosLegajos.forEach(element => {
                $scope.totales.total_bruto += element.bruto;
                $scope.totales.total_neto += element.neto;
            });
            
            var datos = {
                totales: $scope.totales,
                conceptos: $scope.liquidacion
            };
            
            apiService.request(
                'liquidaciones/totales/' + $scope.datosLiquidacion._id, 
                'PUT', 
                datos
            );
        }
        catch (e) {
            console.log(e);
        }
    }

    function selectConceptos (conceptos, codigo) {
        var filtered = [];
        for (var i = 0; i < conceptos.length; i++) {
            if(conceptos[i].codigo){
                if (conceptos[i].codigo.substring(0, 2) == codigo) {
                    filtered.push(conceptos[i]);
                }
            }
        }
        return filtered;
    }

    $scope.changeCollapse = function(item, inside) {
        apiService.get('liquidaciones/' + item._id + '/' + $scope.periodo._id)
        .then(function successCallback(response) {
            // console.log(response.legajo.subempresa.categoria.codigo)
            // setea los conceptos, si las subempresa se cambia 

            try {
                $scope.conceptos = selectConceptos($rootScope.conceptos, response.legajo.subempresa.categoria.codigo);
            } catch (e) {
                console.log('Submperesa no definida', e); 
                $scope.conceptos =  $filter ('filter') ($rootScope.conceptos, {codigo: ''})
            }

            if (inside) {
                $scope.collapse = 1;
            }
            
            $scope.legajoDetalle = response.legajo;
            loadLiquidaciones(response);
        });
    }


    $scope.closeLiquidacion = function(){
        $scope.collapse = false;
        $scope.legajoDetalle = {};
    }

/// FORMULAS
    /**
     * Calcula el $1000d de la formula
     */
    function calcularRemunerativo () {
        return $scope.totales.remunerativo;
    }


    /**
     * Calcula todos los conceptos con ese numerol
     */
    function calculaNumerales (pattron,  codigo) {
        var conceptos = [];
        var findPattron = new RegExp(pattron);
        var resultadoFormula;
        var noFindPattron = new RegExp('#'+pattron);

        // Busca a todos los que tengan ese codig
        for (var i = 0; i < $scope.liquidacion.length; i++) {
            if (findPattron.test($scope.liquidacion[i].codigo) && $scope.liquidacion[i].codigo != pattron
                && $scope.liquidacion[i].codigo < codigo) {
                    conceptos.push($scope.liquidacion[i]);
            }
        }
        var resultadoFormula = '(';

        for (let i = 0; i < conceptos.length; i++) {
            resultadoFormula += '(' + $scope.formularFuc(conceptos[i].formula, conceptos[i]) + ')';

            if ((i + 1) != conceptos.length)
                resultadoFormula += '+';
        }
        if (conceptos.length == 0)
            resultadoFormula += '0';

        resultadoFormula += ')';

        return resultadoFormula;
    }


    /**
     * Busca una categoria por anio - categoria
     * @param {*} anios 
     * @param {*} categoria 
     */
    function calculaAntiguedad (anios, categoria) {
        var retorno = 0;
        $rootScope.antiguedad.forEach(element => {
            if (element.categoria._id === categoria && element.antiguedad === anios) {
                retorno = element.porcentaje+"/100";
            }
        });
        console.log('retorno:', retorno);
        return retorno;
    }


    /**
     * Calculo fechas
     * @param {*} fecha 
     * @param {*} tipo 
     */
    function calcularFecha (fecha, tipo) {
        //
        if (typeof(fecha) === 'undefined') {
            Notification.error('La fecha de ' + tipo + ' no esta definida');
            return 0;
        }
        var dias = moment.utc(fecha).date();
        var difDias = 0;

        if (moment.utc(fecha).isSame($scope.periodo.fecha_inicio, 'year') 
        && moment.utc(fecha).isSame($scope.periodo.fecha_inicio, 'month')) {
            if (tipo == 'baja') { 
                if (dias >= 30) {
                    difDias = 0;
                }
                difDias = 30 - dias;
            } 
            else if (tipo == 'alta') {
                if (dias <= 1) {
                    difDias = 0;
                }
                difDias = dias - 1
            }
    
            return difDias;
        }
        else {
            Notification.error('Este no es la liquidacion del mes de ' + tipo);
            return 0;
        }
        
    }

    $scope.formularFuc = function (formula, liquidacion) {
        if (formula === undefined || formula == '')
          return false;

        if (liquidacion.activo === false) 
            return 0;  
        if (liquidacion !== undefined) {
            formula = formula.replace(/\b(legajo->horas)\b/g, $scope.legajoDetalle.horas);
            formula = formula.replace(/\b(legajo->importe1)\b/g,  $scope.legajoDetalle.importe1);
            formula = formula.replace(/\b(legajo->importe2)\b/g,  $scope.legajoDetalle.importe2);
            formula = formula.replace(/\b(legajo->importe3)\b/g,  $scope.legajoDetalle.importe3);
            formula = formula.replace(/\b(legajo->subsidio)\b/g,  $scope.legajoDetalle.subsidio);
            formula = formula.replace(/\b[Q]\b/g, liquidacion.cantidad);
            formula = formula.replace(/\b[I]\b/g, liquidacion.importe);
            formula = formula.replace(/\b(legajo->antiguedad)\b/g, $scope.legajoDetalle.antiguedad.anios); //check si es anios o meses
            formula = formula.replace(/\bC_I\b/g, $scope.legajoDetalle.cargo.indice);

            // Se desabilita funcion de calcular el importe 
            // if (typeof(liquidacion.importe) !== 'undefined' && liquidacion.importe != 0) {
            //     formula = '(' + formula + ')*' + liquidacion.cantidad;
            // }
        }
        // si se cagotea aca devuelve 1
        try {
            if (anioPat.test(formula)) {
                formula = formula.replace(anioPat, calculaAntiguedad($scope.legajoDetalle.antiguedad.anios, $scope.legajoDetalle.subempresa.categoria._id));
                console.log('con antiguedad:', formula);
            }

            if (patronesRegex.bajaPat.test(formula)) {
                formula = formula.replace(/\b(legajo->baja)\b/g,  calcularFecha($scope.legajoDetalle.fecha_baja, 'baja'));
                console.log('Con fecha de baja: ', formula);
            }

            if (patronesRegex.altaPat.test(formula)) {
                formula = formula.replace(/\b(legajo->alta)\b/g,  calcularFecha($scope.legajoDetalle.fecha_alta, 'alta'));
                console.log('Con fecha de alta: ', formula);
            }

            
            //reemplazar vl por valores
            while (remPat.test(formula)) {
                formula = formula.replace(remPat, calcularRemunerativo());
            }

            while (patt.test(formula)){
                var rx = /(Vl+)\w+/g;
                var arr = rx.exec(formula);
                var l = arr[0].length;
                var sp = formula.indexOf("Vl");
                var val = formula.substring(sp+2, sp+l);
                val = parseInt(val);

                var valor = $filter('filter')(valores, { codigo: val }, true)[0];
                formula = formula.replaceBetween(sp, sp+l, valor.valor.toString());
            }

            //reemplazar ! por su propio indice
            while (patt2.test(formula)) {
                var rx = /(![A-Z])\w+/g;
                var arr = rx.exec(formula);
                var l = arr[0].length;
                var sp = formula.indexOf("!");
                var val = formula.substring(sp+1, sp+l);
                var valor = $filter('filter')($rootScope.conceptos, { codigo: val }, true)[0];

                if (typeof(valor) === "undefined") {
                    Notification.error('El Concepto: '+ val + ' no esta cargado. Requerido por el concepto: ' + liquidacion.codigo );
                    formula = formula.replaceBetween(sp, sp+l, '0');
                }
                else if (valor.indice != 0) {
                    formula = formula.replaceBetween(sp, sp+l, valor.indice.toString());
                }
                else {         
                    var myLiquidacion = $filter('filter')($scope.liquidacion, { codigo: val }, true)[0];
                    if (myLiquidacion !== undefined && myLiquidacion.codigo < liquidacion.codigo)
                        formula = formula.replaceBetween(sp, sp+l, $scope.formularFuc(myLiquidacion.formula, myLiquidacion));
                    else
                        formula = formula.replaceBetween(sp, sp+l, '0');
                }
            }

            //reemplazar functions de excels
            while (patt3.test(formula)) {
                var rx = /([IF])\w+/g;
                var arr = rx.exec(formula);
                var l = arr[0].length;
                var sp = formula.indexOf("IF"); //start point
                var text = formula.substring(sp);

                var temp = 0;
                var temp2 = 0;
                var first = false;
                for (var i=0; i<text.length; i++){
                    if(text.charAt(i) == '('){
                        temp++;
                        first = true;
                    }
                    if(text.charAt(i) == ')'){
                        temp2 = i;
                        temp--;
                    }
                    if(temp == 0 && first == true){
                        break;
                    }
                }
                text = text.substring(0,temp2+1);
                text = text.replace(/(#[A-Z])\w+/g, "1");

                var formattedFormula = excelFormulaUtilities.formula2JavaScript(text);
                formattedFormula = formattedFormula.replace(',','?').replace(',',':');
                // console.log(formattedFormula)
                var x = eval(formattedFormula);
                formula = formula.replaceBetween(sp, sp + temp2+1, x.toString());
            }

            while (patt4.test(formula)) {
                var rx = /(#[A-Z])\w+/g;
                var arr = rx.exec(formula);
                var l = arr[0].length;
                var sp = formula.indexOf("#");
                var val = formula.substring(sp+1, sp+l);
                formula = formula.replaceBetween(sp, sp+l, calculaNumerales(val, liquidacion.codigo));
            }

            while(func.test(formula)){
                var rx = /([variable(])\w+/g;
                
                var arr = rx.exec(formula);
                var sp = formula.indexOf("variable"); //start point
                var text = formula.substring(sp);
                var ep = text.indexOf(")"); //end point
                text = text.slice(9, ep)
                text = text.split(",")
                
                var where = {
                    'campo_1' : text[0],
                    'campo_2' : text[1],
                    'campo_3' : text[2],
                    'campo_4' : text[3],
                    'campo_5' : text[4]
                }
                
                var variable = $filter('filter')($rootScope.variables, where, true)[0];
                formula = formula.replaceBetween(sp, sp+ep+1, variable.valor);
            }

            return formula;
        }
        catch (e) {

            console.log(e);
            console.log(formula);
            return 9;
        }
    }


    // MOdal memo 
    $scope.memo = function (datosLiquidacion) {
        $mdDialog.show({
            controller: memoController,
            templateUrl: 'core/main/liquidacion/memo-modal.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            locals: {
                datosLiquidacion: datosLiquidacion
            }
        }).then(function () {
            liquidacionesService.getLiquidacionById(datosLiquidacion._id).then(function (response) {
                loadLiquidaciones(response);
            });
        });
    }


    $scope.changeLegajo = function (liquidacion) {
        $scope.changeCollapse(liquidacion.legajo, 0);
    }

    $scope.deleteLiquidacion = function (liq) {
        $mdDialog.show({
            controller: liquidacionDeleteController,
            templateUrl: 'core/main/conceptos/confirm-modal.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        }).then(function (r) {
            if (!r) {
                return false;
            }
            apiService.request(
                'liquidaciones/' + $scope.datosLiquidacion._id + '/conceptos/' + liq.liquidacion_id,
                'DELETE',
                liq)
                .then(function successCallback(response) {
                    getLiquidacion($scope.legajoDetalle);
                });
        });
    }

    function getLiquidacion(legajo) {
        return apiService.get('liquidaciones/' + legajo._id + '/' + $scope.periodo._id).then(function (r) {
            loadLiquidaciones(r);
        });
    }

    /**
     * Cambia el concepto de fijo a evental y tambien el activo 
     * @param {Object} liq 
     */
    $scope.cambiarSwitch = function (liq, recalcular) {
        var data = {
            liquidacion_id: liq.liquidacion_id,
            fijo: liq.fijo,
            activo: liq.activo
        };

        apiService.request(
            'liquidaciones/fijo/' + $scope.datosLiquidacion._id,
            'PUT',
            data
        ).then(function (params) {
            if (recalcular === true) loadLiquidaciones($scope.datosLiquidacion);
        });
    }


    function setInactivo () {
        $scope.datosLiquidacion.conceptos.forEach(elment =>  {
            elemt.activo = false;
        })
    }

    // cargar modal de ganancias
    $scope.ganancias = function (liquidacion, totales) {
        var ganancias = $filter('filter') ($scope.datosLiquidacion.conceptos, {codigo: 'ZGAN001'});
        if (ganancias.length > 0) {
            Notification.error('Las ganancias se encuentran calculadas');
            return false;
            // apiService.request(
            //     'liquidaciones/' + $scope.datosLiquidacion._id + '/conceptos/' + ganancias[0].liquidacion_id,
            //     'DELETE',
            //     null)
            // .then(function successCallback(response) {
            //     getLiquidacion($scope.legajoDetalle).then(function (params) {
            //         $timeout(sendGanancias(liquidacion), 10000);
            //     });
            // });
        } else {
            sendGanancias(liquidacion);
        }
            
    }

    function sendGanancias (liquidacion) {
        apiService.get('ganancias/' + liquidacion._id).then(function (response) {
            liquidacionesService.getLiquidacionById(liquidacion._id).then(function (response) {
                loadLiquidaciones(response);
            });
        });
    }

    function cargarModalGanancia (ganancia) {
        $mdDialog.show({
            controller: gananciaModalController,
            templateUrl: 'core/main/liquidacion/modals/ganancia-modal.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            locals: {
                ganancia: ganancia
            }
        });
    }

    $scope.resetPageConceptos = function () {
        $scope.paginaConceptos = 0;
    }

    $scope.getConceptos = function (search) {
        return $filter('filter')($scope.conceptos, search);
    };

    $scope.numberOfPagesConceptos = function (search) {
        return Math.ceil($scope.getConceptos(search).length / $scope.pageSize);
    }

    $scope.agregarConcepto = function (legajoID) {
        if ($scope.verConceptos) {
            $scope.verConceptos = false;
        } else {
            $scope.verConceptos = true;
        }
    };

    $scope.conceptoSeleccionado = function (concepto, legajo) {
        var cantidad = new RegExp(/\b[Q]\b/g);
        var importe = new RegExp(/\b[I]\b/g);

        conceptoEditable = angular.copy(concepto);

        if (cantidad.test(concepto.formula)) {
            modalCantidad(conceptoEditable, legajo._id);
        } else if (importe.test(concepto.formula)) {
            modalImporte(conceptoEditable, legajo._id);
        } else {
            guardarConceptoLiquidacion(conceptoEditable, legajo._id);
        }
    }

    function modalCantidad (concepto, legajoID) {
        $mdDialog.show({
            controller: cantidadController,
            templateUrl: 'core/main/liquidacion/modals/cantidad.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            locals: {
                concepto: concepto
            },
        }).then(function (r) {
            if (!r)
                return false;

            if (typeof (legajoID) != 'undefined') {
                guardarConceptoLiquidacion(r, legajoID);
            }
            else {
                editarConceptoLiquidacion(r);
            }
        });
    }



     /**
     * Modal para cargar Texto Personalizado
     * en una liquidacion especifica
     * @param {Object} concepto
     * @param {_id} legajoID
     */
    function modalTexto(concepto, legajoID) {
        $mdDialog.show({
            controller: textoController,
            templateUrl: 'core/main/liquidacion/modals/texto.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            locals: {
                concepto: concepto
            },
        }).then(function (r) {
            if (!r)
                return false;

            if (typeof (legajoID) != 'undefined')
                guardarConceptoLiquidacion(r, legajoID);
            else
                editarConceptoLiquidacion(r);
        });
    }



    /**
     * Modal para cargar importe de concepto
     * en una liquidacion especifica
     * @param {Object} concepto
     * @param {_id} legajoID
     */
    function modalImporte(concepto, legajoID) {
        $mdDialog.show({
            controller: importeController,
            templateUrl: 'core/main/liquidacion/modals/importe.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            locals: {
                concepto: concepto
            },
        }).then(function (r) {
            if (!r)
                return false;

            if (typeof (legajoID) != 'undefined')
                guardarConceptoLiquidacion(r, legajoID);
            else
                editarConceptoLiquidacion(r);
        });
    }

    /**
    * Guarda una liquidacion en la db (parsea el objeto )
    * @param {Object} Liquidacion
    * @param {Number} legajoID
    */
    function guardarConceptoLiquidacion(liquidacion, legajoID) {
        if (liquidacion.hasOwnProperty('concepto'))
            liquidacion = liquidacion.concepto;
        postData = liquidacion
        if (liquidacion.cantidad !== "undefined")
            postData['cantidad'] = liquidacion.cantidad;
        if (liquidacion.importe !== undefined)
            postData['importe'] = liquidacion.importe;
        if (liquidacion.descripcionCustom !== undefined) {
            postData['descripcion_custom'] = liquidacion.descripcionCustom;
        } else if (patObraSocial.test(liquidacion.codigo)) {
            try {
                postData['descripcion'] = $scope.datosLiquidacion.legajo.persona.datos_administrativos.obra_social.nombre;
            } 
            catch (e) {
                console.log(e);
                postData['descripcion_custom'] = undefined;
            }
        }

        var id = $scope.datosLiquidacion._id;
        apiService.post('liquidaciones/conceptos/' + id, postData)
        .then(function (response) {
            liquidacionesService.getLiquidacionById(id).then(function (response) {
                loadLiquidaciones(response);
            });
        });        
    }


    /**
     * Edita las liquidaciones con cantidad/importe/descripcion
     * @param {Object} Liquidacion
     */
    function editarConceptoLiquidacion(liqui) {
        if (liqui.hasOwnProperty('concepto'))
            liqui = liqui.concepto;

        apiService.request(
            'liquidaciones/concepto/' + liqui.liquidacion_id,
            'PUT',
            liqui
        ).then(function (response) {
            // vuelve a cargar la liquidacion
            liquidacionesService.getLiquidacionById($scope.datosLiquidacion._id)
            .then(function (response) {
                loadLiquidaciones(response);
            });
        });
    }


    $scope.calcularSAC = function (periodo) {
        $mdDialog.show({
            controller: liquidacionDeleteController,
            templateUrl: 'core/main/liquidacion/modals/confirm-SAC.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose:true
        }).then( function (r) {
            if (!r) {
                return false;
            }

            apiService.request(
                'periodos/sac/procesar/liquidaciones',
                'POST',
                periodo
            ).then(function (respuestaSac) {
                $state.go('liquidacion', {estado: 'legajos'}, {reload: true });
            });
        });
    }

    // Esc for exit
    $scope.keyPressed = function(e) {
        if (e.which == "27") {
            $scope.verConceptos = false;
        }
    };

    $scope.mesesFijo = function (liq) {
        $mdDialog.show({
            controller: mesesFijoController,
            templateUrl: 'core/main/liquidacion/modals/meses-fijo.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            locals: {
                concepto: liq
            },
        }).then(function (r) {
            
        });
    }

    $scope.gananciasTodosCalculo = function () {
        apiService.request(
            'ganancias/calcular-ganancias/'+ $scope.colegio._id + '/' + $scope.periodo._id  ,
            'GET'
        ).then(function (data) {
            $state.go('liquiacion', {estado: 'legajo'},  {reload: true});
        });
    }

    $scope.eliminarLegajo = function (legajo){

        $mdDialog.show({
            controller: liquidacionDeleteController,
            templateUrl: 'core/main/conceptos/confirm-modal.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        }).then(function (r) {
            if (!r) {
                return false;
            }
            apiService.request(
                    'legajos/' + legajo._id,
                    'DELETE'
                ).then(function (data) {
                    location.reload();
            });
        });
    }

    $scope.legajosActivos = function (){
        $mdDialog.show({
            controller: legajosActivosController,
            templateUrl: 'core/main/liquidacion/modals/legajos-activos-modal.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        }).then(function (r) {
        });
    }

}]);