liquidacion.controller('gananciasTabController', ['$scope', 'apiService', 
function ($scope, apiService) {
  $scope.pageGanancias = 1;
  var inProcess = false;

  $scope.uploadFile = function () {
    if (inProcess)
      return false;
    inProcess = true;
    var filename = event.target.files[0].name;
    var personaId = event.target.dataset.persona;
    var reader = new FileReader();

    reader.onload = function () {
      //var parsed = new DOMParser().parseFromString(this.result, "text/xml");
      var data = {
        nombre_archivo: filename,
        xml: this.result,
        periodo: $scope.periodo,
        empresa: $scope.colegio,
        persona_id: personaId
      }
      apiService.post('ganancias/xml/' + personaId, data).then(function (r) {
        inProcess = false;
      });
    };
    reader.readAsText(event.target.files[0]);
  };

  $scope.paginarGanancias = function (page) {
    $scope.pageGanancias = page;
  }

  $scope.procesarGanancias = function () {
    var data = {
      periodo: $scope.periodo,
      empresa: $scope.colegio
    }
    apiService.post('ganancias/procesar', data)
    .then(function (params) {
      console.log('ganancias');
    });
  }
}]);

liquidacion.directive('customOnChange', function() {
  return {
      restrict: 'A',
      link: function (scope, element, attrs) {
          var onChangeFunc = scope.$eval(attrs.customOnChange);
          element.bind('change', onChangeFunc);
      }
  };
});

