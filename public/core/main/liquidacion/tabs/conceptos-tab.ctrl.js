liquidacion.controller('ConceptoTabController', ['$scope', '$rootScope', '$state', '$filter', 'conceptosService', '$mdDialog', 'apiService',
function ($scope, $rootScope, $state, $filter, conceptosService, $mdDialog, apiService) {
    $rootScope.sideBar = false;
    $scope.conceptoPager = {};
    $scope.conceptoPager.currentPage = 0;
    $scope.conceptoPager.pageSize = 10;
    
    $scope.empresaId = localStorage.getItem('empresaId');

    function init () {
        conceptosService.getConceptosEmpresa($scope.empresaId)
        .then(function (response) {
            $scope.conceptos = response;
        });
    }

    $scope.conceptoPager.numberOfPages = function () {
        if($scope.conceptosFiltered()) 
            return Math.ceil($scope.conceptosFiltered().length/$scope.pageSize);
    }

    $scope.conceptosFiltered = function () {
        return $filter('filter')($scope.conceptos, $scope.conceptoFilter);
    }

    $scope.conceptoEliminar = function (ev, item) {
        $mdDialog.show({
          controller: conceptoDeleteController,
          templateUrl: 'core/main/conceptos/confirm-modal.tpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        }).then(function (data) {
          if (data) {
              apiService.request('conceptos/' + item._id, 'DELETE')
              .then(function successCallback(response) {
                init();
              });
          }
        });
      };

    
    init();
}]);