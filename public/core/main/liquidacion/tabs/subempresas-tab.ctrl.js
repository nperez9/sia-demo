liquidacion.controller('subempresasTabController', ['$scope', 'apiService', '$mdDialog',
function ($scope, apiService, $mdDialog) {
  $scope.subempresaEliminar = function(ev, item) {
    $mdDialog.show({
      controller: conceptoDeleteController,
      templateUrl: 'core/main/conceptos/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function (data) {
      if (data) {
        apiService.request(
          'subempresas/' + item._id,
          'DELETE',
          null,
          false
        )
        .then(function successCallback (response) {
          getSubempresas($scope.colegio._id);
        });
      }
    });
  };

  /**
   * Realiza un get de las subempresas
   * @param {ObjectID} idEmpresa (obsoleto)
   */
  function getSubempresas (idEmpresa) {
    apiService.get('subempresas/empresa/' + $scope.colegio._id + '/' + $scope.periodo._id).then(function (subempresas) {
        $scope.subempresas = subempresas;
    });
  }

}]);