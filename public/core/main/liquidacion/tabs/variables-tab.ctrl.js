liquidacion.controller('ValoresTabController', ['$scope', '$rootScope', '$state', '$filter', 'conceptosService', '$mdDialog', 'apiService',
function ($scope, $rootScope, $state, $filter, conceptosService, $mdDialog, apiService) {
    $rootScope.sideBar = false;
    $scope.variablePager = {};
    $scope.variablePager.currentPage = 0;
    $scope.variablePager.pageSize = 10;
    
    // $scope.empresaId = localStorage.getItem('empresaId');
    // console.log()
    // console.log()

    function init () {
        apiService.get('variables/' + localStorage.getItem('empresaId') + '/' + localStorage.getItem('periodo'))
        .then(function successCallback(response) {
            $scope.variables = response;
        });
    }

    $scope.variablePager.numberOfPages = function () {
        if($scope.variablesFiltered()) 
            return Math.ceil($scope.variablesFiltered().length/$scope.pageSize);
    }

    $scope.variablesFiltered = function () {
        return $filter('filter')($scope.variables, $scope.variableFilter);
    }

    $scope.variableEliminar = function (ev, item) {
        $mdDialog.show({
          controller: conceptoDeleteController,
          templateUrl: 'core/main/conceptos/confirm-modal.tpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        }).then(function (data) {
          if (data) {
              apiService.request('variables/' + item._id, 'DELETE')
              .then(function successCallback(response) {
                init();
              });
          }
        });
      };

    
    init();
}]);