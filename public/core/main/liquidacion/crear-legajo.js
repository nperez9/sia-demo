conceptos.controller('crearLegajoController', ['$scope', '$rootScope', '$filter', '$state', '$mdDialog', '$stateParams', 'apiService', 'liquidacionesService',
function ($scope, $rootScope, $filter, $state, $mdDialog, $stateParams, apiService, liquidacionesService) {
    $rootScope.sideBar = false;
    $scope.id = $stateParams.id;
    var periodoId = $stateParams.periodoid;
    $scope.legajo = {};
    $scope.legajo.licencias = [];
    $scope.legajo.suplencias = [];

    // DUPLICAR CODIGO -- DUPLICAR RESULTADO DE UNA FUNCION 
    $scope.save = function () {
        if (!liquidacionesService.validarLegajo($scope.legajo)) return false;
        apiService.request('GET','legajos/maxlegajo/' + $scope.id)
        .then(function (response) {
            // ACCEDER DIRECTAMENTE AL INDICE DE UN ARRAY INDEFINIDO
            try {
                $scope.legajo.legajo_interno = parseInt(response[0].legajo_interno);
                $scope.legajo.legajo_interno = $scope.legajo.legajo_interno + 1;
            } catch (e) {
                console.log(e);
                $scope.legajo.legajo_interno = 1;
            }
            guardarLegajo();
        });
    }
    
    function guardarLegajo () {
        apiService.post('legajos', $scope.legajo)
        .then(function (response) {
            $state.go('liquidacion');
        });
    }

    function init() {
        apiService.get('legajos/' + $scope.id + '/data/' + periodoId).then(function (r) {
            $scope.cargos = r.cargos;
            $scope.personas = r.personas;
            $scope.subempresas = r.subempresas;
        });
        apiService.get('periodos/' + periodoId).then(function (r) {
            $scope.legajo.periodo = r;
        });
        apiService.get('legajos/suplea/' + $scope.id + '/' + periodoId).then(function (r) {
            $scope.suplea = r;
        });
    }

    $scope.filtrarCargos = function (subempresa) {
        try {
            return $filter('filter')($scope.cargos, { categoria_codigo: subempresa.categoria.codigo });
        } catch (e) {
            console.log('error en el filtro de cargos');
            return $scope.cargos;
        }
    }

    $scope.selectItemChange = function (item) {
        $scope.legajo.persona = item;
    }

    $scope.selectSupleaChange = function (sup, item) {
        sup.legajo =  item.numero_legajo;
        sup.cuil =  item.persona.cuil;
        sup.titular = item.persona.nombre;
        }

    $scope.agregarLicencia = function () {
        $scope.legajo.licencias.push({});
    }

    $scope.borrarLicencia = function (index) {
        $scope.legajo.licencias.splice(index, 1);
    }

    $scope.agregarSuplencia = function () {
        $scope.legajo.suplencias.push({});
    }

    $scope.borrarSuplencia = function (index) {
        $scope.legajo.suplencias.splice(index, 1);
    }

    $scope.searchPersona = function (searchPersona) {
        return $filter('filter')($scope.personas, { nombre: searchPersona });
    }

    $scope.searchSuplea = function (searchTextSuplea) {
        return $filter('filter')($scope.suplea, { persona: { nombre: searchTextSuplea } });
    }

    $scope.getCargos = function () {
        var categoria = $scope.legajo.subempresa.categoria.codigo;

        apiService.get('legajos/categorias/' + categoria).then(function (r) {
            $scope.cargos = r.cargos;
        });
    }

    $scope.countDays = function (sup){
        if(sup.fecha_desde && sup.fecha_hasta){
            fecha1 = moment(sup.fecha_desde);
            fecha2 = moment(sup.fecha_hasta);
            sup.dias = fecha2.diff(fecha1, 'days');
        }
    }
    
    init();
}]);
