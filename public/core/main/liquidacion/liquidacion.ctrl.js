liquidacion.controller('liquidacionController',
['$scope', '$filter', 'apiService', '$rootScope', '$state', '$mdDialog', 'moneyService', 'liquidacionesService', 'Notification', '$stateParams', 'personaService', 'antiguedadService', 'moment', '$cookies', 'downloadService', '$window',
function($scope, $filter, apiService, $rootScope, $state, $mdDialog, moneyService, liquidacionesService, Notification, $stateParams, personaService, antiguedadService, moment, $cookies, downloadService, $window) {
    $rootScope.sideBar = false;
    $scope.estado = $stateParams.estado;

    $scope.searchText = '';
    $scope.legajos = [];
    $scope.loading = true;
    $scope.currentPage = 0;
    $scope.pageSize = 6;
    $scope.data = [];
    $scope.q = '';
    $scope.actualLegajoID;
    $scope.periodo = {};
    $scope.activo = true;
    
    var periodoId = localStorage.getItem('periodo');

    var cantidad = new RegExp(/[Q]/);
    var importe = new RegExp(/[I]/);
    var patt = new RegExp("Vl");
    var patt2 = new RegExp("!");
    var patt3 = new RegExp("IF");
    var patt4 = new RegExp("#");
    var remPat = new RegExp(/\$1000/);
    var anioPat = /\b(legajo->anti_anios)\b/g;
    var bajaPat = /\b(legajo->baja)\b/g;
    var altaPat = /\b(legajo->alta)\b/g;

    String.prototype.replaceBetween = function(start, end, what) {
        return this.substring(0, start) + what + this.substring(end);
    };

    /**
    * Init
    */
    function init () {
        $scope.collapse = false;
        var colegioId = localStorage.getItem('empresaId');
        var periodoId = localStorage.getItem('periodo');

        if (!$rootScope.conceptos) {
            apiService.get('conceptos/all')
            .then(function (response) {
                $rootScope.conceptos = response;
            });
        }

        if (!$rootScope.variables) {
            apiService.get('variables/')
            .then(function (response) {
                $rootScope.variables = response;
            });
        }

        if (!$rootScope.antiguedad) {
            apiService.get('antiguedad/all')
            .then(function (response) {
                $rootScope.antiguedad = response;
            });
        }

        var idCookie = '0';
        
        if (!$cookies.get('id')) {
            Notification.error({ message: 'Acceso denegado.', delay: 3000 });
            $state.go('liquidacion')
        } 
        if($cookies.get('tipo') == "liquidador") {
            idCookie = $cookies.get('id').toString();
        }

        apiService.get('empresas/liquidacion/' + idCookie)
        .then(function successCallback(response) {
            if (response) {
                $scope.colegios = response;
                if (colegioId) {
                    for (var i = 0, len = $scope.colegios.length; i < len; i++) {
                        if( $scope.colegios[i]._id == colegioId) {
                            $scope.colegio = $scope.colegios[i];
                            localStorage.setItem('empresaId', $scope.colegio._id);
                        }
                    }
                    cambioEmpresa(colegioId);
                } else {
                  $scope.cambioColegio();
                }
            }
        });
  
        $scope.search

    };

    $scope.search = function () {
        apiService.get('personas/')
        .then(function successCallback(response) {
            $scope.items = response.items;
            $scope.total = response.total;
        });
    }

    function cambioEmpresa (empresaId, cambio) {
        getPeriodo(empresaId, cambio);
        getAntiguedades(empresaId);
    }

    function getPeriodo (empresaID, cambio) {
        // Si es cambio levanta un nuevo periodo
        if (periodoId && (typeof(cambio) === 'undefined' || cambio !== true)) {
            apiService.get('periodos/' + periodoId).then(function (params) {
                successPeriodoCallback(empresaID, params)
            });
        } else {
            apiService.get('periodos/actual/' + empresaID).then(function (params) {
                successPeriodoCallback(empresaID, params)
            });
        }
        
        apiService.get('periodos/todos/' + empresaID).then(function (r) {
            $scope.periodosEmpresa = r;
        });
    }

    var successPeriodoCallback = function (empresaID, periodo) {
        $scope.periodo = periodo;
        localStorage.setItem('periodo', periodo._id);
        findPersonas();
        getLegajosColegio(empresaID, periodo);
    }

    function getAntiguedades (empresaID) {
        apiService.get('antiguedad/?empresaId=' + empresaID).then(function (r) {
            $scope.antiguedades = r.items;
        });
    }

    $scope.getSearchAntiguedad = function(empresaID, search) {
        apiService.get('antiguedad/?empresaId=' + empresaID + '&search=' + search).then(function (r) {
            $scope.antiguedades = r.items;
        });
    }

    function getLegajosColegio (colegioID, periodo) {
        apiService.get('empresas/' + colegioID + '/datos-liquidacion/' + periodo._id)
        .then(function (response) {
            $scope.findLegajos();
            $scope.subempresas = response.subempresas;
            $scope.concepts = response.conceptos;
        });
    }
    
    $scope.findLegajos = function () {
      if ($scope.periodo._id === undefined)
        return false;

      if ($scope.colegio) {
        var query = '?empresaId='+$scope.colegio._id + '&periodoId=' + $scope.  periodo._id;
        query += ($scope.subempresasFilter) ? '&subempresaId='+$scope.subempresasFilter : "";
        query += ($scope.estadoLegajo) ? '&estado='+$scope.estadoLegajo : "";

       apiService.get('liquidaciones/legajos/' + query)
        .then(function (response) {
            $scope.legajos = [];
            for (var i = 0; i < response.length; i++) {
                $scope.legajos.push(response[i].legajo);
            }
            // BroadCast de que termino de cargar los legajos 
            $scope.$broadcast('periodos-legajos-cargados');
        });
      }
    }

    
    $scope.resetPage = function () {
        $scope.currentPage = 0;
    }
    
    $scope.numberOfPages = function () {
      return Math.ceil($scope.getLegajosFiltered().length / $scope.pageSize);
    }

    $scope.getLegajosFiltered = function (search) {
      return $filter ('filter') ($scope.legajos, {persona: {nombre: search}});
    };

    $scope.findLegajosPersona = function(nombre){
        return $filter('filter')($scope.legajos, { nombre: nombre });
    }

    $scope.cambioColegio = function(ev) {
      //$scope.loading = true;
      $mdDialog.show({
        controller: cambioColegioController,
        templateUrl: 'core/main/liquidacion/cambio-colegio.tpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        preserveScope: true,
        locals: {
            colegios: $scope.colegios
        }
      }).then(function (data) {
        if (data) {
          $scope.colegio = data;
          localStorage.setItem('colegio', $scope.colegio._id);
          localStorage.setItem('empresaId', $scope.colegio._id);
          localStorage.removeItem('periodo');
          reloadThePage($stateParams.estado);
        }
      });
    };

    $scope.cambioPeriodo = function(ev) {
        //$scope.loading = true;
        $mdDialog.show({
          controller: cambioPeriodoController,
          templateUrl: 'core/main/liquidacion/modals/periodos.tpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          preserveScope: true,
          locals: {
              periodos: $scope.periodosEmpresa
          }
        }).then(function (data) {
          if (data) {
            $scope.periodo = data;
            localStorage.setItem('periodo', $scope.periodo._id);
            reloadThePage($stateParams.estado);
          }
        });
    }

    
    $scope.cerrarPeriodo = function (periodo, empresa) {
        $mdDialog.show({
            controller: liquidacionDeleteController,
            templateUrl: 'core/main/liquidacion/modals/confirm-periodo.tpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose:true
        }).then( function (r) {
            if (!r) {
                return false;
            }
            var data = {
                periodo: periodo,
                empresa: empresa
            };

            apiService.post('periodos/cerrar-periodo', data)
            .then(function (r) {
                localStorage.setItem('periodo', r.periodo._id);
                reloadThePage('legajo');
            });
        });
    }


    function reloadThePage (estado) {
        $state.go('liquidacion', {
                estado: estado, 
                legajo: null, 
                periodo: null
            }, 
            {
                reload:true
            }
        );
    }


    // Ganancias
    // Busca las personas
    function findPersonas () {
        apiService.get('personas/liquidacion/?page=' + $scope.pageGanancias + '&empresaId=' + $scope.colegio._id + '&periodoId=' + $scope.periodo._id)
        .then(function (result) {
           $scope.personasGanancias = result.items;
           $scope.totalGanancias = result.total;
        });
    }


    $scope.numberToMoney = function (number) {
        return moneyService.format(number);
    }


    // periodo eliminar
    $scope.periodoEliminar = function(ev, item) {
        $mdDialog.show({
          controller: conceptoDeleteController,
          templateUrl: 'core/main/conceptos/confirm-modal.tpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        }).then(function (data) {
          if (data) {
            apiService.request(
              'periodos/' + item._id,
              'DELETE',
              null,
              false
            )
            .then(function successCallback (response) {
                getPeriodo($scope.colegio._id);
            });
          }
        });
      };

    $scope.exportarMemos = function(periodo, empresa){
        downloadService.downloadFile('liquidaciones/memos/' + periodo._id + '/' + empresa._id, null, 'memos.pdf');
    }

    init();
}]);