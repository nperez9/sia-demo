liquidacion.service('liquidacionesService', ['apiService', 'Notification',
function (apiService, Notification) {
    this.url = 'liquidaciones/';
    this.liquidaciones = {};

    function errorCallback (r) {
        console.log(r);
    }

    this.scopeConcepto = function ($scope, id) {
        var url = this.url + id;
        return apiService.request(
            url,
            'GET'
        ).then(function (response) {
            $scope.concepto = response;
        }, errorCallback);
    }

    this.scopeAntiguedad = function ($scope, id) {
        var url = this.url + id;
        return apiService.request(
            url,
            'GET'
        ).then(function (response) {
            $scope.antiguedad = response;
        }, errorCallback);
    }

    /**
     * put otrosLegajos in the $scope
     * @param {Object} $scope
     * @param {String} personaid
     * @param {String} empresaid
     */
    this.scopeOtrosLegajos = function ($scope, personaid, empresaid, periodoid, id) {
        var url = 'legajos/otros/' + personaid + '/' + empresaid + '/' + periodoid + '/' + id;
        return apiService.request(
            url,
            'GET'
        ).then(function (response) {
            $scope.otrosLegajos = response;
        }, errorCallback);
    }

    /**
     * Devuelve si el legajo pasa la validacion de datos o no
     * @param {object} legajo 
     * @return {boolean} response
     */
    this.validarLegajo = function (legajo) {
        var error = false;
        var response = true;

        if (typeof(legajo.persona) !== 'object') {
            error = 'Debe ingresar una persona';
        }
        else if (typeof(legajo.subempresa) !== 'object')  {
            error = 'Debe ingresar una subempesa';
        } 
        else if (typeof(legajo.cargo) !== 'object')  {
            error = 'Debe ingresar un Cargo';
        }

        if (error !== false) {
            response = false; 
            Notification.warning(error);
        }
        return response;
    }


    this.getLiquidacionById = function (id) {
        return apiService.get(this.url + id).then(function successCallBack (liquidacion) {
           return liquidacion;
        }, errorCallback);
    }


    // endopint provisorio. CALCULOS DE TOTALES EN EL BACKEND !!!!
    this.putTotales  = function (id, totales) {
        return apiService.request(
            this.url + 'totales/' + id,
            'PUT',
            totales,
            false
        );
    }

    this.insertConcepto = function (concepto) {
        return apiService.request(
            this.url,
            'POST',
            concepto
        );
    }

    this.editConcepto = function (concepto, id) {
        return apiService.request(
            this.url + id,
            'PUT',
            concepto
        );
    }

    this.deleteConcepto = function (id) {
        return apiService.request(
            this.url + id,
            'DELETE'
        );
    }
}]);
