conceptos.controller('editarLegajoController', ['$scope', '$rootScope', '$filter', '$state', '$mdDialog', '$stateParams', '$interval', 'apiService',
function ($scope, $rootScope, $filter, $state, $mdDialog, $stateParams, $interval, apiService) {
  $rootScope.sideBar = false;
  $scope.id = $stateParams.id;
  var legajoId = $stateParams.id;
  var periodoId = $stateParams.periodoid;
  $scope.empresaId = $stateParams.empresaId;
  $scope.cargosFiltrados = {};
  $scope.legajo = {};
  $scope.legajo.licencias = [{}];
  $scope.legajo.suplencias = [{}];

  function getLegajosData (empID) {
        apiService.get('legajos/' + empID + '/data/' + periodoId).then(function (r) {
            $scope.personas = r.personas;
            $scope.subempresas = r.subempresas;
            $scope.cargos = r.cargos;
        });
        apiService.get('legajos/suplea/' + empID + '/' + periodoId).then(function (r) {
            $scope.suplea = r;
        });
    }

    $scope.filtrarCargos = function (subempresa) {
        try {
            return $filter ('filter') ($scope.cargos, {categoria_codigo: subempresa.categoria.codigo})
        } catch (e) {
            console.log('error en el filtro de cargos');
            return $scope.cargos;
        }
    }

    function init () {  
        apiService.get('liquidaciones/' + $scope.id + '/' + periodoId).then(function (r) {
            $scope.legajo = r.legajo;
            $scope.legajo.periodo_id = periodoId;
            getLegajosData($scope.legajo.subempresa.empresa._id);
        });
    }

    $scope.save = function () {
        console.log($scope.legajo);
        apiService.request(
            'legajos/' + legajoId ,
            'PUT',
            $scope.legajo
        ).then(function (response) {
            $state.go('liquidacion', {
                estado: 'legajo', 
                legajo: $scope.legajo._id, 
                periodo: $scope.legajo.periodo_id
            });
        });
    }

    $scope.selectItemChange = function (item) {
        $scope.legajo.persona = item;
    }
    
    $scope.selectSupleaChange = function (sup, item) {
        sup.legajo =  item.numero_legajo;
        sup.cuil =  item.persona.cuil;
        sup.titular = item.persona.nombre;
    }

    $scope.searchPersona = function (searchPersona) {
        return $filter('filter') ($scope.personas, {nombre: searchPersona});
    }
    
    $scope.searchSuplea = function (searchTextSuplea) {
        return $filter('filter') ($scope.suplea, { persona: { nombre: searchTextSuplea } });
    }

    $scope.agregarLicencia = function () {
        $scope.legajo.licencias.push({});
    }

    $scope.borrarLicencia = function (index) {
        $scope.legajo.licencias.splice(index, 1);
    }

    $scope.agregarSuplencia = function () {
        $scope.legajo.suplencias.push({});
      }

    $scope.borrarSuplencia = function (index) {
      $scope.legajo.suplencias.splice(index, 1);
    }

    $scope.countDays = function (sup){
        if(sup.fecha_desde && sup.fecha_hasta){
            fecha1 = moment(sup.fecha_desde);
            fecha2 = moment(sup.fecha_hasta);
            sup.dias = fecha2.diff(fecha1, 'days');
        }
    }

  init();

}]);
