login.controller('checkAccountController', ['$scope', '$cookies', '$state', 'Notification', '$rootScope', 'apiService', '$stateParams',
function ($scope, $cookies, $state, Notification, $rootScope, apiService, $stateParams) {
    $rootScope.sideBar = false;

    $scope.checkAccount = function(){
        apiService.put('usuarios/check/' + $scope.code)
            .then(function successCallback(response) {
                if(response.success){
                    $state.go('login');
                }
            });
    }

}]);
