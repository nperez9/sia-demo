login.controller('loginController', 
['$scope', '$cookies', '$state', 'Notification', '$rootScope', 'apiService', 'Auth',
	function ($scope, $cookies, $state, Notification, $rootScope, apiService, Auth) {
		$rootScope.sideBar = false;

		function init() {
			Auth.logOut();
		};

		$scope.login = function () {
			var user = {
				username: $scope.username,
				password: $scope.password
			}

			apiService.request(
				'login',
				'POST',
				user
			).then(function (data) {
				if (data) {
					apiService.post('/usuarios/login', data.user, false)
						.then(function successCallback(r) {
							$cookies.put('nombre', r.user.nombre);
							$cookies.put('email', r.user.email);
							$cookies.put('id', r.user.id);
							$cookies.put('tipo', r.user.tipo);
							$cookies.put('token', data.token);
							$rootScope.user = {};
							$rootScope.user.loggued = true;
							$rootScope.user.isAdmin = $cookies.get('tipo') == "admin" ? true : false;
							$rootScope.user.nombre = $cookies.get('nombre');

							switch (r.user.tipo) {
								case 'liquidador':
									$state.go('liquidacion');
									break;
								case 'admin':
									$state.go('empresas.listado');
									break;
							}

						}, function errorCallback(r) {
							Auth.logOut();
						});

				} else {
					Notification.error({ message: 'El email o la contraseña no son correctos.', delay: 3000 });
					Auth.logOut();
				}

			});
		}

		init();
	}]);