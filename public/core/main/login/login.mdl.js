var login = angular.module( 'sia.login', ['ui.router','ui.bootstrap', 'ngCookies' ,'ui-notification'])
.config(function config($stateProvider, $urlRouterProvider, NotificationProvider) {

	$stateProvider
	    .state('login',{
	    	url:'/login',
	    	templateUrl:'core/main/login/login.tpl.html',
	    	controller:'loginController'
		})
		.state('check-account',{
	    	url:'/check-account',
	    	templateUrl:'core/main/login/check-account.tpl.html',
	    	controller:'checkAccountController'
	    }); 
});
