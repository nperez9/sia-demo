
function BancoController($scope, $mdDialog, $http, $cookies, $state, apiService, empresa) {

  function init() {
    $scope.banco = empresa.bancos;

    apiService.request(
      '/bancos',
      'get')
      .then(function successCallback(response) {
        $scope.selectBancos = response
      });
  }

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.cancel();
  };

  $scope.save = function () {
    empresa.bancos = $scope.banco;
    $mdDialog.hide();
  };

  init();
};

