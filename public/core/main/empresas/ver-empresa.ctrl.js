empresas.controller('verEmpresasController', ['$scope','$rootScope', '$filter', '$stateParams', '$state', '$mdDialog', '$interval', 'apiService',
function($scope, $rootScope, $filter, $stateParams, $state, $mdDialog, $interval, apiService) {
  $rootScope.sideBar = true;
  var empresaID = $stateParams.id; 

  $scope.confirmModal = function(ev,item) {
    $mdDialog.show({
      controller: empresaDeleteController,
      templateUrl: 'core/main/empresas/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function (data) {
      if (data !== true)
        return false;

      apiService.request(
        'empresas/' + empresaID,
        'DELETE',
        item)
      .then(function successCallback(response) {
         $state.go('empresas.listado');
      });
    });
  };

  $scope.showAdvanced = function(ev, itm) {    
    $mdDialog.show({
        controller: verEmpresaBanco,
        templateUrl: 'core/main/empresas/empresas-modal.tpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        locals: {
          items: itm 
        },
        clickOutsideToClose: false
    })
  };


  function init () {
    apiService.get('empresas/' + empresaID) 
    .then(function successCallback (response) {
        $scope.item = response;
        console.log($scope.item.bancos)
        if ($scope.item.bancos.length > 0) {
          $scope.show = false;
        } else {
          $scope.show = true;
        }
    });
  };

  
  init();
}]);