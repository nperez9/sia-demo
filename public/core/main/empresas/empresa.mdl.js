var empresas = angular.module( 'sia.empresas', ['ui.router','ui.bootstrap','bw.paging', 'ngCookies', 'ui-notification']).config(function config($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when('/empresas', '/empresas/listado')
	$stateProvider
		.state('empresas',{
			url:'/empresas',
			abstract:true,
		})
	    .state('empresas.listado',{
	    	url:'/listado/:search',
	    	templateUrl:'core/main/empresas/empresas.tpl.html',
	    	controller:'empresasController',
	    	params: { 
    		   search: ""         
    		}
	    })
		.state('empresas.ver',{
			url:'/:id/ver',
			templateUrl:'core/main/empresas/ver-empresas.tpl.html',
			controller:'verEmpresasController'
		})
	    .state('empresas.crear',{
	    	url:'/crear',
	    	templateUrl:'core/main/empresas/crear-empresas.tpl.html',
	    	controller:'crearEmpresasController',
	    })
	    .state('empresas.editar',{
	    	url:'/:id/editar',
	    	templateUrl:'core/main/empresas/edit-empresas.tpl.html',
	    	controller:'editEmpresaController'
	    })
});
