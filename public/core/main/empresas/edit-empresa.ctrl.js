empresas.controller('editEmpresaController', ['$scope', '$filter', '$state', '$mdDialog', '$stateParams', 'apiService', '$rootScope',
function($scope, $filter, $state, $mdDialog, $stateParams, apiService, $rootScope) {
  $rootScope.sideBar = true;
  var id = $stateParams.id; 


  // $scope.guardar = function () {
  //   var data = $scope.empresa;
  //   apiService.put('/empresas/', data) 
  //   .then(function successCallback(response) {  
  //     if ($scope.deleteBanco.length > 0) {
  //       var data = {};
  //       data.array = $scope.deleteBanco;
  //       apiService.put('/banco_detalles/delete', data)
  //       .then(function successCallback(response) {

  //           if($scope.createBanco.length>0){
  //             var data = {};
  //             data.array = $scope.createBanco;
  //             data.id = $scope.empresa.id;
  //             apiService.post('/banco_detalles/', data)
  //               .then(function successCallback(response) {
  //                 console.log(response)
  //                 $state.go('empresas');
  //             }, function errorCallback(response) {
  //                 console.log(response)
  //             });   
  //           }else{
  //             $state.go('empresas');
  //           }

  //       }, function errorCallback(response) {
  //           console.log(response)
  //       });   
  //     } else {
  //         if($scope.createBanco.length>0){
  //           var data = {};
  //           data.array = $scope.createBanco;
  //           data.id = $scope.empresa.id;
  //           apiService.post('/banco_detalles/', data)
  //             .then(function successCallback(response) {
  //               console.log(response)
  //               $state.go('empresas');
  //           }, function errorCallback(response) {
  //               console.log(response)
  //           });   
  //         }else{
  //           $state.go('empresas');
  //         }
  //       }
  //   	});
  // 	}

    $scope.cancelar = function () {

        if ($scope.ver == 1) {
          $state.go('verEmpresa'
            // ,{ id:$scope.id, search: $scope.searchText}
          );
        } else {
          $state.go('empresas'
            //, { search: $scope.searchText}
          );
        }
    }

  	$scope.crearBanco = function(ev) {
      $mdDialog.show({
        controller: BancoController,
        templateUrl: 'core/main/empresas/crear-banco.tpl.html',
        parent: angular.element(document.body),
        locals: {
          empresa: $scope.empresa
        },
        clickOutsideToClose: false
      }).then(function(data) {
      });
    };

  	$scope.confirmModal = function(ev, bank) {
    	$mdDialog.show({
    	  	controller: confirmController,
    	  	templateUrl: 'core/main/empresas/confirm-modal.tpl.html',
    	  	parent: angular.element(document.body),
    	  	targetEvent: ev,
    	  	clickOutsideToClose:true
    	}).then(function(data) {
    	  if (data===true) {
    	    var index = $scope.empresa.banco_detalles.indexOf(bank);
          $scope.deleteBanco.push({id:bank.id});
    	    $scope.empresa.banco_detalles.splice(index, 1);  
    	    console.log($scope.deleteBanco);
    	  }
    	}, function(err) {
    	  console.error(err);
    	});
  	};

  	$scope.crearBanco = function(ev) {
      $mdDialog.show({
        controller: BancoController,
        templateUrl: 'core/main/empresas/crear-banco.tpl.html',
        parent: angular.element(document.body),
        locals: {
          empresa: $scope.empresa
        },
        clickOutsideToClose: false
      }).then(function(data) {
      });
    };

  $scope.guardar = function () {
    editSaveEmpresa(); 
  } 

  $scope.traerDatos = function (){
    $scope.empresa.datos_listado = {};
    $scope.empresa.datos_listado.razon_social = $scope.empresa.razon_social;
    $scope.empresa.datos_listado.domicilio = $scope.empresa.direccion;
    $scope.empresa.datos_listado.telefonos = $scope.empresa.telefono;
    $scope.empresa.datos_listado.localidad = $scope.empresa.localidad;
    $scope.empresa.datos_listado.codigo_postal = $scope.empresa.codigo_postal;
    $scope.empresa.datos_listado.cuit = $scope.empresa.cuit;
    $scope.empresa.datos_listado.registro = $scope.empresa.registro;
  }

  function getEmpresa () {
    apiService.get('empresas/' + id).then(function (empresa) {
      $scope.empresa = empresa; 
    }); 
  }

  function editSaveEmpresa () {
    console.log($scope.empresa)
    apiService.request('empresas/' + id, 'PUT', $scope.empresa)
    .then(function (r) {
      if(!r.success) return false;
      $state.go('empresas.listado'); 
    }, function (errorResponse) {
      console.log(errorResponse);
    }); 
  }

  function getProvincias () {
    apiService.get('provincias')
    .then(function successCallback(response) {
      $scope.provincias = response;
    });
  }

  function init () {
    getEmpresa();
    getProvincias();
  }

  init();
}]);