empresas.controller('empresasController', ['$scope', '$rootScope', '$filter', '$state', '$stateParams', '$mdDialog', 'apiService', '$cookies', 'Notification',
function($scope, $rootScope, $filter, $state, $stateParams, $mdDialog, apiService, $cookies, Notification) {
  $rootScope.sideBar = true;
	$scope.currentPage = 1;
	$scope.total = 1;
	$scope.pageSize = 20;
  $scope.searchText = $stateParams.search;

  window.onkeyup = function(e) {
   var key = e.keyCode ? e.keyCode : e.which;
    if (key == 13) {
        $scope.search();
    }
  }

  $scope.borrarEmpresa = function(ev,item) {
    $mdDialog.show({
      controller: empresaDeleteController,
      templateUrl: 'core/main/empresas/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function (data) {
      if (data !== true) 
        return false; 

      apiService.request(
        'empresas/' + item._id, 
        'DELETE',
        item)
      .then(function successCallback (response) {
        $scope.search();
      }); 
    });
  };
  

  $scope.search = function (reset) {
    if (reset) 
      $scope.currentPage = 1;
  
    apiService.get('empresas/?search='+$scope.searchText+'&page='+$scope.currentPage) 
    .then(function successCallback(response) {
      $scope.empresas = response.items;
      $scope.total = response.total;
    }); 
  } 

	function init () {
    
    if(!$cookies.get('token')){
      Notification.error({message: 'Debe iniciar sesión', delay: 3000});
      $state.go('login')
    }else{
      if($cookies.get('tipo') != "admin"){
        Notification.error({message: 'Acceso denegado.', delay: 3000});
        $state.go('liquidacion')
      }else{
        $scope.search(true);
      }
    }

  };

  $scope.paging = function(pagina){
    $scope.currentPage = pagina;
  	$scope.search(); 
  }

  init();

}]);

function empresaDeleteController($scope, $mdDialog) {
    $scope.hide = function() {
      $mdDialog.hide(false);
    };

    $scope.cancel = function() {
      $mdDialog.cancel(false);
    };

    $scope.answer = function(boolean) {
      $mdDialog.hide(boolean);
    };
};