empresas.controller('crearEmpresasController', 
function($scope, $filter, $http, $cookies, $rootScope, $state, $mdDialog, $interval, apiService) {
  $scope.empresa = {};

  $scope.bancos = [];

  function getBancos () {
    apiService.get('bancos')
    .then(function successCallback(response) {
      $scope.bancosId = response.items;
    }, function errorCallback(response) {
      $mdDialog.hide();
    });
  }

  function getProvincias () {
    apiService.get('provincias')
    .then(function successCallback(response) {
      $scope.provincias = response;
    });
  }

  $scope.init = function () {
    $rootScope.sideBar = true;
    getProvincias(); 

    $scope.TipoEmpresas = [
      "TipoEmpresa 1",
      "TipoEmpresa 2",
      "TipoEmpresa 3",
      "TipoEmpresa 4",
      "TipoEmpresa 5",
      "TipoEmpresa 6"
    ];
  };

  $scope.init();

  $scope.guardar = function () {
    apiService.request(
      'empresas',
      'POST',
      $scope.empresa) 
    .then(function successCallback (response) {
      $state.go('empresas.listado', {}, {reload: true});
    });
  }

  $scope.confirmModal = function(ev, bank) {
    $mdDialog.show({
      controller: confirmController,
      templateUrl: 'core/main/empresas/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function(data) {
      if(data===true){
        var index = $scope.empresa.bancos.indexOf(bank);
        $scope.empresa.bancos.splice(index, 1);  
        console.log($scope.empresa.bancos);
      }
    }, function(err) {
      console.log(err);
    });
  };

  $scope.crearBanco = function(ev) {
    $mdDialog.show({
      controller: BancoController,
      templateUrl: 'core/main/empresas/crear-banco.tpl.html',
      parent: angular.element(document.body),
      locals: {
        empresa: $scope.empresa
      },
      clickOutsideToClose: false
    }).then(function(data) {
    });
  };
  
});