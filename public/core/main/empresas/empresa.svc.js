app.service('empresaService', ['apiService', 
function (apiService) {
    this.url = 'empresas/'; 
    
    this.getEmpresa = function (id) {
        var url = this.url + id; 

        return apiService.request(
            url, 
            'GET'
        );
    }
}]); 