function confirmarController($scope, $mdDialog) {
    $scope.hide = function () {
      $mdDialog.hide(false);
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.answer = function(boolean) {
      $mdDialog.hide(boolean);
    };

    $scope.init = function(){

       $scope.bancos = [
            "banco 1",
            "banco 2",
            "banco 3",
            "banco 4",
            "banco 5",
            "banco 6"    
        ];
    };

};