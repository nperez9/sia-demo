function adherentesModalController($scope, $mdDialog, Notification, apiService, persona) {

    $scope.hide = function () {
        $mdDialog.hide();
    };

    $scope.cancel = function () {
        $mdDialog.hide();
    };

    function init() {
        $scope.adherentes = [];
        var cantidad = persona.datos_administrativos.adherentes.cantidad;
        var adherentes = persona.datos_administrativos.adherentes.adherentes;

        if (persona._id == undefined) {
            for (let i = 0; i < cantidad; i++) {
                var adherente = {}
                $scope.adherentes.push(adherente);
            }
        }else{
            for (let i = 0; i < adherentes.length; i++) {
                $scope.adherentes.push(adherentes[i]);
            }
        }
        
        if ( adherentes && cantidad > adherentes.length) {
            for (let i = 0; i < cantidad-adherentes.length; i++) {
                var adherente = {}
                $scope.adherentes.push(adherente);
            }
        }
    }

    $scope.save = function () {

        var data = {
            cantidad: $scope.adherentes.length,
            adherentes: $scope.adherentes
        }

        persona.datos_administrativos.adherentes = data;


        $mdDialog.hide();
    }

    init();
};