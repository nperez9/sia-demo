padron.controller('padronController', ['$scope', '$rootScope', '$filter', '$state', '$stateParams', '$mdDialog', 'apiService', 'personaService',
function ($scope, $rootScope, $filter, $state, $stateParams, $mdDialog, apiService, personaService) {
    $rootScope.sideBar = false;
    $scope.currentPage = 1;
    $scope.pageSize = 20;
    $scope.total = 1;
    $scope.searchText = $stateParams.search;

    window.onkeyup = function (e) {
        var key = e.keyCode ? e.keyCode : e.which;
        if (key == 13) {
            $scope.search();
        }
    }

    $scope.search = function () {
        apiService.get('personas/?page=' + $scope.currentPage + '&search=' + $scope.searchText)
        .then(function successCallback(response) {
            $scope.personas = response.items;
            $scope.total = response.total;
        });
    }

    function init() {
        $scope.search();
    }

    $scope.paging = function (pagina) {
        $scope.currentPage = pagina;
        $scope.search();
    }

    init();
}]);
