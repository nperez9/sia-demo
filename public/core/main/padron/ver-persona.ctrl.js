padron.controller('verPersonaController', ['$scope', '$rootScope', '$state', '$mdDialog', '$stateParams', 'apiService', 'personaService',
function($scope, $rootScope, $state, $mdDialog, $stateParams, apiService, personaService) {
  $rootScope.sideBar = false;
  $scope.searchText = $stateParams.search;
  var id = $stateParams.id;
  var idempresa = $stateParams.empresa;

  $scope.confirmModal = function (ev, item) {
    $mdDialog.show({
        controller: conceptoDeleteController,
        templateUrl: 'core/main/padron/confirm-modal.tpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
    }).then(function(data) {
        var periodo = localStorage.getItem('periodo');
        personaService.deletePersona(item._id, periodo)
        .then(function successCallback(response) {
            $state.go('liquidacion', {estado: 'personas'}, {reload: true});
        }, function errorCallback(response) {
        });
    });
  };

  function getEmpresa () {
      apiService.request(
          'empresas/' + idempresa,
          'GET'
      ).then(function (empresa) {
          $scope.empresa = empresa;
      });
  }

  function getConcepto () {
    personaService.scopePersona($scope, id);
  }

  function init () {
    getConcepto();
    getEmpresa();
  };

  init();
}]);
