padron.controller('crearPersonaController', ['$scope', '$rootScope', '$stateParams', '$state', '$mdDialog', 'apiService', 'Notification', 'personaService',
    function ($scope, $rootScope, $stateParams, $state, $mdDialog, apiService, Notification, personaService) {
        $rootScope.sideBar = false;
        $scope.title = 'Ingresar Persona';
        var empresaid = $stateParams.empresaid;
        var periodoId = $stateParams.periodoid;
        var empresapersona = {};
        $scope.persona = {};
        $scope.dniEditable = true;
        $scope.persona.datos_administrativos = {};
        $scope.persona.datos_administrativos.sv = true;
        $scope.persona.datos_administrativos.agente_retencion = true;

        function getEmpresa() {
            apiService.request(
                'empresas/' + empresaid,
                'GET'
            ).then(function (empresa) {
                $scope.empresa = empresa;
            });
        }

        function getBancos() {
            apiService.request(
                'bancos/',
                'GET'
            ).then(function (bancos) {
                $scope.bancos = bancos;
            });
        }

        function getObraSociales() {
            apiService.request(
                'obrasSociales/',
                'GET'
            ).then(function (obras_sociales) {
                $scope.obras_sociales = obras_sociales;
            });
        }

        function getActividades() {
            apiService.request(
                'actividades/',
                'GET'
            ).then(function (actividades) {
                $scope.c_actividades = actividades;
            });
        }

        function getModalidades() {
            apiService.request(
                'modalidades/',
                'GET'
            ).then(function (modalidades) {
                $scope.modalidades = modalidades;
            });
        }

        function getCondiciones() {
            apiService.request(
                'condiciones/',
                'GET'
            ).then(function (condiciones) {
                $scope.condiciones = condiciones;
            });
        }

        function getSituaciones() {
            apiService.request(
                'situaciones/',
                'GET'
            ).then(function (situaciones) {
                $scope.situaciones = situaciones;
            });
        }

        function getZonasAfip() {
            apiService.request(
                'zonas/',
                'GET'
            ).then(function (zonas) {
                $scope.zonas = zonas;
            });
        }

        function getProvincias() {
            apiService.get('provincias')
                .then(function successCallback(response) {
                    $scope.provincias = response;
                });
        }

        $scope.ingresarDni = function (item) {
            if (item.length == 11) {
                var dni = parseInt(item.substr(2, 8));
                $scope.persona.dni = dni;
            }
            else if (item.length == 13) {
                var dni = parseInt(item.substr(3, 8));
                $scope.persona.dni = dni;
            }
        };

        $scope.save = function () {
            $scope.persona.empresa = $scope.empresa;
            $scope.persona.periodo_id = periodoId;
            personaService.insertPersona($scope.persona)
                .then(function (response) {
                    if (response.success)
                        $state.go('liquidacion');
                });
        };

        $scope.goBack = function () {
            if (empresaid == '0')
                $state.go('padron.listado');
            else
                $state.go('liquidacion', { estado: 'padron' });
        }

        function init() {
            getEmpresa();
            getBancos();
            getObraSociales();
            getActividades();
            getModalidades();
            getCondiciones();
            getSituaciones();
            getZonasAfip();
            getProvincias();
        }

        $scope.adherentesModal = function () {
            $mdDialog.show({
                controller: adherentesModalController,
                templateUrl: 'core/main/padron/adherentes-modal.tpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: {
                    persona: $scope.persona
                }
            }).then(function (r) {
            });
        }

        init();
    }]);
