padron.controller('editPersonaController', ['$scope', '$rootScope', '$state', '$mdDialog', '$stateParams', 'apiService', 'personaService',
    function ($scope, $rootScope, $state, $mdDialog, $stateParams, apiService, personaService) {
        $scope.title = 'Editar Persona';
        $scope.ver = $stateParams.ver;
        var id = $stateParams.id;
        var empresaid = $stateParams.empresa;
        var ver = $stateParams.ver;
        $scope.dniEditable = false;

        $scope.ingresarDni = function (item) {
            if (item.length == 11) {
                var dni = parseInt(item.substr(2, 8));
                $scope.persona.dni = dni;
            }
            else if (item.length == 13) {
                var dni = parseInt(item.substr(3, 8));
                $scope.persona.dni = dni;
            }
        };

        function getConcepto() {
            personaService.scopePersona($scope, id);
        }

        function getEmpresa() {
            apiService.request(
                'empresas/' + empresaid,
                'GET'
            ).then(function (empresa) {
                $scope.empresa = empresa;
            });
        }

        function getBancos() {
            apiService.request(
                'bancos/',
                'GET'
            ).then(function (bancos) {
                $scope.bancos = bancos;
            });
        }

        function getObraSociales() {
            apiService.request(
                'obrasSociales/',
                'GET'
            ).then(function (obras_sociales) {
                $scope.obras_sociales = obras_sociales;
            });
        }

        function getActividades() {
            apiService.request(
                'actividades/',
                'GET'
            ).then(function (actividades) {
                $scope.c_actividades = actividades;
            });
        }

        function getModalidades() {
            apiService.request(
                'modalidades/',
                'GET'
            ).then(function (modalidades) {
                $scope.modalidades = modalidades;
            });
        }

        function getCondiciones() {
            apiService.request(
                'condiciones/',
                'GET'
            ).then(function (condiciones) {
                $scope.condiciones = condiciones;
            });
        }

        function getSituaciones() {
            apiService.request(
                'situaciones/',
                'GET'
            ).then(function (situaciones) {
                $scope.situaciones = situaciones;
            });
        }

        function getZonasAfip() {
            apiService.request(
                'zonas/',
                'GET'
            ).then(function (zonas) {
                $scope.zonas = zonas;
            });
        }

        function getProvincias() {
            apiService.get('provincias')
                .then(function successCallback(response) {
                    console.log(response)
                    $scope.provincias = response;
                });
        }

        function init() {
            getConcepto();
            getEmpresa();
            getBancos();
            getObraSociales();
            getActividades();
            getModalidades();
            getCondiciones();
            getSituaciones();
            getZonasAfip();
            getProvincias();
        }

        $scope.cancelar = function () {
            if ($scope.ver == 1) {
                $state.go('padron.ver', { id: id });
            } else {
                $state.go('padron.listado');
            }
        }

        $scope.save = function () {
            // HORROROSA PRACTICA
            var periodo = localStorage.getItem('periodo');

            if ($scope.persona.datos_administrativos.adherentes.cantidad == 0) {
                $scope.persona.datos_administrativos.adherentes = {
                    cantidad: 0,
                    adherentes: []
                }
            }

            personaService.editPersona($scope.persona, id, periodo)
                .then(function successCallback(response) {
                    // Buena Practica
                    if (response.success)
                        $state.go('liquidacion', { estado: 'personas' });
                });
        }

        $scope.goBack = function () {
            $state.go('liquidacion', { estado: 'personas' });
        }

        $scope.adherentesModal = function () {
            $mdDialog.show({
                controller: adherentesModalController,
                templateUrl: 'core/main/padron/adherentes-modal.tpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: {
                    persona: $scope.persona
                }
            }).then(function (r) {

            });
        }

        init();
    }]);
