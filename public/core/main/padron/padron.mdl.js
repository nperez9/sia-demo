var padron = angular.module( 'sia.padron', ['ui.router','ui.bootstrap','bw.paging', 'ngCookies', 'ui-notification']).config(function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when('/padron', '/padron/listado');

        $stateProvider
            .state('padron',{
                url:'/padron',
                abstract:true,
            })
            .state('padron.listado',{
                url:'/listado/:search',
                templateUrl:'core/main/padron/padron.tpl.html',
                controller:'padronController',
                params: {
                   search: ""
                }
            })
            .state('padron.crear',{
                url:'/crear/:empresaid/:periodoid',
                templateUrl:'core/main/padron/form-persona.tpl.html',
                controller:'crearPersonaController'
            })
            .state('padron.ver',{
                url:'/:id/:empresa/ver',
                templateUrl:'core/main/padron/ver-persona.tpl.html',
                controller:'verPersonaController'
            })
            .state('padron.editar',{
                url:'editar/:id/:ver/:empresa/:search',
                templateUrl:'core/main/padron/form-persona.tpl.html',
                controller:'editPersonaController',
                params: {
                   search: ""
                }
            })
    });
