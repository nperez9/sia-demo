padron.service('periodoService', ['apiService', 
function (apiService) {
    this.url = 'periodos/'; 
    this.padron = {}; 
    
    function errorCallback (r) {
        console.log(r); 
    }
    
    this.scopePeriodo = function ($scope, id) {
        var url = this.url + id; 
        return apiService.request(
            url, 
            'GET'
        ).then(function (response) {
            $scope.periodo = response;
            console.log($scope.perido);
        }, errorCallback);
    }

    this.insertPeriodo = function (periodo) {
        return apiService.request(
            this.url, 
            'POST', 
            periodo
        ); 
    }

    this.editPeriodo = function (periodo, id) {
        return apiService.request(
            this.url + id, 
            'PUT', 
            periodo
        ); 
    }

    this.deletePeriodo = function (id) {
        return apiService.request(
            this.url + id, 
            'DELETE',
            null, 
            false
        ); 
    }
}]); 