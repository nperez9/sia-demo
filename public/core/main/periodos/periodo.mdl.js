var periodos = angular.module( 'sia.periodos', ['ui.router','ui.bootstrap','bw.paging', 'ngCookies', 'ui-notification']).config(function config($stateProvider, $urlRouterProvider) {
    
        $urlRouterProvider.when('/periodos', '/periodos/listado');
    
        $stateProvider
            .state('periodos',{
                url:'/periodos',
                abstract:true
            }) 
            .state('periodos.crear',{ 
                url:'/crear/:empresaid', 
                templateUrl:'core/main/periodos/form-periodos.tpl.html',
                controller:'crearPeriodoController'
            })
            .state('periodos.ver',{
                url:'/:id/ver',
                templateUrl:'core/main/periodos/ver-periodo.tpl.html',
                controller:'verPeriodoController'
            })
            .state('periodos.editar',{
                url:'editar/:id/:ver/',
                templateUrl:'core/main/periodos/form-periodos.tpl.html',
                controller:'editPeriodoController'
            })
    });