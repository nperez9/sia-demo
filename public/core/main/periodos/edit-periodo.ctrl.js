padron.controller('editPeriodoController', ['$scope', '$rootScope', '$state', '$mdDialog', '$stateParams', 'apiService', 'periodoService',
function ($scope, $rootScope, $state, $mdDialog, $stateParams, apiService, periodoService) {
    $scope.title = 'Editar Periodo';
    $scope.ver = $stateParams.ver;
    var id = $stateParams.id;

    function getPeriodo () {
        periodoService.scopePeriodo($scope, id)
        .then(function (resul) {
            apiService.request(
                'empresas/' + $scope.periodo.empresa, 
                'GET'
            ).then(function (empresa) {
                $scope.empresa = empresa;
            });
        });
    }

    function init () {
        getPeriodo();
    }

    $scope.cancelar = function () {
        if ($scope.ver == 1) {
            $state.go('periodos.ver', { id: id});
        } else {
            $state.go('liquidacion');
        }
    }

    $scope.save = function () {
        periodoService.editPeriodo($scope.periodo, $scope.periodo._id)
        .then(function successCallback (response) {
            if (response.success)
                $state.go('liquidacion', {estado: 'periodo'});
        });
    }

    init();
}]);
