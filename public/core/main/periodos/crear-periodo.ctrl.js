padron.controller('crearPeriodoController', ['$scope', '$rootScope', '$state', '$mdDialog', '$stateParams', 'apiService', 'periodoService',
function ($scope, $rootScope, $state, $mdDialog, $stateParams, apiService, periodoService) {
    $scope.title = 'Crear Periodo';
    var empresaid = $stateParams.empresaid;
    $scope.periodo = {};
    $scope.periodo.sac = false;

    function getEmpresa () {
        apiService.request(
            'empresas/' + empresaid, 
            'GET'
        ).then(function (empresa) {
            $scope.empresa = empresa;
            $scope.periodo.empresa = empresa._id;
        });
    }

    function init () {
        getEmpresa();
    }

    $scope.cancelar = function () {
        if ($scope.ver == 1) {
            $state.go('padron.ver', { id: id});
        } else {
            $state.go('padron.listado');
        }
    }


    $scope.onChangeDate = function (date) {
        date = moment.utc(date).format('MM-DD-YYYY');
        return date;
    }

    $scope.cambioMes = function (mes) {
        $scope.periodo.fecha_inicio = moment.utc( mes +'/01/' + $scope.periodo.anio).format('MM-DD-YYYY');
        $scope.periodo.fecha_fin = moment.utc(mes + '/01/' + $scope.periodo.anio).endOf('month').format('MM-DD-YYYY');
    }

    $scope.cambioSemestre = function (periodo) {
        if (periodo == 1) {
            $scope.periodo.fecha_inicio = moment.utc('01/01/' + $scope.periodo.anio).format('MM-DD-YYYY');
            $scope.periodo.fecha_fin = moment.utc('06/30/' + $scope.periodo.anio).format('MM-DD-YYYY');
        } else if (periodo == 2) {
            $scope.periodo.fecha_inicio = moment.utc('07/01/' + $scope.periodo.anio).format('MM-DD-YYYY');
            $scope.periodo.fecha_fin = moment.utc('12/31/' + $scope.periodo.anio).format('MM-DD-YYYY');
        }
    }

    $scope.save = function () {
        periodoService.insertPeriodo($scope.periodo)
        .then(function successCallback(response) {
            if (response.success)
                $state.go('liquidacion', {estado: 'periodo'});
        });
    }

    init();
}]);
