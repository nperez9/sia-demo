periodos.controller('verPeriodoController', ['$scope', '$rootScope', '$stateParams', '$state', '$mdDialog', 'periodoSerice',
function ($scope, $rootScope, $stateParams, $state, $mdDialog, peridoService) {
  $rootScope.sideBar = false;
  $scope.subempresa = {};
  var id = $stateParams.id;

  $scope.confirmModal = function(ev, item) {
    $mdDialog.show({
      controller: confirmarController,
      templateUrl: 'core/main/sub_empresas/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function (data) {
      if (data) {
        apiService.deletePeriodo(item._id)
        .then(function successCallback (response) {
            $state.go('liquidacion');
        });
      }
    });
  };

  function init () {
    peridoService.scopePeriodo($scope, id);
  }

  init();
}]);
