valor.controller('editValorController', 
function($scope, $rootScope, $filter, $http, $cookies, $stateParams, $state, $mdDialog, $interval, apiService) {
  $rootScope.sideBar = true;
  var id = $stateParams.id;
  
  $scope.showAdvanced = function(ev) {
    $mdDialog.show({
      controller: confirmarController,
      templateUrl: 'core/main/valor/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    });
  };

  function init () {
    apiService.get('valores/' + id)
    .then(function successCallback(response) {
      $scope.valor = response;
    });
  };

  
  $scope.cancelar = function () {
    $state.go('valor.listado');
  }


  $scope.save = function () { 
    apiService.request(
      'valores/' + $scope.valor._id,
      'PUT', 
      $scope.valor,
      false
    ).then(function (response) {
      if(response.success)
        $state.go('valor.listado');
    });
  };

  init();
});