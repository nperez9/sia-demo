valor.controller('crearValorController', ['$scope', '$rootScope', '$state', 'apiService','$mdDialog',
function($scope, $rootScope, $state, apiService, $mdDialog) {
  $rootScope.sideBar = true;
  $scope.valor = {};

  $scope.save = function(){

    apiService.get('valores/check/'+$scope.valor.codigo)
    .then(function successCallback (response) {

      if(response != null){
        $mdDialog.show({
          controller: valorModalController,
          templateUrl: 'core/main/liquidacion/modals/valor-modal.tpl.html',
          parent: angular.element(document.body),
          clickOutsideToClose: true,
          locals: {
            valorFind : response,
            valorNew : $scope.valor
          },
        }).then(function (r) {

        });
      }
    });
  };

}]);

