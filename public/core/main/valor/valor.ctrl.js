valor.controller('valorController', ['$scope', '$rootScope', '$filter', '$state', '$stateParams', '$mdDialog', 'apiService', '$cookies', 'Notification',
function($scope, $rootScope, $filter, $state, $stateParams, $mdDialog, apiService, $cookies, Notification) {
  $rootScope.sideBar = true;
  $scope.formatMyDate = $rootScope.formatMyDate;
	$scope.currentPage = 1;
	$scope.pageSize = 20;
	$scope.total = 1;
  $scope.searchText = $stateParams.search;

  window.onkeyup = function(e) {
    var key = e.keyCode ? e.keyCode : e.which;
    if (key == 13) {
          $scope.search();
    }
  }

  $scope.confirmModal = function(ev, item) {
    $mdDialog.show({
      controller: valorDeleteController,
      templateUrl: 'core/main/valor/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function(data) {
      if (data) {
        apiService.request(
          'valores/' + item._id,
          'DELETE',
          null, 
          false
        ).then(function successCallback (response) {
            $scope.search();
        });
      }
    });
  };

  function init () {

    if(!$cookies.get('token')){
      Notification.error({message: 'Debe iniciar sesión', delay: 3000});
      $state.go('login')
    }else{
      if($cookies.get('tipo') != "admin"){
        Notification.error({message: 'Acceso denegado.', delay: 3000});
        $state.go('liquidacion')
      }else{
        $scope.search(true);
      }
    }

  }

  $scope.search = function () {
    apiService.get('valores/?search='+$scope.searchText+'&page='+$scope.currentPage)
      .then(function successCallback(response) {
        console.log(response.items);
        for (const item in response.items) {
          item.fecha_inicio = "algo";
        }
        $scope.items = response.items;
        $scope.total = response.total;
      });
  }
    
  $scope.paging = function (pagina) {
    $scope.currentPage = pagina;
    $scope.search(); 
  }
  
  init();

}]);

function valorDeleteController ($scope, $mdDialog) {
    $scope.hide = function() {
      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.answer = function(boolean) {
      $mdDialog.hide(boolean);
    };

    $scope.init = function(){
    };
};