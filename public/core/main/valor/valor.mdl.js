var valor = angular.module( 'sia.valor', ['ui.router','ui.bootstrap','bw.paging', 'ngCookies', 'ui-notification']).config(function config($stateProvider, $urlRouterProvider) {
	
	$urlRouterProvider.when('/valor', '/valor/listado');

	$stateProvider
	    .state('valor',{
	    	url:'/valor',
	    	abstract:true,
	    }) 
	    .state('valor.listado',{
	    	url:'/listado/:search',
	    	templateUrl:'core/main/valor/valor.tpl.html',
	    	controller:'valorController',
	    	params: { 
    		   search: ""         
    		}
	    })
	    .state('valor.crear',{
	    	url:'/crear',
	    	templateUrl:'core/main/valor/form-valor.tpl.html',
	    	controller:'crearValorController'
	    })
	    .state('valor.edit',{
	    	url:'/edit/:id',
	    	templateUrl:'core/main/valor/form-valor.tpl.html',
	    	controller:'editValorController',
	    	params: { 
    		   search: ""         
    		}
	    })
	    .state('valor.ver',{
	    	url:'/ver/:id',
	    	templateUrl:'core/main/valor/ver-valor.tpl.html',
	    	controller:'verValorController',
	    	params: { 
    		   search: ""         
    		}
	    })
});
