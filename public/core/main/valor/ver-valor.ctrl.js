valor.controller('verValorController', ['$rootScope', '$scope', '$stateParams', '$state', '$mdDialog', 'apiService',
function($rootScope, $scope, $stateParams, $state, $mdDialog, apiService) {
  var id = $stateParams.id;

  $scope.confirmModal = function(ev, item) {
      $mdDialog.show({
        controller: valorDeleteController,
        templateUrl: 'core/main/valor/confirm-modal.tpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true
      }).then(function(data) {
        if (data) {
          apiService.request(
            'valores/' + item._id,
            'DELETE',
            null, 
            false
          ).then(function successCallback (response) {
              $scope.search();
          });
        }
      });
    };

  $scope.showAdvanced = function(ev) {
    $mdDialog.show({
      controller: confirmarController,
      templateUrl: 'core/main/valor/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
  };

  function init () {  
    $rootScope.sideBar = true;
    apiService.get('valores/' + id)
    .then(function successCallback(response) {
      $scope.item = response;
    });
  };

  init();
}]);