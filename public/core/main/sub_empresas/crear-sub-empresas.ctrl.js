subEmpresas.controller('crearSubEmpresasController', ['$scope', '$rootScope', '$stateParams', '$state', '$mdDialog', 'apiService',
function ($scope, $rootScope, $stateParams, $state, $mdDialog, apiService) {  
  $rootScope.sideBar = false;
  $scope.subempresa = {};
  var empresaid = $stateParams.empresaid;
  var periodoid = $stateParams.periodoid;


  $scope.title = 'Crear';

  $scope.showAdvanced = function (ev) {
    $mdDialog.show({
      controller: confirmarController,
      templateUrl: 'core/main/sub_empresas/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
  };

  $scope.save = function () {
    apiService.post('subempresas/',$scope.subempresa)
    .then(function (response) {
        $state.go('liquidacion');
    });
  }

  $scope.goBack = function () {
    if (empresaid == '0')
      $state.go('subempresa.listado');
    else
      $state.go('liquidacion', { estado: 'subempresa' });
  }

  function getEmpresa () {
    apiService.get('empresas/' + empresaid).then(function (empresa) {
      $scope.subempresa.empresa = empresa;
    });
  }

  function init () {
    $scope.subempresa.periodo_id = periodoid;
    getEmpresa();
    apiService.get('categorias/all')
    .then(function successCallback(response) {
        $scope.categorias = response;
    });
  };

  init();
}]);

