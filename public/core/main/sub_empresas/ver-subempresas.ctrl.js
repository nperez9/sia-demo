subEmpresas.controller('verSubEmpresasController', ['$scope', '$rootScope', '$stateParams', '$state', '$mdDialog', 'apiService',
function ($scope, $rootScope, $stateParams, $state, $mdDialog, apiService) {
  $scope.subempresa = {};
  $scope.id = $stateParams.id; 
  
  $scope.confirmModal = function(ev, item) {
    $mdDialog.show({
      controller: confirmarController,
      templateUrl: 'core/main/sub_empresas/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function (data) {
      if (data) {
        apiService.request(
          'subempresas/' + item._id,
          'DELETE',
          null, 
          false
        ).then(function successCallback (response) {
            $state.go('liquidacion');
        }, function errorCallback(response) { 
            console.log(response);
        });
      }
    });
  };

  $scope.goBack = function () {
			$state.go('liquidacion', {estado: 'subempresa'});
  }

  function init () {
    apiService.get('subempresas/' + $scope.id)
    .then(function (response) {
      $scope.subempresa = response;
    });
  }

  init();
}]);

