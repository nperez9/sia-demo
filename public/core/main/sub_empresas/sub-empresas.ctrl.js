subEmpresas.controller('subEmpresasController', ['$scope', '$rootScope', '$state', 'apiService',
function($scope, $rootScope, $state, apiService) {
	$scope.currentPage = 1;
	$scope.pageSize = 20;
	$scope.total = 1;
  	$scope.searchText='';
  
	function init () {
  		$scope.search();
    }
    
    $scope.search = function(){
      	apiService.get('subempresas/?page='+$scope.currentPage+'&pattern='+$scope.searchText) 
      	.then(function successCallback(response) {
          	$scope.items = response.items;
          	$scope.total = response.count[0].count;
      	});
  	}

  	init()

}]);