var subEmpresas = angular.module( 'sia.subEmpresas', ['ui.router','ui.bootstrap','bw.paging', 'ngCookies', 'ui-notification']).config(function config($stateProvider, $urlRouterProvider) {

	$stateProvider
	    .state('subEmpresas',{
	    	url:'/subempresas',
	    	templateUrl:'core/main/sub_empresas/sub-empresas.tpl.html',
	    	controller:'subEmpresasController',
	    })
	    .state('crearSubEmpresas',{
	    	url:'/subempresas/:empresaid/crear/:periodoid',
	    	templateUrl:'core/main/sub_empresas/sub-empresas-form.tpl.html',
	    	controller:'crearSubEmpresasController',
	    })
	    .state('verSubEmpresas',{
	    	url:'/subempresas/:id/:empresaid/ver',
	    	templateUrl:'core/main/sub_empresas/ver-subempresas.tpl.html',
	    	controller:'verSubEmpresasController',
	    })
	    .state('editarSubEmpresas',{
	    	url:'/subempresas/:id/:empresaid/editar/:ver',
	    	templateUrl:'core/main/sub_empresas/sub-empresas-form.tpl.html',
	    	controller:'editarSubEmpresasController',
			})
			.state('clonarSubempresa', {
				url: '/subempresas/clonar/:subempresaid/:periodoid',
				templateUrl: 'core/main/sub_empresas/clonar-subempresa.tpl.html',
				controller: 'clonarSubempresaController'
			})
});
