subEmpresas.controller('editarSubEmpresasController', ['$scope', '$rootScope', '$filter', '$stateParams', '$state', '$mdDialog', 'apiService',
function ($scope, $rootScope, $filter, $stateParams, $state, $mdDialog, apiService) {

    $scope.subempresa = {};
    $scope.id = $stateParams.id;
    var empresaid = $stateParams.empresaid;
    $scope.title = 'Editar';

    $scope.showAdvanced = function (ev) {
        $mdDialog.show({
            controller: confirmarController,
            templateUrl: 'core/main/sub_empresas/confirm-modal.tpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        });
    };

    function getSubempresa () {
        apiService.get('subempresas/' + $scope.id)
        .then(function successCallback(response) {
            $scope.subempresa = response;
        }, function errorCallback(response) {
            console.log(response);
        });
    }

    $scope.save = function () {
        var periodo = localStorage.getItem('periodo');

        apiService.request(
            'subempresas/' + $scope.subempresa._id + '/' + periodo,
            'PUT',
            $scope.subempresa)
            .then(function successCallback(response) {
                $state.go('liquidacion', { estado: 'subempresa' });
            }, function errorCallback(response) {
                console.log(response);
            });
    }

    function init() {
        getSubempresa();
        apiService.get('categorias/all')
            .then(function successCallback(response) {
                $scope.categorias = response;
            }, function errorCallback(response) {
                console.log(response);
            });
    };

    $scope.goBack = function () {
        if (ver == '1')
            $state.go('verSubEmpresas', { id: $scope.id, empresaid: empresaid })
        else
            $state.go('liquidacion', { estado: 'subempresa' });
    }

    init();
}]);

