cargo.controller('editCargoController', 
function($scope, $rootScope, $filter, $http, $cookies, $stateParams, $state, $mdDialog, $interval, apiService, cargoService, Notification) {
  $rootScope.sideBar = true;
  var conceptos = [];
  var id = $stateParams.id;
  $scope.paginaConceptos = 0;
  $scope.pageSize = 10;
  $scope.cargo = {};
  $scope.cargo.conceptos_template = [];
  
  $scope.showAdvanced = function(ev) {
    $mdDialog.show({
      controller: confirmarController,
      templateUrl: 'core/main/cargo/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: true
    }); 
  };

  $scope.save = function () {
    cargoService.editCargo($scope.cargo, $scope.cargo._id)
    .then(function (r) {
      $state.go('cargo.listado'); 
    }); 
  }

  $scope.cancelar = function(){
    console.log($scope.ver);
      if($scope.ver == 1){
        $state.go('cargo.ver', { id:$scope.id, search: $scope.searchText});
      }else{
        $state.go('cargo.listado', { search: $scope.searchText});
      }
  }

  function getCategorias () {
    apiService.get('categorias/').then(function (response) {
      $scope.categorias = response; 
    });
  }


  function init () {      
    cargoService.scopeCargo($scope, id);
    getCategorias();
    getConceptos();
    $scope.cambioCategoria($scope.cargo.categoria_codigo);
  };

  $scope.cambioCategoria = function (cat) {
    try {
      $scope.conceptos = $filter ('filter') (conceptos, {codigo: cat});
    } catch (e) {
        console.log('Submperesa no definida'); 
        $scope.conceptos =  $filter ('filter') (conceptos, {codigo: ''})
    }
  }

  function getConceptos () {
    apiService.get('conceptos/all').then(function (response) {
      conceptos = response;
      $scope.conceptos = response;
    });
  }

  $scope.eliminarConcepto = function (indice) {
    $scope.cargo.conceptos_template.splice(indice, 1);
  }

  $scope.conceptoSeleccionado = function (concepto) {
    var misConceptos =  $filter ('filter') ($scope.cargo.conceptos_template, {_id: concepto._id});
    if (misConceptos.length > 0) {
      Notification.warning('Concepto repetido');
      return false;
    }
    $scope.cargo.conceptos_template.push(concepto);
  }

  init();

});

