cargo.controller('verCargoController', 
function($rootScope, $scope, $filter, $stateParams, $http, $cookies, $state, $mdDialog, $interval, apiService, cargoService) {
  $rootScope.sideBar = true;
  $scope.searchText = $stateParams.search;
  $scope.ver = $stateParams.ver;
  
  var id = $stateParams.id;
  
  $scope.showAdvanced = function(ev) {
    $mdDialog.show({
      controller: confirmarController,
      templateUrl: 'core/main/cargo/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
  };

  $scope.confirmModal = function(ev, item) {
    $mdDialog.show({
      controller: cargoDeleteController,
      templateUrl: 'core/main/cargo/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function(data) {
      if (data !== true) 
        return false; 

      apiService.request(
        'cargos/' + item._id, 
        'DELETE',
        item)
      .then(function successCallback (response) {
        $state.go('cargo.listado');
      }); 
    });
  };

  $scope.init = function () {
    cargoService.scopeCargo($scope, id); 
  };

  function getCargo () {
    apiService.get('cargos/' + id).then(function (r) {
      $scope.cargo = r; 
    });
  }
  
  $scope.init();

});