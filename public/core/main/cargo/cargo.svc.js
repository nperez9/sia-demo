cargo.service('cargoService', ['apiService', 
function (apiService) {
    this.url = 'cargos/'; 
    
    function errorCallback (r) {
        console.log(r); 
    }
    
    this.scopeCargo = function ($scope, id) {
        var url = this.url + id; 
        return apiService.request(
            url, 
            'GET'
        ).then(function (response) {
            $scope.cargo = response; 
        }, errorCallback);
    }

    this.insertCargo = function (cargo) {
        return apiService.request(
            this.url, 
            'POST', 
            cargo
        ); 
    }

    this.editCargo = function (cargo, id) {
        return apiService.request(
            this.url + id, 
            'PUT', 
            cargo
        ); 
    }
}]); 