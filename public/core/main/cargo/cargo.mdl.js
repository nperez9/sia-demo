var cargo = angular.module( 'sia.cargo', ['ui.router','ui.bootstrap','bw.paging', 'ngCookies', 'ui-notification']).config(function config($stateProvider, $urlRouterProvider) {
	
	$urlRouterProvider.when('/cargo', '/cargo/listado');

	$stateProvider
	    .state('cargo',{
	    	url:'/cargo',
	    	abstract:true,
	    }) 
	    .state('cargo.listado',{
	    	url:'/listado/:search',
	    	templateUrl:'core/main/cargo/cargo.tpl.html',
	    	controller:'cargoController',
	    	params: { 
    		   search: ""         
    		}
	    })
	    .state('cargo.crear',{
	    	url:'/crear',
	    	templateUrl:'core/main/cargo/form-cargo.tpl.html',
	    	controller:'crearCargoController',

	    })
	    .state('cargo.edit',{
	    	url:'/edit/:id/:search',
	    	templateUrl:'core/main/cargo/form-cargo.tpl.html',
	    	controller:'editCargoController',
	    	params: { 
    		   search: ""         
    		}
	    })
	    .state('cargo.ver',{
	    	url:'/ver/:id/:search',
	    	templateUrl:'core/main/cargo/ver-cargo.tpl.html',
	    	controller:'verCargoController',
	    	params: { 
    		   search: ""         
    		}
	    })
});
