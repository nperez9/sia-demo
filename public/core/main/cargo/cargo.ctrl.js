cargo.controller('cargoController', ['$scope', '$rootScope', '$state', '$stateParams', '$mdDialog', 'apiService', '$cookies', 'Notification',
function($scope, $rootScope, $state, $stateParams, $mdDialog, apiService, $cookies, Notification) {
  $scope.currentPage = 1;
  $scope.pageSize = 20;
  $scope.total = 1;
  $scope.searchText = $stateParams.search;
  
  window.onkeyup = function (e) {
    var key = e.keyCode ? e.keyCode : e.which;

    if (key == 13) {
        $scope.search();
    }
  }

  $scope.confirmModal = function(ev, item) {
    $mdDialog.show({
      controller: cargoDeleteController,
      templateUrl: 'core/main/cargo/confirm-modal.tpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    }).then(function (data) {
      if (data !== true) 
        return false; 

      apiService.request(
        'cargos/' + item._id, 
        'DELETE',
        item)
      .then(function successCallback (response) {
        $scope.search();
      }); 
    });
  };

  $scope.search = function(reset){
   if(reset)$scope.currentPage = 1;
    //  /search/?nombre='+$scope.searchText+'&page='+$scope.currentPage
    apiService.get('cargos/?search='+$scope.searchText+'&page='+$scope.currentPage) 
    .then(function successCallback (response) {
      if (response) {
        $scope.cargos = response.items;
        $scope.total = response.total;
      }
    }); 
  } 
 
  $scope.init = function(){
    if(!$cookies.get('token')){
      Notification.error({message: 'Debe iniciar sesión', delay: 3000});
      $state.go('login')
    }else{
      if($cookies.get('tipo') != "admin"){
        Notification.error({message: 'Acceso denegado.', delay: 3000});
        $state.go('liquidacion')
      }else{
        $scope.search(true);
      }
    }
  };

  $scope.paging = function (pagina){
    $scope.currentPage = pagina;
    $scope.search(); 
  }

  $scope.init();

}]);

function cargoDeleteController($scope, $mdDialog) {
    $scope.hide = function() {
      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.answer = function(boolean) {
      $mdDialog.hide(boolean);
    };

    $scope.init = function(){
    };

};