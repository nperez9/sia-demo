app.factory('downloadService', ['$http', '$window', 'Notification',
    function ($http, $window, Notification) {
        return {
            downloadFile: downloadFile
        };

        function downloadFile(url, data, fileName) {
            
            $http({
                url: 'app/' + url,
                method: 'POST',
                data: data,
                responseType: 'blob'
            })
                .then(function (datos, status, headers, config) {
                    var filename = fileName;
                    $window.saveAs(datos.data, filename); // This is from FileSaver.js
                    return 1;
                })
                .catch(function (datos) {
                    if (datos.status == "504") {
                        Notification.error("El reporte no posee registros");
                    } else if (datos.status == "500") {
                        Notification.error("Datos indefinidos en el reporte");
                    }
                    return 2;
                });
    }

  }]);