app.service("Auth", ['$cookies', '$timeout', '$state', '$cookies', '$rootScope',
    function ($cookies, $timeout, $state, $cookies, $rootScope) {

        this.logOut = function () {
            $rootScope.user = {};

            $cookies.remove('email');
            $cookies.remove('id');
            $cookies.remove('nombre');
            $cookies.remove('tipo');
            $cookies.remove('token');
            
            $rootScope.user.loggued = false;
            $state.go('login');
        };

        // this.logIn = function (logData) {
        //     $cookies.put('nombre', r.nombre);
        //     $cookies.put('email', r.email);
        //     $cookies.put('id', r.id);
        //     $cookies.put('tipo', r.tipo);
        //     $cookies.put('token', r.token);  
            
        //     $state.go('home'); 
        // }

        this.getUserData = function (data) {
            var userData = $cookies.get(data);
            if (userData != null && typeof (userData) != 'undefined')
                return userData;
            else
                return null;
        }

        this.getToken = function () {
            var token = $cookies.get('token');

            if (token != null && typeof (token) != 'undefined')
                return token;
            else {
                this.logOut();
                return null;
            }
        }

}]); 