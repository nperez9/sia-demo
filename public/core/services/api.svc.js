Array.prototype.remove = function () {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};


app.service('apiService', function (Notification, $http, $rootScope, $cookies, $state) {
  this.baseurl = '/app/';
  this.showMsg = true;
  $rootScope.promises = [];
  $rootScope.token = $cookies.get('token');

  if ($rootScope.token == undefined || $rootScope.token.length < 1) {
    $state.go('login', {}, { reload: true });
  }


  function loadingBar(boolean) {

    if ($rootScope.loadingbarCounter == undefined) {
      $rootScope.loadingbarCounter = 0;
    }

    if (boolean) {
      $rootScope.loadingbarCounter = $rootScope.loadingbarCounter + 1;
    } else {
      $rootScope.loadingbarCounter = $rootScope.loadingbarCounter - 1;
    }

    if ($rootScope.loadingbarCounter < 1) {
      $rootScope.loadingBar = false;
      if ($rootScope.loadingbarCounter < 0) {
        $rootScope.loadingbarCounter = 0;
        // para prevenir los casos unusual, e.g. counter negativo 
      }
    } else {
      $rootScope.loadingBar = true;
    }
  };

  /**
  * New Request !! Holy Shit Nigga!!!!
  */
  this.request = function (url, method, data) {
    var configReq = {
      url: this.baseurl + url,
      method: method,
      data: data
    }

    var req = $http(configReq).then(function (response) {
      if (response.data.hasOwnProperty('message')) {
        Notification.success(response.data.message);
      }
      return response.data;
    }).catch(function (response) {
      if (response.data) {
        if (response.data.message !== undefined) Notification.error({ message: response.data.message });
      }
      else {
        Notification.error({ message: 'Hubo un error en el servidor' });
      }
      if (response.status == "403") //Auth.logOut();
        return {};

    }).finally(function () {
      $rootScope.promises.remove(req);
    });
    $rootScope.promises.push(req);
    return req;
  };

  this.get = function (url, showLoading) {
    loadingBar(true); //  $rootScope.loadingbarCounter++ y $rootScope.loadingBar = true;
    var get = $http({
      method: 'GET',
      url: this.baseurl + url,
      headers: {
        'token': $rootScope.token
      }
    }).then(function (response) {
      loadingBar(false);

      return response.data;
    }).catch(function (response) {
      loadingBar(false);
      if (response.data.message == 'No token provided') {
        $state.go('login', { expired: true });
        return;
      }
      if (response.status == "403") {
        $rootScope.$emit('access_denied');
        Notification.error(response.data.message);
        $state.go('login', { expired: true });
        return;
      }
      if (response.data) {
        Notification.error(response.data.message);
      } else {
        Notification.error('Hubo un error en el servidor')
      }
      return {};
    })
      .finally(function () {
        loadingBar(false);
        $rootScope.promises.remove(get);
      });

    if (showLoading) {
      $rootScope.promises.push(get);
    }

    return get;
  };


  this.post = function (url, data, multiple) {
    loadingBar(true);
    var post = $http({
      method: 'POST',
      url: this.baseurl + url,
      data: data,
      headers: {
        'token': $rootScope.token
      }
    }).then(function (response) {
      loadingBar(false);
      if (response.data.hasOwnProperty('success')) Notification.success(response.data.message);
      return response.data;
    }).catch(function (response) {
      loadingBar(false);
      if (response.status == "403") {
        $rootScope.$emit('access_denied');
      }
      if (response.data) {
        Notification.error(response.data.message);
      } else {
        Notification.error('Hubo un error en el servidor')
      }
      return {};

    }).finally(function () {
      loadingBar(false);
      $rootScope.promises.remove(post);
    });
    $rootScope.promises.push(post);
    return post;
  };

  this.put = function (url, data, multiple) {
    loadingBar(true);
    var put = $http({
      method: 'PUT',
      url: this.baseurl + url,
      data: data,
      headers: {
        'token': $rootScope.token
      }
    }).then(function (response) {
      loadingBar(false);
      if (response.data.hasOwnProperty('success')) Notification.success(response.data.message);
      return response.data;
    }).catch(function (response) {
      loadingBar(false);
      if (response.status == "403") $rootScope.$emit('access_denied');
      if (response.data) {
        Notification.error(response.data.message);
      } else {
        Notification.error('Hubo un error en el servidor')
      }
      return {};

    }).finally(function () {
      loadingBar(false);
      $rootScope.promises.remove(put);
    });
    $rootScope.promises.push(put);
    return put;
  };

});