db.sub_empresas.find({}).snapshot().forEach(
    function (elem) {    
        var empresaID = new ObjectId(elem.empresa._id); 
        db.sub_empresas.update(
            {
                _id: elem._id
            },
            {
                $set: {
                    'empresa._id': empresaID
                }
            }
        );
    }
);