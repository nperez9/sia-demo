// Coneccion a moongose 
const Mongoose = require('mongoose');
const config = require('./config/config');
const express = require('express');
const app = express();
const path = require('path');
const favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const jwt = require('jsonwebtoken');
const passwordHash = require('password-hash');
const glob = require('glob');
const log4js = require('log4js');

///////////////////////////// Coneccion a DB
try {
	Mongoose.Promise = require('bluebird');
	Mongoose.connect(config.db, {
		useMongoClient: true
	});
} catch (e) {
	console.log('No se pudo conectar a la base de datos \r\n');
	console.log(e);
	process.exit(1);
}

Mongoose.set('debug', false);
///////////////////////////// Configuracion del Logger
log4js.configure({
	appenders: { ganancias: { type: 'file', filename: 'logs/ganancias.log' } },
	categories: { default: { appenders: ['ganancias'], level: 'error' } }
});

///////////////////////////// Configuracion de Server (powered by cortiz)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(cookieParser());

app.use(cors());
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

require('./src/schemas');

const loginStrategy = require('./src/auth/loginStrategy');
passport.use('local', new LocalStrategy(loginStrategy));

passport.serializeUser(function (user, done) {
	done(null, user);
});

passport.deserializeUser(function (user, done) {
	done(null, user);
});

app.use(session({
	secret: config.secret,
	cookie: {
		maxAge: 900000
	},
	resave: true,
	saveUninitialized: true
})); // session secret
app.use(passport.initialize());
app.use(passport.session());


////////////////////////////////////// Nueva arquitectura de  de node orientado a modules 
/// Carga cada route.js, lo que va a armar las distintas rutas 
var router = express.Router();
var modules = glob.sync('./src/modules/*/router.js');

if (modules.length <= 0) throw new Error('Theres no modules, check you routes');
modules.forEach(function (module) {
	require(module)(router);
});

app.use('/app', router);

app.post('/app/login', passport.authenticate('local'), function (req, res) {
	var user = req.body;

	var token = jwt.sign(user, 'siasueldo-secret', {
		expiresIn: 60 * 6000
	});

	res.json({
		success: true,
		token: token,
		user: user
	});
});

app.get('/logout', function (req, res) {
	req.logout();
	res.redirect('/');
});

app.get('/Loginfailed', function (req, res) {
	var r = {
		estado_login: false
	}
	var data = JSON.stringify(r);
	res.send(JSON.parse(data));
});

//app.use(express.static(__dirname + 'public'));
app.get('/', function (req, res) {
	res.sendFile('index.html', {
		root: __dirname + "/public"
	});
});

app.use('/', express.static(__dirname + '/public/'));

app.listen(config.port);

console.log('express listening in port ' + config.port);