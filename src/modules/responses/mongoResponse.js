// This sieems to work
module.exports = function (res, successMessage, errorMessage) {
    return (err, result) => {
        if (err) {
            console.log(err);
            var eMessage = 'Error en la operacion de base de datos'
            if (typeof(errorMessage) !== 'undefined') eMessage = errorMessage; 
            return res.status(500).json({
                success: false,
                message: eMessage
            });
        }

        var sMessage = 'Operacion realizada con exito';
        if (typeof (successMessage) !== 'undefined')
            sMessage = successMessage;

        return res.json({
            success: true,
            message: sMessage,
            result: result
        });
    }

}
