const base = '/obrassociales';
const obraSocial = require('./obrassociales');

module.exports = function (router) {
    router.get(base, obraSocial.get);
}
