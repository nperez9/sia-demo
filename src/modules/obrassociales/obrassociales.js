const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const mongoose = require('mongoose'); 
const objectID = mongoose.Types.ObjectId; 
const ObrasSociales = mongoose.model('obras_sociales');

function get (req, res) {
    ObrasSociales.find({}, (err, docs) => {
        res.json(docs); 
    });
}

module.exports = {
    get
}