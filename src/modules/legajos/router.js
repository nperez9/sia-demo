const base = '/legajos';
const legajos = require('./legajos');
//const inserter = require('./insertEmpresas');

module.exports = function (router) {
    router.get(base + '/', legajos.get);
    router.get(base + '/all', legajos.getAll);
    router.get(base + '/:id', legajos.getById);
    router.get(base + '/:empresaid/data/:periodoid', legajos.getData);
    router.get(base + '/otros/:personaid/:empresaid/:periodoid/:id', legajos.getRelacionados);
    router.get(base + '/categorias/:categoria', legajos.getCategorias);
    router.get(base + '/suplea/:empresaid/:periodoid', legajos.getSuplea);
    router.get(base + '/maxlegajo/:empresaid', legajos.getMaxLegajo);
    
    router.post(base, legajos.post);

    router.put(base + '/:id', legajos.put);

    router.delete(base + '/:id', legajos.destroy);
}
