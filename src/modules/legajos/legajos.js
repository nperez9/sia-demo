const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');

const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId;

const Empresas = mongoose.model('empresas');
const SubEmpresas = mongoose.model('sub_empresas');
const Cargos = mongoose.model('cargos');
const Legajos = mongoose.model('legajos');
const Conceptos = mongoose.model('conceptos');
const Liquidaciones = mongoose.model('liquidaciones');
const Personas = mongoose.model('personas');

function get (req, res) {
    var filter = {};
    filter['subempresa.empresa._id'] = req.query.empresaId;

    if (typeof(req.query.subempresaId) !== 'undefined')
        filter['subempresa._id'] = new objectID(req.query.subempresaId);

    if (typeof(req.query.estado) !== 'undefined') {
        let estados = req.query.estado.split(',');
        filter['$or'] = [];
        estados.forEach(element => {
            filter['$or'].push({estado: element});
        });
    }

    if (typeof(req.query.nombre) !== 'undefined')
        filter['persona.nombre'] = req.query.nombre;

    console.log(filter);
    Legajos.find(filter).sort({'persona.nombre':1}).exec((err, items) => {
        return res.json(items);
    });
}


function getOne (req, res) {
    var legajoObjId = new objectID(req.params.id);

    Legajos.findOne({_id: legajoObjId}, (err, doc) => {
        return res.json(doc);
    });
}

function getAll (req, res) {
    Empresas.find({}, (err, docs) => {
        res.json(docs);
    });
}

function getData (req, res) {
  let empresaID = new objectID(req.params.empresaid);
  let periodoID = new objectID(req.params.periodoid);

  SubEmpresas.find({
      'empresa._id': empresaID, 
      'periodo_id': periodoID
  }, 
  (err, subempresas) => {
    Cargos.find({}, (err, cargos) => {
      Personas.find({
          'empresa._id': empresaID,
          'periodo_id': periodoID,
        },
        (err, personas) => {
            res.json({
                subempresas,
                cargos,
                personas
            });
      });
    });
  });
}

function getCategorias (req, res) {
  var categoria = req.params.categoria;

  Cargos.find({'categoria_codigo': categoria}, (err, cargos) => {
    res.json({
      cargos
    });
  });
}

function getRelacionados (req, res) {
    var findCriteria = {
        '_id': {$ne: new objectID(req.params.id) },
        'legajo.persona.persona_id': new objectID(req.params.personaid),
        'legajo.subempresa.empresa._id': new objectID(req.params.empresaid),
        'periodo._id': new objectID(req.params.periodoid)
        
    };
    Liquidaciones.find(findCriteria, (err, docs) => {
        res.json(docs);
    });
}

function post (req, res) {
    let legajo = req.body;
    let periodo = legajo.periodo;
    const conceptos_template = legajo.cargo.conceptos_template;
    const sMessage = 'Legajo creado con exito!';
    
    // Formatea Subempresas
    const subempresa = {
        _id: new objectID(legajo.subempresa.subempresa_id),
        descripcion: legajo.subempresa.descripcion,
        empresa: legajo.subempresa.empresa,
        categoria:legajo.subempresa.categoria,
        subvencion: legajo.subempresa.subvencion,
        afip: legajo.subempresa.afip,
        banco: legajo.subempresa.banco,
        libro_sueldo: legajo.subempresa.libro_sueldo,
        codigo: legajo.subempresa.codigo
    }
    const empresaID = new objectID(subempresa.empresa._id);
    
    legajo.subempresa = subempresa;
    
    Legajos.findOne({'subempresa.empresa._id': empresaID}).sort({numero_sistema: -1}).exec((err, legajoInterno) => {
        legajo.numero_sistema = (legajoInterno !== null && legajoInterno.numero_sistema !== undefined) ? legajoInterno.numero_sistema + 1 : 1;
        Legajos.create(legajo, (err, newLegajo) => {
            if (err) return console.log(err);
            
            let nuevaLiquidacion = {
                legajo: newLegajo,
                periodo: periodo
            };
            if (conceptos_template.length > 0) {
                let conceptos_id = [];
                conceptos_template.forEach(element => {conceptos_id.push(element._id)});

                Conceptos.find({_id: {$in:conceptos_id}}, (err, conceptos) => {
                    nuevaLiquidacion.conceptos = conceptos;
                    Liquidaciones.create(nuevaLiquidacion, mongoResponse(res, sMessage));
                });
            } else {
                Liquidaciones.create(nuevaLiquidacion, mongoResponse(res, sMessage));
            }
        });
    });
}

function getById (req, res) {
    let legajoID = new objectID(req.params.id);

    Legajos.findOne({_id: legajoID},
    (err, legajo) => {
        return res.json(legajo);
    });
}

function getCount (find) {
    if (typeof(find) ===  undefined)
        find = {};

    return Empresas.count(find).exec((err, count) => {
        return count;
    });
}

function put (req, res) {
    var legajo = req.body;
    var legajoID = new objectID(legajo._id);
    var periodoID = new objectID(legajo.periodo_id);
    var message = 'Legajo editado con exito';
    console.log('-------FIRS----------------');
    console.log(legajo.subempresa);
    console.log('-------------SECod----------');
    const subempresa = {
        _id: new objectID(legajo.subempresa.subempresa_id),
        descripcion: legajo.subempresa.descripcion,
        empresa: legajo.subempresa.empresa,
        categoria:legajo.subempresa.categoria,
        subvencion: legajo.subempresa.subvencion,
        afip: legajo.subempresa.afip,
        banco: legajo.subempresa.banco,
        libro_sueldo: legajo.subempresa.libro_sueldo,
        codigo: legajo.subempresa.codigo
    }
    
    legajo.subempresa = subempresa;
    console.log(legajo.subempresa);

    Legajos.findByIdAndUpdate(legajoID, legajo, (err, doc) => {
        Liquidaciones.update(
            {
                'legajo._id': legajoID,
                'periodo._id': periodoID
            },
            {
                $set: {legajo: legajo}
            },
            mongoResponse(res, message)
        );
    });
}

function destroy (req, res) {
    var legajoID = new objectID(req.params.id);
    var sMessage = 'Legajo eliminado con exito';

    Legajos.remove({_id: legajoID }, (err, result) => {
        Liquidaciones.remove({'legajo._id': legajoID}, (err, r) => {
            if (err) console.log(err);
            return res.json(r);
        });
    });
}

function getSuplea(req, res) {
    let empresaID = new objectID(req.params.empresaid);
    // 'empresa._id': empresaID 
    Legajos.find({ }, (err, personas) => {
        res.json(personas);
    });
}

function getMaxLegajo (req, res) {
    var findCriteria = {
        'subempresa.empresa._id': new objectID(req.params.empresaid)
    };
    
    Legajos.find(findCriteria,{legajo_interno:1}).sort({legajo_interno:-1}).limit(1).exec((err, item) => {
        res.json(item);
    });
}

module.exports = {
    get,
    getAll,
    getData,
    getRelacionados,
    post,
    put,
    destroy,
    getById,
    getCategorias,
    getSuplea,
    getMaxLegajo
}
