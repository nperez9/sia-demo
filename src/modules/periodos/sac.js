const moment = require('moment');

const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId;

const Periodo = mongoose.model('periodos');
const Liquidaciones = mongoose.model('liquidaciones');
const Legajos = mongoose.model('legajos');

const conceptoSAC = {
  descripcion: 'SAC',
  codigo: '0SAC001',
  formula: 'I',
  cantidad: 1,
  tipo_remuneracion: 1,
  indice: 0
}

const descuentoSAC = {
  descripcion: 'Descuento SAC x Día',
  codigo: '0SAC009',
  formula: '(($1000/180)*Q)*-1',
  cantidad: 0,
  tipo_remuneracion: 1,
  indice: 0
}

function procesarLiquidaciones(req, res) {
  const sac = req.body;
  const empresaID = new objectID(sac.empresa);
  const periodoID = new objectID(sac._id);

  const fechaInicio = new Date(sac.fecha_inicio);
  const fechaFin = new Date(sac.fecha_fin);
  const anio = sac.anio;
  const mesInicio = (sac.semestre == 1) ? 1 : 7;
  const mesFin = (sac.semestre == 1) ? 6 : 12;

  // console.log(anio);
  // console.log(sac);
  // process.exit(500);

  var liquidacionFilter = {
    'legajo.subempresa.empresa._id': empresaID,
    '$or': [
      { 'legajo.fecha_baja': { $gt: fechaFin } },
      { 'legajo.fecha_baja': { $exists: false } }
    ],
    'periodo.mes': { $gte: mesInicio, $lte: mesFin },
    'periodo.anio': anio,
    'legajo.estado': { $nin: ['baja'] },
    'periodo.sac': false
  };
  // console.log('------------------------------------');
  // console.log(liquidacionFilter);
  // console.log('------------------------------------');

  var liquidaciones = Liquidaciones.aggregate([
    {
      $match: liquidacionFilter,
    },
    {
      $group: {
        _id: "$legajo._id",
        'saldos_remunerativos': { $push: '$remunerativo_sac' },
        'legajo': { $last: '$legajo' },
        'ultimos_conceptos': { $last: '$conceptos' },
        'ultimo_periodo': { $last: '$periodo' },
        'periodos': { $addToSet: '$periodo.mes' },
        'count': { $sum: 1 }
      }
    }
  ])
    .exec((err, documents) => {
      return documents;
    });
  console.log(liquidaciones);

  Promise.all(
    [liquidaciones]
  ).then(data => {
    var liquis = data[0]; 
    var liquidacionesValidas = [];
    
    // Valida las liquidaciones
    for (let i = 0; i < liquis.length; i++) {
      if (liquis[i].periodos.length >= 6) {
        liquidacionesValidas.push(liquis[i]);
      }
    }
    
    // 
    for (let i = 0; i < liquidacionesValidas.length; i ++) {
      delete liquidacionesValidas[i]._id;
      liquidacionesValidas[i]._id = new objectID();
      generaLiquidacionesSac(liquidacionesValidas, i, sac);
    }
    
    Liquidaciones.remove({ 'periodo._id': periodoID }, (error, borrado) => {
      liquis.forEach( elem => {console.log(elem._id)});
      Liquidaciones.insertMany(liquis, (err, inserted) => {
        if (err) return console.log(err);
        return res.json({ success: true, message: 'SAC Generado', datos: liquis });
      });
    });
  }, error => {
    console.log('Error en el callback:', error);
  });
}

function diferenciaDeDias(fechaMenor, fechaMayor) {
  // calcula la diferencia de dias 
  var fecha1 = moment(fechaMenor);
  var fecha2 = moment(fechaMayor);

  return fecha2.diff(fecha1, 'days');
}


function generaLiquidacionesSac(liquis, i, sac) {
  const fechaInicio = new Date(sac.fecha_inicio);
  const fechaFin = new Date(sac.fecha_fin);

  liquis[i].conceptos = [];

  let _conceptoSAC = JSON.parse(JSON.stringify(conceptoSAC));
  let valorSac = 0;
  let diasDescuento = 0;

  // si tiene saldo le manda el mayor
  if (liquis[i].saldos_remunerativos.length > 0) {
    valorSac = liquis[i].saldos_remunerativos.sort(sortNumber)[0] / 2;
  }
  _conceptoSAC.importe = valorSac;

  // Se setea distancia de dias
  liquis[i].conceptos.push(_conceptoSAC);
  liquis[i].periodo = sac;

  if (liquis[i].legajo.fecha_alta > fechaInicio) {
    diasDescuento += diferenciaDeDias(fechaInicio, liquis[i].legajo.fecha_alta);
    // console.log('------------------------------------');
    // console.log('Persona: ', liquis[i].legajo.persona.nombre);
    // console.log('DiasDescuento:', diasDescuento);
    // console.log('------------------------------------');
  }
  try {
    if (liquis[i].legajo.licencias.length > 0) {
      liquis[i].legajo.licencias.forEach(licencia => {
        if ((licencia.goce_sueldo || typeof (licencia.goce_sueldo) === 'undefined')
          && (licencia.fecha_inicio > fechaFin || typeof (licencia.fecha_inicio) === 'undefined')
          && (licencia.fecha_fin < fechaInicio || typeof (licencia.fecha_fin) === 'undefined')) {

        } else {
          // Saca los dias de descuento para el periodo del SAC
          let licenciaFechaInicio;
          let licenciaFechaFin;

          if (licencia.fecha_inicio <= fechaInicio) {
            licenciaFechaInicio = fechaInicio;
          } else {
            licenciaFechaInicio = licencia.fecha_inicio;
          }

          if (licencia.fecha_fin >= fechaFin) {
            licenciaFechaFin = fechaFin;
          } else {
            licenciaFechaFin = licencia.fecha_fin;
          }
          //sumo 1 dia porque la licencias son inclusive
          diasDescuento += diferenciaDeDias(licenciaFechaInicio, licenciaFechaFin) + 1;

          // console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<');
          // console.log('Persona: ', liquis[i].legajo.persona.nombre);
          // console.log('Fecha-inicio: ', licencia.fecha_inicio);
          // console.log('Fecha-fin: ', licencia.fecha_fin);
          // console.log('dias: ', diasDescuento);
          // console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<');
        }
      });
    }
  } catch (e) {
    console.log(e);
  }

  if (diasDescuento > 0) {
    let _descuentoSAC = JSON.parse(JSON.stringify(descuentoSAC));

    _descuentoSAC.cantidad = diasDescuento;
    liquis[i].conceptos.push(_descuentoSAC);
  }

  liquis[i].ultimos_conceptos.forEach(concepto => {

    if (concepto.sac && (concepto.tipo_remuneracion == 3 || concepto.tipo_remuneracion == 2)) {
      let conceptoClon = JSON.parse(JSON.stringify(concepto));
      delete conceptoClon.liquidacion_id;
      conceptoClon.liquidacion_id = new objectID();
      liquis[i].conceptos.push(conceptoClon);
    }
  });

  delete liquis[i].ultimos_conceptos;
}

function sortNumber(a, b) {
  return b - a;
}

module.exports = {
  procesarLiquidaciones
}