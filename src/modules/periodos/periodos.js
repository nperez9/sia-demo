const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const moment = require('moment');

const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId;

const Periodo = mongoose.model('periodos');
const Liquidaciones = mongoose.model('liquidaciones');
const Legajos = mongoose.model('legajos');
const Subempresas = mongoose.model('sub_empresas');
const Personas = mongoose.model('personas');
const Variables = mongoose.model('variables');

function getActual (req, res) {
    empresaID = new objectID(req.params.empresaid); 
    var curDate = moment.utc();
    Periodo.findOne({empresa: empresaID}).sort({fecha_inicio:-1})
    .exec((err, doc) => {
        console.log(doc); 
        if (doc != null) {
            return res.json(doc);
        } else {
            createActualPeriodo(empresaID).then((newPeriodo) => {
                console.log(newPeriodo);
                return res.json(newPeriodo);
            }); 
        }
    });
}

function getAllPeriodos (req, res) {
    empresaID = new objectID(req.params.empresaid);
    
    Periodo.find({empresa: empresaID}, (err, periodos) => {
        if (err) console.log(err);
        return res.json(periodos);
    });
}

/**
 * devuelve un periodo por ID
 */
function getOne (req, res) {
    let periodoID = new objectID(req.params.id);

    Periodo.findOne({_id: periodoID}, (err, doc) => {
        res.json(doc);
    });
}



/**
 * Cierra todo el periodo, es un proceso
 */
function cerrarPeriodo (req, res) {
    var datosPeriodo = req.body; 
    var empresaObjId = new objectID(datosPeriodo.empresa._id); 
    var periodoObjId = new objectID(datosPeriodo.periodo._id);
    console.log('periodo', periodoObjId)

    Periodo.findOneAndUpdate({'_id': periodoObjId}, {$set: {cerrado: true}}, (err, periodoActualizado) => {       
    var newMonth = moment.utc(periodoActualizado.fecha_inicio).add(1, 'month').format('YYYY/MM/DD'); 
        
        //Crearia el nuevo periodo
        checkNextPeriodo(empresaObjId, newMonth).then(function (existingPeriodo) {
            if (existingPeriodo !== null) {
                console.log('Periodo Existente!');
                return res.json({
                    success: true, 
                    message: 'Periodo cerrado - Posterior ya existente',
                    periodo: existingPeriodo
                });s
            } 
            else {
                createActualPeriodo(empresaObjId, newMonth).then(function (newPeriodo) {
                    console.log('Periodo Nuevo');
                    var findCriteria = {
                        'periodo._id': periodoObjId, 
                        'legajo.subempresa.empresa._id': empresaObjId,
                        'legajo.estado': {$nin:['baja']}
                        // '$or': [
                        //     {'legajo.fecha_baja': {$lt: newPeriodo.fecha_inicio}},
                        //     {'legajo.fecha_baja': {$exists: false}}
                        // ]
                    }

                    Liquidaciones.find(findCriteria, (err, liquidaciones) => {
                        const liqui = liquidaciones;
                        var nuevasLiquidaciones = [];
                        
                        if (liqui.length > 0) {
                            nuevasLiquidaciones = liquidacionesNewPeriodo(liqui, newPeriodo);
                            Liquidaciones.insertMany(nuevasLiquidaciones)
                            .then(insertedLiquidacion => {
                                let subempresaPromise = Subempresas.find({
                                    'periodo_id': periodoObjId,
                                    'empresa._id': empresaObjId
                                }, (err, subempresas) => {
                                    let insertSubempresas = [];

                                    for (let index = 0; index < subempresas.length; index++) {
                                        let nuevaSubEmpresa = JSON.parse(JSON.stringify(subempresas[index]));
                                        delete nuevaSubEmpresa._id;
                                        nuevaSubEmpresa._id = new objectID();
                                        nuevaSubEmpresa.periodo_id = new objectID(newPeriodo._id);
                                        
                                        insertSubempresas.push(nuevaSubEmpresa);
                                    }

                                    Subempresas.insertMany(insertSubempresas, (err, docs) => {
                                        if (err) console.log(err);
                                        return docs;
                                    })
                                });

                                let personaPromise = Personas.find({
                                    'periodo_id': periodoObjId
                                }, (err, personas) => {
                                    let insertPersonas = [];
                                    
                                    for (let index = 0; index < personas.length; index++) {
                                        let nuevaPersona = JSON.parse(JSON.stringify(personas[index]));
                                        nuevaPersona._id = new objectID();
                                        nuevaPersona.periodo_id = newPeriodo._id;
                                        insertPersonas.push(nuevaPersona);
                                    }

                                    Personas.insertMany(insertPersonas, (err, docs) => {
                                        if (err) console.log(err);
                                        return docs;
                                    })
                                });

                                let variablesPromise = Variables.find({
                                    'periodo': periodoObjId
                                }, (err, variables) => {
                                    let insertVariables = [];
                                    
                                    for (let index = 0; index < variables.length; index++) {
                                        let nuevaVariable = JSON.parse(JSON.stringify(variables[index]));
                                        nuevaVariable._id = new objectID();
                                        nuevaVariable.periodo = newPeriodo._id;
                                        insertVariables.push(nuevaVariable);
                                    }

                                    Variables.insertMany(insertVariables, (err, docs) => {
                                        if (err) console.log(err);
                                        return docs;
                                    })
                                });

                                Promise.all([subempresaPromise, personaPromise, variablesPromise]).then(function (promiseData) {
                                    return res.json({
                                        success: true,
                                        message: 'Periodo cerrado',
                                        periodo: newPeriodo
                                    });
                                });
                            });
                        } 
                        else {
                            return res.json({
                                success: true,
                                message: 'Periodo cerrado',
                                periodo: newPeriodo
                            });
                           
                        }
                    });
                });


                
            }
        })
    });   
}
// Mira lo que esta esa identacion papa!!!!

/**
 * Inseta un periodo
 */
function insert (req, res) {
    var periodo = req.body;

    Periodo.create(periodo, mongoResponse(res, 'Periodo insertado con exito'));
}


/**
 * Formatea las liquidaciones para el nuevo periodo
 * @param {Array} liquidaciones 
 * @param {Object} newPeriodo 
 */
function liquidacionesNewPeriodo (liquidaciones, newPeriodo) {
    var nuevasLiquidaciones = [];
    
    for (let i = 0; i < liquidaciones.length; i++) {
        let nuevaLiquidacion = JSON.parse(JSON.stringify(liquidaciones[i]));
        nuevaLiquidacion._id = new objectID();
        nuevaLiquidacion.periodo = newPeriodo;
        var goceSueldo = false;

        // cambia el estado del legajo
        console.log(nuevaLiquidacion.legajo.fecha_baja);
        console.log(newPeriodo.fecha_inicio);
        console.log(moment(newPeriodo.fecha_inicio).isAfter(nuevaLiquidacion.legajo.fecha_baja));
        if (typeof(nuevaLiquidacion.legajo.fecha_baja) !== 'undefined' && moment(newPeriodo.fecha_inicio).isAfter(nuevaLiquidacion.legajo.fecha_baja)) {
            nuevaLiquidacion.legajo.estado = 'baja';
        } 
        else if (nuevaLiquidacion.legajo.licencias.length > 0 ) 
        {
            nuevaLiquidacion.legajo.licencias.forEach(element => {
                if (moment(newPeriodo.fecha_inicio).isAfter(element.fecha_inicio) &&
                moment(element.fecha_fin).isAfter(newPeriodo.fecha_fin)) {
                    nuevaLiquidacion.legajo.estado = 'licencia';
                    goceSueldo = element.goce_sueldo;
                }
            });
        } // No digan como vivo

        // Aumenta la antiguedad, si hay liquidacion sin goce de sueldo no lo deberia hacer
        if ((nuevaLiquidacion.legajo.estado != 'licencia' || goceSueldo) && nuevaLiquidacion.legajo.estado != 'baja') {
            if (nuevaLiquidacion.legajo.antiguedad.anios === undefined) {
                nuevaLiquidacion.legajo.antiguedad = {
                    anios: 0,
                    meses: 1
                }
            }
            else if (nuevaLiquidacion.legajo.antiguedad.meses == 11) {
                nuevaLiquidacion.legajo.antiguedad.anios += 1; 
                nuevaLiquidacion.legajo.antiguedad.meses = 0;
            } else {
                nuevaLiquidacion.legajo.antiguedad.meses += 1;
            }
        }

        nuevaLiquidacion.conceptos = [];
        // Si esta de baja lo muestra pero quieren que este vacio ( # entien)
        if (nuevaLiquidacion.legajo.estado != 'baja') {
            
            for (let j = 0; j < liquidaciones[i].conceptos.length; j ++) {
                if (liquidaciones[i].conceptos[j].fijo) {
                    let copyConcepto = JSON.parse(JSON.stringify(liquidaciones[i].conceptos[j]));

                    if (copyConcepto.fijo_mensual !== null) {
                        copyConcepto.fijo_mensual = copyConcepto.fijo_mensual - 1;
                        if (copyConcepto.fijo_mensual == 0) {
                            copyConcepto.fijo = false;
                            copyConcepto.fijo_mensual = null;
                        }
                    }

                    copyConcepto.liquidacion_id = new objectID();
                    nuevaLiquidacion.conceptos.push(copyConcepto);
                }
            }
        }
        
        updateAntiguedadLegajo(nuevaLiquidacion.legajo.antiguedad, nuevaLiquidacion.legajo._id);
        nuevasLiquidaciones.push(nuevaLiquidacion);
    }
    return nuevasLiquidaciones;
}


function updateAntiguedadLegajo (antiguedad, id) {
    objID = new objectID(id);
    Legajos.findOneAndUpdate({_id: id}, {$set: {antiguedad: antiguedad}}, (err, legajo) => {
        
    }); 
    return true;
}


function test(req, res) {
    createActualPeriodo();
    res.json('asd');
}

/**
 * Chekea si exise el otro periodo
 * @param {*} idEmpresa 
 * @param {*} periodoDate 
 */
function checkNextPeriodo (idEmpresa, periodoDate) {

    findCriteria = {
        $or: [
            {
                fecha_inicio: moment.utc(periodoDate).startOf('month'),
                fecha_fin: moment.utc(periodoDate).endOf('month')
            },
            {
                anio: parseInt(moment(periodoDate).year()),
                mes: parseInt(moment(periodoDate).format('M'))
            }
        ],
        empresa: idEmpresa,
        sac: false
    }
    
    return Periodo.findOne(findCriteria, (err, doc) => {
        if (err) console.log(err);
        return doc;
    })
}


/**
 * Devuelve el proximo periodo
 * @param {String} idEmpresa 
 * @param {Object|String|Undefined} periodoDate 
 */
function createActualPeriodo (idEmpresa, periodoDate) {
    var empresa = (typeof(idEmpresa) !== 'undefined') ? idEmpresa : null;
    var periodo;

    if (typeof(periodoDate) === 'Object') {
        periodo = periodoDate;
    }
    else if (typeof(periodoDate) === 'string') {
        periodo = {
            fecha_inicio: moment(periodoDate).startOf('month').format('YYYY-MM-DD'), 
            fecha_fin: moment(periodoDate).endOf('month').format('YYYY-MM-DD'), 
            descripcion: moment(periodoDate).locale('es').format('MMMM YYYY'),
            empresa: empresa,
            anio: parseInt(moment(periodoDate).year()),
            mes: parseInt(moment(periodoDate).format('M'))
        }
    } else {
        periodo = {
            fecha_inicio: moment().startOf('month').format('YYYY-MM-DD'), 
            fecha_fin: moment().endOf('month').format('YYYY-MM-DD'), 
            descripcion: moment().locale('es').format('MMMM YYYY'), 
            empresa: empresa,
            anio: parseInt(moment(periodoDate).year()),
            mes: parseInt(moment(periodoDate).format('M'))
        };
    }
    console.log(periodo);

    return Periodo.create(periodo, (err, insertedDoc) => {
        return insertedDoc;
    });
}


function destroy (req, res) {
    var periodoID = new objectID(req.params.id);

    let successMessage = 'Periodo eliminado';
    Periodo.delete({ _id: periodoID }, (err, response) => {
        let liqProm = Liquidaciones.remove({'periodo._id': periodoID}, (err, remLiq) => {
            if (err) console.log(err);
            return remLiq;
        });
        let subempPeriodo = Subempresas.remove({'periodo_id':periodoID }, (err, deletedSubempresas) => {
            if (err) console.log(err);
            return deletedSubempresas;
        });
        let pardronPeriodo = Personas.remove({'periodo_id':periodoID }, (err, deletedPersonas) => {
            if (err) console.log(err);
            return deletedPersonas;
        });
        
        Promise.all([
            liqProm,
            subempPeriodo,
            pardronPeriodo
        ]).then(function (params) {
            return res.json({success: true, message: successMessage});
        });
    });
}


function seekAndDestoy (req, res) {
    console.log('searching.... Seek and Destroy');
}

function put (req, res) {
    var periodo = req.body;
    var periodoID = new objectID(req.params.id); 
    var message = 'Periodo editado con exito!'; 

    Periodo.findByIdAndUpdate(periodoID, periodo, (err, update) => {
        if (err) console.log(err);
        Liquidaciones.update(
            {'periodo._id': periodoID},
            {$set: {periodo: periodo}},
            {multi: true},
            mongoResponse(res, message)
        );
    }); 
}

module.exports = {
    test,
    getOne,
    insert,
    getActual,
    cerrarPeriodo,
    getAllPeriodos,
    destroy,
    put
}