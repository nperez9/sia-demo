const base = '/periodos';
const periodos = require('./periodos');
const sac = require('./sac');
//const inserter = require('./insertperiodos');

module.exports = function (router) {
    router.get(base + '/actual/:empresaid', periodos.getActual);
    router.get(base + '/todos/:empresaid', periodos.getAllPeriodos);
    router.get(base + '/:id', periodos.getOne);

    router.get(base + '/test', periodos.test);

    router.post(base + '/cerrar-periodo', periodos.cerrarPeriodo);
    router.post(base + '/sac/procesar/liquidaciones', sac.procesarLiquidaciones);
    router.post(base, periodos.insert);

    router.put(base + '/:id', periodos.put);

    router.delete(base + '/:id', periodos.destroy);
}