const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');

const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId;
const Conceptos = mongoose.model('conceptos');
const Liquidaciones = mongoose.model('liquidaciones');

function get(req, res) {
    var findCriteria = {};
    var documentsPerPage = 20;
    var skip = (req.query.page - 1) * documentsPerPage;

    findCriteria['empresa._id'] = { $exists: false };

    if (req.query.search != '') {
        findCriteria['$or'] = [
            { descripcion: new RegExp(req.query.search, 'i') },
            { codigo: new RegExp(req.query.search, 'i') }
        ];
    }

    Conceptos.find(findCriteria)
        .limit(documentsPerPage)
        .skip(skip)
        .sort({ codigo: 1 })
        .exec((err, items) => {
            getCount(findCriteria).then(total => {
                return res.json({
                    items,
                    total
                });
            });
        });
}

function getAll(req, res) {
    Conceptos.find({}, (err, docs) => {
        res.json(docs);
    });
}

function getAllByCategory(req, res) {
    var findCriteria = {};

    if (req.params.categoria != '') {
        console.log(req.params.categoria, );
        findCriteria = { codigo: new RegExp(req.params.categoria, 'i') }
    }

    Conceptos.find(findCriteria)
        .exec((err, docs) => {
            res.json(docs);
        });

}

function getByEmpresaId(req, res) {
    let empresaObjId = new objectID(req.params.empresaid);

    Conceptos.find({ 'empresa._id': empresaObjId })
        .sort({ codigo: 1 })
        .exec((err, docs) => {
            return res.json(docs);
        });
}

function post(req, res) {
    let concepto = req.body;
    let successMessage = 'Concepto creado con exito'

    Conceptos.create(concepto, mongoResponse(res, successMessage));
}

function getById(req, res) {
    let conceptoID = new objectID(req.params.id);

    Conceptos.findOne({ _id: conceptoID },
        (err, concepto) => {
            return res.json(concepto);
        });
}

function getCount(find) {
    if (typeof (find) === undefined)
        find = {};

    return Conceptos.count(find).exec((err, count) => {
        return count;
    });
}

function put(req, res) {
    var concepto = req.body;
    var conceptoID = new objectID(concepto._id);
    var message = 'concepto editado con exito';

    Conceptos.findByIdAndUpdate(conceptoID, concepto, mongoResponse(res, message));
}

function destroy(req, res) {
    // Searhing..... seek and destroy
    var conceptoID = new objectID(req.params.id);
    let successMessage = 'Concepto eliminado';

    Conceptos.delete({ _id: conceptoID }, mongoResponse(res, successMessage));
}

function getModal(req, res) {
    let empresaID = new objectID(req.params.empresaid);
    let periodoID = new objectID(req.params.periodoid);

    console.log(empresaID)
    console.log(periodoID)

    Liquidaciones.find({ "periodo._id": periodoID, "periodo.empresa": empresaID },
        (err, liquidaciones) => {
            var conceptosPeriodo = [];

            for (let i = 0; i < liquidaciones.length; i++) {
                for (let e = 0; e < liquidaciones[i].conceptos.length; e++) {
                    if (conceptosPeriodo.indexOf(liquidaciones[i].conceptos[e].codigo) == -1) {
                        conceptosPeriodo.push(liquidaciones[i].conceptos[e].codigo);
                    }
                }
            }

            Conceptos.find({ codigo: { $in: conceptosPeriodo } }, { codigo: 1, descripcion: 1 }, (err, conceptos) => {
                return res.json(conceptos);
            });
        });
}

function getCaja(req, res) {
    Conceptos.find({ caja_complementaria: true },
        (err, conceptos) => {
            return res.json(conceptos);
        });
}


module.exports = {
    get,
    getAll,
    getByEmpresaId,
    post,
    put,
    destroy,
    getById,
    getAllByCategory,
    getModal,
    getCaja
}
