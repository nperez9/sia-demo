const base = '/conceptos'; 
const conceptos = require('./conceptos');
const isLoggued = require('../../auth/auth_middleware.js');

module.exports = function (router) {
    router.get(base + '/all', conceptos.getAll);
    router.get(base + '/modal/:empresaid/:periodoid', conceptos.getModal); 
    router.get(base, isLoggued, conceptos.get);
    router.get(base + '/caja', conceptos.getCaja);
    router.get(base + '/:id', conceptos.getById); 
    router.get(base + '/categoria/:categoria', conceptos.getAllByCategory);
    router.get(base + '/empresa/:empresaid', conceptos.getByEmpresaId); 
    
    router.post(base, conceptos.post);
    
    router.put(base + '/:id', conceptos.put); 
    
    router.delete(base + '/:id', conceptos.destroy);
};
