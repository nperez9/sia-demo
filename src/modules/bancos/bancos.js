const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId;
const Bancos = mongoose.model('bancos');
const ReporteBanco = mongoose.model('reporte_banco');

function get(req, res) {
    Bancos.find({}, (err, docs) => {
        res.json(docs);
    });
}

function initBanco(req, res) {

    var data = {
        banco: {
            _id: new objectID(req.body.id),
            nombre: req.body.nombre
        }
    };

    ReporteBanco.create(data, mongoResponse(res, 'Banco creada con exito'));
}

function postColumna(req, res) {
    var banco = req.body.banco;
    var archivo = req.body.archivo;
    var columna = req.body.columna;

    ReporteBanco.findOne({ _id: new objectID(banco._id) }, (err, columnas) => {

        switch (archivo) {
            case 'head':
                for (let i = 0; i < columnas.head.length; i++) {
                    if (columnas.head[i].columna == columna) {
                        return res.json({
                            success: false,
                            message: 'Columna ya ingresada'
                        });
                    }
                }
                break;

            case 'body':
                for (let i = 0; i < columnas.body.length; i++) {
                    if (columnas.body[i].columna == columna) {
                        return res.json({
                            success: false,
                            message: 'Columna ya ingresada'
                        });
                    }
                }
                break;

            case 'footer':
                for (let i = 0; i < banco.footer.length; i++) {
                    if (banco.footer[i].columna == columna) {
                        return res.json({
                            success: false,
                            message: 'Columna ya ingresada'
                        });
                    }
                }
                break;
        }

        ReporteBanco.update(
            { _id: new objectID(banco._id) },
            { $set: banco },
            {
                multi: false,
                upsert: true
            },
            (err, updatedDoc) => {
                ReporteBanco.find({ _id: new objectID(banco._id) }, (err, banco) => {
                    return res.json(banco)
                });
            }
        );
    });
}

function getColumnas(req, res) {

    var id = new objectID(req.params.id);

    ReporteBanco.findOne({ _id: id })
        .exec((err, banco) => {
            res.json(banco)
        });
}

function postEditColumna (req, res){
    var banco = req.body.banco;

    ReporteBanco.update(
        { _id: new objectID(banco._id) },
        { $set: banco },
        {
            multi: false
        },
        (err, updatedDoc) => {
            ReporteBanco.find({ _id: new objectID(banco._id) }, (err, banco) => {
                return res.json(banco)
            });
        }
    );
}

function getReportesBancos(req, res) {
    ReporteBanco.find({})
        .exec((err, bancos) => {
            res.json(bancos)
        });
}

function deleteColumna(req, res) {
    var banco = req.body.banco;

    ReporteBanco.update(
        { _id: new objectID(banco._id) },
        { $set: banco },
        {
            multi: false
        },
        (err, updatedDoc) => {
            return res.json({})
        }
    );
}

function deleteBanco(req, res) {
    var banco = new objectID(req.params.id);

    ReporteBanco.delete({ _id: banco }, 
        (err, result) => {
            if (err) console.log(err);

            return res.json({
                success: true, 
                message: 'Banco eliminado'
            });
        }); 
}

module.exports = {
    get,
    initBanco,
    postColumna,
    getColumnas,
    postEditColumna,
    getReportesBancos,
    deleteColumna,
    deleteBanco
}


