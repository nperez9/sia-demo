const base = '/bancos';
const bancos = require('./bancos');

module.exports = function (router) {
    router.get(base, bancos.get);
    router.get(base + '/reportes', bancos.getReportesBancos);
    router.get(base + '/columna/:id', bancos.getColumnas);
    router.get(base + '/delete/:id', bancos.deleteBanco);
    
    router.post(base + "/init", bancos.initBanco);
    router.post(base + "/columna", bancos.postColumna);
    router.post(base + "/edit/columna", bancos.postEditColumna);
    router.post(base + '/columna/delete', bancos.deleteColumna);
}
