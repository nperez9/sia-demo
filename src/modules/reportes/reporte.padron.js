const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./reportes.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const objectID = mongoose.Types.ObjectId;

const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');


function subempresas(req, res) {
    var wb = new excel4node.Workbook();
    var ws = wb.addWorksheet();

    const periodo = req.body.periodo;
    const empresa = req.body.empresa;

    let periodoID = new objectID(periodo);
    let empresaID = new objectID(empresa);

    var match = { 'legajo.subempresa.empresa._id': empresaID, 'periodo._id': periodoID,  };
    var sort = { subempresa: 1, nombre: 1 };

    Liquidaciones.aggregate([
        { $match: match },
        {
          $group: {
            _id: '$legajo.subempresa._id',
            legajos: {$push : '$legajo'}
          }
        },
        { $sort: sort }
      ]).exec((err, docs) => {
        var cantPersonas = 0;

        for (let auxSubs = 0; auxSubs < docs.length; auxSubs++) {
            var nombres = [];

            ws.cell(cantPersonas+1, 1).string(docs[auxSubs].legajos[0].subempresa.descripcion);
            
            ws.cell(cantPersonas+3, 2).string("PERSONAL").style(styles.subEmpresasHeaders).style(styles.leftLine);
            ws.cell(cantPersonas+3, 3).string("DOCUMENTO").style(styles.subEmpresasHeaders);
            ws.cell(cantPersonas+3, 4).string("CUIT").style(styles.subEmpresasHeaders);
            ws.cell(cantPersonas+3, 5).string("F. NAC.").style(styles.subEmpresasHeaders);
            ws.cell(cantPersonas+3, 6).string("ALTA").style(styles.subEmpresasHeaders);
            ws.cell(cantPersonas+3, 7).string("OBRA SOCIAL").style(styles.subEmpresasHeaders);
            ws.cell(cantPersonas+3, 8).string("N° LEG.").style(styles.subEmpresasHeaders);
            ws.cell(cantPersonas+3, 9).string("ANTIG.").style(styles.subEmpresasHeaders);
            ws.cell(cantPersonas+3, 10).string("HORAS").style(styles.subEmpresasHeaders);
            ws.cell(cantPersonas+3, 11).string("CARAC.").style(styles.subEmpresasHeaders).style(styles.rightLine);;

            for (let auxLega = 0; auxLega < docs[auxSubs].legajos.length; auxLega++) {

                if(nombres.indexOf(docs[auxSubs].legajos[auxLega].persona.nombre) == -1){

                var fecha_alta = moment(docs[auxSubs].legajos[auxLega].persona.datos_administrativos.fecha_alta).format('DD/MM/YYYY');
                var fecha_nac = docs[auxSubs].legajos[auxLega].persona.fecha_nacimiento == undefined ? " / / " : moment(docs[auxSubs].legajos[auxLega].persona.fecha_nacimiento).format('DD/MM/YYYY');
                var caracter = docs[auxSubs].legajos[auxLega].caracter == undefined ? " " : docs[auxSubs].legajos[auxLega].caracter;
                var nro_legajo = docs[auxSubs].legajos[auxLega].numero_legajo ? docs[auxSubs].legajos[auxLega].numero_legajo : docs[auxSubs].legajos[auxLega].numero_sistema;

                if(nro_legajo == undefined ){
                    nro_legajo = " ";
                }else{
                    nro_legajo = nro_legajo.toString();
                }

                ws.cell(cantPersonas+4, 2).string(docs[auxSubs].legajos[auxLega].persona.nombre);
                ws.cell(cantPersonas+4, 3).string(docs[auxSubs].legajos[auxLega].persona.dni.toString());
                ws.cell(cantPersonas+4, 4).string(docs[auxSubs].legajos[auxLega].persona.cuil);
                ws.cell(cantPersonas+4, 5).string(fecha_nac);
                ws.cell(cantPersonas+4, 6).string(fecha_alta.toString());
                ws.cell(cantPersonas+4, 7).string(docs[auxSubs].legajos[auxLega].persona.datos_administrativos.obra_social.nombre);
                ws.cell(cantPersonas+4, 8).string(nro_legajo);
                ws.cell(cantPersonas+4, 9).string(docs[auxSubs].legajos[auxLega].antiguedad.anios.toString()+"/"+docs[auxSubs].legajos[auxLega].antiguedad.anios.toString());
                ws.cell(cantPersonas+4, 10).string(docs[auxSubs].legajos[auxLega].horas.toString());
                ws.cell(cantPersonas+4, 11).string(caracter);
                cantPersonas++;

                nombres.push(docs[auxSubs].legajos[auxLega].persona.nombre)
                }

            }

            cantPersonas++;
            cantPersonas++;
            cantPersonas++;
            cantPersonas++;
        }

        console.log(nombres)
        ws.column(2).setWidth(30);
        ws.column(3).setWidth(10);
        ws.column(4).setWidth(15);
        // ws.column(4).setWidth(15);
        // ws.column(4).setWidth(15);
        ws.column(7).setWidth(30);
        ws.column(8).setWidth(8);
        // ws.column(9).setWidth(2);
        // ws.column(10).setWidth(2);
        // ws.column(11).setWidth(2);

        wb.write('padron_subempresa.xlsx', res);
      });

}

module.exports = {
    subempresas
};