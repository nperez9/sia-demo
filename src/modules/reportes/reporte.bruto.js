const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./reportes.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const objectID = mongoose.Types.ObjectId;

const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');

function sueldosBruto(req, res) {
  const periodo = req.body.periodo;
  const empresa = req.body.empresa;
  const subempresas = req.body.subempresas;
  const agrupar = req.body.agrupar;

  let periodoID = new objectID(periodo);
  let empresaID = new objectID(empresa);

  if (subempresas.length != 0) {
    for (let a = 0; a < subempresas.length; a++) {
      subempresas[a] = new objectID(subempresas[a]);
    }
  }

  var wb = new excel4node.Workbook();
  var ws = wb.addWorksheet();

  var styleTotal = wb.createStyle({
    font: {
      name: 'Arial',
      color: '#000000',
      size: 8,
      bold: true
    },
    border: {
      top: {
        style: 'medium',
        color: '#000000'
      }
    }
  });

  var titulo = 'Sueldos brutos';
  var headers = [
    'Apellido y Nombre',
    'Nivel',
    'CUIL',
    'Remunerativos',
    'No remunerativos',
    'Descuentos',
    'Asig. familiar',
    'Jubilación',
    'Decreto 137/05 Res.33/05',
    'Ley 19032',
    'Caja complementaria',
    'Obra social',
    'Obra social familiar',
    'Dif. Min. O.s. Personal / Patronal',
    'Imp. a las ganancias',
    'Sindicato s.a.d.o.p.',
    'Fondo solidario',
    'Sindicato uda'

  ];

  let match = {
    'periodo._id': periodoID,
    'legajo.subempresa.empresa._id': empresaID
  };
  if (subempresas.length > 0) 
    match['legajo.subempresa._id'] = { $in: subempresas };

  if (agrupar) {
    var group = {
      _id: '$legajo.persona._id',
      empresa: { $first: '$legajo.subempresa.empresa' },
      periodo: { $first: '$periodo.descripcion' },
      nombre: { $first: '$legajo.persona.nombre' },
      nivel: { $first: '$legajo.subempresa.descripcion' },
      cuil: { $first: '$legajo.persona.cuil' },
      conceptos: { $push: '$conceptos' }
    };

  } else {
    var group = {
      _id: '$_id',
      empresa: { $first: '$legajo.subempresa.empresa' },
      periodo: { $first: '$periodo.descripcion' },
      nombre: { $first: '$legajo.persona.nombre' },
      nivel: { $first: '$legajo.subempresa.descripcion' },
      cuil: { $first: '$legajo.persona.cuil' },
      antiguedad: { $first: '$legajo.antiguedad' },
      conceptos: { $push: '$conceptos'}
    };
    headers.push('Antiguedad');
  }


  Liquidaciones.aggregate([
    { $match: match },
    { $unwind: '$legajo' },
    {
      $group: group
    },
    { $sort: { nombre: 1 } }
  ]).exec((err, docs) => {
    
    if (docs.length < 1) {
      return res.status(504).json({success: false , message: 'El reporte no posee registros'})
    }
    // primero va la ROW  y despues el  columna   
    ws.cell(1, 2).string(docs[0].empresa.razon_social).style(styles.title);
    ws.cell(2, 2).string('Fecha: ' + moment().format('DD/MM/YYYY')).style(styles.datas);
    ws.cell(3, 2).string(titulo).style(styles.datas);
    ws.cell(4, 2).string('Periodo: ' + docs[0].periodo).style(styles.datas);

    // var total = 0;

    var total_c_aportes = 0;
    var total_s_aportes = 0;
    var total_familiar = 0;
    var total_descuento = 0;
    var total_jubilacion = 0;
    var total_dec_137_5 = 0;
    var total_ley_19032 = 0;
    var total_obra_social = 0;
    var total_obra_social_familiar = 0;
    var total_diferencia_obra_social = 0;
    var total_caja_complementaria = 0;
    var total_ganancias = 0;
    var total_fondo_solidario = 0;
    var total_sadop = 0;
    var total_uda = 0;


    var a = 0;
    // Headers de la tabla y datos
    for (let i = 0; i < headers.length; i++) {
      //   // Distancia de columnas con Offset (1)s
      let distance = i + 2;

      ws.cell(6, distance).string(headers[i]).style(styles.headers);

      for (a; a < docs.length; a++) {

        var row = 8 + a;
        var c_aportes = 0;
        var s_aportes = 0;
        var familiar = 0;
        var descuento = 0;
        var jubilacion = 0;
        var dec_137_5 = 0;
        var ley_19032 = 0;
        var obra_social = 0;
        var obra_social_familiar = 0;
        var diferencia_obra_social = 0;
        var caja_complementaria = 0;
        var ganancias = 0;
        var fondo_solidario = 0;
        var sadop = 0;
        var uda = 0;

        ws.cell(row, 2).string(docs[a].nombre).style(styles.rows);
        ws.cell(row, 3).string(docs[a].nivel).style(styles.rows);
        ws.cell(row, 4).string(docs[a].cuil).style(styles.rows);

        if(!agrupar){
          var antiguedad = docs[a].antiguedad.anios+'año/s '+ docs[a].antiguedad.meses + 'mes/es';
          ws.cell(row, 20).string(antiguedad).style(styles.rows);
        }
        


        for (var x = 0; x < docs[a].conceptos.length; x++) {
          
          for (let z = 0; z < docs[a].conceptos[x].length; z++) {

            monto = docs[a].conceptos[x][z].calculado == null ? 0 : docs[a].conceptos[x][z].calculado;

            switch (docs[a].conceptos[x][z].tipo_remuneracion) {

              case 1:
                c_aportes = c_aportes + monto;
                break;

              case 2:

                if (docs[a].conceptos[x][z].codigo.substring(2, 3) == "M") {
                  s_aportes = s_aportes + monto;
                  familiar = familiar + monto;
                } else {
                  s_aportes = s_aportes + monto;
                }
                break;

              case 3:
                descuento = descuento + monto;
                break;
            }

            switch (docs[a].conceptos[x][z].orden) {
              case 24:
                jubilacion = jubilacion + monto;
                break;
              case 56:
                dec_137_57 = dec_137_5 + monto;
                break;
              case 25:
                ley_19032 = ley_19032 + monto;
                break;
              case 30:
                obra_social = obra_social + monto;
                break;
              case 31:
                obra_social_familiar = obra_social_familiar + monto;
                break;
              case 32:
                diferencia_obra_social = diferencia_obra_social + monto;
                break;
              case 26:
                caja_complementaria = caja_complementaria + monto;
                break;
              case 33:
                ganancias = ganancias + monto;
                break;
              case 35:
                fondo_solidario = fondo_solidario + monto;
                break;
              case 34:
                sadop = sadop + monto;
                break;
              case 57:
                uda = uda + monto;
                break;
            }
          }
        }

        ws.cell(row, 5).string("$ " + c_aportes.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 6).string("$ " + s_aportes.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 7).string("$ " + descuento.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 8).string("$ " + familiar.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 9).string("$ " + jubilacion.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 10).string("$ " + dec_137_5.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 11).string("$ " + ley_19032.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 12).string("$ " + caja_complementaria.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 13).string("$ " + obra_social.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 14).string("$ " + obra_social_familiar.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 15).string("$ " + diferencia_obra_social.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 16).string("$ " + ganancias.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 17).string("$ " + sadop.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 18).string("$ " + fondo_solidario.toFixed(2).toString()).style(styles.rows);
        ws.cell(row, 19).string("$ " + uda.toFixed(2).toString()).style(styles.rows);

        total_c_aportes = total_c_aportes + c_aportes;
        total_s_aportes = total_s_aportes + s_aportes;
        total_descuento = total_descuento + descuento;
        total_familiar = total_familiar + familiar;
        total_jubilacion = total_jubilacion + jubilacion
        total_dec_137_5 = total_dec_137_5 + dec_137_5;
        total_ley_19032 = total_ley_19032 + ley_19032;
        total_caja_complementaria = total_caja_complementaria + caja_complementaria;
        total_obra_social = total_obra_social + obra_social;
        total_obra_social_familiar = total_obra_social_familiar + obra_social_familiar;
        total_diferencia_obra_social = total_diferencia_obra_social + diferencia_obra_social;
        total_ganancias = total_ganancias + ganancias;
        total_sadop = total_sadop + sadop;
        total_fondo_solidario = total_fondo_solidario + fondo_solidario;
        total_uda = total_uda + uda;
      }
    }
    ws.cell(9 + a, 2).string('TOTAL').style(styleTotal);
    ws.cell(9 + a, 3).string('').style(styleTotal);
    ws.cell(9 + a, 4).string('').style(styleTotal);
    ws.cell(9 + a, 5).string('$ ' + total_c_aportes.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 6).string('$ ' + total_s_aportes.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 7).string('$ ' + total_descuento.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 8).string('$ ' + total_familiar.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 9).string('$ ' + total_jubilacion.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 10).string('$ ' + total_dec_137_5.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 11).string('$ ' + total_ley_19032.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 12).string('$ ' + total_caja_complementaria.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 13).string('$ ' + total_obra_social.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 14).string('$ ' + total_obra_social_familiar.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 15).string('$ ' + total_diferencia_obra_social.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 16).string('$ ' + total_ganancias.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 17).string('$ ' + total_sadop.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 18).string('$ ' + total_fondo_solidario.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 19).string('$ ' + total_uda.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 20).string('').style(styleTotal);
    ws.column(1).setWidth(1);
    ws.column(2).setWidth(30);
    ws.column(3).setWidth(20);
    ws.column(4).setWidth(13);
    ws.column(5).setWidth(10);
    wb.write(titulo + '.xlsx', res);
  });

}

module.exports = {
  sueldosBruto
};
