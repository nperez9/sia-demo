const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const money = require('../../helpers/moneyHelper');
const moneyToSting = require('../../helpers/moneyToStringHelper');

const mongoose = require('mongoose');
const moment = require('moment');
const fs = require('fs');
var pdf = require('html-pdf');
const mustache = require('mustache');
const objectID = mongoose.Types.ObjectId;

const Empresas = mongoose.model('empresas');
const Liquidaciones = mongoose.model('liquidaciones');

function sueldos(req, res) {
    const periodo = req.body.periodo;
    const empresa = req.body.empresa;
    const type = req.body.type == "true" ? true : false;

    let periodoID = new objectID(periodo);
    let empresaID = new objectID(empresa);

    Empresas.findOne({ _id: empresaID }).exec((err, empresa) => {

        var view = {
            show: type,
            empresa: {
                nombre: empresa.razon_social,
                domicilio: empresa.direccion,
                actividad: empresa.actividad,
                cuit: empresa.cuit,
                registro: empresa.registro
            },
            legajos: []
        }

        var html = "";

        let match = {
            'periodo._id': periodoID,
            'legajo.subempresa.empresa._id': empresaID,
            'legajo.estado' : {$ne : "baja"}
        };

        let group = {
            _id: '$_id',
            legajo: { $first: '$legajo.numero_legajo' },
            nombre: { $first: '$legajo.persona.nombre' },
            dni: { $first: '$legajo.persona.dni' },
            cuil: { $first: '$legajo.persona.cuil' },
            fecha_alta_instituto: { $first: '$legajo.persona.datos_administrativos.fecha_alta' },
            cargo: { $first: '$legajo.cargo.nombre' },
            fecha_alta: { $first: '$legajo.fecha_alta' },
            domicilio: { $first: '$legajo.persona.domicilio' },
            periodo: { $first: '$periodo.descripcion' },
            neto: { $first: '$neto' },
            remunerativo: { $first: '$remunerativo' },
            deducciones: { $first: '$deducciones' },
            horas: { $first: '$legajo.horas' },
            situacion: { $first: '$legajo.persona.datos_administrativos.situacion.codigo' },
            adherentes: { $first: '$legajo.persona.datos_administrativos.adherentes.adherentes' },
            conceptos: { $push: '$conceptos' }
        };

        Liquidaciones.aggregate([
            { $match: match },
            { $unwind: '$legajo' },
            {
                $group: group
            },
            { $sort: { nombre: 1 } }
        ]).exec((err, docs) => {

            for (var a = 0; a < docs.length; a++) {

                var legajo = {
                    numero: docs[a].legajo,
                    nombre: docs[a].nombre,
                    dni: docs[a].dni,
                    cuil: docs[a].cuil,
                    fecha_alta_instituto: moment(docs[a].fecha_alta_instituto).format('DD/MM/YYYY'),
                    cargo: docs[a].cargo,
                    fecha_alta: moment(docs[a].fecha_alta).format('DD/MM/YYYY'),
                    domicilio: docs[a].domicilio.calle + " " + docs[a].domicilio.numero + " - " + docs[a].domicilio.localidad,
                    periodo: docs[a].periodo,
                    neto: money(docs[a].neto).substr(1),
                    remunerativo: money(docs[a].remunerativo).substr(1),
                    deducciones: money(docs[a].deducciones).substr(1),
                    conceptos_remunerativos: [],
                    conceptos_deducciones: [],
                    leyenda : false
                }

                if(docs[a].situacion == 13){
                    legajo.leyenda = true;
                    legajo.texto = "Licencia sin sueldo";
                }
                if(docs[a].situacion == 14){
                    legajo.leyenda = true;
                    legajo.texto = "Reserva de puesto";
                }
                else{
                    for (var x = 0; x < docs[a].conceptos.length; x++) {
                        for (let z = 0; z < docs[a].conceptos[x].length; z++) {
    
                            var unConcepto = {};
    
                            var reg = /\b(legajo->horas)\b/g
                            if (reg.test(docs[a].conceptos[x][z].formula)) {
                                unConcepto.cantidad = docs[a].horas
                            } else if (docs[a].conceptos[x][z].cantidad != undefined) {
                                unConcepto.cantidad = docs[a].conceptos[x][z].cantidad
                            } else {
                                unConcepto.cantidad = "1"
                            }
    
                            // unConcepto.cantidad = docs[a].conceptos[x][z].cantidad == undefined ? docs[a].legajos.horas : docs[a].conceptos[x][z].cantidad;
                            unConcepto.descripcion = docs[a].conceptos[x][z].descripcion;
                            monto = docs[a].conceptos[x][z].calculado == null ? 0 : docs[a].conceptos[x][z].calculado;
                            unConcepto.valor = money(monto).substr(1);
    
                            switch (docs[a].conceptos[x][z].tipo_remuneracion) {
                                case 1:
                                    legajo.conceptos_remunerativos.push(unConcepto);
                                    break;
    
                                case 2:
                                    // legajo.conceptos_remunerativos.push(unConcepto);
                                    break;
    
                                case 3:
                                    legajo.conceptos_deducciones.push(unConcepto);
                                    break;
                            }
                        }
                    }

                }

                view.legajos.push(legajo);

                if (docs[a].adherentes != null && docs[a].adherentes.length > 0 && legajo.leyenda == false) {
                    var legajoAdherente = {
                        numero: docs[a].legajo,
                        nombre: docs[a].nombre,
                        dni: docs[a].dni,
                        cuil: docs[a].cuil,
                        adherente: true,
                        adherentes: []
                    }

                    for (let i = 0; i < docs[a].adherentes.length; i++) {
                        var adherente = {
                            nombre_adherente: docs[a].adherentes[i].nombre,
                            dni_adherente: docs[a].adherentes[i].doc,
                            tipo_adherente: docs[a].adherentes[i].tipo
                        }

                        legajoAdherente.adherentes.push(adherente)
                    }

                    view.legajos.push(legajoAdherente);
                }
            }

            var content = fs.readFileSync("src/modules/reportes/templates/reporte.sueldos.tpl.html", 'utf8');
            html += mustache.render(content, view);

            Empresas.findOne({ _id: empresaID })
                .exec((err, empresa) => {
                    var options = {};
                    options.format = 'a4';

                    if (empresa.provincia == 'Ciudad de Buenos Aires') {
                        options.format = 'Legal';
                    }

                    pdf.create(html, options).toStream(function (err, stream) {
                        stream.pipe(res);
                    });
                });
        });
    });
}

module.exports = {
    sueldos
};
