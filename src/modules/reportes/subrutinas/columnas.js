function completaColumnas (legajo){
    // [CAMPO01]
    legajo.campo_1 = legajo.campo_1;

    // [CAMPO02]
    legajo.campo_2 = legajo.cuit_empresa == "30-57834962-2" ? (legajo.campo_2 + legajo._campo_3) : legajo.campo_2;
    
    // [CAMPO03]
    legajo.campo_3 =  legajo.cuit_empresa == "30-57834962-2" ? 0 : legajo.campo_3;

    // [CAMPO04]
    legajo.campo_4 = legajo.campo_4;
    
    // [CAMPO05]
    legajo.campo_5 = legajo.campo_5;
    
    // [CAMPO06]
    legajo.campo_6 = legajo.campo_6;
    
    // [CAMPO07]
    legajo.campo_7 = legajo.campo_7 == undefined ? 0 : legajo.campo_7;
    
    // [CAMPO08]
    legajo.campo_8 = legajo.campo_8;

    // [CAMPO09]
    legajo.campo_9 = legajo.empresa_familiar == true ? legajo.campo_9 : 0;

    // [CAMPO10]
    legajo.campo_10 = legajo.campo_10;

    // [CAMPO11]
    legajo.campo_11 = legajo.campo_11;

    // [CAMPO12]
    legajo.campo_12 = legajo.campo_12;

    // [CAMPO13]
    legajo.campo_13 = legajo.campo_13;

    // [CAMPO14]
    legajo.campo_14 =legajo.campo_14;

    // [CAMPO15]
    legajo.campo_15 = legajo.campo_15;

    // [CAMPO16]
    legajo.campo_16 = legajo.campo_16;

    // [CAMPO17]
    legajo.campo_17 = legajo.campo_17;

    // [CAMPO18]
    legajo.campo_18 = legajo.campo_18;

    // [CAMPO19]
    legajo.campo_18 = legajo.campo_2 < 0 ? (legajo.campo_2 * -1) : legajo.campo_19;
    
    // [CAMPO20]
    legajo.campo_20 = legajo.campo_20;

    // [CAMPO21]
    legajo.campo_21 = legajo.campo_21;

    // [CAMPO22]
    legajo.campo_22 = legajo.campo_22;

    // [CAMPO23]
    legajo.campo_23 = legajo.campo_23;

    // [CAMPO24]
    legajo.campo_24 = legajo.campo_24;

    // [CAMPO25]
    legajo.campo_25 = (legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6+legajo.campo_7+legajo.campo_8+legajo.campo_9)

    // [CAMPO26]
    legajo.campo_26 = legajo.categoria.substr(0,1) == "C" ? (legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6) : (legajo.condicion == 2 && legajo.codigo_actividad != 16) ? (legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6): 0;
    
    // [CAMPO27]
    legajo.campo_27 = legajo.categoria.substr(0,1) != "P" ? (legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6) : 0;

    // [CAMPO28]
    legajo.campo_28 = legajo.codigo_actividad == 49 ? (legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6) : 0;

    // [CAMPO29]
    legajo.campo_29 = legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6+legajo.campo_8;

    // [CAMPO30]
    legajo.campo_30 = legajo.categoria.substr(0,1) != "P" ? (legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6) : 0;

    // [CAMPO31]
    legajo.campo_31 = legajo.categoria.substr(0,1) == "A" && legajo.condicion != 2 ? (legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6) : 0;
    
    // [CAMPO32]
    legajo.campo_32 = legajo.categoria.substr(0,1) == "A" && legajo.condicion != 2 ? (legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6) : 0;

    // [CAMPO33]
    legajo.campo_33 = (legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6+legajo.campo_8) > ((legajo.campo_10 / .03) - 1) ? (legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6+legajo.campo_8) : legajo.campo_10 / .03;
    
    // [CAMPO34]
    legajo.campo_34 = legajo.art ? (legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6+legajo.campo_7+legajo.campo_8+legajo.campo_9) : (legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6+legajo.campo_7+legajo.campo_8);
    
    // [CAMPO35]
    legajo.campo_35 = legajo.campo_35;

    // [CAMPO36]
    legajo.campo_36 = legajo.campo_1 < 0 ? 0 : ((legajo.campo_1 > legajo.campo_27) ? legajo.campo_27 : (legajo.campo_1 - legajo.campo_19));

    // [CAMPO37]
    legajo.campo_37 = legajo.campo_2 < 0 ? 0 : (legajo.campo_2 > (legajo.campo_27 - legajo.campo_36)) ? (legajo.campo_27 - legajo.campo_36) : legajo.campo_2 ;
    
    // [CAMPO38]
    legajo.campo_38 = legajo.campo_3 < 0 ? 0 : (legajo.campo_3>(legajo.campo_27-legajo.campo_36-legajo.campo_37)) ? (legajo.campo_27-legajo.campo_36-legajo.campo_37) : legajo.campo_3;

    // [CAMPO39]
    legajo.campo_39 = legajo.campo_4 < 0 ? 0 : (legajo.campo_4 > legajo.campo_27-legajo.campo_36-legajo.campo_37-legajo.campo_38) ? legajo.campo_27-legajo.campo_36-legajo.campo_37-legajo.campo_38 : legajo.campo_4;

    // [CAMPO40]
    legajo.campo_40 = legajo.campo_5 < 0 ? 0 : (legajo.campo_5 > legajo.campo_27-legajo.campo_36-legajo.campo_37-legajo.campo_38-legajo.campo_39) ? legajo.campo_27-legajo.campo_36-legajo.campo_37-legajo.campo_38-legajo.campo_39 : legajo.campo_5;

    // [CAMPO41]
    legajo.campo_41 = legajo.campo_6 < 0 ? 0 : (legajo.campo_6 > legajo.campo_27-legajo.campo_36-legajo.campo_37-legajo.campo_38-legajo.campo_39-legajo.campo_40) ? (legajo.campo_6 > legajo.campo_27-legajo.campo_36-legajo.campo_37-legajo.campo_38-legajo.campo_39-legajo.campo_40) : legajo.campo_6;

    // [CAMPO42]
    legajo.campo_42 = legajo.codigo_actividad == 16 || legajo.codigo_actividad == 15 ? legajo.campo_7+legajo.campo_8+legajo.campo_9+legajo.campo_15 : legajo.campo_25-legajo.campo_27;

    // [CAMPO43]
    legajo.campo_43 = legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6+legajo.campo_8 > (legajo.campo_10/.03)-1 ? legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6+legajo.campo_8 : legajo.campo_10/.03;
    
    // [CAMPO44]
    legajo.campo_44 = legajo.campo_12;

    // [CAMPO45]
    legajo.campo_45 = legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6

    // [CAMPO46]
    legajo.campo_46 = legajo.campo_46;
    
    // [CAMPO47]
    legajo.campo_47 = legajo.campo_47;

    // [CAMPO48]
    legajo.campo_48 = legajo.campo_48;

    // [legajo.campo_49]
    legajo.campo_49 = legajo.categoria.substr(0,1) == "P" ? legajo.campo_1+legajo.campo_2+legajo.campo_3+legajo.campo_4+legajo.campo_5+legajo.campo_6 : 0;
    
    // [CAMPO50]
    legajo.campo_50 = legajo.categoria.substr(0,1) == "P" ? (legajo.campo_1 < 0) ? 0 : (legajo.campo_10>legajo.campo_49) ? legajo.campo_49-legajo.campo_54-legajo.campo_53-legajo.campo_52 : legajo.campo_1-legajo.campo_19 : 0;   
    
    // [CAMPO51]
    legajo.campo_51 = legajo.categoria.substr(0,1) == "P" ? (legajo.campo_2 < 0) ? 0 : (legajo.campo_2 > legajo.campo_49-legajo.campo_36) ? legajo.campo_49-legajo.campo_36 : legajo.campo_2 : 0;

    // [CAMPO52] 
    legajo.campo_52 = legajo.categoria.substr(0,1) == "P" ? (legajo.campo_3 < 0) ? 0 : (legajo.campo_3 > legajo.campo_49-legajo.campo_36-legajo.campo_37) ? legajo.campo_49-legajo.campo_36-legajo.campo_37 : legajo.campo_3 : 0;
    
    // [CAMPO53]
    legajo.campo_53 = legajo.categoria.substr(0,1) == "P" ? (legajo.campo_4 < 0) ? 0 : (legajo.campo_4 > legajo.campo_49-legajo.campo_36-legajo.campo_37-legajo.campo_38) ? (legajo.campo_49-legajo.campo_36-legajo.campo_37-legajo.campo_38) : legajo.campo_4 : 0;

    // [CAMPO54]
    legajo.campo_54 = legajo.categoria.substr(0,1) == "P" ? (legajo.campo_5 < 0) ? 0 :(legajo.campo_5 > legajo.campo_49-legajo.campo_36-legajo.campo_37-legajo.campo_38-legajo.campo_39) ? (legajo.campo_49-legajo.campo_36-legajo.campo_37-legajo.campo_38-legajo.campo_39) : legajo.campo_5 : 0;
    
    // [CAMPO55]
    legajo.campo_55 = legajo.categoria.substr(0,1) == "P" ? (legajo.campo_6 < 0) ? 0 : (legajo.campo_6>(legajo.campo_49-legajo.campo_36-legajo.campo_37-legajo.campo_38-legajo.campo_39-legajo.campo_40)) ? (legajo.campo_49-legajo.campo_36-legajo.campo_37-legajo.campo_38-legajo.campo_39-legajo.campo_40) : legajo.campo_6 : 0; 
    
    // [CAMPO56]
    legajo.campo_56 = legajo.categoria.substr(0,1) == "P" ? legajo.campo_42 : 0;

    // [CAMPO57]
    legajo.campo_57 = legajo.campo_57;

    // [CAMPO58]
    legajo.campo_58 = legajo.campo_58;

    // [CAMPO59]
    legajo.campo_59 = legajo.campo_59;

    // [CAMPO60]
    legajo.campo_60 = legajo.campo_60;

    return legajo;
}

module.exports = {
    completaColumnas   
};