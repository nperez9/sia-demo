
function modificaAporte (legajo) {

  let  aporte = {};
  aporte.empresa = legajo.empresa;
  aporte.periodo = legajo.periodo;

  //COLUMNA 1
  aporte.col01 = legajo.col01;

  aporte.col02 = legajo.col02;
  
  aporte.col03 = legajo.col03;
  
  aporte.col04 = legajo.col04;
  
  aporte.col05 = (legajo.col05 == '13' && (legajo.col14 + legajo.col23)>1) ? "01" : legajo.col05;
  
  aporte.col06 = legajo.col06;

  aporte.col07 = (legajo.col07 == '16' && legajo.col14 > 1) ? "039" : legajo.col07;
  
  aporte.col08 = legajo.col08;

  aporte.col09 = legajo.col09;

  aporte.col10 = legajo.col10;

  aporte.col11 = legajo.col11;

  aporte.col12 = legajo.col12;

  aporte.col13 = legajo.col13;
  
  aporte.col14 = (legajo.col14 < 0 ? 0 : ((legajo.col14 + legajo.col23 + legajo.col48) == 0 ? (legajo.col05 != "13" && legajo.col05 != "14" ? 0.01 : legajo.col14) : legajo.col14));
  
  aporte.col15 = legajo.col15;
  
  aporte.col16 = legajo.col16;
  
  aporte.col17 = legajo.col17;
  
  aporte.col18 = legajo.col18;
  
  aporte.col19 = legajo.col19;

  aporte.col20 = legajo.col20;
  
  aporte.col21 = (legajo.col07 == "016") 
                  ? legajo.col59 + legajo.col60 + legajo.col61 +legajo.col62+legajo.col63+legajo.col64
                  : (legajo.col21 < 0 ? 0 : legajo.col21);
  
  aporte.col22 = legajo.col22;
  
  aporte.col23 =(legajo.col23 == 0 && legajo.col06 == "02")  ? legajo.col21 : legajo.col23;
  
  aporte.col24 = legajo.col24;
  
  aporte.col25 = legajo.col25;
  
  aporte.col26 = legajo.col26;
  
  aporte.col27 = legajo.col27;
  
  aporte.col28 = legajo.col28;
  
  aporte.col29 = legajo.col29;

  aporte.col30 = (legajo.col30 == "13" && (legajo.col14 + legajo.col23) > 1) ? "01" : legajo.col30;
  
  aporte.col31 = legajo.col31;
  
  aporte.col32 = legajo.col32;
  
  aporte.col33 = legajo.col33;
  
  aporte.col34 = legajo.col34;
  
  aporte.col35 = legajo.col35;
  
  aporte.col36 = (legajo.col07 == "016") ? legajo.col59 : legajo.col36;
  
  aporte.col37 = (legajo.col07 == "016") ? legajo.col63 : (legajo.col37 < 0) ? 0 : legajo.col37;
  
  aporte.col38 = (legajo.col07 =="016") ? legajo.col62 : (legajo.col38 < 0) ? 0 : legajo.col38;
  
  aporte.col39 = legajo.col39;
  
  aporte.col40 = (legajo.col07 == "016") ? legajo.col64 : (legajo.col40 < 0) ? 0 : legajo.col40;
  
  aporte.col41 = (legajo.col41 > 30) ? 30 : legajo.col41;
  
  aporte.col42 = (legajo.col42 == 0) ? (legajo.col23 >= 0 && legajo.col05 != "13" && legajo.col05 != "14") ? 0.01 : legajo.col42 : legajo.col42;

  aporte.col43 = legajo.col43;
  
  aporte.col44 = legajo.col44;
  
  aporte.col45 = legajo.col45;
  
  aporte.col46= (legajo.col07 == "016") ? legajo.col60 : (legajo.col46 < 0) ? 0 : legajo.col46;
  
  aporte.col47 = (legajo.col07 == "016") ? legajo.col61 : (legajo.col47 < 0) ? 0 : legajo.col47;
  
  aporte.col48 = legajo.col48;
  
  aporte.col49 = legajo.col49;
  
  aporte.col50 = legajo.col50;

  aporte.col51 = (legajo.col42 == 0.01 && (legajo.col36+legajo.col37+legajo.col38+legajo.col40+legajo.col46+legajo.col47+legajo.col51) == 0)  ? 0.01 : (legajo.col07 =="016") ? legajo.col65 : legajo.col51;
  
  aporte.col52 = legajo.col52;
  
  aporte.col53 = legajo.col53;
  
  aporte.col54 = (legajo.col05 != "13" && legajo.col05 != "14" && legajo.col54< 1969.12,1969.12) ? legajo.col54 : 0;
  
  aporte.col55 = legajo.col55;
  
  aporte.col56 = legajo.col56;
  
  aporte.col57 = legajo.col57;
  
  aporte.col58 = legajo.col58;
  
  aporte.col59 = legajo.col59;

  aporte.col60 = legajo.col60;
  
  aporte.col61 = legajo.col61;

  aporte.col62 = legajo.col62;

  aporte.col63 = legajo.col63;

  aporte.col64 = legajo.col64;

  aporte.col65 = legajo.col65;

  return aporte;
}

module.exports = {
  modificaAporte   
};