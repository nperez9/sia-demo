function completaColumnas (legajo){
    aporte = {};
    aporte.empresa = legajo.empresa;
    aporte.periodo = legajo.periodo;
    
    // [Columna01]
    aporte.col01 = legajo.cuil.substr(0,2)+legajo.cuil.substr(3,8)+legajo.cuil.substr(12)
    
    // [Columna02]
    aporte.col02 = legajo.nombre.toUpperCase();
    
    // [Columna03]
    aporte.col03 = legajo.conyuge ? "1" : "0";

    // [Columna04]
    aporte.col04 = legajo.cantidad_hijos.toString();
    
    // [Columna05]
    aporte.col05 = legajo.situacion == 1 && (legajo.campo_29+legajo.campo_27+legajo.campo_33+legajo.campo_25) < 0.02 ? "13" : legajo.situacion.toString();

    // [Columna06]
    aporte.col06 = legajo.condicion.toString();
    
    // [Columna07]
    aporte.col07 = legajo.codigo_actividad == 16 && legajo.categoria.substr(0,1) != "P" ? "039" : "0"+legajo.codigo_actividad.toString();

    // [Columna08]
    aporte.col08 = legajo.codigo_zona.toString();
    
    // [Columna09]
    aporte.col09 = 0;
    
    // [Columna10]
    aporte.col10 = legajo.modalidad.toString();

    // [Columna11]
    aporte.col11 = legajo.codigo_os.toString();
    
    // [Columna12]
    aporte.col12 = legajo.adherentes == 0 ? "00" : legajo.adherentes.toString();
    
    // [Columna13]
    aporte.col13 = legajo.campo_25;
    
    // [Columna14]
    aporte.col14 = legajo.modalidad == 27 ? 0.01 : legajo.campo_26;
    
    // [Columna15]
    aporte.col15 = 0;
    
    // [Columna16]
    aporte.col16 = 0;
    
    // [Columna17]
    aporte.col17 = legajo.campo_46 * 2;
    
    // [Columna18]
    aporte.col18 = 0;
    
    // [Columna19]
    aporte.col19 = 0;
    
    // [Columna20]
    aporte.col20 = legajo.localidad == undefined ? " " : legajo.localidad.toUpperCase() ;
    
    // [Columna21]
    aporte.col21 = legajo.campo_27;
    
    // [Columna22]
    aporte.col22 = legajo.campo_28;
    
    // [Columna23]
    aporte.col23 = legajo.campo_43;
    
    // [Columna24]
    aporte.col24 = "00";
    
    // [Columna25]
    aporte.col25 = legajo.tipo == 1 ? "0" : "1";
    
    // [Columna26]
    aporte.col26 = 0;
    
    // [Columna27]
    aporte.col27 = legajo.tipo.toString();
    
    // [Columna28]
    aporte.col28 = legajo.campo_44 + legajo.campo_46;
    
    // [Columna29]
    aporte.col29 = "1";
    
    // [Columna30]
    aporte.col30 = legajo.situacion == 1 && (legajo.campo_29+legajo.campo_27+legajo.campo_33+legajo.campo_25)<0.02 ? 13 : legajo.situacion;
    
    // [Columna31]
    aporte.col31 = "01";
    
    // [Columna32]
    aporte.col32 = "01";
    
    // [Columna33]
    aporte.col33 = "00";
    
    // [Columna34]
    aporte.col34 = "01";
    
    // [Columna35]
    aporte.col35 = "00";
    
    // [Columna36]
    aporte.col36 = legajo.campo_36;
    
    // [Columna37]
    aporte.col37 = legajo.campo_40;
    
    // [Columna38]
    aporte.col38 = legajo.campo_39;
    
    // [Columna39]
    aporte.col39 = 0;
    
    // [Columna40]
    aporte.col40 = legajo.campo_40;
    
    // [Columna41]
    aporte.col41 = legajo.dias_trabajados < 1 ? 1 : legajo.dias_trabajados;
    
    // [Columna42]
    aporte.col42 = legajo.campo_30 < 0 ? 0.01 : (legajo.modalidad == 27) ? 0.01 : legajo.campo_30;
    
    // [Columna43]
    aporte.col43 = legajo.convenio_colectivo ?  "1" : "0";
    
    // [Columna44]
    aporte.col44 = legajo.campo_31;

    // [Columna45]
    aporte.col45 = 0;
    
    // [Columna46]
    aporte.col46 = legajo.campo_37;
    
    // [Columna47]
    aporte.col47 = legajo.campo_38;
    
    // [Columna48]
    aporte.col48 = legajo.modalidad == 27 ? legajo.campo_07+legajo.campo_08 : legajo.campo_33;
    
    // [Columna49]
    aporte.col49 = legajo.campo_32;

    // [Columna50]
    aporte.col50 = legajo.campo_39 != 0 ? "001" : "000";
    
    // [Columna51]
    aporte.col51 = legajo.campo_42 > 0.01 ? legajo.campo_42 : 0;

    // [Columna52]
    aporte.col52 = 0;
    
    // [Columna53]
    aporte.col53 = 0;
    
    // [Columna54]
    aporte.col54 = legajo.campo_34;
     
    // [Columna55]
    aporte.col55 = 0;
    
    // [Columna56]
    aporte.col56 = "0";
    
    // [Columna57]
    aporte.col57 = legajo.seguro_vida ? "1" : "0";
    
    // [Columna58]
    aporte.col58 = 0;
    
    // [Columna59]
    aporte.col59 = legajo.campo_50;
    
    // [Columna60]
    aporte.col60 = legajo.campo_51;
    
    // [Columna61]
    aporte.col61 = legajo.campo_52;
    
    // [Columna62]
    aporte.col62 = legajo.campo_53;
    
    // [Columna63]
    aporte.col63 = legajo.campo_54;
    
    // [Columna64]
    aporte.col64 = legajo.campo_55;
    
    // [Columna65]
    aporte.col65 = legajo.campo_56;

    return aporte;
}

module.exports = {
    completaColumnas   
};