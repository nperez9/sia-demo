function completaColumnas (columnas){
    
    columnasTransformadas = {};

    columnasTransformadas.clase3 = columnas.clase3;
    columnasTransformadas.clase4 = columnas.clase4;
    columnasTransformadas.clase5 = columnas.clase5;
    columnasTransformadas.clase6 = columnas.clase6;
    columnasTransformadas.clase7 = columnas.clase7;
    columnasTransformadas.clase8 = columnas.clase8;
    columnasTransformadas.clase9 = columnas.clase9;
    columnasTransformadas.clase10 = columnas.clase10;
    columnasTransformadas.clase11 = columnas.clase11;
    columnasTransformadas.clase12 = columnas.clase12;
    columnasTransformadas.clase13 = columnas.clase13;
    columnasTransformadas.clase14 = columnas.clase14;
    columnasTransformadas.clase15 = columnas.clase15;
    columnasTransformadas.clase16 = columnas.clase16;
    columnasTransformadas.clase17 = columnas.clase17;
    columnasTransformadas.clase18 = columnas.clase18;
    columnasTransformadas.clase19 = columnas.clase19;
    columnasTransformadas.clase20 = columnas.clase20;
    columnasTransformadas.clase20 = columnas.clase20;
    columnasTransformadas.clase21 = columnas.clase21;
    columnasTransformadas.clase22 = columnas.clase22;
    columnasTransformadas.clase23 = columnas.clase23;
    columnasTransformadas.clase24 = columnas.clase24;
    columnasTransformadas.clase25 = columnas.clase25;
    columnasTransformadas.clase26 = columnas.clase26;
    columnasTransformadas.clase27 = columnas.clase27;
    columnasTransformadas.clase28 = columnas.clase28;
    columnasTransformadas.clase29 = columnas.clase29 - (columnas.clase27 * .03);
    columnasTransformadas.clase30 = columnas.clase30;
    columnasTransformadas.clase31 = columnas.clase31 - columnas.clase32;
    columnasTransformadas.clase32 = columnas.clase32;
    columnasTransformadas.clase33 = columnas.clase33;
    columnasTransformadas.clase34 = columnas.clase34;
    columnasTransformadas.clase35 = columnas.clase35;
    columnasTransformadas.clase36 = columnas.clase36;
    columnasTransformadas.clase37 = columnas.clase37;
    columnasTransformadas.clase38 = columnas.clase38;
    columnasTransformadas.clase39 = columnas.clase39;
    columnasTransformadas.clase40 = columnas.clase40;
    // columnasTransformadas.clase41 = columnas.clase41;
    // columnasTransformadas.clase42 = columnas.clase42;
    // columnasTransformadas.clase43 = columnas.clase43;
    columnasTransformadas.clase44 = columnas.clase29 != 0 ? (columnas.clase28/.16)*.084 :(columnas.clase28/.11)*.0781 ;
    columnasTransformadas.clase45 = (columnas.clase3+columnas.clase4+columnas.clase5+columnas.clase6+columnas.clase7+columnas.clase8+columnas.clase9+columnas.clase10+columnas.clase11+columnas.clase12+columnas.clase13+columnas.clase14+columnas.clase15+columnas.clase16+columnas.clase17)>columnas.clase27/.03 && columnas.clase29 != 0 ? (columnas.clase3+columnas.clase4+columnas.clase5+columnas.clase6+columnas.clase7+columnas.clase8+columnas.clase9+columnas.clase10+columnas.clase11+columnas.clase12+columnas.clase13+columnas.clase14+columnas.clase15+columnas.clase16)*.06 : columnas.clase29*2; 

    return columnasTransformadas;
}

module.exports = {
    completaColumnas   
};