function pad(n, caracter, sentido, length) {
    var n = n.toString();
    while (n.length < length){

        if(sentido == "IZQ"){
            n = n + caracter;
        }
        if(sentido == "DER"){
            n = caracter + n;
        }
    }
    return n;
}

function darFormato(legajo) {
    var personaFormatoAfip = "";

    personaFormatoAfip += legajo.col01;

    personaFormatoAfip += pad(legajo.col02," ","IZQ",30);
    
    personaFormatoAfip += pad(legajo.col03,"0","IZQ",1);
    
    personaFormatoAfip += pad(legajo.col04,"0","IZQ",2);
    
    personaFormatoAfip += pad(legajo.col05,"0","DER",2);//AFIP 3
    
    personaFormatoAfip += pad(legajo.col06,"0","DER",2);//AFIP 3
    
    personaFormatoAfip += pad(legajo.col07,"0","IZQ",3);
        
    personaFormatoAfip += pad(legajo.col08,"0","DER",2);

    personaFormatoAfip += pad(legajo.col09.toFixed(2),"0","DER",5);

    personaFormatoAfip += pad(legajo.col10,"0","DER",3);
    
    personaFormatoAfip += pad(legajo.col11,"0","DER",6);
    
    personaFormatoAfip += pad(legajo.col12,"0","DER",2);
    
    personaFormatoAfip += pad(legajo.col13.toFixed(2),"0","DER",12);

    personaFormatoAfip += pad(legajo.col14.toFixed(2),"0","DER",12);
    
    personaFormatoAfip += pad(legajo.col15.toFixed(2),"0","DER",9);

    personaFormatoAfip += pad(legajo.col16.toFixed(2),"0","DER",9);
    
    personaFormatoAfip += pad(legajo.col17.toFixed(2),"0","DER",9);
    
    personaFormatoAfip += pad(legajo.col18.toFixed(2),"0","DER",9);
    
    personaFormatoAfip += pad(legajo.col19.toFixed(2),"0","DER",9);
    
    personaFormatoAfip += pad(legajo.col20," ","IZQ",50);

    personaFormatoAfip += pad(legajo.col21.toFixed(2),"0","DER",12);
    
    personaFormatoAfip += pad(legajo.col22.toFixed(2),"0","DER",12);

    personaFormatoAfip += pad(legajo.col23.toFixed(2),"0","DER",12);
    
    personaFormatoAfip += pad(legajo.col24,"0","DER",2);
    
    personaFormatoAfip += pad(legajo.col25,"0","DER",1);
    
    personaFormatoAfip += pad(legajo.col26.toFixed(2),"0","DER",9);
    
    personaFormatoAfip += pad(legajo.col27," ","DER",1);
    
    personaFormatoAfip += pad(legajo.col28.toFixed(2),"0","DER",9);
    
    personaFormatoAfip += pad(legajo.col29,"0","IZQ",1);
    
    personaFormatoAfip += pad(legajo.col30,"0","DER",2);

    personaFormatoAfip += pad(legajo.col31,"0","DER",2);

    personaFormatoAfip += pad(legajo.col32,"0","IZQ",2);

    personaFormatoAfip += pad(legajo.col33,"0","IZQ",2);

    personaFormatoAfip += pad(legajo.col34,"0","IZQ",2);

    personaFormatoAfip += pad(legajo.col35,"0","IZQ",2);

    personaFormatoAfip += pad(legajo.col36.toFixed(2),"0","DER",12);

    personaFormatoAfip += pad(legajo.col37.toFixed(2),"0","DER",12);

    personaFormatoAfip += pad(legajo.col38.toFixed(2),"0","DER",12);

    personaFormatoAfip += pad(legajo.col39.toFixed(2),"0","DER",12);

    personaFormatoAfip += pad(legajo.col40.toFixed(2),"0","DER",12);
    
    personaFormatoAfip += pad(legajo.col41.toFixed(2),"0","DER",9); 

    personaFormatoAfip += pad(legajo.col42.toFixed(2),"0","DER",12);
    
    personaFormatoAfip += pad(legajo.col43,"0","DER",1);

    personaFormatoAfip += pad(legajo.col44.toFixed(2),"0","DER",12);

    personaFormatoAfip += pad(legajo.col45,"0","DER",1);

    personaFormatoAfip += pad(legajo.col46.toFixed(2),"0","DER",12);
    
    personaFormatoAfip += pad(legajo.col47.toFixed(2),"0","DER",12);

    personaFormatoAfip += pad(legajo.col48.toFixed(2),"0","DER",12);

    personaFormatoAfip += pad(legajo.col49.toFixed(2),"0","DER",12);
    
    personaFormatoAfip += pad(legajo.col50, "0","DER",3);

    personaFormatoAfip += pad(legajo.col51.toFixed(2),"0","DER",12); //TXT DIF. A APO DIF A AFIP

    personaFormatoAfip += pad(legajo.col52.toFixed(2),"0","DER",12); //TXT DIF. A APO DIF A AFIP

    personaFormatoAfip += pad(legajo.col53.toFixed(2),"0","DER",9);

    personaFormatoAfip += pad(legajo.col54.toFixed(2),"0","DER",12);

    personaFormatoAfip += pad(legajo.col55.toFixed(2),"0","DER",9);

    personaFormatoAfip += pad(legajo.col56,"0","DER",3);

    personaFormatoAfip += pad(legajo.col57,"0","DER",1);

    personaFormatoAfip += "          ";

    personaFormatoAfip += pad(legajo.col59.toFixed(2),"0","DER",10);

    personaFormatoAfip += pad(legajo.col60.toFixed(2),"0","DER",10);

    personaFormatoAfip += pad(legajo.col61.toFixed(2),"0","DER",10);

    personaFormatoAfip += pad(legajo.col62.toFixed(2),"0","DER",10);

    personaFormatoAfip += pad(legajo.col63.toFixed(2),"0","DER",10);

    personaFormatoAfip += pad(legajo.col64.toFixed(2),"0","DER",10);

    personaFormatoAfip += pad(legajo.col65.toFixed(2),"0","DER",10);

    personaFormatoAfip = personaFormatoAfip.split(".").join(",");

    return personaFormatoAfip
}

module.exports = {
    darFormato   
  };