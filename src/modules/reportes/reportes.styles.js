const excel4node = require('excel4node');

var wb = new excel4node.Workbook();

var title = wb.createStyle({
  font: {
    name: 'Arial',
    color: '#000000',
    size: 14,
    bold: true,
  }
});

var datas = wb.createStyle({
  font: {
    name: 'Arial',
    color: '#000000',
    size: 8,
    italics: true
  }
});

var headers = wb.createStyle({
  font: {
    name: 'Arial',
    color: '#000000',
    size: 8,
    bold: true
  },
  border: {
    bottom: {
      style: 'medium',
      color: '#000000'
    }
  }
});

var rows = wb.createStyle({
  font: {
    name: 'Arial',
    color: '#000000',
    size: 8,
    italics: true
  }
});

//estilos para reporte no remu.
var colorGreen = wb.createStyle({
  font: {
    size: 10,
  },
  fill: {
    type: 'pattern', 
    patternType: 'solid', 
    fgColor: '#A9F5BC'
  },
  border: {
    left: {
      style: 'thin', 
      color: '#000000' 
    },
    right: {
      style: 'thin',
      color: '#000000'
    },
    top: {
      style: 'thin',
      color: '#000000'
    },
    bottom: {
      style: 'thin',
      color: '#000000'
    }
  }
});

var colorYellow = wb.createStyle({
  font: {
    size: 10,
  },
  fill: {
    type: 'pattern', 
    patternType: 'solid', 
    fgColor: '#F4FA58'
  },
  border: {
    left: {
      style: 'thin', 
      color: '#000000' 
    },
    right: {
      style: 'thin',
      color: '#000000'
    },
    top: {
      style: 'thin',
      color: '#000000'
    },
    bottom: {
      style: 'thin',
      color: '#000000'
    }
  }
});

var textHead = wb.createStyle({
  font: {
    size: 10,
  },
  font: {
    bold: true
  },
  border: {
    bottom: {
      style: 'medium',
      color: '#000000'
    },
    top: {
      style: 'medium',
      color: '#000000'
    } 
  }
});

var generalLines = wb.createStyle({
  font: {
    size: 10,
  },
  border: {
    left: {
      style: 'thin', 
      color: '#000000' 
    },
    right: {
      style: 'thin',
      color: '#000000'
    },
    top: {
      style: 'thin',
      color: '#000000'
    },
    bottom: {
      style: 'thin',
      color: '#000000'
    }
  }
});

var leftLine = wb.createStyle({
  border: {
    left: {
      style: 'medium', 
      color: '#000000' 
    }
  }
});

var rightLine = wb.createStyle({
  border: {
    right: {
      style: 'medium', 
      color: '#000000' 
    }
  }
});

var subEmpresasHeaders = wb.createStyle({
  font: {
    size: 10,
    bold: true
  },
  border: {
    top: {
      style: 'medium',
      color: '#000000'
    },
    bottom: {
      style: 'medium',
      color: '#000000'
    }
  }
});

module.exports = {
  title,
  datas,
  headers,
  rows,
  colorGreen,
  colorYellow,
  textHead,
  generalLines,
  leftLine,
  rightLine,
  subEmpresasHeaders
};