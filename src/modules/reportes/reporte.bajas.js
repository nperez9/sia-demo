const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');

const mongoose = require('mongoose');
const moment = require('moment');
const fs = require('fs'); 
var pdf = require('html-pdf');
const mustache = require('mustache');
const objectID = mongoose.Types.ObjectId;

// const Personas = mongoose.model('personas');
// const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');

function bajas(req, res) {
    const periodo = req.body.periodo;
    const empresa = req.body.empresa;

    let periodoID = new objectID(periodo);
    let empresaID = new objectID(empresa);

    var find = { 'legajo.subempresa.empresa._id': empresa, 'periodo._id': periodo };

    Liquidaciones.find(find, (err, liquidaciones) => {

        var bajas = [];
        
        for (let a = 0; a < liquidaciones.length; a++) {
            
            if (liquidaciones[a].legajo.fecha_baja && liquidaciones[a].legajo.estado == "normal") {
                var unaBaja = {};
                unaBaja.nombre = liquidaciones[a].legajo.persona.nombre;
                unaBaja.fecha = moment(liquidaciones[a].legajo.fecha_baja).format('DD/MM/YYYY');
                bajas.push(unaBaja);
            }
        }

        var view = {
            bajas: bajas,
            fecha_actual: moment().format('DD/MM/YYYY')
        }

        var content = fs.readFileSync("src/modules/reportes/templates/reporte.bajas.tpl.html", 'utf8');
        html = mustache.render(content, view);

        var options = {
            format: 'a4'
        };

        pdf.create(html, options).toStream(function (err, stream) {
            stream.pipe(res);
        });
    });
}

module.exports = {
    bajas
};
