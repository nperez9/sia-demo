const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./reportes.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const objectID = mongoose.Types.ObjectId;

const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');


function asientosContables(req, res) {
  const periodo = req.body.periodo;
  const empresa = req.body.empresa;
  let periodoID = new objectID(periodo._id);
  let empresaID = new objectID(empresa._id);

  var workBook = new excel4node.Workbook();
  var ws = workBook.addWorksheet(titulo);

  var styleTotal = workBook.createStyle({
    font: {
      name: 'Arial',
      color: '#000000',
      size: 8,
      bold: true
    },
    border: {
      top: {
        style: 'medium',
        color: '#000000'
      }
    }
  });

  var titulo = 'Asiento Contable';
  var headers = [
    'Descripción',
    'C / Aporte',
    'S / Aporte',
    'A / Familiar',
    'Descuento'
  ];

  var match = {
    'periodo._id': periodoID,
    'legajo.subempresa.empresa._id': empresaID
  }

  // Trae toda la papota
  Liquidaciones.aggregate([
    { $match: match },
    { $unwind: '$conceptos' },
    // {$project: {_id:1}},
    {
      $group: {
        _id: '$conceptos._id',
        nombre: { $first: '$conceptos.descripcion' },
        empresa: { $first: '$legajo.subempresa.empresa' },
        periodo: { $first: '$periodo.descripcion' },
        sumatoria: { $sum: '$conceptos.calculado' },
        tipo_remuneracion: { $first: '$conceptos.tipo_remuneracion' },
        codigo: { $first: '$conceptos.codigo' },
        count: { $sum: 1 }
      }
    },
    { $sort: { tipo_remuneracion: 1 } }
  ]).exec((err, docs) => {

    if (docs.length < 1) {
      return res.status(504).json({success: false , message: 'El reporte no posee registros'})
    }
    
    // primero va la ROW  y despues el  columna   
    ws.cell(1, 2).string(docs[0].empresa.razon_social).style(styles.title);
    ws.cell(2, 2).string('Fecha: ' + moment().format('DD/MM/YYYY')).style(styles.datas);
    ws.cell(3, 2).string(titulo).style(styles.datas);
    ws.cell(4, 2).string('Periodo: ' + docs[0].periodo).style(styles.datas);

    var total_c_aportes = 0;
    var total_s_aportes = 0;
    var total_familiar = 0;
    var total_descuento = 0;

    var a = 0;
    // Headers de la tabla y datos
    for (let i = 0; i < headers.length; i++) {
      //   // Distancia de columnas con Offset (1)s
      let distance = i + 2;

      ws.cell(6, distance).string(headers[i]).style(styles.headers);

      for (a; a < docs.length; a++) {
        var row = 8 + a;

        ws.cell(row, 2).string(docs[a].nombre).style(styles.rows);

        switch (docs[a].tipo_remuneracion) {

          case 1:
            ws.cell(row, 3).string("$ " + docs[a].sumatoria.toFixed(2).toString()).style(styles.rows);
            total_c_aportes = total_c_aportes + docs[a].sumatoria;
            break;

          case 2:

            if (docs[a].codigo.substring(2, 3) == "M") {
              ws.cell(row, 5).string("$ " + docs[a].sumatoria.toFixed(2).toString()).style(styles.rows);
              total_familiar = total_familiar + docs[a].sumatoria;
            } else {
              ws.cell(row, 4).string("$ " + docs[a].sumatoria.toFixed(2).toString()).style(styles.rows);
              total_s_aportes = total_s_aportes + docs[a].sumatoria;
            }
            break;

          case 3:
            ws.cell(row, 6).string("$ " + docs[a].sumatoria.toFixed(2).toString()).style(styles.rows);
            total_descuento = total_descuento + docs[a].sumatoria;
            break;
        }
      }
    }

    var neto = total_c_aportes + total_s_aportes + total_familiar - total_descuento
    ws.cell(9 + a, 2).string('TOTALES').style(styleTotal);
    ws.cell(9 + a, 3).string('$ ' + total_c_aportes.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 4).string('$ ' + total_s_aportes.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 5).string('$ ' + total_familiar.toFixed(2).toString()).style(styleTotal);
    ws.cell(9 + a, 6).string('$ ' + total_descuento.toFixed(2).toString()).style(styleTotal);
    ws.cell(10 + a, 2).string('NETO').style(styleTotal);
    ws.cell(10 + a, 3).string('').style(styleTotal);
    ws.cell(10 + a, 4).string('').style(styleTotal);
    ws.cell(10 + a, 5).string('').style(styleTotal);
    ws.cell(10 + a, 6).string('$ ' + neto.toFixed(2).toString()).style(styleTotal);

    ws.column(1).setWidth(1);
    ws.column(2).setWidth(30);
    workBook.write(titulo + '.xlsx', res);
  });

  //return res.json({'success': true, message: 'Mensaje enviado'});
}

module.exports = {
  asientosContables
};