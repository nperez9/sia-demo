const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./reportes.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const objectID = mongoose.Types.ObjectId;

const Liquidaciones = mongoose.model('liquidaciones');

function conceptosNoRemunerativos(req, res) {

    var wb = new excel4node.Workbook();
    var ws = wb.addWorksheet();
    var titulo = "Conceptos no remunerativos";

    const periodo = req.body.periodo;
    const empresa = req.body.empresa;

    let periodoID = new objectID(periodo);
    let empresaID = new objectID(empresa);

    var match = {
        'periodo._id': periodoID,
        'legajo.subempresa.empresa._id': empresaID,
        'conceptos.marcas': { $exists: true }
    }

    Liquidaciones.aggregate([
        { $match: match },
        { $unwind: '$legajo' },
        {
            $group: {
                _id: '$legajo.persona._id',
                periodo_descripcion: { $first: '$periodo.descripcion' },
                empresa: { $first: '$legajo.subempresa.empresa.razon_social' },
                legajos: { $push: '$legajo' },
                conceptos: { $push: '$conceptos' }
            }
        }
    ]).exec((err, docs) => {

        if (docs.length < 1) {
            return res.status(504).json({ success: false, message: 'El reporte no posee registros' })
        }

        ws.cell(1, 1).string('Establecimiento: ' + docs[0].empresa);
        ws.cell(2, 1).string('Periodo: ' + docs[0].periodo_descripcion);

        ws.cell(4, 1).string('CLASE').style(styles.colorGreen).style(styles.textHead);
        ws.cell(5, 1).string('').style(styles.generalLines).style(styles.leftLine);
        ws.cell(6, 1).string('10').style(styles.generalLines).style(styles.leftLine);
        ws.cell(7, 1).string('11').style(styles.generalLines).style(styles.leftLine);
        ws.cell(8, 1).string('12').style(styles.generalLines).style(styles.leftLine);
        ws.cell(9, 1).string('13').style(styles.colorYellow).style(styles.leftLine);
        ws.cell(10, 1).string('14').style(styles.colorYellow).style(styles.leftLine);
        ws.cell(11, 1).string('15').style(styles.generalLines).style(styles.leftLine);
        ws.cell(12, 1).string('16').style(styles.generalLines).style(styles.leftLine);
        ws.cell(13, 1).string('17').style(styles.generalLines).style(styles.leftLine);
        ws.cell(14, 1).string('18').style(styles.generalLines).style(styles.leftLine);
        ws.cell(15, 1).string('19').style(styles.generalLines).style(styles.leftLine);
        ws.cell(16, 1).string('20').style(styles.generalLines).style(styles.leftLine);
        ws.cell(17, 1).string('21').style(styles.generalLines).style(styles.leftLine);

        ws.cell(4, 2).string('CONCEPTO').style(styles.colorGreen).style(styles.textHead);
        ws.cell(5, 2).string('').style(styles.generalLines);
        ws.cell(6, 2).string('Asignacion Familiar').style(styles.generalLines);
        ws.cell(7, 2).string('Material Didactico').style(styles.generalLines);
        ws.cell(8, 2).string('Incentivo').style(styles.generalLines);
        ws.cell(9, 2).string('Vacac./Indem./Preavis, Etc.').style(styles.colorYellow);
        ws.cell(10, 2).string('Gratificaciones Especiales').style(styles.colorYellow);
        ws.cell(11, 2).string('Garantia').style(styles.generalLines);
        ws.cell(12, 2).string('Bonificacion No /Rem.').style(styles.generalLines);
        ws.cell(13, 2).string('Pasantia').style(styles.generalLines);
        ws.cell(14, 2).string('Ley de financiamiento educativo').style(styles.generalLines);
        ws.cell(15, 2).string('Haber minimo de bolsillo').style(styles.generalLines);
        ws.cell(16, 2).string('Garantia').style(styles.generalLines);
        ws.cell(17, 2).string('RESOLUCION 1321/15').style(styles.generalLines);

        ws.cell(4, 3).string('JURIDICCIÓN').style(styles.colorGreen).style(styles.textHead);
        ws.cell(5, 3).string('').style(styles.generalLines);
        ws.cell(6, 3).string('NACIONAL').style(styles.generalLines);
        ws.cell(7, 3).string('PROVINCIAL').style(styles.generalLines);
        ws.cell(8, 3).string('NACIONAL').style(styles.generalLines);
        ws.cell(9, 3).string('NACIONAL').style(styles.colorYellow);
        ws.cell(10, 3).string('NACIONAL').style(styles.colorYellow);
        ws.cell(11, 3).string('PROVINCIAL').style(styles.generalLines);
        ws.cell(12, 3).string('PROVINCIAL').style(styles.generalLines);
        ws.cell(13, 3).string('NACIONAL').style(styles.generalLines);
        ws.cell(14, 3).string('PROVINCIAL').style(styles.generalLines);
        ws.cell(15, 3).string('PROVINCIAL').style(styles.generalLines);
        ws.cell(16, 3).string('PROVINCIAL').style(styles.generalLines);
        ws.cell(17, 3).string('MUNICIPAL').style(styles.generalLines);

        ws.cell(4, 4).string('NORMA').style(styles.colorGreen).style(styles.textHead);
        ws.cell(5, 4).string('').style(styles.generalLines);
        ws.cell(6, 4).string('RESOLUCIÓN').style(styles.generalLines);
        ws.cell(7, 4).string('DECRETO').style(styles.generalLines);
        ws.cell(8, 4).string('RESOLUCIÓN').style(styles.generalLines);
        ws.cell(9, 4).string('LEY').style(styles.colorYellow);
        ws.cell(10, 4).string('LEY').style(styles.colorYellow);
        ws.cell(11, 4).string('DECRETO').style(styles.generalLines);
        ws.cell(12, 4).string('DECRETO').style(styles.generalLines);
        ws.cell(13, 4).string('LEY').style(styles.generalLines);
        ws.cell(14, 4).string('LEY').style(styles.generalLines);
        ws.cell(15, 4).string('DECRETO').style(styles.generalLines);
        ws.cell(16, 4).string('RESOLUCIÓN').style(styles.generalLines);
        ws.cell(17, 4).string('RESOLUCIÓN').style(styles.generalLines);

        ws.cell(4, 5).string('ORGANISMO').style(styles.colorGreen).style(styles.textHead);
        ws.cell(5, 5).string('').style(styles.generalLines);
        ws.cell(6, 5).string('OTROS').style(styles.generalLines);
        ws.cell(7, 5).string('').style(styles.generalLines);
        ws.cell(8, 5).string('OTROS').style(styles.generalLines);
        ws.cell(9, 5).string('').style(styles.colorYellow);
        ws.cell(10, 5).string('').style(styles.colorYellow);
        ws.cell(11, 5).string('').style(styles.generalLines);
        ws.cell(12, 5).string('').style(styles.generalLines);
        ws.cell(13, 5).string('').style(styles.generalLines);
        ws.cell(14, 5).string('').style(styles.generalLines);
        ws.cell(15, 5).string('').style(styles.generalLines);
        ws.cell(16, 5).string('OTROS').style(styles.generalLines);
        ws.cell(17, 5).string('OTROS').style(styles.generalLines);

        ws.cell(4, 6).string('(especificar)').style(styles.colorGreen).style(styles.textHead);
        ws.cell(5, 6).string('').style(styles.generalLines);
        ws.cell(6, 6).string('SEC. SEG. SOCIAL').style(styles.generalLines);
        ws.cell(7, 6).string('').style(styles.generalLines);
        ws.cell(8, 6).string('CJO.GREM.ENS.PRIV').style(styles.generalLines);
        ws.cell(9, 6).string('').style(styles.colorYellow);
        ws.cell(10, 6).string('').style(styles.colorYellow);
        ws.cell(11, 6).string('').style(styles.generalLines);
        ws.cell(12, 6).string('').style(styles.generalLines);
        ws.cell(13, 6).string('').style(styles.generalLines);
        ws.cell(14, 6).string('').style(styles.generalLines);
        ws.cell(15, 6).string('').style(styles.generalLines);
        ws.cell(16, 6).string('SSPECD').style(styles.generalLines);
        ws.cell(17, 6).string('MEGC').style(styles.generalLines);

        ws.cell(4, 7).string('N° NORMA').style(styles.colorGreen).style(styles.textHead);
        ws.cell(5, 7).string('').style(styles.generalLines);
        ws.cell(6, 7).string('71').style(styles.generalLines);
        ws.cell(7, 7).string('1294').style(styles.generalLines);
        ws.cell(8, 7).string('2').style(styles.generalLines);
        ws.cell(9, 7).string('20744').style(styles.colorYellow);
        ws.cell(10, 7).string('24241').style(styles.colorYellow);
        ws.cell(11, 7).string('444').style(styles.generalLines);
        ws.cell(12, 7).string('519').style(styles.generalLines);
        ws.cell(13, 7).string('26427').style(styles.generalLines);
        ws.cell(14, 7).string('26075').style(styles.generalLines);
        ws.cell(15, 7).string('490').style(styles.generalLines);
        ws.cell(16, 7).string('1386').style(styles.generalLines);
        ws.cell(17, 7).string('1321').style(styles.generalLines);

        ws.cell(4, 8).string('AÑO NORMA').style(styles.colorGreen).style(styles.textHead);
        ws.cell(5, 8).string('').style(styles.generalLines);
        ws.cell(6, 8).string('1999').style(styles.generalLines);
        ws.cell(7, 8).string('2007').style(styles.generalLines);
        ws.cell(8, 8).string('2004').style(styles.generalLines);
        ws.cell(9, 8).string('1993').style(styles.colorYellow);
        ws.cell(10, 8).string('1993').style(styles.colorYellow);
        ws.cell(11, 8).string('2007').style(styles.generalLines);
        ws.cell(12, 8).string('2008').style(styles.generalLines);
        ws.cell(13, 8).string('2008').style(styles.generalLines);
        ws.cell(14, 8).string('2006').style(styles.generalLines);
        ws.cell(15, 8).string('2004').style(styles.generalLines);
        ws.cell(16, 8).string('2014').style(styles.generalLines);
        ws.cell(17, 8).string('2015').style(styles.generalLines);

        ws.cell(4, 9).string('EMPLEADOS').style(styles.colorGreen).style(styles.textHead);
        ws.cell(4, 10).string('IMPORTE').style(styles.colorGreen).style(styles.textHead).style(styles.rightLine);;

        var clase_10 = 0;
        var clase_11 = 0;
        var clase_12 = 0;
        var clase_13 = 0;
        var clase_14 = 0;
        var clase_15 = 0;
        var clase_16 = 0;
        var clase_17 = 0;
        var clase_18 = 0;
        var clase_19 = 0;
        var clase_20 = 0;
        var clase_21 = 0;

        var importe_clase_10 = 0;
        var importe_clase_11 = 0;
        var importe_clase_12 = 0;
        var importe_clase_13 = 0;
        var importe_clase_14 = 0;
        var importe_clase_15 = 0;
        var importe_clase_16 = 0;
        var importe_clase_17 = 0;
        var importe_clase_18 = 0;
        var importe_clase_19 = 0;
        var importe_clase_20 = 0;
        var importe_clase_21 = 0;

        var total_clases = 0;
        var total_importe_clases = 0;
        console.log(docs)
        for (let g = 0; g < docs.length; g++) {

            flag10 = 0
            flag11 = 0
            flag12 = 0
            flag13 = 0
            flag14 = 0
            flag15 = 0
            flag16 = 0
            flag17 = 0
            flag18 = 0
            flag19 = 0
            flag20 = 0
            flag21 = 0

            for (var a = 0; a < docs[g].conceptos.length; a++) {
                for (let z = 0; z < docs[g].conceptos[a].length; z++) {
                    switch (docs[g].conceptos[a][z].marcas.marca_3) {
                        case 10:
                            if (flag10 == 0) {
                                clase_10 = clase_10 + 1;
                                total_clases = total_clases + 1;
                                flag10 = 1
                            }
                            importe_clase_10 = importe_clase_10 + docs[g].conceptos[a][z].calculado;
                            total_importe_clases = total_importe_clases + docs[g].conceptos[a][z].calculado;
                            break;
                        case 11:
                            if (flag11 == 0) {
                                clase_11 = clase_11 + 1;
                                total_clases = total_clases + 1;
                                flag11 = 1
                            }
                            importe_clase_11 = importe_clase_11 + docs[g].conceptos[a][z].calculado;
                            total_importe_clases = total_importe_clases + docs[g].conceptos[a][z].calculado;
                            break;
                        case 12:
                            if (flag12 == 0) {
                                clase_12 = clase_12 + 1;
                                total_clases = total_clases + 1;
                                flag12 = 1
                            }
                            importe_clase_12 = importe_clase_12 + docs[g].conceptos[a][z].calculado;
                            total_importe_clases = total_importe_clases + docs[g].conceptos[a][z].calculado;
                            break;
                        case 13:
                            if (flag13 == 0) {
                                clase_13 = clase_13 + 1;
                                total_clases = total_clases + 1;
                                flag13 = 1
                            }
                            importe_clase_13 = importe_clase_13 + docs[g].conceptos[a][z].calculado;
                            total_importe_clases = total_importe_clases + docs[g].conceptos[a][z].calculado;
                            break;
                        case 14:
                            if (flag14 == 0) {
                                clase_14 = clase_14 + 1;
                                total_clases = total_clases + 1;
                                flag14 = 1
                            }
                            importe_clase_14 = importe_clase_14 + docs[g].conceptos[a][z].calculado;
                            total_importe_clases = total_importe_clases + docs[g].conceptos[a][z].calculado;
                            break;
                        case 15:
                            if (flag15 == 0) {
                                clase_15 = clase_15 + 1;
                                total_clases = total_clases + 1;
                                flag15 = 1
                            }
                            importe_clase_15 = importe_clase_15 + docs[g].conceptos[a][z].calculado;
                            total_importe_clases = total_importe_clases + docs[g].conceptos[a][z].calculado;
                            break;
                        case 16:
                            if (flag16 == 0) {
                                clase_16 = clase_16 + 1;
                                total_clases = total_clases + 1;
                                flag16 = 1
                            }
                            importe_clase_16 = importe_clase_16 + docs[g].conceptos[a][z].calculado;
                            total_importe_clases = total_importe_clases + docs[g].conceptos[a][z].calculado;
                            break;
                        case 17:
                            if (flag17 == 0) {
                                clase_17 = clase_17 + 1;
                                total_clases = total_clases + 1;
                                flag17 = 1
                            }
                            importe_clase_17 = importe_clase_17 + docs[g].conceptos[a][z].calculado;
                            total_importe_clases = total_importe_clases + docs[g].conceptos[a][z].calculado;
                            break;
                        case 18:
                            if (flag18 == 0) {
                                clase_18 = clase_18 + 1;
                                total_clases = total_clases + 1;
                                flag18 = 1
                            }
                            importe_clase_18 = importe_clase_18 + docs[g].conceptos[a][z].calculado;
                            total_importe_clases = total_importe_clases + docs[g].conceptos[a][z].calculado;
                            break;
                        case 19:
                            if (flag19 == 0) {
                                clase_19 = clase_19 + 1;
                                total_clases = total_clases + 1;
                                flag19 = 1
                            }
                            importe_clase_19 = importe_clase_19 + docs[g].conceptos[a][z].calculado;
                            total_importe_clases = total_importe_clases + docs[g].conceptos[a][z].calculado;
                            break;
                        case 20:
                            if (flag20 == 0) {
                                clase_20 = clase_20 + 1;
                                total_clases = total_clases + 1;
                                flag20 = 1
                            }
                            importe_clase_20 = importe_clase_20 + docs[g].conceptos[a][z].calculado;
                            total_importe_clases = total_importe_clases + docs[g].conceptos[a][z].calculado;
                            break;
                        case 21:
                            if (flag21 == 0) {
                                clase_21 = clase_21 + 1;
                                total_clases = total_clases + 1;
                                flag21 = 1
                            }
                            importe_clase_21 = importe_clase_21 + docs[g].conceptos[a][z].calculado;
                            total_importe_clases = total_importe_clases + docs[g].conceptos[a][z].calculado;
                            break;
                    }
                }
            }
        }


        ws.cell(5, 9).string('').style(styles.generalLines);
        ws.cell(6, 9).number(clase_10).style(styles.generalLines);
        ws.cell(7, 9).number(clase_11).style(styles.generalLines);
        ws.cell(8, 9).number(clase_12).style(styles.generalLines);
        ws.cell(9, 9).number(clase_13).style(styles.colorYellow);
        ws.cell(10, 9).number(clase_14).style(styles.colorYellow);
        ws.cell(11, 9).number(clase_15).style(styles.generalLines);
        ws.cell(12, 9).number(clase_16).style(styles.generalLines);
        ws.cell(13, 9).number(clase_17).style(styles.generalLines);
        ws.cell(14, 9).number(clase_18).style(styles.generalLines);
        ws.cell(15, 9).number(clase_19).style(styles.generalLines);
        ws.cell(16, 9).number(clase_20).style(styles.generalLines);
        ws.cell(17, 9).number(clase_21).style(styles.generalLines);

        ws.cell(5, 10).string('').style(styles.generalLines).style(styles.rightLine);;
        ws.cell(6, 10).number(importe_clase_10).style(styles.generalLines).style(styles.rightLine);
        ws.cell(7, 10).number(importe_clase_11).style(styles.generalLines).style(styles.rightLine);
        ws.cell(8, 10).number(importe_clase_12).style(styles.generalLines).style(styles.rightLine);
        ws.cell(9, 10).number(importe_clase_13).style(styles.colorYellow).style(styles.rightLine);
        ws.cell(10, 10).number(importe_clase_14).style(styles.colorYellow).style(styles.rightLine);
        ws.cell(11, 10).number(importe_clase_15).style(styles.generalLines).style(styles.rightLine);
        ws.cell(12, 10).number(importe_clase_16).style(styles.generalLines).style(styles.rightLine);
        ws.cell(13, 10).number(importe_clase_17).style(styles.generalLines).style(styles.rightLine);
        ws.cell(14, 10).number(importe_clase_18).style(styles.generalLines).style(styles.rightLine);
        ws.cell(15, 10).number(importe_clase_19).style(styles.generalLines).style(styles.rightLine);
        ws.cell(16, 10).number(importe_clase_20).style(styles.generalLines).style(styles.rightLine);
        ws.cell(17, 10).number(importe_clase_21).style(styles.generalLines).style(styles.rightLine);

        ws.cell(18, 1).string("").style(styles.colorGreen).style(styles.textHead).style(styles.leftLine);
        ws.cell(18, 2).string("").style(styles.colorGreen).style(styles.textHead);
        ws.cell(18, 3).string("").style(styles.colorGreen).style(styles.textHead);
        ws.cell(18, 4).string("").style(styles.colorGreen).style(styles.textHead);
        ws.cell(18, 5).string("").style(styles.colorGreen).style(styles.textHead);
        ws.cell(18, 6).string("").style(styles.colorGreen).style(styles.textHead);
        ws.cell(18, 7).string("").style(styles.colorGreen).style(styles.textHead);
        ws.cell(18, 8).string("").style(styles.colorGreen).style(styles.textHead);
        ws.cell(18, 9).number(total_clases).style(styles.colorGreen).style(styles.textHead);
        ws.cell(18, 10).number(total_importe_clases).style(styles.colorGreen).style(styles.textHead).style(styles.rightLine);

        ws.column(2).setWidth(25);
        ws.column(3).setWidth(12);
        ws.column(5).setWidth(12);
        ws.column(6).setWidth(20);
        ws.column(8).setWidth(12);
        ws.column(9).setWidth(12);

        wb.write(titulo + '.xlsx', res);
    });
}

module.exports = {
    conceptosNoRemunerativos
};