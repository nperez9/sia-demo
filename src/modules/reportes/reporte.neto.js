const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./reportes.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const objectID = mongoose.Types.ObjectId;

const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');

function sueldosNeto (req, res) {
  try {
    const periodo = req.body.periodo;
    const empresa = req.body.empresa; 
    const subempresas = req.body.subempresas;
    const agrupar = req.body.agrupar;
    const cta = req.body.cta;

    let periodoID = new objectID(periodo);
    let empresaID = new objectID(empresa);

    if(subempresas.length != 0){
      for (let a = 0; a < subempresas.length ; a++) {
        subempresas[a] = new objectID(subempresas[a]);
      }
    }
    
    var wb = new excel4node.Workbook();
    var ws = wb.addWorksheet();

    var styleTotal = wb.createStyle({
      font: {
        name: 'Arial',
        color: '#000000',
        size: 8,
        bold: true
      },
      border: { 
        top: {
            style: 'medium',
            color: '#000000'
        }
      }
    });
  
    var titulo = 'Sueldos neto';
    var headers = [
      'Apellido y Nombre',
      'Nivel',
      'CUIL', 
      'Netos'
    ];
  
    let match;

    switch(cta){
      case "conCta":
        match = {
          'periodo._id': periodoID,
          'legajo.subempresa.empresa._id': empresaID,
          'legajo.subempresa._id': {$in: subempresas},
          'legajo.persona._id': {$exists: true},
          $and: [{ 'legajo.persona.datos_administrativos.numero_cuenta': {$exists: true} }, { 'legajo.persona.datos_administrativos.numero_cuenta': {$ne: "" }}]
        }
      break;
      case "sinCta":
        match = {
          'periodo._id': periodoID,
          'legajo.subempresa.empresa._id': empresaID,
          'legajo.subempresa._id': {$in: subempresas},
          'legajo.persona._id': {$exists: true},
          $or: [{ 'legajo.persona.datos_administrativos.numero_cuenta': "" },{ 'legajo.persona.datos_administrativos.numero_cuenta': {$exists: false}}]
        }
      break;
      case "all":
        match = {
          'periodo._id': periodoID,
          'legajo.subempresa.empresa._id': empresaID,
          'legajo.subempresa._id': {$in: subempresas},
          'legajo.persona._id': {$exists: true}
        }
      break;
      default: 
        match = {
          'periodo._id': periodoID,
          'legajo.subempresa.empresa._id': empresaID,
          'legajo.subempresa._id': {$in: subempresas},
          'legajo.persona._id': {$exists: true}
        }
      break;
    }
    
    if(agrupar){
      var group = {
        _id: '$legajo.persona._id',
        empresa: {$first: '$legajo.subempresa.empresa'},
        periodo: {$first: '$periodo.descripcion'},
        nombre: {$first: '$legajo.persona.nombre'},
        nivel: {$first: '$legajo.subempresa.descripcion'},
        cuil: {$first: '$legajo.persona.cuil'},
        sueldo: {$sum: '$neto'}
      };
    }else{
      var group = {
        _id: '$_id',
        empresa: {$first: '$legajo.subempresa.empresa'},
        periodo: {$first: '$periodo.descripcion'},
        nombre: {$first: '$legajo.persona.nombre'},
        nivel: {$first: '$legajo.subempresa.descripcion'},
        cuil: {$first: '$legajo.persona.cuil'},
        sueldo: {$sum: '$neto'}
      };
    }

    Liquidaciones.aggregate([
      {$match: match},
      {$unwind: '$legajo'},
      {
        $group: group
      },
      {$sort: {nombre: 1}}
    ]).exec((err, docs) => {
      if (docs.length < 1) {
        return res.status(504).json({success: false , message: 'El reporte no posee registros'})
      }
      // primero va la ROW  y despues el  columna   
      ws.cell(1, 2).string(docs[0].empresa.razon_social).style(styles.title);
      ws.cell(2, 2).string('Fecha: ' + moment().format('DD/MM/YYYY')).style(styles.datas);
      ws.cell(3, 2).string(titulo).style(styles.datas);
      ws.cell(4, 2).string('Periodo: ' + docs[0].periodo).style(styles.datas);
      
      var total = 0;
      var a = 0;
      // Headers de la tabla y datos
      for (let i = 0; i < headers.length; i ++) {
        //   // Distancia de columnas con Offset (1)s
        let distance = i + 2;
  
        ws.cell(6, distance).string(headers[i]).style(styles.headers);
  
        for (a; a < docs.length; a++) {
          var row = 8 + a;
  
          ws.cell(row, 2).string(docs[a].nombre).style(styles.rows);
          ws.cell(row, 3).string(docs[a].nivel).style(styles.rows);
          ws.cell(row, 4).string(docs[a].cuil).style(styles.rows);

          docs[a].sueldo = docs[a].sueldo == null ? 0 : docs[a].sueldo; 

          ws.cell(row, 5).string("$ " + docs[a].sueldo.toFixed(2).toString()).style(styles.rows);
          total = total + docs[a].sueldo;
        }
      }
  
      ws.cell(9 + a, 2).string('TOTAL').style(styleTotal);
      ws.cell(9 + a, 3).string('').style(styleTotal);
      ws.cell(9 + a, 4).string('').style(styleTotal);
      ws.cell(9 + a, 5).string('$ ' + total.toFixed(2).toString()).style(styleTotal);
      ws.column(1).setWidth(1);
      ws.column(2).setWidth(30);
      ws.column(3).setWidth(20);
      ws.column(4).setWidth(13);
      ws.column(5).setWidth(10);
      wb.write(titulo + '.xlsx', res);
    });
   }
   catch (e) {
     console.log(e);
     return res.status(500).json({
       success: false,
       message: 'Error al imprimer reporte, contacte al administrador'
     });
   }
  }

  module.exports = {
    sueldosNeto
   };