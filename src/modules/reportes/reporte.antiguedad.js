const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');

const mongoose = require('mongoose');
const moment = require('moment');
const fs = require('fs');
var pdf = require('html-pdf');
const mustache = require('mustache');
const objectID = mongoose.Types.ObjectId;

const Antiguedades = mongoose.model('antiguedad');
const Liquidaciones = mongoose.model('liquidaciones');

function cambio(req, res) {
    const periodo = req.body.periodo;
    const empresa = req.body.empresa;

    let periodoID = new objectID(periodo);
    let empresaID = new objectID(empresa);

    var find = { 'legajo.subempresa.empresa._id': empresa, 'periodo._id': periodo };

    Antiguedades.find({}, (err, antiguedades) => {
        Liquidaciones.find(find, (err, liquidaciones) => {

            var cambios = [];

            for (let a = 0; a < liquidaciones.length; a++) {
                // console.log(liquidaciones[a].legajo.antiguedad.meses)
                if (liquidaciones[a].legajo.antiguedad.meses == "1" && liquidaciones[a].legajo.antiguedad.anios != "0") {
                    var unCambio = {};
                    unCambio.nombre = liquidaciones[a].legajo.persona.nombre;
                    unCambio.antiguedad = liquidaciones[a].legajo.antiguedad.anios + "/" + liquidaciones[a].legajo.antiguedad.meses;
                    
                    var antiguedadLegajo = antiguedades.find(item => item.categoria.codigo == liquidaciones[a].legajo.subempresa.categoria.codigo && item.antiguedad == liquidaciones[a].legajo.antiguedad.anios);
                    unCambio.porcentaje = antiguedadLegajo.porcentaje;
                    cambios.push(unCambio);
                }
            }

            var view = {
                cambios: cambios,
                fecha_actual: moment().format('DD/MM/YYYY')
            }

            var content = fs.readFileSync("src/modules/reportes/templates/reporte.cambio.antiguedad.tpl.html", 'utf8');
            html = mustache.render(content, view);

            var options = {
                format: 'a4'
            };

            pdf.create(html, options).toStream(function (err, stream) {
                stream.pipe(res);
            });
        });
    });
}

module.exports = {
    cambio
};
