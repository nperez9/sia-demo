const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const mongoose = require('mongoose');
const moment = require('moment');
const fs = require('fs');
const os = require('os');
const columnas = require('./subrutinas/columnas');
const apo001 = require('./subrutinas/apo_001');
const juntador = require('./subrutinas/juntaLegajos');
const apo003 = require('./subrutinas/apo_003');
const apo004 = require('./subrutinas/apo_004');
const apo005 = require('./subrutinas/apo_005');
const apo006 = require('./subrutinas/apo_006');
const formato = require('./subrutinas/formatoAfip');

const objectID = mongoose.Types.ObjectId;

const Empresas = mongoose.model('empresas');
const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');
const TempAportes = mongoose.model('tempAportes');
const Aportes = mongoose.model('aportes');
const Aportes1 = mongoose.model('aportes1');

function cargasSociales(req, res) {

    const periodo = req.body.periodo;
    const empresa = req.body.empresa;

    let periodoID = new objectID(periodo);
    let empresaID = new objectID(empresa);

    var tablaTempAportes = [];
    var tablaAportes = [];

    var match = {
        'periodo._id': periodoID,
        'legajo.subempresa.empresa._id': empresaID
    }

    Liquidaciones.aggregate([
        { $match: match },
        { $unwind: '$legajo' },
        {
            $group: {
                _id: '$legajo.subempresa._id',
                legajos: { $push: '$legajo' },
                conceptos: { $push: '$conceptos' }
            }
        }
    ]).exec((err, docs) => {

        try {

            for (let i = 0; i < docs.length; i++) {
                for (var a = 0; a < docs[i].conceptos.length; a++) {
                    var utilizarLegajo = true;
                    var situacion = docs[i].legajos[a].persona.datos_administrativos.situacion;

                    if (situacion && situacion.codigo == 13) {
                        for (let p = 0; p < docs[i].legajos.length; p++) {
                            if (docs[i].legajos[p].persona.nombre == docs[i].legajos[a].persona.nombre) {
                                utilizarLegajo = false;
                            }
                        }
                    }

                    if (utilizarLegajo) {

                        var legajoTempAfip = {};

                        var fechaHoy = new Date()
                        var diasTrabajados = 30;

                        if (docs[i].legajos[a].fecha_baja) {
                            if (docs[i].legajos[a].fecha_alta) {
                                diasTrabajados = diasTrabajados - docs[i].legajos[a].fecha_alta.getDay();
                            }
                        } else {
                            if (docs[i].legajos[a].fecha_alta && docs[i].legajos[a].fecha_alta.getMonth() == fechaHoy.getMonth()) {
                                diasTrabajados = diasTrabajados - docs[i].legajos[a].fecha_alta.getDay();
                            }
                        }

                        legajoTempAfip.nombre = docs[i].legajos[a].persona.nombre;
                        legajoTempAfip.nro_legajo = docs[i].legajos[a].numero_legajo;
                        legajoTempAfip.categoria = docs[i].legajos[a].subempresa.categoria.codigo;
                        legajoTempAfip.ordenador = docs[i].legajos[a].ordenador;
                        legajoTempAfip.caracter = docs[i].legajos[a].caracter;
                        legajoTempAfip.situacion = 0;
                        if (docs[i].legajos[a].persona.datos_administrativos.situacion) {
                            legajoTempAfip.situacion = docs[i].legajos[a].persona.datos_administrativos.situacion.codigo;
                        }
                        legajoTempAfip.estado = docs[i].legajos[a].estado;
                        legajoTempAfip.jubilacion = "REPARTO";
                        legajoTempAfip.seguro_vida = docs[i].legajos[a].persona.datos_administrativos.sv;
                        legajoTempAfip.convenio_colectivo = docs[i].legajos[a].persona.datos_administrativos.cc;
                        legajoTempAfip.alta = docs[i].legajos[a].registro_alta;
                        legajoTempAfip.dias_trabajados = diasTrabajados;
                        legajoTempAfip.dni = docs[i].legajos[a].persona.dni;
                        legajoTempAfip.cuil = docs[i].legajos[a].persona.cuil;

                        legajoTempAfip.codigo_actividad = docs[i].legajos[a].persona.datos_administrativos.c_actividad == null ? 0 : docs[i].legajos[a].persona.datos_administrativos.c_actividad.codigo;
                        legajoTempAfip.condicion = docs[i].legajos[a].persona.datos_administrativos.condicion == null ? 0 : docs[i].legajos[a].persona.datos_administrativos.condicion.codigo;
                        legajoTempAfip.modalidad = docs[i].legajos[a].persona.datos_administrativos.modalidad == null ? 0 : docs[i].legajos[a].persona.datos_administrativos.modalidad.codigo;
                        legajoTempAfip.adherentes = docs[i].legajos[a].persona.datos_administrativos.adherentes == undefined ? 0 : docs[i].legajos[a].persona.datos_administrativos.adherentes;
                        legajoTempAfip.codigo_os = docs[i].legajos[a].persona.datos_administrativos.obra_social == null ? 0 : docs[i].legajos[a].persona.datos_administrativos.obra_social.codigo;
                        legajoTempAfip.conyuge = docs[i].legajos[a].persona.conyugue;
                        legajoTempAfip.cantidad_hijos = docs[i].legajos[a].persona.cantidad_hijos == undefined ? 0 : docs[i].legajos[a].persona.cantidad_hijos;
                        legajoTempAfip.codigo_zona = docs[i].legajos[a].persona.datos_administrativos.zona == null ? 0 : docs[i].legajos[a].persona.datos_administrativos.zona.codigo;
                        legajoTempAfip.provincia = docs[i].legajos[a].provincia;
                        legajoTempAfip.localidad = docs[i].legajos[a].persona.domicilio == undefined ? " " : docs[i].legajos[a].persona.domicilio.localidad;
                        legajoTempAfip.tipo = docs[i].legajos[a].subempresa.empresa.tipo == undefined ? " " : docs[i].legajos[a].subempresa.empresa.tipo;
                        legajoTempAfip.cuit_empresa = docs[i].legajos[a].subempresa.empresa.cuit;
                        legajoTempAfip.empresa_familiar = docs[i].legajos[a].subempresa.empresa.no_remunerativo_familiar;
                        legajoTempAfip.art = docs[i].legajos[a].subempresa.empresa.asignacion_familiar;

                        legajoTempAfip.campo_1 = 0;
                        legajoTempAfip.campo_2 = 0;
                        legajoTempAfip.campo_3 = 0;
                        legajoTempAfip.campo_4 = 0;
                        legajoTempAfip.campo_5 = 0;
                        legajoTempAfip.campo_6 = 0;
                        legajoTempAfip.campo_7 = 0;
                        legajoTempAfip.campo_8 = 0;
                        legajoTempAfip.campo_9 = 0;
                        legajoTempAfip.campo_10 = 0;
                        legajoTempAfip.campo_11 = 0;
                        legajoTempAfip.campo_12 = 0;
                        legajoTempAfip.campo_13 = 0;
                        legajoTempAfip.campo_14 = 0;
                        legajoTempAfip.campo_15 = 0;
                        legajoTempAfip.campo_16 = 0;
                        legajoTempAfip.campo_17 = 0;
                        legajoTempAfip.campo_18 = 0;
                        legajoTempAfip.campo_19 = 0;
                        legajoTempAfip.campo_20 = 0;
                        legajoTempAfip.campo_21 = 0;
                        legajoTempAfip.campo_22 = 0;
                        legajoTempAfip.campo_23 = 0;
                        legajoTempAfip.campo_24 = 0;
                        legajoTempAfip.campo_25 = 0;
                        legajoTempAfip.campo_26 = 0;
                        legajoTempAfip.campo_27 = 0;
                        legajoTempAfip.campo_28 = 0;
                        legajoTempAfip.campo_29 = 0;
                        legajoTempAfip.campo_30 = 0;
                        legajoTempAfip.campo_31 = 0;
                        legajoTempAfip.campo_32 = 0;
                        legajoTempAfip.campo_33 = 0;
                        legajoTempAfip.campo_34 = 0;
                        legajoTempAfip.campo_35 = 0;
                        legajoTempAfip.campo_36 = 0;
                        legajoTempAfip.campo_37 = 0;
                        legajoTempAfip.campo_38 = 0;
                        legajoTempAfip.campo_39 = 0;
                        legajoTempAfip.campo_40 = 0;
                        legajoTempAfip.campo_41 = 0;
                        legajoTempAfip.campo_42 = 0;
                        legajoTempAfip.campo_43 = 0;
                        legajoTempAfip.campo_44 = 0;
                        legajoTempAfip.campo_45 = 0;
                        legajoTempAfip.campo_46 = 0;
                        legajoTempAfip.campo_47 = 0;
                        legajoTempAfip.campo_48 = 0;
                        legajoTempAfip.campo_49 = 0;
                        legajoTempAfip.campo_50 = 0;
                        legajoTempAfip.campo_51 = 0;
                        legajoTempAfip.campo_52 = 0;
                        legajoTempAfip.campo_53 = 0;
                        legajoTempAfip.campo_54 = 0;
                        legajoTempAfip.campo_55 = 0;
                        legajoTempAfip.campo_56 = 0;
                        legajoTempAfip.campo_57 = 0;
                        legajoTempAfip.campo_58 = 0;
                        legajoTempAfip.campo_59 = 0;
                        legajoTempAfip.campo_60 = 0;

                        for (let z = 0; z < docs[i].conceptos[a].length; z++) {
                            if (docs[i].conceptos[a][z].marcas) {

                                switch (docs[i].conceptos[a][z].marcas.marca) {
                                    case 1:
                                        legajoTempAfip.campo_1 = legajoTempAfip.campo_1 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 2:
                                        legajoTempAfip.campo_2 = legajoTempAfip.campo_2 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 3:
                                        legajoTempAfip.campo_3 = legajoTempAfip.campo_3 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 4:
                                        legajoTempAfip.campo_4 = legajoTempAfip.campo_4 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 5:
                                        legajoTempAfip.campo_5 = legajoTempAfip.campo_5 + docs[i].conceptos[a][z].calculado;
                                        
                                        break;
                                    case 6:
                                        legajoTempAfip.campo_6 = legajoTempAfip.campo_6 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 7:
                                        legajoTempAfip.campo_7 = legajoTempAfip.campo_7 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 8:
                                        legajoTempAfip.campo_8 = legajoTempAfip.campo_8 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 9:
                                        legajoTempAfip.campo_9 = legajoTempAfip.campo_9 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 10:
                                        legajoTempAfip.campo_10 = legajoTempAfip.campo_10 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 11:
                                        legajoTempAfip.campo_11 = legajoTempAfip.campo_11 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 12:
                                        legajoTempAfip.campo_12 = legajoTempAfip.campo_12 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 13:
                                        legajoTempAfip.campo_13 = legajoTempAfip.campo_13 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 14:
                                        legajoTempAfip.campo_14 = legajoTempAfip.campo_14 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 15:
                                        legajoTempAfip.campo_15 = legajoTempAfip.campo_15 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 16:
                                        legajoTempAfip.campo_16 = legajoTempAfip.campo_16 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 17:
                                        legajoTempAfip.campo_17 = legajoTempAfip.campo_17 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 18:
                                        legajoTempAfip.campo_18 = legajoTempAfip.campo_18 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 19:
                                        legajoTempAfip.campo_19 = legajoTempAfip.campo_19 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 20:
                                        legajoTempAfip.campo_20 = legajoTempAfip.campo_20 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 21:
                                        legajoTempAfip.campo_21 = legajoTempAfip.campo_21 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 22:
                                        legajoTempAfip.campo_22 = legajoTempAfip.campo_22 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 23:
                                        legajoTempAfip.campo_23 = legajoTempAfip.campo_23 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 24:
                                        legajoTempAfip.campo_24 = legajoTempAfip.campo_24 + docs[i].conceptos[a][z].calculado;
                                        break;
                                    case 25:
                                        legajoTempAfip.campo_25 = legajoTempAfip.campo_25 + docs[i].conceptos[a][z].calculado;
                                        break;
                                }
                            }
                        }

                        legajoTempAfip.campo_1 = parseFloat(legajoTempAfip.campo_1.toFixed(2));
                        legajoTempAfip.campo_2 = parseFloat(legajoTempAfip.campo_2.toFixed(2));
                        legajoTempAfip.campo_3 = parseFloat(legajoTempAfip.campo_3.toFixed(2));
                        legajoTempAfip.campo_4 = parseFloat(legajoTempAfip.campo_4.toFixed(2));
                        legajoTempAfip.campo_5 = parseFloat(legajoTempAfip.campo_5.toFixed(2));
                        legajoTempAfip.campo_6 = parseFloat(legajoTempAfip.campo_6.toFixed(2));
                        legajoTempAfip.campo_7 = parseFloat(legajoTempAfip.campo_7.toFixed(2));
                        legajoTempAfip.campo_8 = parseFloat(legajoTempAfip.campo_8.toFixed(2));
                        legajoTempAfip.campo_9 = parseFloat(legajoTempAfip.campo_9.toFixed(2));
                        legajoTempAfip.campo_10 = parseFloat(legajoTempAfip.campo_10.toFixed(2));
                        legajoTempAfip.campo_11 = parseFloat(legajoTempAfip.campo_11.toFixed(2));
                        legajoTempAfip.campo_12 = parseFloat(legajoTempAfip.campo_12.toFixed(2));
                        legajoTempAfip.campo_13 = parseFloat(legajoTempAfip.campo_13.toFixed(2));
                        legajoTempAfip.campo_14 = parseFloat(legajoTempAfip.campo_14.toFixed(2));
                        legajoTempAfip.campo_15 = parseFloat(legajoTempAfip.campo_15.toFixed(2));
                        legajoTempAfip.campo_16 = parseFloat(legajoTempAfip.campo_16.toFixed(2));
                        legajoTempAfip.campo_17 = parseFloat(legajoTempAfip.campo_17.toFixed(2));
                        legajoTempAfip.campo_18 = parseFloat(legajoTempAfip.campo_18.toFixed(2));
                        legajoTempAfip.campo_19 = parseFloat(legajoTempAfip.campo_19.toFixed(2));
                        legajoTempAfip.campo_20 = parseFloat(legajoTempAfip.campo_20.toFixed(2));
                        legajoTempAfip.campo_21 = parseFloat(legajoTempAfip.campo_21.toFixed(2));
                        legajoTempAfip.campo_22 = parseFloat(legajoTempAfip.campo_22.toFixed(2));
                        legajoTempAfip.campo_23 = parseFloat(legajoTempAfip.campo_23.toFixed(2));
                        legajoTempAfip.campo_24 = parseFloat(legajoTempAfip.campo_24.toFixed(2));
                        legajoTempAfip.campo_25 = parseFloat(legajoTempAfip.campo_25.toFixed(2));


                        legajoTempAfip = columnas.completaColumnas(legajoTempAfip);

                        legajoTempAfip.periodo = periodoID;
                        legajoTempAfip.empresa = empresaID;

                        tablaTempAportes.push(legajoTempAfip);

                        legajoAporte = apo001.completaColumnas(legajoTempAfip);

                        tablaAportes.push(legajoAporte);
                    }

                }
            }

            var removeTemp = TempAportes.remove({
                'periodo': periodoID,
                'empresa': empresaID
            });

            var removeAportes = Aportes.remove({
                'periodo': periodoID,
                'empresa': empresaID
            });

            var removeAportes1 = Aportes1.remove({
                'periodo': periodoID,
                'empresa': empresaID
            });

            //Limpia tablas
            Promise.all([removeTemp, removeAportes, removeAportes1]).then(limpiador => {

                var crearTempAportes = TempAportes.create(tablaTempAportes,
                    (err, newLinea) => {
                        console.log(err)
                        console.log("soy temp")
                    });

                var crearAportes = Aportes.create(tablaAportes,
                    (err, newLinea) => {
                        console.log("soy aporte")
                    });

                //crea tablas
                Promise.all([crearTempAportes, crearAportes]).then((err,a) => {
                    var legajosTemporales = "";

                    //Obtiene los legajos x cuit y genera tabla aportes1
                    var crearAportes1 = Aportes.aggregate([
                        {
                            $match: {
                                'periodo': periodoID,
                                'empresa': empresaID
                            }
                        },
                        {
                            $group: {
                                _id: '$col01',
                                legajos: { $push: '$$ROOT' },
                            }
                        }
                    ]).exec((err, docs) => {
                        for (let i = 0; i < docs.length; i++) {

                            for (let b = 0; b < docs[i].legajos.length; b++) {
                                legajosTemporales += formato.darFormato(docs[i].legajos[b]) + os.EOL;
                            }

                            datoAfip = juntador.juntaLegajos(docs[i].legajos);
                            Aportes1.create(datoAfip,
                                (err, newLinea) => {
                                    console.log("soy aporte1")
                                });
                        }
                    });

                    //genera txt final
                    Promise.all([crearAportes1]).then(response => {
                        Aportes1.find({
                            'periodo': periodoID,
                            'empresa': empresaID
                        })
                            .exec((err, items) => {
                                var datosAfip = "";
                                for (let i = 0; i < items.length; i++) {
                                    items[i] = apo003.modificaAporte(items[i]);
                                    items[i] = apo004.modificaAporte(items[i]);
                                    items[i] = apo005.modificaAporte(items[i]);
                                    items[i] = apo006.modificaAporte(items[i]);
                                    datosAfip += formato.darFormato(items[i]) + os.EOL;
                                }
                                res.write(legajosTemporales + '-----------------------------------------' + os.EOL + datosAfip);
                                return res.end();
                            });
                    });
                });
            });

        } catch (error) {
            console.log(error);
            res.status(500).json();
        }

    });
}

function validaciones(req, res) {

    const periodo = req.params.periodo;
    const empresa = req.params.empresa;

    let periodoID = new objectID(periodo);
    let empresaID = new objectID(empresa);
    var datoEmpresa;

    let match = {
        'periodo._id': periodoID,
        'legajo.subempresa.empresa._id': empresaID
    };

    var localidadEmpresa = Empresas.findById(empresaID, function (err, doc) {
        datoEmpresa = doc.provincia;
    });

    Promise.all([localidadEmpresa]).then(docs => {

        Liquidaciones.aggregate([
            { $match: match },
            {
                $group: {
                    _id: '$legajo.persona.persona_id',
                    legajos: { $push: '$legajo' },
                }
            }]).exec((err, docs) => {

                var arrayProvinciaAlert = [];
                var arrayCapitalAlert = [];

                try {

                    if (datoEmpresa == "Buenos Aires") {

                        for (let i = 0; i < docs.length; i++) {
                            var aux = 0;
                            var codigo_act = docs[i].legajos[0].persona.datos_administrativos.c_actividad.codigo;

                            for (let x = 0; x < docs[i].legajos.length; x++) {
                                if (docs[i].legajos[x].subempresa.categoria.codigo.substr(0, 1) == "P") {
                                    aux = 1;
                                }
                            }

                            if ((codigo_act != 16 && aux == 1) || (codigo_act == 16 && aux == 0)) {
                                if (arrayProvinciaAlert.indexOf(docs[i].legajos[0].persona) == -1) {
                                    arrayProvinciaAlert.push(docs[i].legajos[0].persona);
                                }
                            }
                        }
                        res.json(arrayProvinciaAlert);
                    }
                    else if (datoEmpresa == "Ciudad de Buenos Aires") {
                        for (let i = 0; i < docs.length; i++) {

                            var aux = 0;
                            var codigo_act = docs[i].legajos[0].persona.datos_administrativos.c_actividad.codigo;

                            for (let x = 0; x < docs[i].legajos.length; x++) {
                                if (docs[i].legajos[x].subempresa.categoria.codigo.substr(0, 1) == "A") {
                                    aux = 1;
                                }
                            }

                            if ((codigo_act != 38 && aux == 1) || (codigo_act == 38 && aux == 0)) {
                                if (arrayCapitalAlert.indexOf(docs[i].legajos[0].persona) == -1) {
                                    arrayCapitalAlert.push(docs[i].legajos[0].persona);
                                }
                            }
                        }

                        res.json(arrayCapitalAlert);
                    } else {
                        res.json();
                    }

                } catch (error) {
                    res.json({ success: false, message: '' });
                }



            });
    });
}

function txtValidaciones(req, res) {

    const personas = req.body.personas;

    for (let i = 0; i < personas.length; i++) {
        res.write(personas[i].nombre + " - Cod. actividad: " + personas[i].datos_administrativos.c_actividad.codigo + os.EOL);
    }

    return res.end();
}


module.exports = {
    cargasSociales,
    validaciones,
    txtValidaciones
};