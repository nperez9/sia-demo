const base = '/reportes';
const reporteAsientos = require('./reporte.asientos');
const reporteNeto = require('./reporte.neto');
const reporteBruto = require('./reporte.bruto');
const reporteOS = require('./reporte.obras_sociales');
const conceptosNoRemu = require('./reporte.conceptos_no_remu');
const afip = require('./afip.cargas_sociales');
const dgegp = require('./rendicion.dgegp');
const reporteBajas = require('./reporte.bajas');
const reporteAntiguedad = require('./reporte.antiguedad');
const reportePadron = require('./reporte.padron');
const reporteSueldos = require('./reporte.sueldos');

module.exports = function (router) {
    router.post(base + '/asientos-contables', reporteAsientos.asientosContables);
    router.post(base + '/sueldos-neto', reporteNeto.sueldosNeto);
    router.post(base + '/sueldos-bruto', reporteBruto.sueldosBruto);
    router.post(base + '/obras-sociales', reporteOS.obrasSociales);
    router.post(base + '/conceptos-no-remu', conceptosNoRemu.conceptosNoRemunerativos);
    router.post(base + '/afip_cargas_sociales', afip.cargasSociales);
    router.post(base + '/afip_perosonas_alert', afip.txtValidaciones);
    router.post(base + '/dgegp/:modal', dgegp.crearCSV);
    router.post(base + '/bajas', reporteBajas.bajas);
    router.post(base + '/cambio-antiguedad', reporteAntiguedad.cambio);
    router.post(base + '/padron-subempresas', reportePadron.subempresas);
    router.post(base + '/sueldos', reporteSueldos.sueldos);

    router.get(base + '/afip_validaciones/:empresa/:periodo', afip.validaciones);
}