const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./reportes.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const columnasRendicion = require('./subrutinas/rendicion');
const os = require('os');
const fs = require('fs');
const objectID = mongoose.Types.ObjectId;


const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');
const Empresas = mongoose.model('empresas');
const Periodos = mongoose.model('periodos');


function crearCSV(req, res) {
  try {
    const periodo = req.body.periodo;
    const empresa = req.body.empresa;
    const subempresas = req.body.subempresas;
    const modal = req.params.modal;
    
    let periodoID = new objectID(periodo);
    let empresaID = new objectID(empresa);

    if (subempresas.length != 0) {
      for (let a = 0; a < subempresas.length; a++) {
        subempresas[a] = new objectID(subempresas[a]);
      }
    }

    var wb = new excel4node.Workbook();
    var ws = wb.addWorksheet();

    var match = {
      'periodo._id': periodoID,
      'legajo.subempresa.empresa._id': empresaID,
      'legajo.subempresa._id': { $in: subempresas },
      'legajo.estado': { $ne: 'baja' }
    }

    var consultaEmpresa = Empresas.findById(empresaID).exec((err, item) => {
      // empresa = item
    });

    var consultaPeriodo = Periodos.findById(periodoID).exec((err, periodo) => {
      // items.anio;
      // items.mes;
    });

    var style = wb.createStyle({
      numberFormat: '#,##0.00; (#,##0.00);'
    });

    Liquidaciones.aggregate([
      {
        $match: match
      },
      { $unwind: '$legajo' },
      {
        $group: {
          _id: '$legajo.subempresa._id',
          legajos: { $push: '$legajo' },
          conceptos: { $push: '$conceptos' }
        }
      }
    ]).exec((err, legajos) => {

      var csv = "";

      if (legajos.length < 1) {
        return res.status(504).json({ success: false, message: 'El reporte no posee registros' })
      }

      var header = "*carac.;Nivel;Año;Mes;(vacío);CUIL;Apellido y Nombre;Revista;Reemplaza_a;CodCargo;Horas;Antigüedad(años);Antigüedad(meses);Descuento;Licencia_Art.;SueldoBasico;Antigüedad;Presentismo;Dto.243;(vacío);Adic.Ed.Especial;Adic.Jerarq.Directivos;Garantía;plus dif estab;(vacío);(vacío);(vacío);OtrasRem.;Desc.Pres.;Desc.Ajuste;Prop.SAC;Acum.Ene-Jun;Acum.Feb-Jul;Acum.Mar-Ago;Acum.Abr-Sep;Acum.May-Oct;Acum.Jun-Nov;MejorSueldo;(vacío);(vacío);Juvilación;Obra Social;CajaComplementaria;OtrasRetenciones;DEVOL GAN;SalarioFamiliar;ActaAcuerdo02/11;GARANTIA;ADIC. 2017;MAT.DID. 2017;GRAT EXT OBLI;Desc.No Rem.;Prop.Vac;(vacío);(vacío);(vacío);Contrib.Jubilacion;Contrib.O. Social;Suplencia_Desde;Suplencia_Hasta;Causal;Cónyuge;Hijos;Hijos discapacitados;Hijos < 5 años;Hijos < 5 años discapacitados;Escolaridad primaria y preprimaria;Escolaridad primaria y preprimaria de hijos discapacitados;Escolaridad media y superior;Escolaridad media y superior de hijos discapacitados;Familia numerosa;Matrimonio;Prenatal;Nacimieto;Nacimiento de hijo discapacitado;Adopción;Adopción de hijo discapacitado;Ayuda escolar;Ayuda escolar de hijo discapacitado" + os.EOL;
      csv += header;

      Promise.all([consultaPeriodo, consultaEmpresa, ]).then((datos) => {
        
        var alertaNegativos = false;
        var cantLegajos = [];

        for (let i = 0; i < legajos.length; i++) {

          var unLegajo = "";

          if(cantLegajos.indexOf(legajos[i].legajos[0].persona.cuil) == -1){
            cantLegajos.push(legajos[i].legajos[0].persona.cuil);
          }

          unLegajo += datos[1].registro + ";";
          unLegajo += " " + ";"; //falta nivel
          unLegajo += datos[0].anio + ";";
          unLegajo += datos[0].mes + ";";
          unLegajo += "M;";
          unLegajo += legajos[i].legajos[0].persona.cuil + ";";
          unLegajo += legajos[i].legajos[0].persona.nombre.toUpperCase() + ";";
          unLegajo += legajos[i].legajos[0].caracter + ";";
          
          if(legajos[i].legajos[0].suplencias.length != 0){
            unLegajo += legajos[i].legajos[0].suplencias[0].cuil + ";";
          }else{
            unLegajo += " " + ";";
          }

          unLegajo += legajos[i].legajos[0].cargo.cargo + ";";
          unLegajo += legajos[i].legajos[0].horas + ";";
          unLegajo += legajos[i].legajos[0].antiguedad.anios + ";";
          unLegajo += legajos[i].legajos[0].antiguedad.meses + ";";
          
          unLegajo += "0;"//falta descuento

          if(legajos[i].legajos[0].licencias.length != 0){
            unLegajo += legajos[i].legajos[0].licencias[0].articulo + ";";
          }else{
            unLegajo += " " + ";";
          }

          var columnas = {
            clase3 : 0,
            clase4 : 0,
            clase5 : 0,
            clase6 : 0,
            clase7 : 0,
            clase8 : 0,
            clase9 : 0,
            clase10 : 0,
            clase11 : 0,
            clase12 : 0,
            clase13 : 0,
            clase14 : 0,
            clase15 : 0,
            clase16 : 0,
            clase17 : 0,
            clase18 : 0,
            clase19 : 0,
            clase20 : 0,
            clase21 : 0,
            clase22 : 0,
            clase23 : 0,
            clase24 : 0,
            clase25 : 0,
            clase26 : 0,
            clase27 : 0,
            clase28 : 0,
            clase29 : 0,
            clase30 : 0,
            clase30 : 0,
            clase30 : 0,
            clase31 : 0,
            clase32 : 0,
            clase33 : 0,
            clase34 : 0,
            clase35 : 0,
            clase36 : 0,
            clase37 : 0,
            clase38 : 0,
            clase39 : 0,
            clase40 : 0
          }

          for (var a = 0; a < legajos[i].conceptos[0].length; a++) {
            switch (legajos[i].conceptos[0][a].clase) {
              case 3:
                columnas.clase3 = columnas.clase3 + legajos[i].conceptos[0][a].calculado;
                break;

              case 4:
                columnas.clase4 = columnas.clase4 + legajos[i].conceptos[0][a].calculado;
                break;

              case 5:
                columnas.clase5 = columnas.clase5 + legajos[i].conceptos[0][a].calculado;
                break;

              case 6:
                columnas.clase6 = columnas.clase6 + legajos[i].conceptos[0][a].calculado;
                break;

              case 7:
                columnas.clase7 = columnas.clase7 + legajos[i].conceptos[0][a].calculado;
                break;

              case 8:
                columnas.clase8 = columnas.clase8 + legajos[i].conceptos[0][a].calculado;
                break;

              case 9:
                columnas.clase9 = columnas.clase9 + legajos[i].conceptos[0][a].calculado;
                break;

              case 10:
                columnas.clase10 = columnas.clase10 + legajos[i].conceptos[0][a].calculado;
                break;

              case 11:
                columnas.clase11 = columnas.clase11 + legajos[i].conceptos[0][a].calculado;
                break;

              case 12:
                columnas.clase12 = columnas.clase12 + legajos[i].conceptos[0][a].calculado;
                break;

              case 13:
                columnas.clase13 = columnas.clase13 + legajos[i].conceptos[0][a].calculado;
                break;

              case 14:
                columnas.clase14 = columnas.clase14 + legajos[i].conceptos[0][a].calculado;
                break;

              case 15:
                columnas.clase15 = columnas.clase15 + legajos[i].conceptos[0][a].calculado;
                break;

              case 16:
                columnas.clase16 = columnas.clase16 + legajos[i].conceptos[0][a].calculado;
                break;

              case 17:
                columnas.clase17 = columnas.clase17 + legajos[i].conceptos[0][a].calculado;
                break;

              case 18:
                columnas.clase18 = columnas.clase18 + legajos[i].conceptos[0][a].calculado;
                break;

              case 19:
                columnas.clase19 = columnas.clase19 + legajos[i].conceptos[0][a].calculado;
                break;

              case 20:
                columnas.clase20 = columnas.clase20 + legajos[i].conceptos[0][a].calculado;
                break;

              case 21:
                columnas.clase21 = columnas.clase21 + legajos[i].conceptos[0][a].calculado;
                break;

              case 22:
                columnas.clase22 = columnas.clase22 + legajos[i].conceptos[0][a].calculado;
                break;

              case 23:
                columnas.clase23 = columnas.clase23 + legajos[i].conceptos[0][a].calculado;
                break;

              case 24:
                columnas.clase24 = columnas.clase24 + legajos[i].conceptos[0][a].calculado;
                break;

              case 25:
                columnas.clase25 = columnas.clase25 + legajos[i].conceptos[0][a].calculado;
                break;

              case 26:
                columnas.clase26 = columnas.clase26 + legajos[i].conceptos[0][a].calculado;
                break;

              case 27:
                columnas.clase27 = columnas.clase27 + legajos[i].conceptos[0][a].calculado;
                break;

              case 28:
                columnas.clase28 = columnas.clase28 + legajos[i].conceptos[0][a].calculado;
                break;

              case 29:
                columnas.clase29 = columnas.clase29 + legajos[i].conceptos[0][a].calculado;
                break;

              case 30:
                columnas.clase30 = columnas.clase30 + legajos[i].conceptos[0][a].calculado;
                break;

              case 31:
                columnas.clase31 = columnas.clase31 + legajos[i].conceptos[0][a].calculado;
                break;

              case 32:
                columnas.clase32 = columnas.clase32 + legajos[i].conceptos[0][a].calculado;
                break;

              case 33:
                columnas.clase33 = columnas.clase33 + legajos[i].conceptos[0][a].calculado;
                break;

              case 34:
                columnas.clase34 = columnas.clase34 + legajos[i].conceptos[0][a].calculado;
                break;

              case 35:
                columnas.clase35 = columnas.clase35 + legajos[i].conceptos[0][a].calculado;
                break;

              case 36:
                columnas.clase36 = columnas.clase36 + legajos[i].conceptos[0][a].calculado;
                break;

              case 37:
                columnas.clase37 = columnas.clase37 + legajos[i].conceptos[0][a].calculado;
                break;

              case 38:
                columnas.clase38 = columnas.clase38 + legajos[i].conceptos[0][a].calculado;
                break;

              case 39:
                columnas.clase39 = columnas.clase39 + legajos[i].conceptos[0][a].calculado;
                break;

              case 40:
                columnas.clase40 = columnas.clase40 + legajos[i].conceptos[0][a].calculado;
                break;

              default:
                break;
            }
          }


          var columnasTransformadas = columnasRendicion.completaColumnas(columnas);

          unLegajo += columnasTransformadas.clase3 == 0 ? " " + ";" : columnasTransformadas.clase3.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase4 == 0 ? " " + ";" : columnasTransformadas.clase4.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase5 == 0 ? " " + ";" : columnasTransformadas.clase5.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase6 == 0 ? " " + ";" : columnasTransformadas.clase6.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase7 == 0 ? " " + ";" : columnasTransformadas.clase7.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase8 == 0 ? " " + ";" : columnasTransformadas.clase8.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase9 == 0 ? " " + ";" : columnasTransformadas.clase9.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase10 == 0 ? " " + ";" : columnasTransformadas.clase10.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase11 == 0 ? " " + ";" : columnasTransformadas.clase11.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase12 == 0 ? " " + ";" : columnasTransformadas.clase12.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase13 == 0 ? " " + ";" : columnasTransformadas.clase13.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase14 == 0 ? " " + ";" : columnasTransformadas.clase14.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase15 == 0 ? " " + ";" : columnasTransformadas.clase15.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase16 == 0 ? " " + ";" : columnasTransformadas.clase16.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase17 == 0 ? " " + ";" : columnasTransformadas.clase17.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase18 == 0 ? " " + ";" : columnasTransformadas.clase18.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase19 == 0 ? " " + ";" : columnasTransformadas.clase19.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase20 == 0 ? " " + ";" : columnasTransformadas.clase20.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase21 == 0 ? " " + ";" : columnasTransformadas.clase21.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase22 == 0 ? " " + ";" : columnasTransformadas.clase22.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase23 == 0 ? " " + ";" : columnasTransformadas.clase23.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase24 == 0 ? " " + ";" : columnasTransformadas.clase24.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase25 == 0 ? " " + ";" : columnasTransformadas.clase25.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase26 == 0 ? " " + ";" : columnasTransformadas.clase26.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase27 == 0 ? " " + ";" : columnasTransformadas.clase27.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase28 == 0 ? " " + ";" : columnasTransformadas.clase28.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase29 == 0 ? " " + ";" : columnasTransformadas.clase29.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase30 == 0 ? " " + ";" : columnasTransformadas.clase30.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase31 == 0 ? " " + ";" : columnasTransformadas.clase31.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase32 == 0 ? " " + ";" : columnasTransformadas.clase32.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase33 == 0 ? " " + ";" : columnasTransformadas.clase33.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase34 == 0 ? " " + ";" : columnasTransformadas.clase34.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase35 == 0 ? " " + ";" : columnasTransformadas.clase35.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase36 == 0 ? " " + ";" : columnasTransformadas.clase36.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase37 == 0 ? " " + ";" : columnasTransformadas.clase37.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase38 == 0 ? " " + ";" : columnasTransformadas.clase38.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase39 == 0 ? " " + ";" : columnasTransformadas.clase39.toFixed(2) + ";";
          unLegajo += columnasTransformadas.clase40 == 0 ? " " + ";" : columnasTransformadas.clase40.toFixed(2) + ";";
          unLegajo += " "+ ";";

          if(legajos[i].legajos[0].suplencias.length != 0){
            unLegajo += moment(legajos[i].legajos[0].suplencias[0].fecha_desde).format('DD/MM/YY') + ";";
            unLegajo += moment(legajos[i].legajos[0].suplencias[0].fecha_hasta).format('DD/MM/YY') + ";";
          }else{
            unLegajo += " / / " + ";";
            unLegajo += " / / " + ";";
          }

          unLegajo += columnasTransformadas.clase44 + ";";          
          unLegajo += columnasTransformadas.clase45 + ";";

          csv += unLegajo + os.EOL;

          if(columnasTransformadas.clase32 < 0){
            alertaNegativos = true;
          }

          
        }

        if(modal == 1){
          return res.json({
            alertaNegativos : alertaNegativos,
            cantLegajos : cantLegajos.length
          });
        }else{
          res.write(csv);
          return res.end();
        }
        

      });

      
    });
  }
  catch (e) {
    console.log(e);
    return res.status(500).json({
      success: false,
      message: 'Error al crear reporte, contacte al administrador'
    });
  }
}

module.exports = {
  crearCSV
};