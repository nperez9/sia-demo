const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./reportes.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const objectID = mongoose.Types.ObjectId;

const ObrasSociales = mongoose.model('obras_sociales');


function obrasSociales(req, res) {
    var wb = new excel4node.Workbook();
    var ws = wb.addWorksheet();

    var styleTotal = wb.createStyle({
        font: {
            name: 'Arial',
            color: '#000000',
            size: 8,
            bold: true
        },
        border: {
            top: {
                style: 'medium',
                color: '#000000'
            }
        }
    });

    var titulo = 'Obras sociales';
    var headers = [
        'Código',
        'Nombre'
    ];

    ObrasSociales.find({}, (err, docs) => {

        ws.cell(1, 2).string('Obras sociales').style(styles.title);
        ws.cell(2, 2).string('Fecha: ' + moment().format('DD/MM/YYYY')).style(styles.datas);
        var a = 0;

        for (let i = 0; i < headers.length; i++) {

            let distance = i + 2;

            ws.cell(4, distance).string(headers[i]).style(styles.headers);

            for (a; a < docs.length; a++) {
                var row = 5 + a;
                console.log(docs[a]);

                ws.cell(row, 2).number(docs[a].codigo).style(styles.rows);
                ws.cell(row, 3).string(docs[a].nombre).style(styles.rows);
            }
        }

        ws.column(1).setWidth(1);
        ws.column(2).setWidth(8);
        ws.column(3).setWidth(50);
        wb.write(titulo + '.xlsx', res);
    });
}

module.exports = {
    obrasSociales
};