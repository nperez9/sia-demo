const base = '/antiguedad';
const antiguedad = require('./antiguedad');
const isLoggued = require('../../auth/auth_middleware.js');

module.exports = function (router) {
    router.get(base + '/all', isLoggued, antiguedad.getAll);
    router.get(base + '/listado/', antiguedad.getAntiguedadGeneral);
    router.get(base + '/:id', antiguedad.getById);
    router.get(base, antiguedad.get);
    
    router.post(base, antiguedad.post);
    
    router.put(base + '/:id', antiguedad.put);
    
    router.delete(base + '/:id', antiguedad.destroy);
};
