const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');

const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId;

const Legajos = mongoose.model('legajos');
const Antiguedades = mongoose.model('antiguedad');

function getAntiguedadGeneral (req, res) {
  var findCriteria = {};
  var documentsPerPage = 20;
  var skip = (req.query.page - 1) * documentsPerPage;

  findCriteria['empresa'] = {$exists: false};

  if (req.query.search != '') {
      findCriteria['categoria.codigo'] = new RegExp(req.query.search, 'i');
  }

  Antiguedades.find(findCriteria)
  .limit(documentsPerPage)
  .skip(skip)
  .sort({nombre: 1})
  .exec((err, items) => {
      getCount(findCriteria).then(total => {
          return res.json({
              items,
              total
          });
      });
  });
}

function get (req, res) {
    var findCriteria = {};
    var documentsPerPage = 20;
    var skip = (req.query.page - 1) * documentsPerPage;
    var query =  req.query;

    // if(req.cookies.tipo != "admin"){
    //     return res.status(403).json({success: false, message: 'Acceso denegado'});
    // }

    if(query.empresaId != ''){
      findCriteria['empresa._id'] = new objectID(query.empresaId)
    }

    if (query.search != ''){
        findCriteria['categoria.codigo'] = new RegExp(query.search, 'i');
    }

    Antiguedades.find(findCriteria)
    .limit(documentsPerPage)
    .skip(skip)
    .sort({nombre: 1})
    .exec((err, items) => {
        getCount(findCriteria).then(total => {
            return res.json({
                items,
                total
            });
        });
    });
}

function getCount (find, model) {
    return Antiguedades.count(find).exec((err, count) => {
        return count;
    });
}

/** 
 * devuleve TODAS las antiguedades
*/
function getAll (req, res) {
    Antiguedades.find({}, (err, docs) => {
        res.json(docs);
    });
}

function getById (req, res) {
    var antiguedadID = new objectID(req.params.id);

    Antiguedades.findOne({_id: antiguedadID}, (err, doc) => {
        res.json(doc);
        console.log(doc);
    });
}

function post (req, res) {
    var antiguedad = req.body;
    console.log(antiguedad);
    Antiguedades.create(antiguedad, mongoResponse(res, 'Antiguedad insertada con exito'));
}

function put (req, res) {
    var antiguedad = req.body;
    var antiguedadID = new objectID(antiguedad._id);

    Antiguedades.findOneAndUpdate(
        {_id: antiguedadID},
        {$set: antiguedad},
        mongoResponse(res, 'Antiguedad actualizada')
    );
}

function destroy (req, res) {
    var antiguedadID = new objectID(req.params.id);
    var sMessage = 'Antiguedad eliminada con exito';

    Antiguedades.delete({ _id: antiguedadID}, mongoResponse(res, sMessage));
}

module.exports = {
    get,
    getAll,
    post,
    put,
    getById,
    destroy,
    getAntiguedadGeneral
}
