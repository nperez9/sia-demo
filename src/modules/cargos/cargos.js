const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');

const mongoose = require('mongoose'); 
const objectID = mongoose.Types.ObjectId; 
const Cargos = mongoose.model('cargos'); 

function get (req, res) {
    var findCriteria = {};
    var documentsPerPage = 20; 
    var skip = (req.query.page - 1) * documentsPerPage;

    if (req.query.search != '')
        findCriteria['nombre'] = new RegExp(req.query.search, 'i'); 

    Cargos.find(findCriteria)
    .limit(documentsPerPage)
    .skip(skip)
    .exec((err, items) => {
        getCount(findCriteria).then(total => {
            return res.json({
                items,
                total
            });
        }); 
    }); 
}

function post (req, res) {
    let cargo = req.body;
    
    Cargos.create(
        cargo, 
        mongoResponse(res, 'Cargo creado con exito')
    );
}

function getById (req, res) {
    let cargoID = new objectID(req.params.id); 
    
    Cargos.findOne({_id: cargoID}, 
    (err, cargo) => {
        return res.json(cargo);
    });
}

function getCount (find) {
    if (typeof(find) ===  undefined)
        find = {}; 

    return Cargos.count(find).exec((err, count) => {
        return count; 
    });
}

function put (req, res) {
    var cargo = req.body; 
    var cargoID = new objectID(cargo._id); 
    var message = 'Cargo editado con exito'; 

    Cargos.findByIdAndUpdate(cargoID, cargo, mongoResponse(res, message)); 
}

function destroy (req, res) {
    var cargoID = new objectID(req.params.id); 

    Cargos.delete({ _id: cargoID }, 
    (err, result) => {
        if (err) process.exit(err); 
        return res.json({
            success: true, 
            message: 'Cargo eliminado'
        });
    }); 
}

module.exports = {
    get, 
    post,
    put, 
    destroy,
    getById
}


