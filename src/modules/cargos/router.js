const base = '/cargos';
const cargos = require('./cargos');
const isLoggued = require('../../auth/auth_middleware.js');

module.exports = function (router) {
    router.get(base, isLoggued, cargos.get);
    router.get(base + '/:id', cargos.getById);

    router.post(base, cargos.post);

    router.put(base + '/:id', cargos.put);

    router.delete(base + '/:id', cargos.destroy);
}
