const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./listados.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const objectID = mongoose.Types.ObjectId;

const Personas = mongoose.model('personas');
const Periodos = mongoose.model('periodos');

module.exports =

    function personal(req, res) {

        try {
            const periodo = req.body.periodo;
            const empresa = req.body.empresa;

            let periodoID = new objectID(periodo);
            let empresaID = new objectID(empresa);

            var wb = new excel4node.Workbook();

            let match;

            Personas.find({ 'periodo_id': periodoID, 'empresa._id': empresaID })
                .sort({ "nombre": 1 })
                .exec((err, personas) => {

                    if (err) return res.status(500).json({ 'success': false, message: err });
                    var ws = wb.addWorksheet();

                    ws.cell(1, 1).string(personas[0].empresa.razon_social).style(styles.personalEmpresa).style(styles.bold).style(styles.italic);
                    ws.cell(2, 1).string("Fecha " + moment().lang('es').format('LL')).style(styles.personal);
                    ws.cell(3, 1, 3, 5, true).string("Listado de Personal").style(styles.personalEmpresa).style(styles.text_center).style(styles.bold);
                    ws.cell(4, 1).string("NOMBRE").style(styles.personal).style(styles.text_center);
                    ws.cell(4, 2).string("ALTA INSTITUTO").style(styles.personal).style(styles.text_center);
                    ws.cell(4, 3).string("TIPO").style(styles.personal).style(styles.text_center);
                    ws.cell(4, 4).string("DOCUMENTO").style(styles.personal).style(styles.text_center);
                    ws.cell(4, 5).string("CUIL").style(styles.personal).style(styles.text_center);

                    for (let i = 0; i < personas.length; i++) {
                        ws.cell(5 + i, 1).string(personas[i].nombre);
                        ws.cell(5 + i, 2).string(moment(personas[i].datos_administrativos.fecha_alta).format('DD/MM/YYYY'));
                        ws.cell(5 + i, 3).string(personas[i].tipo_documento.toUpperCase());
                        ws.cell(5 + i, 4).string(personas[i].dni.toString());
                        ws.cell(5 + i, 5).string(personas[i].cuil);
                    }

                    ws.column(1).setWidth(30);
                    ws.column(2).setWidth(12);
                    ws.column(3).setWidth(5);
                    ws.column(4).setWidth(10);
                    ws.column(5).setWidth(15);

                    wb.write('listado_personal.xlsx', res);
                });
        }
        catch (e) {
            console.log(e);
            return res.status(500).json({
                success: false,
                message: 'Error al imprimir reporte, contacte al administrador'
            });
        }
    };
