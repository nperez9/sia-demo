const base = '/listados';
const personasCuil = require('./personas_cuil');
const suplentes = require('./suplentes');
const novedades = require('./novedades');
const conceptos = require('./conceptos');
const caja_complementaria = require('./caja_complementaria');
const estadistica = require('./estadistica');
const personal = require('./personal');

module.exports = function (router) {
    router.post(base + '/personas_cuil', personasCuil.personas_cuil);
    router.post(base + '/suplentes', suplentes);
    router.post(base + '/novedades', novedades);
    router.post(base + '/conceptos', conceptos.conceptos);
    router.post(base + '/forzados', conceptos.forzados);
    router.post(base + '/caja_complementaria', caja_complementaria.cajaComplementaria);
    router.post(base + '/resumen_caja_complementaria', caja_complementaria.resumenCajaComplementaria);
    router.post(base + '/caja_complementaria/save_configuracion', caja_complementaria.saveConfiguracion);
    router.post(base + '/estadistica', estadistica);
    router.post(base + '/personal', personal);

    router.get(base + '/caja_complementaria/get_configuracion/:empresa', caja_complementaria.getConfiguracion);
}