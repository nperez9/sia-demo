const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./listados.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const fs = require('fs');
var pdf = require('html-pdf');
const mustache = require('mustache');
const objectID = mongoose.Types.ObjectId;

const Empresas = mongoose.model('empresas');
const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');
const ConfigCaja = mongoose.model('caja_config');

function saveConfiguracion(req, res) {
    var empresa = req.body.empresa;
    var conceptos = req.body.conceptos;
    var newConceptos = [];

    for (let i = 0; i < conceptos.length; i++) {
        if (conceptos[i] != 1) {
            newConceptos.push(conceptos[i])
        }
    }

    var config = {
        empresa: new objectID(empresa),
        conceptos: newConceptos
    }

    ConfigCaja.update({ empresa: new objectID(empresa) }, config, { multi: false, upsert: true })
        .exec((err, notifications) => {
            if (err) return res.status(500).json({ 'success': false, message: "Error" });
            return res.json({ 'success': true, message: 'Conceptos guardados!' })
        });
}

function getConfiguracion(req, res) {
    var empresa = new objectID(req.params.empresa)

    ConfigCaja.find({ empresa: empresa },
        (err, conceptos) => {
            return res.json(conceptos);
        });
}

function cajaComplementaria(req, res) {

    try {
        const periodo = req.body.periodo;
        const empresa = req.body.empresa;
        const conceptosCodigos = req.body.conceptos;

        let periodoID = new objectID(periodo);
        let empresaID = new objectID(empresa);

        var wb = new excel4node.Workbook();

        let match;

        match = {
            'periodo._id': periodoID,
            'legajo.subempresa.empresa._id': empresaID
        }

        var group = {
            _id: '$_id',
            fecha_nac: { $first: '$legajo.persona.fecha_nacimiento' },
            sexo: { $first: '$legajo.persona.sexo' },
            nombre: { $first: '$legajo.persona.nombre' },
            tipo_doc: { $first: '$legajo.persona.dni' },
            dni: { $first: '$legajo.persona.dni' },
            cuil: { $first: '$legajo.persona.cuil' },
            conceptos: { $first: '$conceptos' }
        };

        Liquidaciones.aggregate([
            { $match: match },
            { $unwind: '$legajo' },
            {
                $group: group
            }
        ]).exec((err, docs) => {

            ConfigCaja.find({ empresa: empresa },
                (err, conceptos) => {

                    for (let i = 0; i < docs.length; i++) {

                        if (docs.length < 1) {
                            return res.status(504).json({ success: false, message: 'El reporte no posee registros' })
                        }

                        var ws = wb.addWorksheet();

                        var fecha = docs[i].fecha_nac == null ? "00/00/0000" : moment(fecha_nac).format("DD/MM/YYYY");
                        ws.cell(i + 1, 1).string(fecha);
                        ws.cell(i + 1, 2).string(sexo);
                        ws.cell(i + 1, 3).string(docs[i].nombre);
                        ws.cell(i + 1, 4).string("DNI");
                        ws.cell(i + 1, 5).string(docs[i].dni.toString());
                        ws.cell(i + 1, 6).string(docs[i].cuil);

                        var ccTotal = 0;

                        for (let a = 0; a < docs[i].conceptos.length; a++) {
                            if (docs[i].conceptos[a].caja_complementaria) {
                                for (let x = 0; x < conceptos[0].conceptos.length; x++) {
                                    if (docs[i].conceptos[a].codigo == conceptos[0].conceptos[x].codigo) {
                                        ccTotal = ccTotal + docs[i].conceptos[a].calculado;
                                    }
                                }
                            }
                        }

                        ws.cell(i + 1, 7).string(ccTotal.toFixed(2));
                        var ccRetencion = (ccTotal * 4.5) / 100;
                        ws.cell(i + 1, 8).string(ccRetencion.toFixed(2));
                    }

                    ws.column(1).setWidth(10);
                    ws.column(2).setWidth(2);
                    ws.column(3).setWidth(30);
                    ws.column(4).setWidth(5);
                    ws.column(5).setWidth(10);
                    ws.column(6).setWidth(10);
                    ws.column(7).setWidth(5);
                    ws.column(8).setWidth(5);

                    wb.write('persona_cuil.xlsx', res);
                });
        });
    }
    catch (e) {
        console.log(e);
        return res.status(500).json({
            success: false,
            message: 'Error al imprimir reporte, contacte al administrador'
        });
    }

}

function resumenCajaComplementaria(req, res) {

    try {
        const periodo = req.body.periodo;
        const empresa = req.body.empresa;
        const conceptosCodigos = req.body.conceptos;

        let periodoID = new objectID(periodo);
        let empresaID = new objectID(empresa);

        var wb = new excel4node.Workbook();

        let match;

        match = {
            'periodo._id': periodoID,
            'legajo.subempresa.empresa._id': empresaID
        }

        var group = {
            _id: '$_id',
            nombre: { $first: '$legajo.persona.nombre' },
            conceptos: { $first: '$conceptos' },
            periodo: { $first: '$periodo.descripcion' }
        };

        Liquidaciones.aggregate([
            { $match: match },
            { $unwind: '$legajo' },
            {
                $group: group
            }
        ]).exec((err, docs) => {

            ConfigCaja.find({ empresa: empresa },
                (err, conceptos) => {

                    Empresas.findOne({ _id: empresaID },
                        (err, empresa) => {
                            var ccTotal = 0;
                            var empleados = [];

                            for (let i = 0; i < docs.length; i++) {
                                if (docs.length < 1) {
                                    return res.status(504).json({ success: false, message: 'El reporte no posee registros' })
                                }

                                if (empleados.indexOf(docs[i].nombre) == -1) {
                                    empleados.push(docs[i].nombre);
                                }

                                for (let a = 0; a < docs[i].conceptos.length; a++) {
                                    if (docs[i].conceptos[a].caja_complementaria) {
                                        for (let x = 0; x < conceptos[0].conceptos.length; x++) {
                                            if (docs[i].conceptos[a].codigo == conceptos[0].conceptos[x].codigo) {
                                                ccTotal = ccTotal + docs[i].conceptos[a].calculado;
                                            }
                                        }
                                    }
                                }
                            }

                            var view = {
                                empresa: empresa.razon_social,
                                domicilio: empresa.direccion,
                                localidad: empresa.localidad,
                                cuit: empresa.cuit,
                                periodo: docs[0].periodo.toUpperCase(),
                                ccTotal: ccTotal.toFixed(2),
                                ccRetencion: ((ccTotal * 4.5) / 100).toFixed(2),
                                empleados: empleados.length,
                                fecha_hoy: moment().format('DD/MM/YYYY'),
                                hora: moment().format('LT')
                            };

                            var content = fs.readFileSync("src/modules/listados/templates/resumen_caja_complementaria.tpl.html", 'utf8');
                            html = mustache.render(content, view);

                            var options = {
                                format: 'a4'
                            };

                            pdf.create(html, options).toStream(function (err, stream) {
                                stream.pipe(res);
                            });

                        });
                });
        });
    }
    catch (e) {
        console.log(e);
        return res.status(500).json({
            success: false,
            message: 'Error al imprimir reporte, contacte al administrador'
        });
    }

}

module.exports = {
    saveConfiguracion,
    getConfiguracion,
    cajaComplementaria,
    resumenCajaComplementaria
};