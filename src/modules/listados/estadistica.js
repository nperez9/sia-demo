const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./listados.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const objectID = mongoose.Types.ObjectId;

const Empresas = mongoose.model('empresas');
const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');

module.exports = function persona(req, res) {
    try {
        const periodo = req.body.periodo;
        const empresa = req.body.empresa;
        const conceptosCodigos = req.body.conceptos;
        const personas = req.body.personas;

        let periodoID = new objectID(periodo);
        let empresaID = new objectID(empresa);

        var wb = new excel4node.Workbook();

        let match;

        match = {
            'periodo._id': periodoID,
            'legajo.subempresa.empresa._id': empresaID,
        }

        var agrupador = '$_id';

        if (personas) {
            agrupador = '$legajo.persona._id';
        }

        var group = {
            _id: agrupador,
            empresa: { $first: '$legajo.subempresa.empresa.razon_social' },
            cuil: { $first: '$legajo.persona.cuil' },
            nombre: { $first: '$legajo.persona.nombre' },
            conceptos: { $push: '$conceptos' },
            periodo: { $first: '$periodo.descripcion' }
        };

        Liquidaciones.aggregate([
            { $match: match },
            { $unwind: '$legajo' },
            {
                $group: group
            },
            { $sort: { nombre: 1 } }
        ]).exec((err, docs) => {
            if (docs.length < 1) {
                return res.status(504).json({ success: false, message: 'El reporte no posee registros' })
            }

            var ws = wb.addWorksheet();

            ws.cell(1, 2).string(docs[0].empresa).style(styles.estadisticas);
            ws.cell(2, 2).string("Fecha: " + moment().lang('es').format('LL')).style(styles.estadisticas);
            ws.cell(3, 2).string("Estadistica de conceptos - Periodo: " + docs[0].periodo.toUpperCase()).style(styles.estadisticas);

            ws.cell(5, 2).string("CUIL").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 3).string("Apellido y nombres").style(styles.estadisticas).style(styles.text_center);


            ws.cell(5, 4).string("Sueldos Basicos").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 5).string("Antiguedad").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 6).string("Presentismo").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 7).string("Decreto 1295").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 8).string("A cta Futuros Aumentos").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 9).string("Bonif. Institucionales").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 10).string("Adicional Sueldo Basico Dto.1567").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 11).string("Plus vacacional").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 12).string("Sac").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 13).string("Bap").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 14).string("Horas Extras 50%").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 15).string("Horas Extras 100%").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 16).string("Blt").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 17).string("Adicional Título").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 18).string("Tareas Riesgosas").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 19).string("Adelanto/plus X Vacaciones").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 20).string("Capacitación").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 21).string("Premio Asistencia").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 22).string("Adicional Institucionales").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 23).string("Diferecnia antiguedad").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 24).string("Adicionales Institucionales S/caja").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 25).string("Desc. De Dias/alta/baja/oblig").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 26).string("Asig. Por Hijo").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 27).string("Prenatal").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 28).string("Asignación Por Matrimonio").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 29).string("Asignación por Nacimiento").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 30).string("Asignación por Adopción").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 31).string("Ayuda Escolar").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 32).string("Hijo Discapacitado").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 33).string("Dif. Salario").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 34).string("Asig.cgep Res.2/04").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 35).string("Incentivo Docente S/Ap").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 36).string("Asig. Anual X Vacaciones").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 37).string("Diferencia De Salario").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 38).string("Prop. Vacaciones No Gozadas").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 39).string("Idemnizacion Por Antig.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 40).string("Preaviso").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 41).string("Sac Sobre Preaviso").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 42).string("Dias de Integracion de Haberes").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 43).string("Sumas No Remun.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 44).string("Adelanto de Haberes").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 45).string("Desc. Por Dias/alta/baja/hs").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 46).string("Jubilación").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 47).string("Ley 19032").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 48).string("Caja Complementaria").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 49).string("Embargo").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 50).string("Redondeo").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 51).string("Obra Social").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 52).string("Obra Social Familiar").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 53).string("Dif. Min. O.s. Personal / Patronal").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 54).string("Impuesto a las Ganancias").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 55).string("Sindicato S.A.D.O.P.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 56).string("Fondo Solidario").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 57).string("S.A.E.O.E.P.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 58).string("Decreto 137/05 Res.33/05").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 59).string("Sindicato Uda").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 60).string("Dec.682/06 Art. 1°").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 61).string("Dif. Mes Anterior").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 62).string("Dec.618/07 Art. 3°").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 63).string("Conyuge").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 64).string("Adic.hijo Menor 4 Años A-OS").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 65).string("Adic.hijo Menor 4 Años A-OS Disc.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 66).string("Familia Numerosa").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 67).string("Asig.fam.esc.primaria").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 68).string("Asig.fam.esc.primaria Disc.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 69).string("Asig.fam.esc.secundaria").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 70).string("Asig.fam.esc.secundaria Disc.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 71).string("Asig.fam.esc.superior").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 72).string("Asig.fam.esc.superior Disc.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 73).string("Total Remun.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 74).string("Total No Rem.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 75).string("Total Aa.ff.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 76).string("Total Retenc.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 77).string("Total Neto").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 78).string("Jub. Pat.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 79).string("Ley Pat.").style(styles.estadisticas).style(styles.text_center);
            ws.cell(5, 80).string("O.s. Pat").style(styles.estadisticas).style(styles.text_center);

            var aux = 7;

            var totalCol1 = 0;
            var totalCol2 = 0;
            var totalCol3 = 0;
            var totalCol4 = 0;
            var totalCol5 = 0;
            var totalCol6 = 0;
            var totalCol7 = 0;
            var totalCol8 = 0;
            var totalCol9 = 0;
            var totalCol10 = 0;
            var totalCol11 = 0;
            var totalCol12 = 0;
            var totalCol13 = 0;
            var totalCol14 = 0;
            var totalCol15 = 0;
            var totalCol16 = 0;
            var totalCol17 = 0;
            var totalCol18 = 0;
            var totalCol19 = 0;
            var totalCol20 = 0;
            var totalCol21 = 0;
            var totalCol22 = 0;
            var totalCol23 = 0;
            var totalCol24 = 0;
            var totalCol25 = 0;
            var totalCol26 = 0;
            var totalCol27 = 0;
            var totalCol28 = 0;
            var totalCol29 = 0;
            var totalCol30 = 0;
            var totalCol31 = 0;
            var totalCol32 = 0;
            var totalCol33 = 0;
            var totalCol34 = 0;
            var totalCol35 = 0;
            var totalCol36 = 0;
            var totalCol37 = 0;
            var totalCol38 = 0;
            var totalCol39 = 0;
            var totalCol40 = 0;
            var totalCol41 = 0;
            var totalCol42 = 0;
            var totalCol43 = 0;
            var totalCol44 = 0;
            var totalCol45 = 0;
            var totalCol46 = 0;
            var totalCol47 = 0;
            var totalCol48 = 0;
            var totalCol49 = 0;
            var totalCol50 = 0;
            var totalCol51 = 0;
            var totalCol52 = 0;
            var totalCol53 = 0;
            var totalCol54 = 0;
            var totalCol55 = 0;
            var totalCol56 = 0;
            var totalCol57 = 0;
            var totalCol58 = 0;
            var totalCol59 = 0;
            var totalCol60 = 0;
            var totalCol61 = 0;
            var totalCol62 = 0;
            var totalCol63 = 0;
            var totalCol64 = 0;
            var totalCol65 = 0;
            var totalCol66 = 0;
            var totalCol67 = 0;
            var totalCol68 = 0;
            var totalCol69 = 0;
            var totalCol70 = 0;
            var totalCol71 = 0;
            var totalCol72 = 0;
            var totalCol73 = 0;
            var totalCol74 = 0;
            var totalCol75 = 0;
            var totalCol76 = 0;
            var totalCol77 = 0;
            var totalCol78 = 0;

            for (let i = 0; i < docs.length; i++) {
                ws.cell(aux, 2).string(docs[i].cuil).style(styles.estadisticas).style(styles.text_center);
                ws.cell(aux, 3).string(docs[i].nombre).style(styles.estadisticas);

                var col1 = 0;
                var col2 = 0;
                var col3 = 0;
                var col4 = 0;
                var col5 = 0;
                var col6 = 0;
                var col7 = 0;
                var col8 = 0;
                var col9 = 0;
                var col10 = 0;
                var col11 = 0;
                var col12 = 0;
                var col13 = 0;
                var col14 = 0;
                var col15 = 0;
                var col16 = 0;
                var col17 = 0;
                var col18 = 0;
                var col19 = 0;
                var col20 = 0;
                var col21 = 0;
                var col22 = 0;
                var col23 = 0;
                var col24 = 0;
                var col25 = 0;
                var col26 = 0;
                var col27 = 0;
                var col28 = 0;
                var col29 = 0;
                var col30 = 0;
                var col31 = 0;
                var col32 = 0;
                var col33 = 0;
                var col34 = 0;
                var col35 = 0;
                var col36 = 0;
                var col37 = 0;
                var col38 = 0;
                var col39 = 0;
                var col40 = 0;
                var col41 = 0;
                var col42 = 0;
                var col43 = 0;
                var col44 = 0;
                var col45 = 0;
                var col46 = 0;
                var col47 = 0;
                var col48 = 0;
                var col49 = 0;
                var col50 = 0;
                var col51 = 0;
                var col52 = 0;
                var col53 = 0;
                var col54 = 0;
                var col55 = 0;
                var col56 = 0;
                var col57 = 0;
                var col58 = 0;
                var col59 = 0;
                var col60 = 0;
                var col61 = 0;
                var col62 = 0;
                var col63 = 0;
                var col64 = 0;
                var col65 = 0;
                var col66 = 0;
                var col67 = 0;
                var col68 = 0;
                var col69 = 0;
                var col70 = 0;


                for (var x = 0; x < docs[i].conceptos.length; x++) {
                    for (let z = 0; z < docs[i].conceptos[x].length; z++) {
                        monto = docs[i].conceptos[x][z].calculado == null ? 0 : docs[i].conceptos[x][z].calculado;

                        switch (docs[i].conceptos[x][z].orden) {
                            case 1:
                                col1 = col1 + monto;
                                totalCol1 = totalCol1 + monto;
                                break;
                            case 2:
                                col2 = col2 + monto;
                                totalCol2 = totalCol2 + monto;
                                break;
                            case 3:
                                col3 = col3 + monto;
                                totalCol3 = totalCol3 + monto;
                                break;
                            case 4:
                                col4 = col4 + monto;
                                totalCol4 = totalCol4 + monto;
                                break;
                            case 5:
                                col5 = col5 + monto;
                                totalCol5 = totalCol5 + monto;
                                break;
                            case 6:
                                col6 = col6 + monto;
                                totalCol6 = totalCol6 + monto;
                                break;
                            case 7:
                                col7 = col7 + monto;
                                totalCol7 = totalCol7 + monto;
                                break;
                            case 8:
                                col8 = col8 + monto;
                                totalCol8 = totalCol8 + monto;
                                break;
                            case 9:
                                col9 = col9 + monto;
                                totalCol9 = totalCol9 + monto;
                                break;

                            case 10:
                                col22 = col22 + monto;
                                totalCol22 = totalCol22 + monto;
                                break;
                            case 11:
                                col23 = col23 + monto;
                                totalCol23 = totalCol23 + monto;
                                break;
                            case 12:
                                col24 = col24 + monto;
                                totalCol24 = totalCol24 + monto;
                                break;
                            case 13:
                                col25 = col25 + monto;
                                totalCol25 = totalCol25 + monto;
                                break;
                            case 14:
                                col26 = col26 + monto;
                                totalCol26 = totalCol26 + monto;
                                break;
                            case 15:
                                col27 = col27 + monto;
                                totalCol27 = totalCol27 + monto;
                                break;
                            case 16:
                                col28 = col28 + monto;
                                totalCol28 = totalCol28 + monto;
                                break;
                            case 17:
                                col29 = col29 + monto;
                                totalCol29 = totalCol29 + monto;
                                break;
                            case 18:
                                col30 = col30 + monto;
                                totalCol30 = totalCol30 + monto;
                                break;
                            case 19:
                                col31 = col31 + monto;
                                totalCol31 = totalCol31 + monto;
                                break;
                            case 20:
                                col32 = col32 + monto;
                                totalCol32 = totalCol32 + monto;
                                break;
                            case 21:
                                col33 = col33 + monto;
                                totalCol33 = totalCol33 + monto;
                                break;
                            case 22:
                                col34 = col34 + monto;
                                totalCol34 = totalCol34 + monto;
                                break;
                            case 23:
                                col42 = col42 + monto;
                                totalCol42 = totalCol42 + monto;
                                break;
                            case 24:
                                col43 = col43 + monto;
                                totalCol43 = totalCol43 + monto;
                                break;
                            case 25:
                                col44 = col44 + monto;
                                totalCol44 = totalCol44 + monto;
                                break;
                            case 26:
                                col45 = col45 + monto;
                                totalCol45 = totalCol45 + monto;
                                break;
                            case 27:
                                col46 = col46 + monto;
                                totalCol46 = totalCol46 + monto;
                                break;
                            case 28:
                                col28 = col28 + monto;
                                totalCol28 = totalCol28 + monto;
                                break;
                            case 29:
                                col47 = col47 + monto;
                                totalCol47 = totalCol47 + monto;
                                break;
                            case 30:
                                col48 = col48 + monto;
                                totalCol48 = totalCol48 + monto;
                                break;
                            case 31:
                                col49 = col49 + monto;
                                totalCol49 = totalCol49 + monto;
                                break;
                            case 32:
                                col50 = col50 + monto;
                                totalCol50 = totalCol50 + monto;
                                break;
                            case 33:
                                col51 = col51 + monto;
                                totalCol51 = totalCol51 + monto;
                                break;
                            case 34:
                                col52 = col52 + monto;
                                totalCol52 = totalCol52 + monto;
                                break;
                            case 35:
                                col53 = col53 + monto;
                                totalCol53 = totalCol53 + monto;
                                break;
                            case 36:
                                col10 = col10 + monto;
                                totalCol10 = totalCol10 + monto;
                                break;
                            case 37:
                                col11 = col11 + monto;
                                totalCol11 = totalCol11 + monto;
                                break;
                            case 38:
                                col12 = col12 + monto;
                                totalCol12 = totalCol12 + monto;
                                break;
                            case 39:
                                col13 = col13 + monto;
                                totalCol13 = totalCol13 + monto;
                                break;
                            case 40:
                                col14 = col14 + monto;
                                totalCol14 = totalCol14 + monto;
                                break;
                            case 41:
                                col15 = col15 + monto;
                                totalCol15 = totalCol15 + monto;
                                break;
                            case 42:
                                col16 = col16 + monto;
                                totalCol16 = totalCol16 + monto;
                                break;
                            case 43:
                                col17 = col17 + monto;
                                totalCol17 = totalCol17 + monto;
                                break;
                            case 44:
                                col35 = col35 + monto;
                                totalCol35 = totalCol35 + monto;
                                break;
                            case 45:
                                col36 = col36 + monto;
                                totalCol36 = totalCol36 + monto;
                                break;
                            case 46:
                                col37 = col37 + monto;
                                totalCol37 = totalCol37 + monto;
                                break;
                            case 47:
                                col38 = col38 + monto;
                                totalCol38 = totalCol38 + monto;
                                break;
                            case 48:
                                col39 = col39 + monto;
                                totalCol39 = totalCol39 + monto;
                                break;
                            case 49:
                                col49 = col49 + monto;
                                totalCol49 = totalCol49 + monto;
                                break;
                            case 50:
                                col18 = col18 + monto;
                                totalCol18 = totalCol18 + monto;
                                break;
                            case 51:
                                col19 = col19 + monto;
                                totalCol19 = totalCol19 + monto;
                                break;
                            case 52:
                                col20 = col20 + monto;
                                totalCol20 = totalCol20 + monto;
                                break;
                            case 53:
                                col21 = col21 + monto;
                                totalCol21 = totalCol21 + monto;
                                break;
                            case 54:
                                col40 = col40 + monto;
                                totalCol40 = totalCol40 + monto;
                                break;
                            case 55:
                                col41 = col41 + monto;
                                totalCol41 = totalCol41 + monto;
                                break;
                            case 56:
                                col55 = col55 + monto;
                                totalCol55 = totalCol55 + monto;
                                break;
                            case 57:
                                col56 = col56 + monto;
                                totalCol56 = totalCol56 + monto;
                                break;
                            case 58:
                                col57 = col57 + monto;
                                totalCol57 = totalCol57 + monto;
                                break;
                            case 59:
                                col58 = col58 + monto;
                                totalCol58 = totalCol58 + monto;
                                break;
                            case 60:
                                col59 = col59 + monto;
                                totalCol59 = totalCol59 + monto;
                                break;
                            case 61:
                                col60 = col60 + monto;
                                totalCol60 = totalCol60 + monto;
                                break;
                            case 62:
                                col61 = col61 + monto;
                                totalCol61 = totalCol61 + monto;
                                break;
                            case 63:
                                col62 = col62 + monto;
                                totalCol62 = totalCol62 + monto;
                                break;
                            case 64:
                                col63 = col63 + monto;
                                totalCol63 = totalCol63 + monto;
                                break;
                            case 65:
                                col64 = col64 + monto;
                                totalCol64 = totalCol64 + monto;
                                break;
                            case 66:
                                col65 = col65 + monto;
                                totalCol65 = totalCol65 + monto;
                                break;
                            case 67:
                                col66 = col66 + monto;
                                totalCol66 = totalCol66 + monto;
                                break;
                            case 68:
                                col67 = col67 + monto;
                                totalCol67 = totalCol67 + monto;
                                break;
                            case 69:
                                col68 = col68 + monto;
                                totalCol68 = totalCol68 + monto;
                                break;
                            case 70:
                                col69 = col69 + monto;
                                totalCol69 = totalCol69 + monto;
                                break;
                        }
                    }
                }

                ws.cell(aux, 4).string(col1.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 5).string(col2.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 6).string(col3.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 7).string(col4.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 8).string(col5.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 9).string(col6.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 10).string(col7.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 11).string(col8.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 12).string(col9.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 13).string(col36.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 14).string(col37.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 15).string(col38.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 16).string(col39.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 17).string(col40.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 18).string(col41.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 19).string(col42.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 20).string(col43.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 21).string(col50.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 22).string(col51.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 23).string(col52.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 24).string(col53.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 25).string(col10.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 26).string(col11.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 27).string(col12.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 28).string(col13.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 29).string(col14.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 30).string(col15.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 31).string(col16.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 32).string(col17.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 33).string(col18.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 34).string(col19.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 35).string(col20.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 36).string(col21.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 37).string(col22.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 38).string(col44.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 39).string(col45.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 40).string(col46.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 41).string(col47.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 42).string(col48.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 43).string(col54.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 44).string(col55.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 45).string(col23.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 46).string(col24.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 47).string(col25.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 48).string(col26.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 49).string(col27.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 50).string(col29.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 51).string(col30.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 52).string(col31.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 53).string(col32.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 54).string(col33.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 55).string(col34.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 56).string(col35.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 57).string(col49.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 58).string(col56.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 59).string(col57.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 60).string(col58.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 61).string(col59.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 62).string(col60.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 63).string(col61.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 64).string(col62.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 65).string(col63.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 66).string(col64.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 67).string(col65.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 68).string(col66.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 69).string(col67.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 70).string(col68.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 71).string(col69.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 72).string(col70.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);


                var col71 = col4 + col5 + col6 + col7 + col8 + col9 + col10 + col11 + col12 + col13 + col14 + col15 + col16 + col17 + col18 + col19 + col20 + col21 + col22 + col23 + col24 + col25 + col60 + col61 + col62;
                var col72 = col34 + col35 + col36 + col37 + col38 + col39 + col40 + col41 + col42 + col43 + col45;
                var col73 = col26 + col27 + col28 + col29 + col30 + col31 + col32 + col33 + col63 + col64 + col65 + col66 + col67 + col68 + col69 + col70 + col71 + col72;
                var col74 = col44 + col46 + col47 + col48 + col49 + col50 + col51 + col52 + col53 + col54 + col55 + col56 + col57 + col58 + col59;
                var col75 = col73 + col74 - col76;
                var col76 = col46 / 11 * 7.81;
                var col77 = col47 / 3 * 0.59;
                var col78 = col51 * 2;

                totalCol71 = totalCol71 + col71;
                totalCol72 = totalCol72 + col72;
                totalCol73 = totalCol73 + col73;
                totalCol74 = totalCol74 + col74;
                totalCol75 = totalCol75 + col75;
                totalCol77 = totalCol77 + col77;
                totalCol78 = totalCol78 + col78;

                ws.cell(aux, 73).string(col71.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 74).string(col72.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 75).string(col73.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 76).string(col74.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 77).string(col75.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 78).string(col76.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 79).string(col77.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
                ws.cell(aux, 80).string(col78.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);

                aux++
            }

            ws.cell(aux, 4).string(totalCol1.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 5).string(totalCol2.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 6).string(totalCol3.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 7).string(totalCol4.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 8).string(totalCol5.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 9).string(totalCol6.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 10).string(totalCol7.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 11).string(totalCol8.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 12).string(totalCol9.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 13).string(totalCol36.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 14).string(totalCol37.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 15).string(totalCol38.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 16).string(totalCol39.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 17).string(totalCol40.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 18).string(totalCol41.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 19).string(totalCol42.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 20).string(totalCol43.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 21).string(totalCol50.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 22).string(totalCol51.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 23).string(totalCol52.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 24).string(totalCol53.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 25).string(totalCol10.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 26).string(totalCol11.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 27).string(totalCol12.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 28).string(totalCol13.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 29).string(totalCol14.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 30).string(totalCol15.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 31).string(totalCol16.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 32).string(totalCol17.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 33).string(totalCol18.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 34).string(totalCol19.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 35).string(totalCol20.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 36).string(totalCol21.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 37).string(totalCol22.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 38).string(totalCol44.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 39).string(totalCol45.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 40).string(totalCol46.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 41).string(totalCol47.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 42).string(totalCol48.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 43).string(totalCol54.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 44).string(totalCol55.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 45).string(totalCol23.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 46).string(totalCol24.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 47).string(totalCol25.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 48).string(totalCol26.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 49).string(totalCol27.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 50).string(totalCol29.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 51).string(totalCol30.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 52).string(totalCol31.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 53).string(totalCol32.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 54).string(totalCol33.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 55).string(totalCol34.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 56).string(totalCol35.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 57).string(totalCol49.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 58).string(totalCol56.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 59).string(totalCol57.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 60).string(totalCol58.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 61).string(totalCol59.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 62).string(totalCol60.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 63).string(totalCol61.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 64).string(totalCol62.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 65).string(totalCol63.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 66).string(totalCol64.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 67).string(totalCol65.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 68).string(totalCol66.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 69).string(totalCol67.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 70).string(totalCol68.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 71).string(totalCol69.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 72).string(totalCol70.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 73).string(totalCol71.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 74).string(totalCol72.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 75).string(totalCol73.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 76).string(totalCol74.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 77).string(totalCol75.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 78).string(totalCol76.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 79).string(totalCol77.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);
            ws.cell(aux, 80).string(totalCol78.toFixed(2)).style(styles.estadisticas).style(styles.text_rigth);

            ws.column(1).setWidth(2);
            ws.column(3).setWidth(30);

            wb.write('persona_cuil.xlsx', res);

        });
    }
    catch (e) {
        console.log(e);
        return res.status(500).json({
            success: false,
            message: 'Error al imprimir reporte, contacte al administrador'
        });
    }
}