const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./listados.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const objectID = mongoose.Types.ObjectId;

const Empresas = mongoose.model('empresas');
const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');

function conceptos(req, res) {
    try {
        const periodo = req.body.periodo;
        const empresa = req.body.empresa;
        const conceptosCodigos = req.body.conceptos;

        let periodoID = new objectID(periodo);
        let empresaID = new objectID(empresa);

        var wb = new excel4node.Workbook();

        let match;

        match = {
            'periodo._id': periodoID,
            'legajo.subempresa.empresa._id': empresaID,
        }

        var group = {
            _id: '$_id',
            empresa: { $first: '$legajo.subempresa.empresa.razon_social' },
            numero: { $first: '$legajo.numero_legajo' },
            cuil: { $first: '$legajo.persona.cuil' },
            nombre: { $first: '$legajo.persona.nombre' },
            neto: { $first: '$neto' },
            bruto: { $first: '$bruto' },
            conceptos: { $first: '$conceptos' }
        };

        Liquidaciones.aggregate([
            { $match: match },
            { $unwind: '$legajo' },
            {
                $group: group
            }
        ]).exec((err, docs) => {
            if (docs.length < 1) {
                return res.status(504).json({ success: false, message: 'El reporte no posee registros' })
            }

            var ws = wb.addWorksheet();

            ws.cell(1, 2).string(docs[0].empresa).style(styles.conceptos_text);
            ws.cell(2, 2).string("Fecha: " + moment().format("DD/MM/YYYY")).style(styles.conceptos_text);
            ws.cell(3, 2).string("Listado de Importes de conceptos").style(styles.conceptos_text);

            ws.cell(7,2).string("N° Leg.").style(styles.forzados).style(styles.text_center);
            ws.cell(7,3).string("C.U.I.L.").style(styles.forzados).style(styles.text_center);
            ws.cell(7,4).string("Nombre").style(styles.forzados).style(styles.text_center);
            ws.cell(7,5).string("Netos").style(styles.forzados).style(styles.text_center);
            ws.cell(7,6).string("Brutos").style(styles.forzados).style(styles.text_center);
            ws.cell(7,7).string("Importe").style(styles.forzados).style(styles.text_center);
            ws.cell(7,8).string("Descripción").style(styles.forzados).style(styles.text_center);
            
            var totalNeto = 0;
            var totalBruto = 0;
            var totalImporte = 0;
            var aux = 8;

            for (let i = 1; i < conceptosCodigos.length; i++) {
                for (let e = 0; e < docs.length; e++) {
                    for (let u = 0; u < docs[e].conceptos.length; u++) {
                        if(docs[e].conceptos[u].codigo == conceptosCodigos[i]){

                            var legajo = docs[e].numero == null ? "" : docs[e].numero;
                            ws.cell(aux, 2).string(legajo).style(styles.conceptos_text).style(styles.text_center);
                            ws.cell(aux, 3).string(docs[e].cuil).style(styles.conceptos_text).style(styles.text_center);
                            ws.cell(aux, 4).string(docs[e].nombre).style(styles.conceptos_text);
                            ws.cell(aux, 5).string(docs[e].neto.toFixed(2)).style(styles.conceptos_text).style(styles.text_rigth);
                            ws.cell(aux, 6).string(docs[e].bruto.toFixed(2)).style(styles.conceptos_text).style(styles.text_rigth);
                            ws.cell(aux, 7).string(docs[e].conceptos[u].calculado.toFixed(2)).style(styles.conceptos_text).style(styles.text_rigth);
                            ws.cell(aux, 8).string(docs[e].conceptos[u].descripcion).style(styles.conceptos_text);

                            totalNeto = totalNeto + docs[e].neto;
                            totalBruto = totalBruto + docs[e].bruto;
                            totalImporte = totalImporte + docs[e].conceptos[u].calculado;
                            aux = aux+1
                        }
                    }
                }
            }

            ws.cell(aux+2, 2).string("").style(styles.conceptos_totals);
            ws.cell(aux+2, 3).string("TOTAL").style(styles.conceptos_totals);
            ws.cell(aux+2, 4).string("").style(styles.conceptos_totals);
            ws.cell(aux+2, 5).string(totalNeto.toFixed(2)).style(styles.conceptos_totals).style(styles.text_rigth);
            ws.cell(aux+2, 6).string(totalBruto.toFixed(2)).style(styles.conceptos_totals).style(styles.text_rigth);
            ws.cell(aux+2, 7).string(totalImporte.toFixed(2)).style(styles.conceptos_totals).style(styles.text_rigth);
            ws.cell(aux+2, 8).string("").style(styles.conceptos_totals);

            ws.column(1).setWidth(2);
            ws.column(2).setWidth(5);
            ws.column(3).setWidth(15);
            ws.column(4).setWidth(30);
            ws.column(5).setWidth(10);
            ws.column(6).setWidth(10);
            ws.column(7).setWidth(10);
            ws.column(8).setWidth(30);

            wb.write('persona_cuil.xlsx', res);

        });
    }
    catch (e) {
        console.log(e);
        return res.status(500).json({
            success: false,
            message: 'Error al imprimir reporte, contacte al administrador'
        });
    }
}

function forzados(req, res) {
    try {
        const periodo = req.body.periodo;
        const empresa = req.body.empresa;
        const conceptosCodigos = req.body.conceptos;

        let periodoID = new objectID(periodo);
        let empresaID = new objectID(empresa);

        var wb = new excel4node.Workbook();

        let match;

        match = {
            'periodo._id': periodoID,
            'legajo.subempresa.empresa._id': empresaID,
        }

        var group = {
            _id: '$_id',
            subempresa: { $first: '$legajo.subempresa.descripcion' },
            numero: { $first: '$legajo.numero_legajo' },
            nombre: { $first: '$legajo.persona.nombre' },
            conceptos: { $first: '$conceptos' }
        };

        Liquidaciones.aggregate([
            { $match: match },
            { $unwind: '$legajo' },
            {
                $group: group
            },
            {$sort: {nombre: 1}}
        ]).exec((err, docs) => {
            if (docs.length < 1) {
                return res.status(504).json({ success: false, message: 'El reporte no posee registros' })
            }

            var ws = wb.addWorksheet();

            ws.cell(1,2).string("EMPLEADO").style(styles.forzados).style(styles.text_center);
            ws.cell(1,3).string("N° LEG.").style(styles.forzados).style(styles.text_center);
            ws.cell(1,4).string("CÓDIGO").style(styles.forzados).style(styles.text_center);
            ws.cell(1,5).string("DESCRIP.").style(styles.forzados).style(styles.text_center);
            ws.cell(1,6).string("IMPORTE").style(styles.forzados).style(styles.text_center);
            ws.cell(1,7).string("SUBEMPRESA").style(styles.forzados).style(styles.text_center);
            
            var aux = 3;

            for (let i = 1; i < conceptosCodigos.length; i++) {
                for (let e = 0; e < docs.length; e++) {
                    for (let u = 0; u < docs[e].conceptos.length; u++) {
                        if(docs[e].conceptos[u].codigo == conceptosCodigos[i]){

                            ws.cell(aux, 2).string(docs[e].nombre).style(styles.forzados);
                            var legajo = docs[e].numero == null ? "" : docs[e].numero;
                            ws.cell(aux, 3).string(legajo).style(styles.forzados).style(styles.text_center);
                            ws.cell(aux, 4).string(docs[e].conceptos[u].codigo).style(styles.forzados).style(styles.text_center);
                            ws.cell(aux, 5).string(docs[e].conceptos[u].descripcion).style(styles.forzados);
                            ws.cell(aux, 6).string(docs[e].conceptos[u].calculado.toFixed(2)).style(styles.forzados).style(styles.text_rigth);
                            ws.cell(aux, 7).string(docs[e].subempresa).style(styles.forzados);

                            aux = aux+1
                        }
                    }
                }
            }

            ws.column(1).setWidth(2);
            ws.column(2).setWidth(25);
            ws.column(3).setWidth(10);
            ws.column(4).setWidth(10);
            ws.column(5).setWidth(20);
            ws.column(6).setWidth(10);
            ws.column(7).setWidth(10);

            wb.write('listado_conceptos_forzados.xlsx', res);

        });
    }
    catch (e) {
        console.log(e);
        return res.status(500).json({
            success: false,
            message: 'Error al imprimir reporte, contacte al administrador'
        });
    }
}

module.exports = {
    conceptos,
    forzados
};