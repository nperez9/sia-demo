const excel4node = require('excel4node');

var wb = new excel4node.Workbook();

var cuil_title = wb.createStyle({
    font: {
        name: 'Arial',
        color: '#000000',
        size: 10,
        bold: true,
    }
});

var cuil_subempresa = wb.createStyle({
    font: {
        name: 'Arial',
        color: '#000000',
        size: 8,
    }
});

var text_center = wb.createStyle({
    alignment: {
        horizontal: 'center',
    }
});

var text_rigth = wb.createStyle({
    alignment: {
        horizontal: 'right',
    }
});

var border_bottom = wb.createStyle({
    border: {
        bottom: {
            style: 'thin',
            color: '#000000'
        }
    }
});

var border_left = wb.createStyle({
    border: {
        left: {
            style: 'thin',
            color: '#000000'
        }
    }
});

var border_right = wb.createStyle({
    border: {
        right: {
            style: 'thin',
            color: '#000000'
        }
    }
});

var border_top = wb.createStyle({
    border: {
        top: {
            style: 'thin',
            color: '#000000'
        }
    }
});

var all_lines = wb.createStyle({
    border: {
        right: {
            style: 'thin',
            color: '#000000'
        },
        left: {
            style: 'thin',
            color: '#000000'
        },
        top: {
            style: 'thin',
            color: '#000000'
        },
        bottom: {
            style: 'thin',
            color: '#000000'
        }
    }
});

var grey_background = wb.createStyle({
    fill: {
        type: 'pattern',
        patternType: 'solid',
        fgColor: '#A4A4A4'
    },
    font: {
        bold: true
    }
});

var novedades_header = wb.createStyle({
    font: {
        name: 'Arial',
        color: '#000000',
        size: 8
    }
});

var conceptos_text = wb.createStyle({
    font: {
        name: 'Arial',
        size: 8
    }
});

var conceptos_header = wb.createStyle({
    font: {
        name: 'Arial',
        size: 8,
        bold: true
    },
    border: {
        bottom: {
            style: 'thin',
            color: '#000000'
        }
    }
});

var conceptos_totals = wb.createStyle({
    font: {
        name: 'Arial',
        size: 8,
        bold: true
    },
    border: {
        top: {
            style: 'thin',
            color: '#000000'
        }
    }
});

var forzados = wb.createStyle({
    font: {
        name: 'Arial',
        size: 8
    }
});

var estadisticas = wb.createStyle({
    font: {
        name: 'Arial',
        size: 8
    }
});

var personalEmpresa = wb.createStyle({
    font: {
        name: 'Arial',
        size: 18,
    }
});

var personal = wb.createStyle({
    font: {
        name: 'Arial',
        size: 8
    }
});

var bold = wb.createStyle({
    font: {
        bold: true
    }
});

var italic = wb.createStyle({
    font: {
        italic: true
    }
});

var center = wb.createStyle({
    font: {
        italic: true
    }
});

module.exports = {
    cuil_title,
    all_lines,
    text_center,
    border_bottom,
    border_left,
    border_top,
    border_right,
    cuil_subempresa,
    grey_background,
    novedades_header,
    conceptos_text,
    conceptos_header,
    conceptos_totals,
    text_rigth,
    forzados,
    estadisticas,
    personalEmpresa,
    personal,
    italic,
    bold
};