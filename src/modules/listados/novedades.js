const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./listados.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const objectID = mongoose.Types.ObjectId;

const Liquidaciones = mongoose.model('liquidaciones');

module.exports =

    function novedades(req, res) {

        try {
            const periodo = req.body.periodo;
            const empresa = req.body.empresa;
            const subempresas = req.body.subempresas;

            let periodoID = new objectID(periodo);
            let empresaID = new objectID(empresa);

            if (subempresas.length != 0) {
                for (let a = 0; a < subempresas.length; a++) {
                    subempresas[a] = new objectID(subempresas[a]);
                }
            }

            var wb = new excel4node.Workbook();

            let match;

            match = {
                'legajo.persona.periodo_id': periodoID,
                'legajo.persona.empresa._id': empresaID,
                'legajo.subempresa._id': { $in: subempresas },
            }

            Liquidaciones.find(match).sort({ "legajo.subempresa.descripcion": 1 }).exec((err, legajos) => {

                if (err) return res.status(500).json({ 'success': false, message: err });

                var aux = "";

                for (let i = 0; i < legajos.length; i++) {

                    if (i == 0 || aux != legajos[i].legajo.subempresa.descripcion) {
                        var ws = wb.addWorksheet(legajos[i].legajo.subempresa.descripcion);

                        ws.cell(1, 2).string(legajos[0].legajo.subempresa.empresa.razon_social).style(styles.cuil_title);
                        ws.cell(2, 2).string("Fecha: " + moment().lang('es').format('LL'));
                        ws.cell(3, 2).string("Listado de novedades: " + legajos[0].periodo.descripcion);
                        ws.cell(4, 2).string("Sección: " + legajos[i].legajo.subempresa.descripcion);

                        ws.cell(6, 2).string("Nro. Legajo").style(styles.novedades_header).style(styles.text_center);
                        ws.cell(6, 3).string("Empleado").style(styles.novedades_header).style(styles.text_center);
                        ws.cell(6, 4).string("Caracter").style(styles.novedades_header).style(styles.text_center);
                        ws.cell(6, 5).string("Antiguedad").style(styles.novedades_header).style(styles.text_center);
                        ws.cell(6, 6).string("Cargo").style(styles.novedades_header).style(styles.text_center);
                        ws.cell(6, 7).string("Estado").style(styles.novedades_header).style(styles.text_center);
                        ws.cell(6, 8).string("Horas").style(styles.novedades_header).style(styles.text_center);
                        ws.cell(6, 9).string("Observaciones").style(styles.novedades_header).style(styles.text_center);

                        aux = legajos[i].legajo.subempresa.descripcion;
                        auxPersonas = i;
                    }

                    var nro = legajos[i].numero_legajo == undefined ? " " : legajos[i].numero_legajo;
                    ws.cell(6 + (i - auxPersonas) + 1, 2).string(nro).style(styles.all_lines);
                    ws.cell(6 + (i - auxPersonas) + 1, 3).string(legajos[i].legajo.persona.nombre).style(styles.all_lines);
                    ws.cell(6 + (i - auxPersonas) + 1, 4).string(legajos[i].legajo.caracter).style(styles.all_lines).style(styles.text_center);
                    ws.cell(6 + (i - auxPersonas) + 1, 5).string(legajos[i].legajo.antiguedad.meses + "/" + legajos[i].legajo.antiguedad.anios).style(styles.all_lines).style(styles.text_center);
                    ws.cell(6 + (i - auxPersonas) + 1, 6).string(legajos[i].legajo.cargo.nombre).style(styles.all_lines);
                    ws.cell(6 + (i - auxPersonas) + 1, 7).string(legajos[i].legajo.estado.substr(0, 1).toUpperCase()).style(styles.all_lines).style(styles.text_center);
                    ws.cell(6 + (i - auxPersonas) + 1, 8).number(legajos[i].legajo.horas).style(styles.all_lines).style(styles.text_center);
                    ws.cell(6 + (i - auxPersonas) + 1, 9).string("").style(styles.all_lines);

                    ws.column(1).setWidth(2);
                    ws.column(2).setWidth(10);
                    ws.column(3).setWidth(30);
                    ws.column(4).setWidth(7);
                    ws.column(5).setWidth(7);
                    ws.column(6).setWidth(30);
                    ws.column(7).setWidth(5);
                    ws.column(8).setWidth(5);
                    ws.column(9).setWidth(50);

                    ws.row(6 + (i - auxPersonas) + 1).setHeight(25)

                }


                wb.write('novedades.xlsx', res);
            });
        }
        catch (e) {
            console.log(e);
            return res.status(500).json({
                success: false,
                message: 'Error al imprimir reporte, contacte al administrador'
            });
        }
    };
