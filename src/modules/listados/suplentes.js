const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./listados.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const objectID = mongoose.Types.ObjectId;

const Empresas = mongoose.model('empresas');
const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');
const Periodos = mongoose.model('periodos');

module.exports =

    function personas_cuil(req, res) {

        try {
            const periodo = req.body.periodo;
            const empresa = req.body.empresa;
            const subempresas = req.body.subempresas;

            let periodoID = new objectID(periodo);
            let empresaID = new objectID(empresa);

            if (subempresas.length != 0) {
                for (let a = 0; a < subempresas.length; a++) {
                    subempresas[a] = new objectID(subempresas[a]);
                }
            }

            var wb = new excel4node.Workbook();

            let match;

            match = {
                'legajo.persona.periodo_id': periodoID,
                'legajo.persona.empresa._id': empresaID,
                'legajo.subempresa._id': { $in: subempresas },
                'legajo.suplencias': { $ne: [] }
            }

            Liquidaciones.find(match).sort({ "legajo.subempresa.descripcion": 1 }).exec((err, suplentes) => {

                if (err) return res.status(500).json({ 'success': false, message: err });

                var find = {
                    'persona.periodo_id': periodoID,
                    'persona.empresa._id': empresaID,
                    'subempresa._id': { $in: subempresas },
                    'licencias': { $ne: [] }
                }

                Legajos.find(find).exec((err, suplantados) => {

                    if (err) return res.status(500).json({ 'success': false, message: err });

                            var aux = "";

                            for (let i = 0; i < suplentes.length; i++) {

                                if (i == 0 || aux != suplentes[i].legajo.subempresa.descripcion) {
                                    var ws = wb.addWorksheet(suplentes[i].legajo.subempresa.descripcion);

                                    ws.cell(1, 1, 1, 11, true).string("SUPLENTES - Periodo: " + suplentes[0].periodo.descripcion).style(styles.text_center);
                                    ws.cell(3, 1, 3, 6, true).string("Instituto: " + suplentes[0].legajo.subempresa.empresa.razon_social);
                                    ws.cell(5, 1, 5, 6, true).string("Nivel: " + suplentes[i].legajo.subempresa.descripcion);

                                    ws.cell(3, 6, 3, 11, true).string("Caracter: ");
                                    ws.cell(5, 6, 5, 11, true).string("Subv.: ");

                                    ws.cell(7, 1, 7, 11, true).string("Suplencias del perido").style(styles.text_center).style(styles.grey_background);

                                    ws.cell(8, 1, 9, 1, true).string("Código").style(styles.text_center).style(styles.all_lines);
                                    ws.cell(8, 2, 9, 2, true).string("Cargo").style(styles.text_center).style(styles.all_lines);
                                    ws.cell(8, 3, 9, 3, true).string("Suplente").style(styles.text_center).style(styles.all_lines);
                                    ws.cell(8, 4, 8, 5, true).string("Periodo").style(styles.text_center).style(styles.all_lines);
                                    ws.cell(9, 4).string("Desde").style(styles.text_center).style(styles.all_lines);
                                    ws.cell(9, 5).string("Hasta").style(styles.text_center).style(styles.all_lines);
                                    ws.cell(8, 6, 9, 6, true).string("Remun.").style(styles.text_center).style(styles.all_lines);
                                    ws.cell(8, 7, 9, 7, true).string("No Remun.").style(styles.text_center).style(styles.all_lines);
                                    ws.cell(8, 8, 9, 8, true).string("Titular").style(styles.text_center).style(styles.all_lines);
                                    ws.cell(8, 9, 9, 9, true).string("Motivo").style(styles.text_center).style(styles.all_lines);
                                    ws.cell(8, 10, 8, 11, true).string("Periodo").style(styles.text_center).style(styles.all_lines);
                                    ws.cell(9, 10).string("Desde").style(styles.text_center).style(styles.all_lines);
                                    ws.cell(9, 11).string("Hasta").style(styles.text_center).style(styles.all_lines);

                                    aux = suplentes[i].legajo.subempresa.descripcion;
                                    auxPersonas = i;
                                }

                                for (let e = 0; e < suplentes[i].legajo.suplencias.length; e++) {
                                    var codigo = suplentes[i].legajo.numero_legajo == undefined ? suplentes[i].legajo.numero_sistema : suplentes[i].legajo.numero_legajo;
                                    if (codigo == undefined) {
                                        codigo = "-";
                                    }

                                    ws.cell(9 + (i - auxPersonas) + 1, 1).string(codigo);
                                    ws.cell(9 + (i - auxPersonas) + 1, 2).string(suplentes[i].legajo.cargo.nombre).style(styles.text_center);
                                    ws.cell(9 + (i - auxPersonas) + 1, 3).string(suplentes[i].legajo.persona.nombre).style(styles.text_center);
                                    ws.cell(9 + (i - auxPersonas) + 1, 4).string(moment(suplentes[i].legajo.suplencias[e].fecha_desde).format("DD/MM/YYYY"));
                                    ws.cell(9 + (i - auxPersonas) + 1, 5).string(moment(suplentes[i].legajo.suplencias[e].fecha_hasta).format("DD/MM/YYYY"));
                                    ws.cell(9 + (i - auxPersonas) + 1, 6).number(suplentes[i].remunerativo);
                                    ws.cell(9 + (i - auxPersonas) + 1, 7).number(suplentes[i].no_remunerativo);
                                    ws.cell(9 + (i - auxPersonas) + 1, 8).string(suplentes[i].legajo.suplencias[e].titular).style(styles.text_center);
                                    ws.cell(9 + (i - auxPersonas) + 1, 9).string(suplentes[i].legajo.suplencias[e].motivo)
                                    ws.cell(9 + (i - auxPersonas) + 1, 10).string(moment(suplentes[i].legajo.suplencias[e].fecha_desde).format("DD/MM/YYYY"));
                                    ws.cell(9 + (i - auxPersonas) + 1, 11).string(moment(suplentes[i].legajo.suplencias[e].fecha_hasta).format("DD/MM/YYYY"));
                                }
                            }

                            ws.column(2).setWidth(20);
                            ws.column(3).setWidth(30);
                            ws.column(8).setWidth(30);
                            ws.column(9).setWidth(30);
                            wb.write('suplentes.xlsx', res);
                });

            });
        }
        catch (e) {
            console.log(e);
            return res.status(500).json({
                success: false,
                message: 'Error al imprimir reporte, contacte al administrador'
            });
        }
    };
