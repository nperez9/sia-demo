const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const styles = require('./listados.styles');

const mongoose = require('mongoose');
const moment = require('moment');
const excel4node = require('excel4node');
const objectID = mongoose.Types.ObjectId;

const Empresas = mongoose.model('empresas');
const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');

function personas_cuil(req, res) {
    try {
        const periodo = req.body.periodo;
        const empresa = req.body.empresa;
        const subempresas = req.body.subempresas;

        let periodoID = new objectID(periodo);
        let empresaID = new objectID(empresa);

        if (subempresas.length != 0) {
            for (let a = 0; a < subempresas.length; a++) {
                subempresas[a] = new objectID(subempresas[a]);
            }
        }

        var wb = new excel4node.Workbook();

        var styleTotal = wb.createStyle({
            font: {
                name: 'Arial',
                color: '#000000',
                size: 8,
                bold: true
            },
            border: {
                top: {
                    style: 'medium',
                    color: '#000000'
                }
            }
        });

        let match;

        match = {
            'periodo._id': periodoID,
            'legajo.subempresa.empresa._id': empresaID,
            'legajo.subempresa._id': { $in: subempresas }
        }

        var group = {
            _id: '$_id',
            subempresa: { $first: '$legajo.subempresa.descripcion' },
            nombre: { $first: '$legajo.persona.nombre' },
            cuil: { $first: '$legajo.persona.cuil' },
        };

        Liquidaciones.aggregate([
            { $match: match },
            { $unwind: '$legajo' },
            {
                $group: group
            },
            { $sort: {subempresa : 1, nombre: 1} }
        ]).exec((err, docs) => {

            if (docs.length < 1) {
                return res.status(504).json({ success: false, message: 'El reporte no posee registros' })
            }

            Empresas.findOne({ '_id': new objectID(empresaID) })
                .exec((err, empresa) => {
                    if (err) return res.status(500).json({ 'success': false, message: err });

                    var aux = "";

                    for (let i = 0; i < docs.length; i++) {

                        if(i == 0 || aux != docs[i].subempresa){
                            var ws = wb.addWorksheet(docs[i].subempresa);

                            ws.cell(1, 1).string(docs[i].subempresa).style(styles.cuil_subempresa);
                            ws.cell(2, 1, 2, 2, true).string("LISTADO DE NOMBRES DEL PERSONAL CON SU RESPECTIVO CUIL").style(styles.cuil_title).style(styles.text_center);
                            
                            ws.cell(4, 1).string("Instituto: " + empresa.razon_social).style(styles.border_left).style(styles.border_top);
                            ws.cell(5, 1).string("Domicilio: " + empresa.direccion).style(styles.border_left).style(styles.border_bottom);
                            ws.cell(4, 2).string("Localidad: " + empresa.localidad).style(styles.border_right).style(styles.border_top);
                            ws.cell(5, 2).string("Fecha: " + moment().format('DD/MM/YYYY')).style(styles.border_right).style(styles.border_bottom);

                            ws.cell(7, 1).string("APELLIDO Y NOMBRES").style(styles.all_lines).style(styles.text_center);
                            ws.cell(7, 2).string("C.U.I.L.").style(styles.all_lines).style(styles.text_center);

                            aux = docs[i].subempresa;
                            auxPersonas = i;
                        }

                        ws.cell(8 + (i-auxPersonas)+1, 1).string(docs[i].nombre).style(styles.all_lines).style(styles.text_center);
                        ws.cell(8 + (i-auxPersonas)+1, 2).string(docs[i].cuil).style(styles.all_lines).style(styles.text_center);
                        ws.column(1).setWidth(50);
                        ws.column(2).setWidth(30);
                    }

                    wb.write('persona_cuil.xlsx', res);
                });
        });
    }
    catch (e) {
        console.log(e);
        return res.status(500).json({
            success: false,
            message: 'Error al imprimir reporte, contacte al administrador'
        });
    }
}

module.exports = {
    personas_cuil
};