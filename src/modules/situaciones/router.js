const base = '/situaciones';
const situaciones = require('./situaciones');

module.exports = function (router) {
    router.get(base, situaciones.get);
}
