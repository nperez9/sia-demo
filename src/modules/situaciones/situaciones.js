const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const mongoose = require('mongoose'); 
const objectID = mongoose.Types.ObjectId; 
const Situaciones = mongoose.model('situaciones');

function get (req, res) {
    Situaciones.find({}, (err, docs) => {
        res.json(docs); 
    });
}

module.exports = {
    get
}