const base = '/actividades';
const actividad = require('./actividades');

export const actividadesRouter = (router) => {
  router.get(base, actividad.get);
}
