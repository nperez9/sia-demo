const mongoose = require('mongoose'); 
const Actividades = mongoose.model('actividades');

function get (req, res) {
    Actividades.find({}, (err, docs) => {
        res.json(docs); 
    });
}

module.exports = {
    get
}