const base = '/usuarios'; 
const usuarios = require('./usuarios');
const isLoggued = require('../../auth/auth_middleware.js');
const passport = require('passport');


module.exports = function (router) {
    router.get(base + '/listado/', isLoggued, usuarios.get);
    router.get(base + '/:id', usuarios.getById);
    
    router.post(base, usuarios.post);
    router.post(base+ '/login', usuarios.login);
    
    router.put(base + '/:id', usuarios.put);
    // router.put(base + '/check/:code', usuarios.checkAccount);
    
    router.delete(base + '/:id', usuarios.destroy);
}