const mongoResponse = require('../responses/mongoResponse');
const crypt = require('../../helpers/cryptHelper');

const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const objectID = mongoose.Types.ObjectId;
const Usuarios = mongoose.model('usuarios');

function get(req, res) {
	var findCriteria = {};
	var documentsPerPage = 20;
	var skip = (req.query.page - 1) * documentsPerPage;

	if (req.query.search != '') {
		findCriteria['nombre'] = new RegExp(req.query.search, 'i');
		findCriteria['email'] = new RegExp(req.query.search, 'i');
	}

	Usuarios.find(findCriteria)
		.limit(documentsPerPage)
		.skip(skip)
		.exec((err, items) => {
			getCount(findCriteria).then(total => {
				return res.json({
					items,
					total
				});
			});
		});
}

function getById(req, res) {
	let id = new objectID(req.params.id);

	Usuarios.findOne({ '_id': id },
		(err, usuario) => {
			usuario.password = crypt.decrypt(usuario.password);
			return res.json(usuario);
		});
}

function post(req, res) {
	let usuario = req.body;
	usuario.password = crypt.encrypt(usuario.password);

	Usuarios.create(usuario, (err, newUsuario) => {
		return res.json({
			success: true,
			message: 'Usuario creado con exito'
		});
	});
}

function put(req, res) {
	let usuario = req.body;
	usuario.password = crypt.encrypt(usuario.password);
	const usuarioID = new objectID(usuario._id);
	const message = 'Usuario editado con exito';

	Usuarios.findByIdAndUpdate(usuarioID, usuario, mongoResponse(res, message));
}

function destroy(req, res) {
	var usuarioID = new objectID(req.params.id);

	Usuarios.delete({ _id: usuarioID },
		(err, result) => {
			if (err) process.exit(err);
			return res.json({
				success: true,
				message: 'Usuario eliminado'
			});
		});
}

function getCount(find, model) {
	if (typeof (find) === undefined)
		find = {};
	if (typeof (model) === undefined)
		model = Usuarios;

	return Usuarios.count(find).exec((err, count) => {
		return count;
	});
}

function login(req, res) {
	const { username, password } = req.body;
	// const password = crypt.encrypt(usuario.password);
	Usuarios.findOne({ 'email': username, 'password': password },
		(err, usuario) => {
			if (usuario) {
				return res.json({
					success: true,
					message: 'Bienvenido ' + usuario.nombre,
					user: {
						nombre: usuario.nombre,
						email: usuario.email,
						id: usuario._id,
						tipo: usuario.tipo
					}
				});
			} else {
				return res.status(409).json({
					message: 'El usuario no existe',
					success: false
				});
			}
		});
}


module.exports = {
	get,
	post,
	put,
	destroy,
	getById,
	login
}