'use strict';
const logguer = require('log4js').getLogger('ganacias');
/// Maximos Anuales
const MAXIMO_SEPELIO = 996.23;
const MAXIMO_HIPOTECARIO = 20000;
const MAXIMO_SERVICIO_DOMESTICO = 66917.91;
const MAXIMO_ALQUILER = 66917.91;
const MAXIMO_SEGURO_VIDA = 996.23;
// Porcentajes
const HORAS_EXTRA = 1;
const OBRA_SOCIAL_PREPAGA = 0.05;
const ASISTENCIA_MEDICA = 0.05;
const DONACIONES = 0.05;

// valores mensuales
const HIJO_MES = 2621.76;
const CONYUGE_MES = 5198.77;
const MNI_MES = 5576.49;
const DEDUCCION_ESPECIAL_MES = 26767.16;

// Calcula lo deducible
function calcularDeducciones (ganancias, neto, cantiadadDeMeses) {
  let valorSepelio = (MAXIMO_SEPELIO / 12) * cantiadadDeMeses;
  let valorHipotecario = (MAXIMO_HIPOTECARIO / 12) * cantiadadDeMeses;
  let valorServicioDemestico = (MAXIMO_SERVICIO_DOMESTICO / 12) * cantiadadDeMeses;
  let valorAlquiler = (MAXIMO_ALQUILER / 12) * cantiadadDeMeses;
  let valorSeguroVida = (MAXIMO_SEGURO_VIDA / 12) * cantiadadDeMeses;

  let alquiler = (ganancias.alquiler * 0.4);

  let deducciones = {
    sepelio: (ganancias.sepelio < valorSepelio) ? ganancias.sepelio : valorSepelio,
    hipoteca: (ganancias.hipoteca < valorHipotecario) ? ganancias.hipoteca : valorHipotecario,
    servicio_domestico: (ganancias.servicio_domestico < valorServicioDemestico) ? ganancias.servicio_domestico : valorServicioDemestico,
    alquiler: (alquiler < valorAlquiler) ? alquiler : valorAlquiler,
    seguro_vida: (ganancias.seguro_vida < valorSeguroVida) ? ganancias.seguro_vida : valorSeguroVida,
    horas_extra: ganancias.horas_extra
  };

  let valorActualDeduccion = 0;
  let keysDeduccion = Object.keys(deducciones);
  keysDeduccion.forEach(key => {
    valorActualDeduccion += deducciones[key];
  });
  let restanteSinDeduccion = neto - valorActualDeduccion;

  logguer.info('Deduccion Acumulada: ' + valorActualDeduccion, 'Total a gananciar (sin Deducciones): ' +  restanteSinDeduccion);

  let valorPrepaga = restanteSinDeduccion * OBRA_SOCIAL_PREPAGA;
  let valorAsistenciaMedica = restanteSinDeduccion * ASISTENCIA_MEDICA;
  let valorDonaciones = restanteSinDeduccion * DONACIONES;
  
  
  deducciones.prepaga = (ganancias.prepaga < valorPrepaga) ? ganancias.prepaga : valorPrepaga;
  deducciones.gastos_medicos = (ganancias.gastos_medicos < valorAsistenciaMedica) ? ganancias.prepaga : valorAsistenciaMedica;
  deducciones.donacion = (ganancias.donacion < valorDonaciones) ? ganancias.donacion : valorDonaciones;

  deducciones.conyugue = (ganancias.conyugue) ? CONYUGE_MES * cantiadadDeMeses : 0;
  deducciones.hijos = (HIJO_MES * cantiadadDeMeses) * ganancias.hijos; 
  deducciones.mni = MNI_MES * cantiadadDeMeses;
  deducciones.deduccion_especial = DEDUCCION_ESPECIAL_MES * cantiadadDeMeses;

  let valorTotalDeuccion = 0;
  let allKeys = Object.keys(deducciones);
  allKeys.forEach(key => {
    if (Number.isNaN(deducciones[key])) {
      console.log('------------------------------------');
      console.log('valores:');
      console.log('alquiler:', valorAlquiler);
      console.log('hipoteca:', valorHipotecario);
      console.log('servicios domesticos:', valorServicioDemestico);
      console.log('sepelio:', valorSepelio);
      console.log('seguro de vida:', valorSeguroVida);
      console.log('Prepaga:', valorPrepaga);
      console.log('------------------------------------');
    } else {
      valorTotalDeuccion += deducciones[key];
    }
  });
  
  console.log('------------------------------------');
  console.log('----------MESES :'+ cantiadadDeMeses + '-------');
  console.log(deducciones);
  console.log(cantiadadDeMeses);
  console.log('------------------------------------');
  
  return valorTotalDeuccion;
}

module.exports = calcularDeducciones;