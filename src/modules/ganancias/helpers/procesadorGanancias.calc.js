function procesarGanancias (ganancias, liquidacion) {
  let mes = liquidacion.periodo.mes;
  let datosProcesados = {
    hijos: 0,
    o_cargos: 0,
    prepaga: 0,
    seguro_vida: 0,
    hipoteca: 0,
    donacion: 0,
    servicio_domestico: 0,
    gastos_medicos: 0,
    sepelio: 0,
    desc_sac: 0,
    alquiler: 0,
    material_didactico: 0,
    horas_extra: 0,
    pago_cta: 0
  }

  // Sumalizacion de conceptos de otras tipos
  ganancias.datos_ganancias.forEach(element => {
    if (element.periodo_mes <= mes) {
      if (element.periodo_mes == mes) {
        datosProcesados.conyugue = element.conyugue;
        datosProcesados.hijos = element.hijos;
      }
      
      datosProcesados.o_cargos += element.o_cargos;
      datosProcesados.prepaga += element.prepaga;
      datosProcesados.seguro_vida += element.seguro_vida;
      datosProcesados.hipoteca += element.hipoteca;
      datosProcesados.donacion += element.donacion;
      datosProcesados.servicio_domestico += element.servicio_domestico;
      datosProcesados.gastos_medicos += element.gastos_medicos;
      datosProcesados.sepelio += element.sepelio;
      datosProcesados.desc_sac += element.desc_sac;
      datosProcesados.alquiler += element.alquiler;
      datosProcesados.material_didactico += element.material_didactico;
      datosProcesados.horas_extra += element.horas_extra;
      datosProcesados.pago_cta += element.pago_cta;
    }
  });

  //sumalizacion de otros sueldos  
  datosProcesados.otrosSueldos = 0;
  ganancias.otros_sueldos.forEach(element => {
    if (element.periodo_mes <= mes) {
      datosProcesados.otrosSueldos += element.neto;
    }
  });

  return datosProcesados;
}

module.exports = procesarGanancias;