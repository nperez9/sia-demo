const proporcionalSac = 0.0833;

function filterConceptos (conceptos) {
  let noRem = [];
  for (let i = 0; i < conceptos.length; i++) {
    if (conceptos[i].tipo_remuneracion == 2 && !conceptos[i].sac) {
      noRem.push(conceptos[i]);
    }
  }
  return noRem;
}

function obtenerProporcionalSAC (neto, conceptos, meses) {
  let noRemunerativosSinSac = [];
  let sueldoSAC = neto;

  conceptos.forEach(arrayConceptos => {
    noRemunerativosSinSac.concat(arrayConceptos);
  });

  noRemunerativosSinSac.forEach(cd => {
    sueldoSAC = sueldoSAC - cd.calculado;
  });

  const proporcional =  sueldoSAC * proporcionalSac;
  return proporcional;
}

module.exports = obtenerProporcionalSAC;