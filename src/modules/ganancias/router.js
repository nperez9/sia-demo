const base = '/ganancias'; 
const ganancias = require('./ganancias'); 
const datosGanancias = require('./datosGanancias');
ganancias['procesar'] = require('./procesarGanancias');
//const inserter = require('./insertganancias');

module.exports = function (router) {
    router.get(base + '/:id', ganancias.calcularGanancias);
    router.get(base + '/calcular-ganancias/:empresaid/:periodoid', ganancias.calcularTodosGanancias);
    router.get(base + '/datos-ganancias/:empresaid/:personaid/:anio', datosGanancias.getDatosGanancias);
    
    router.post(base + '/datos-ganancias/:empresaid/:personaid/:anio', datosGanancias.postDatoGanancias);
    router.post(base + '/otros-sueldos/:empresaid/:personaid/:anio', datosGanancias.postOtrosSueldos);
    
    router.post(base + '/xml/:personaid', ganancias.cargarxml);
    router.post(base + '/procesar', ganancias.procesar);

    router.put(base + '/datos-ganancias/:empresaid/:personaid/:anio/:datogananciasid', datosGanancias.putDatoGanancias);
    router.put(base + '/otros-sueldos/:empresaid/:personaid/:anio/:otrosueldosid', datosGanancias.putOtrosSueldos);

    router.delete(base + '/datos-ganancias/:empresaid/:personaid/:anio/:datogananciasid', datosGanancias.deleteDatosGanancias);
    router.delete(base + '/otros-sueldos/:empresaid/:personaid/:anio/:otrosueldosid', datosGanancias.deleteOtrosSueldos);
}
