const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId;

const Liquidaciones = mongoose.model('liquidaciones');
const Ganancias = mongoose.model('ganancias');
const logguer = require('log4js').getLogger();
logguer.level = 'debug';

// heplers para calcular
const calcularDeducciones = require('./helpers/deducciones.calc');
const procesarGanancias = require('./helpers/procesadorGanancias.calc');
const calcularProporcionalSAC = require('./helpers/proporcionalSAC.calc');

// menor que tope, va
const topesEscalas =       [25800, 51600, 77400,  103200,   154800,   206400,  309600,   412800, 9999999999];
const porcentajesEscalas = [ 0.05,  0.09,  0.12,    0.15,     0.19,     0.23,    0.27,     0.31,       0.35];
const fijosEscalas =       [    0,  1290,  3612,    6708,    10578,    20382,   32250,    60114,      92106];
const excedentesEscalas =  [    0, 25800, 51600,   77400,   103200,   154800,   206400,  309600,     412800];

const conceptoGanancia = {
  tipo_remuneracion: 3,
  codigo: 'ZGAN001',
  descripcion: 'Ganancias',
  cantidad: 1,
  formula: 'I*1',
  indice: 0
};

function limpiaGananciasActuales (liquidacion) {
  let personaID = new objectID(liquidacion.legajo.persona.persona_id);
  let periodoID = new objectID(liquidacion.periodo._id);

  let match = {
    'legajo.persona.persona_id': personaID,
    'periodo._id': periodoID
  };
  
  return Liquidaciones.update(
    {
      'legajo.persona.persona_id': personaID,
      'periodo._id': periodoID
    },
    {$set: {'ganancias_calculada': {  
      alicuota: 0,
      mensual: 0,
      anual: 0
    }}},
    {multi: true}).exec(
    (err, docs) => {
      if (err) console.log(err);
      return docs;
    }
  );
}

function calcular (liquidaciones, ganancias, mes) {
    console.log('MESES ', mes);
    console.log('liquidacion: ', liquidaciones);
    if (typeof(liquidaciones[0]) === 'undefined' || liquidaciones === null) return false;
    var mesesCalculado = mes;

    var gananciasAcumuladas = liquidaciones[0].ganancias_acumuladas;
    var sueldoNetoSinSac = liquidaciones[0].neto;
    
    // Aculmula los sueldos y le saca las ganancias ya calculadas
    let sueldoNetoAcumulado = liquidaciones[0].neto;
    sueldoNetoAcumulado += ganancias.otrosSueldos;
    sueldoNetoAcumulado += gananciasAcumuladas;
    // obtiene el proporcional del SAC por que el calculo es en base 12
    sueldoNetoAcumulado += calcularProporcionalSAC(sueldoNetoAcumulado, liquidaciones[0].conceptos, mesesCalculado);
    console.log('--OtrosSueldos--Acumulado + OS + SAC --Neto--Ganancias--');
    console.log(ganancias.otrosSueldos, sueldoNetoAcumulado, liquidaciones[0].neto, liquidaciones[0].ganancias_acumuladas);
    // Calcula minimos no imponibles
    var MNI_actual = calcularDeducciones(ganancias, sueldoNetoAcumulado, mesesCalculado);
    

    if (MNI_actual >= sueldoNetoAcumulado) {
      return false;
    }

    // obtenemos el imponible que se puede impuestar
    const imponible = sueldoNetoAcumulado - MNI_actual;


    console.log('----------Imponible--------------');
    console.log('Neto acumulado con SAC: ', sueldoNetoAcumulado);
    console.log('MNI con deducciones especiales: ', MNI_actual, ' Meses: ', mesesCalculado);
    console.log('Imponible: ', imponible);
    console.log('------------------------------------');

    var impuestoActual = 0;
    //Calculo Escalas
    for (var i = 0; i <= topesEscalas.length; i++) {
      let topeEscalaActual = (topesEscalas[i] / 12) * mesesCalculado;
      console.log('-----------Escalas------------------');
      console.log('valor '+ imponible, 'TopesEscalas:'+ topeEscalaActual, i);

      if (imponible <= topeEscalaActual) {
        // obtiene el monto fijo a pagar de ganancias
        let escalaActual = (fijosEscalas[i] / 12) * mesesCalculado;
        impuestoActual += escalaActual;
        
        // se le suma el porcentaje sobre el excente
        let limiteExendente = (excedentesEscalas[i] / 12) * mesesCalculado;
        let imponiblePorcentaje = (imponible - limiteExendente) * porcentajesEscalas[i];
        impuestoActual += imponiblePorcentaje;
        
        console.log('----------Impuestado----------------');
        console.log('deducible: ', imponible);
        console.log('EscalaFijo->', escalaActual)
        console.log('Limite para el exedente->', limiteExendente,
                    'Porcentaje->', porcentajesEscalas[i],
                    'Desc porcentaje ->', imponiblePorcentaje);
        console.log('impuestoActual: ', impuestoActual);
        console.log('------------------------------------');
        break;
      }
    }

    //Calculo Resultados
    var resultados = {};
    console.log('------------------------------------');
    console.log(gananciasAcumuladas, ganancias.pago_cta);
    let gananciasPagadas = ganancias.pago_cta;
    console.log('------------------------------------');
    var impuestoMensual = impuestoActual - gananciasAcumuladas - gananciasPagadas;
    resultados.impuestoMensual = "$" + Math.ceil(impuestoMensual) + ".00";

    var alicuota = (impuestoActual / sueldoNetoAcumulado) * 100;
    resultados.alicuota = alicuota.toFixed(2) + "%";

    var sueldoEnMano = sueldoNetoSinSac - impuestoActual;
    resultados.sueldoEnMano = "$" + Math.ceil(sueldoEnMano) + ".00";

    resultados.totales = {
      sueldoEnMano,
      impuestoMensual,
      impuestoActual,
      alicuota
    }

    console.log(resultados);
    return resultados;
}

function calcularGanancias (req, res) {
  var liquidacionID = new objectID(req.params.id);
  Liquidaciones.findOne({
    '_id': liquidacionID
  }, (err, liquidaciones) => { 
      if (liquidaciones == null) return res.json({ success: false, message: 'No se es agente de retencion' });
      
      var persona = limpiaGananciasActuales(liquidaciones).then(params => {
        return persona = getLiquidacionesGanancias(liquidaciones);
      });
      
      var ganancias = getGananciasData(liquidaciones);
      
      Promise.all([persona, ganancias]).then((datos) => {
        const liquidacionesGanancia = datos[0];
        const ganancias = datos[1];
        if (ganancias === null) {
          return res.status(500).json({
            success: true, 
            message: 'No estan cargados los datos de ganancias'
          });
        }
        let gananciasProcesadas = procesarGanancias(ganancias, liquidaciones);
        let esc = calcular(liquidacionesGanancia, gananciasProcesadas, liquidaciones.periodo.mes);
        
        if (esc === false) {
         return res.status(500).json({success: false, message: 'No retiene ganancias'}); 
        }

        let concepto = JSON.parse(JSON.stringify(conceptoGanancia));
        concepto.importe = esc.totales.impuestoMensual;
        
        var ganancias_calculada = {
          alicuota: esc.totales.alicuota,
          mensual: esc.totales.impuestoMensual,
        }

        let periodoId = new objectID(liquidaciones.periodo._id);
        let personaId = new objectID(liquidaciones.legajo.persona.persona_id);
        //sprocess.exit();
        Liquidaciones.findOneAndUpdate(
          { '_id':  liquidacionID},
          { $push: { conceptos: concepto }, 
            $set: {ganancias_calculada: ganancias_calculada} },
          (err, docs) => {
            if (err) console.log(err);
            return res.json({
              "success": true,
              "message": "Ganancias Calculadas"
            });
          }
        );
      });
      // , (error) => {
      //   console.log(error);
      // }
    });
}

//  993, 806
function calcularTodoElPeirodo (req, res) {
  const periodoID = new objectID(req.params.periodoid);
  const empresaID = new objectID(req.params.empresaid); 
  
  let match = {
    'periodo._id': periodoID,
    'legajo.subempresa.empresa._id': empresaID
  };

  console.log(match)

  Liquidaciones.aggregate([
    {$match: match},
    {
      $group: {
        _id: '$legajo.persona.persona_id',
        periodo: {$first: '$periodo'},
        legajo: {$first: '$legajo'},
        liquidacion_id: {$first: '$_id'}
      }
    }]).exec((err, personasAgananciar) => {
      console.log(personasAgananciar);
      var misterArray = [];

      personasAgananciar.forEach(personaGanacia => {
        misterArray.push(calcularPorPromesa(personaGanacia))
      });
      Promise.all(misterArray).then(resultados => {
        return res.json({
          success: true,
          message: 'Liquidaciones realizadas con exito',
          resultados: resultados
        });
      });
    });
}

function calcularPorPromesa (liquidaciones) {
  let persona = getLiquidacionesGanancias(liquidaciones);
  let ganancias = getGananciasData(liquidaciones);

  return Promise.all([persona, ganancias]).then((datos) => {
    const liquidacionesGanancia = datos[0];
    const ganancias = datos[1];
    if (ganancias === null) {
      return {
        success: true, 
        message: 'No estan cargados los datos de ganancias'
      };
    }
    let gananciasProcesadas = procesarGanancias(ganancias, liquidaciones);
    let esc = calcular(liquidacionesGanancia, gananciasProcesadas, liquidaciones.periodo.mes);
    
    if (esc === false) {
      return {success: false, message: 'No retiene ganancias'}; 
    }

    let concepto = JSON.parse(JSON.stringify(conceptoGanancia));
    concepto.importe = esc.totales.impuestoMensual;
    
    var ganancias_calculada = {
      alicuota: esc.totales.alicuota,
      mensual: esc.totales.impuestoMensual,
    }

    let periodoId = new objectID(liquidaciones.periodo._id);
    let personaId = new objectID(liquidaciones.legajo.persona.persona_id);
    let liquidacionID = new objectID(liquidaciones.liquidacion_id);

    Liquidaciones.findOneAndUpdate(
      { '_id':  liquidacionID},
      { $push: { conceptos: concepto }, 
        $set: {ganancias_calculada: ganancias_calculada} },
      (err, docs) => {
        if (err) console.log(err);
        return {
          "success": true,
          "message": "Ganancias Calculadas"
        };
      }
    );
  }, (error) => {
    console.log(error);
  });
}


function getLiquidacionesGanancias (liquidaciones) {
  const anio = liquidaciones.periodo.anio;
  const empresaId = new objectID(liquidaciones.legajo.subempresa.empresa._id);
  const personaId = new objectID(liquidaciones.legajo.persona.persona_id);

  let match = {
    'periodo.anio': anio,
    'periodo.mes': {$lte: liquidaciones.periodo.mes},
    'legajo.subempresa.empresa._id': empresaId,
    //'legajo.persona.datos_administrativos.agente_retencion': true,
    'legajo.persona.persona_id': personaId
  };

  return Liquidaciones.aggregate([
    {
      $match: match
    },
    {
      $group: {
        _id: '$legajo.persona.persona_id',
        legajo: { $first: '$legajo' },
        liquidacion_id: { $first: '$_id' },
        bruto: { $sum: '$bruto' },
        neto: { $sum: '$neto' },
        periodos: { $addToSet: '$periodo._id' },
        conceptos: {$push: '$conceptos'},
        ganancias_calculada: { $first: '$ganancias_calculada'},
        ganancias_acumuladas: { $sum: '$ganancias_calculada.mensual'},
        count: {$sum: 1}
      }
    }
  ]).exec((err, liquidacionesGanancia) => {
    if (err) console.log(err);
    return liquidacionesGanancia;
  });
}

function getGananciasData (liquidaciones) {
  const anio = liquidaciones.periodo.anio;
  const empresaId = new objectID(liquidaciones.legajo.subempresa.empresa._id);
  const personaId = new objectID(liquidaciones.legajo.persona.persona_id);

  return Ganancias.findOne({
    'keys.persona_id': personaId,
    'keys.empresa_id': empresaId,
    'keys.anio': anio
  }, (err, ganancias) => {
    if (err) console.log(err);
    return ganancias;
  });
}

module.exports = {
  calcularTodoElPeirodo,
  calcularGanancias
};