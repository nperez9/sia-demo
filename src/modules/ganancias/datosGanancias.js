const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId;

const Ganancias = mongoose.model('ganancias');

function getDatosGanancias (req, res) {
  const empresaID = new objectID(req.params.empresaid);
  const personaID = new objectID(req.params.personaid);
  const anio = req.params.anio;

  Ganancias.findOne({
    'keys.empresa_id': empresaID, 
    'keys.persona_id': personaID,
    'keys.anio': anio
  }, (err, ganancia) => {
    console.log(ganancia);
    if (ganancia === null) crearDatosGanancias (req, res);
    else return res.json(ganancia);
  });
}

function crearDatosGanancias (req, res) {
  const empresaID = new objectID(req.params.empresaid);
  const personaID = new objectID(req.params.personaid);
  const anio = req.params.anio;

  let keys = {
    empresa_id: empresaID,
    persona_id: personaID,
    anio: anio
  }
  console.log(keys);
  Ganancias.create({keys: keys}, (err, ganancia) => {
    if (err) console.log(err);
    res.json(ganancia);
  });
}

function postDatoGanancias (req, res) {
  const empresaID = new objectID(req.params.empresaid);
  const personaID = new objectID(req.params.personaid);
  const anio = req.params.anio;
  const dato = req.body;

  Ganancias.findOneAndUpdate({
    'keys.empresa_id': empresaID, 
    'keys.persona_id': personaID,
    'keys.anio': anio
  },
  {$push: {datos_ganancias: dato}}, 
  (err, ganancia) => {
    if (err) console.log(err); 
    return res.json({success: true, message: 'Linea Agregada con exito'});
  });
}




function putDatoGanancias (req, res) {
  const empresaID = new objectID(req.params.empresaid);
  const personaID = new objectID(req.params.personaid);
  const anio = req.params.anio;
  const datoGanancias = req.body;
  const datoGananciasID = new objectID(datoGanancias._id);

  Ganancias.findOneAndUpdate(
    {'datos_ganancias._id': datoGanancias},
    {'datos_ganancias.$': datoGanancias},
    (err, doc) => {
        if (err) console.log(err);
        return res.json({success: true, message: 'Linea editada'});
    }
  );
}


function deleteDatosGanancias (req, res) {
  const empresaID = new objectID(req.params.empresaid);
  const personaID = new objectID(req.params.personaid);
  const datoGananciasID = new objectID(req.params.datogananciasid);
  const anio = req.params.anio;

  Ganancias.findOneAndUpdate(
    {
      'keys.empresa_id': empresaID, 
      'keys.persona_id': personaID,
      'keys.anio': anio
    },
    {$pull: {'datos_ganancias': {_id: datoGananciasID}}},
    (err, doc) => {
        return res.json({
            success: true,
            message: 'Linea eliminada'
        });
    });
}

// Otros Sueldos
function postOtrosSueldos (req, res) {
  const empresaID = new objectID(req.params.empresaid);
  const personaID = new objectID(req.params.personaid);
  const anio = req.params.anio;
  const dato = req.body;

  Ganancias.findOneAndUpdate({
    'keys.empresa_id': empresaID, 
    'keys.persona_id': personaID,
    'keys.anio': anio
  },
  {$push: {otros_sueldos: dato}}, 
  (err, ganancia) => {
    if (err) console.log(err); 
    return res.json({success: true, message: 'Linea Agregada con exito'});
  });
}


function putOtrosSueldos (req, res) {
  const empresaID = new objectID(req.params.empresaid);
  const personaID = new objectID(req.params.personaid);
  const anio = req.params.anio;
  const otrosSueldos = req.body;
  const otrosSueldosID = new objectID(otrosSueldos._id);

  Ganancias.findOneAndUpdate(
    {'otros_sueldos._id': otrosSueldosID},
    {'otros_sueldos.$': otrosSueldos},
    (err, doc) => {
      return res.json({success: true, message: 'Linea editada con exito'});
    }
  );
}


function deleteOtrosSueldos (req, res) {
  const empresaID = new objectID(req.params.empresaid);
  const personaID = new objectID(req.params.personaid);
  const otrosSueldosID = new objectID(req.params.otrosueldosid);
  const anio = req.params.anio;

  Ganancias.findOneAndUpdate({
      'keys.empresa_id': empresaID, 
      'keys.persona_id': personaID,
      'keys.anio': anio
    },
    {$pull: {'otros_sueldos': {_id: otrosSueldosID}}},
    (err, doc) => {
        return res.json({
            success: true,
            message: 'linea eliminada'
        });
    });
}


module.exports = {
  getDatosGanancias,
  crearDatosGanancias,
  postDatoGanancias,
  putDatoGanancias,
  deleteDatosGanancias,
  postOtrosSueldos,
  putOtrosSueldos,
  deleteOtrosSueldos
}