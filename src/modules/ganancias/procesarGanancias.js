const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId;
const Liquidaciones = mongoose.model('liquidaciones')

function liquidacionesPersona (liquidaciones) {
    var persons = []; 
    
    liquidaciones.forEach(liquidacion => {
        var pid = liquidacion.legajo.persona._id;
        if (typeof(persons[pid]) !== "object")
            persons[pid] = [];
        persons[pid].push(liquidacion);
    });
    return persons;
}

module.exports = function (req, res) {
    empresaObjId = new objectID(req.body.empresa._id);
    periodoObjId = new objectID(req.body.periodo._id);

    Liquidaciones.find({
        'legajo.subempresa.empresa._id': empresaObjId,
        'periodo._id': periodoObjId,
        'ganancias': {'$exists': true}
    }, (err, liquidaciones) => {
        if (liquidaciones.length < 1) 
            return res.json({
                success:false, 
                message: 'No hay ganancias cargadas'
            });
        var liquiPersona = liquidacionesPersona(liquidaciones);
        console.log('------------------------------------');
        console.log(liquiPersona);
        
        res.json({'message': 'procesado'});
    });
}