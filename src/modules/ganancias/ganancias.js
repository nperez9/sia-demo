const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse'); 

const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId;
const Empresas = mongoose.model('empresas'); 
const SubEmpresas = mongoose.model('sub_empresas'); 
const Conceptos = mongoose.model('conceptos'); 
const Legajos = mongoose.model('legajos'); 
const Liquidaciones = mongoose.model('liquidaciones');
const Ganancias = mongoose.model('ganancias');

const Math = require('mathjs');
const xml2json = require('xml2js').parseString;

// Get Externals Endpoints;
const calcGanancias = require('./calcularGanancias');
const calcularGanancias = calcGanancias.calcularGanancias;
const calcularTodosGanancias = calcGanancias.calcularTodoElPeirodo;

function get (req, res) {
    var findCriteria = {};
    var documentsPerPage = 20; 
    var skip = (req.query.page - 1) * documentsPerPage;

    if (req.query.search != '')
        findCriteria['razon_social'] = new RegExp(req.query.search, 'i'); 

    Empresas.find(findCriteria)
    .limit(documentsPerPage)
    .skip(skip)
    .exec((err, items) => {
        getCount(findCriteria).then(total => {
            return res.json({
                items,
                total
            });
        }); 
    }); 
}


/**
 * Parsea el XML y lo guarda
 * en la coleccion de ganancias y en cada liquidacion
 */
function cargarxml (req, res) {
    const myXML = req.body.xml;
    const nombre_archivo = req.body.nombre_archivo;
    const periodoObjId = new objectID(req.body.periodo._id); 
    const empresaObjId = new objectID(req.body.empresa._id);
    const personaObjId = new objectID(req.params.personaid);

    xml2json(myXML, function (err, result) {
        let ganancia = {
            nombre_archivo,
            presentacion: JSON.stringify(result.presentacion)
        }
        // debido a caracteres especiale hay que guardarlo como JSON
        Ganancias.create(ganancia).then(function (r) {
            var findUpdate = {
                'periodo._id': periodoObjId,
                'legajo.persona._id': personaObjId,
                'legajo.subempresa.empresa._id': empresaObjId 
            };
            Liquidaciones.update(
                findUpdate, 
                {
                    $set: {ganancias: r}
                },
                {
                    multi: true
                }, 
                (err, updatedDoc) => {
                    console.log('------------------------------------');
                    console.log(updatedDoc);
                    console.log('------------------------------------');
                    return res.json({
                        success: true, 
                        message: 'XML de ganancias Subido!!'
                    });
                }
            );
        });
    });
   
}


module.exports = {
    get,
    cargarxml,
    calcularGanancias,
    calcularTodosGanancias
}