const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const mongoose = require('mongoose'); 
const objectID = mongoose.Types.ObjectId; 
const Modalidades = mongoose.model('modalidades');

function get (req, res) {
    Modalidades.find({}, (err, docs) => {
        res.json(docs); 
    });
}

module.exports = {
    get
}