const base = '/modalidades';
const modalidades = require('./modalidades');

module.exports = function (router) {
    router.get(base, modalidades.get);
}