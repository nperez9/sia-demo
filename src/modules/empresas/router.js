const base = '/empresas'; 
const empresas = require('./empresas');
const isLoggued = require('../../auth/auth_middleware.js');
//const inserter = require('./insertEmpresas');

module.exports = function (router) {
    router.get(base, isLoggued, empresas.get);
    router.get(base + '/all', empresas.getAll);
    router.get(base + '/liquidacion/:id', empresas.getAll);
    router.get(base + '/:empresaid/datos-liquidacion/:periodo', empresas.getDataLiquidaciones);
    router.get(base + '/:id', empresas.getById);
    
    router.post(base, empresas.post);
    
    router.put(base + '/:id', empresas.put); 
    
    router.delete(base + '/:id', empresas.destroy);
}
