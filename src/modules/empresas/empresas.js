const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');

const mongoose = require('mongoose'); 
const objectID = mongoose.Types.ObjectId; 
const Empresas = mongoose.model('empresas'); 
const SubEmpresas = mongoose.model('sub_empresas'); 
const Conceptos = mongoose.model('conceptos'); 
const Legajos = mongoose.model('legajos'); 
const Usuarios = mongoose.model('usuarios');
const jwt = require('jsonwebtoken');


function get (req, res) {

    var findCriteria = {};
    var documentsPerPage = 20; 
    var skip = (req.query.page - 1) * documentsPerPage;

    if (req.query.search != '')
        findCriteria['razon_social'] = new RegExp(req.query.search, 'i'); 

    Empresas.find(findCriteria)
    .limit(documentsPerPage)
    .skip(skip)
    .exec((err, items) => {
        getCount(findCriteria).then(total => {
            return res.json({
                items,
                total
            });
        }); 
    }); 
}

function getAll (req, res) {
    if(req.params.id){
        if(req.params.id != "0"){
            var usuarioID = new objectID(req.params.id);
            
            Usuarios.find({_id : usuarioID},
                (err, usuario) => {
                    var idsEmpresas = [];
    
                    for (let i = 0; i < usuario[0].permiso_empresas.length; i++) {
                        idsEmpresas.push(new objectID(usuario[0].permiso_empresas[i]._id))
                    }
    
                    Empresas.find({_id : {$in : idsEmpresas}}, (err, docs) => {
                        console.log(docs)
                        res.json(docs);
                    });
                });
        }
        else {
            Empresas.find({}, (err, docs) => {
                res.json(docs);
            });
        }
    }else{
        Empresas.find({}, (err, docs) => {
            res.json(docs);
        });
    }
    
}

function getDataLiquidaciones (req, res) {
    let empresaID = new objectID(req.params.empresaid);
    let periodoID = new objectID(req.params.periodo)

    SubEmpresas.find({ 'empresa._id': empresaID, periodo_id: periodoID }, (err, subempresas) => {
        Conceptos.find({ $or: [{ empresa: null }, { 'empresa._id': empresaID }] }, (err, conceptos) => {
            res.json({
                subempresas,
                conceptos
            });
        });
    });
}

function post (req, res) {
    let empresa = req.body;

    Empresas.create(empresa, 
    (err, newEmpresa) => {
        return res.json({
            success: true,
            message: 'Empresa creada con exito'
        });
    });
}

function getById (req, res) {
    let empresaID = new objectID(req.params.id); 
    
    Empresas.findOne({_id: empresaID}, 
    (err, empresa) => {
        return res.json(empresa);
    });
}

function getCount (find) {
    if (typeof(find) ===  undefined)
        find = {}; 

    return Empresas.count(find).exec((err, count) => {
        return count; 
    });
}

function put (req, res) {
    var empresa = req.body; 
    var empresaID = new objectID(empresa._id); 
    var message = 'Empresa editada con exito'; 
    
    Empresas.findByIdAndUpdate(empresaID, empresa, mongoResponse(res, message)); 
}

function destroy (req, res) {
    var empresaID = new objectID(req.params.id); 

    Empresas.delete({ _id: empresaID }, 
    (err, result) => {
        if (err) process.exit(err); 
        return res.json({
            success: true, 
            message: 'Empresa eliminada'
        });
    }); 
}

module.exports = {
    get, 
    getAll,
    getDataLiquidaciones,
    post,
    put, 
    destroy,
    getById
}