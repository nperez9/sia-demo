const base = '/subempresas'; 
const subempresas = require('./subempresas'); 
//const inserter = require('./insertsubempresas');

module.exports = function (router) {
    router.get(base, subempresas.get);
    router.get(base + '/all', subempresas.getAll);
    router.get(base + '/tabla_subsidios/:empresa/:periodo', subempresas.getTablaSubsidios);
    router.get(base + '/empresa/:empresaid/:periodo', subempresas.getByEmpresaId);
    router.get(base + '/clonar/:subempresaid/:periodoid', subempresas.getSubempresaClonacion);
    router.get(base + '/:id', subempresas.getById);
    
    router.post(base + '/clonar', subempresas.clonar);
    router.post(base + '/tabla_subsidios', subempresas.postTablaSubsidios);
    router.post(base, subempresas.post);
    
    router.put(base + '/:id/:periodo', subempresas.put); 
    
    router.delete(base + '/:id', subempresas.destroy);
}
