const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');

const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId;
const Empresas = mongoose.model('empresas');
const SubEmpresas = mongoose.model('sub_empresas');
const Conceptos = mongoose.model('conceptos');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');
const TablaSubsidios = mongoose.model('tabla_subsidios');

function get(req, res) {
    var findCriteria = {};
    var documentsPerPage = 20;
    var skip = (req.query.page - 1) * documentsPerPage;
    var periodo = req.params.periodo;

    if (req.query.search != '')
        findCriteria['razon_social'] = new RegExp(req.query.search, 'i');
    findCriteria['periodo'] = new objectID(req.query.empresaId);

    Empresas.find(findCriteria)
        .limit(documentsPerPage)
        .skip(skip)
        .exec((err, items) => {
            getCount(findCriteria).then(total => {
                return res.json({
                    items,
                    total
                });
            });
        });
}

function getAll(req, res) {
    Empresas.find({}, (err, docs) => {
        res.json(docs);
    });
}

function getByEmpresaId(req, res) {
    let periodoObj = new objectID(req.params.periodo);

    SubEmpresas.find({ 'periodo_id': periodoObj },
        (err, docs) => {
            res.json(docs);
        });
}

function post(req, res) {
    let subempresa = req.body;
    subempresa.subempresa_id = new objectID();
    subempresa.periodo_id = new objectID(subempresa.periodo_id);
    SubEmpresas.create(subempresa,
        (err, newEmpresa) => {
            return res.json({
                success: true,
                message: 'Sub empresa creada con exito'
            });
        });
}

function getById(req, res) {
    let subEmpresaID = new objectID(req.params.id);

    SubEmpresas.findOne({ _id: subEmpresaID },
        (err, subempresa) => {
            return res.json(subempresa);
        });
}

function getCount(find) {
    if (typeof (find) === undefined)
        find = {};

    return Empresas.count(find).exec((err, count) => {
        return count;
    });
}

function put(req, res) {
    const subempresa = req.body;
    const subempresaID = new objectID(subempresa.subempresa_id);
    const subempresaActual = new objectID(subempresa._id);
    const periodoID = new objectID(subempresa.periodo_id);

    let message = 'Sub empresa editada con exito';

    SubEmpresas.update(
        { _id: subempresaActual },
        { $set: subempresa },
        (err, doc) => {
            Liquidaciones.update({
                'legajo.subempresa._id': subempresaID,
                'periodo._id': periodoID
            },
                { $set: { 'legajo.subempresa': subempresa } },
                { multi: true },
                (err, doc) => {
                    Legajos.update(
                        {
                            'subempresa._id': subempresaID
                        },
                        {
                            $set: { subempresa: subempresa }
                        },
                        { multi: true },
                        mongoResponse(res, message)
                    );
                }
            );
        });
}

function destroy(req, res) {
    var subempresaID = new objectID(req.params.id);

    SubEmpresas.delete({ _id: subempresaID },
        (err, result) => {
            if (err) process.exit(err);
            return res.json({
                success: true,
                message: 'Sub empresa eliminada'
            });
        });
}

function getTablaSubsidios(req, res) {
    var empresa = new objectID(req.params.empresa);
    var periodo = new objectID(req.params.periodo);

    TablaSubsidios.find({ 'periodo': periodo, 'empresa': empresa },
        (err, docs) => {
            res.json(docs);
        });
}

function postTablaSubsidios(req, res) {
    const subempresas = req.body;

    TablaSubsidios.remove({}).exec((err, response) => {

        TablaSubsidios.insertMany(subempresas,
        (err, docs) => {
            if (err) return res.status(500).json({ 'success': false, message: "Error" });
            return res.json({ 'success': true, message: 'Datos guardados correctamente.' })
        });
    });
}


function clonar (req, res) {
    let subToClone = req.body;
    let successMessage = 'Subempresa clonada con exito';
    const subempresa = {

    };

    SubEmpresas.create(subempresa, mongoResponse(res, successMessage));
}

/**
 * Devuelve todo lo necesario para clonar la subempresa
 * @param subempresaid 
 * @param periodoid
 * @return {Object} subempresa-personas
 */
function getSubempresaClonacion (req, res) {
    const subEmpObjId = new objectID(req.params.subempresaid);
    const periodoObjId = new objectID(req.params.periodoid);

    let subempresa = Subempresa.findOne({"subempresa_id": subEmpObjId, "periodo_id": periodoObjId});
    let personasSubempesa = Liquidaciones.aggregate([
        {$match: {"legajo.subempresa.subempresa_id": subEmpObjId, "periodo._id": periodoObjId}},
        {
            $group: {
                _id: "$legajo.persona.persona_id",
                
            }                
        }
    ]);
    
}

module.exports = {
    get,
    getAll,
    getByEmpresaId,
    post,
    put,
    destroy,
    getById,
    getTablaSubsidios,
    postTablaSubsidios,
    getSubempresaClonacion,
    clonar
}