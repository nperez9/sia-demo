const base = '/variables'; 
const variables = require('./variables');
const isLoggued = require('../../auth/auth_middleware.js');
const passport = require('passport');

module.exports = function (router) {
    router.get(base + '/:empresa/:periodo', isLoggued, variables.get);
    router.get(base + '/:id', variables.getById);
    router.get(base + '/', variables.getAll);
    
    router.post(base, variables.post);
    // router.post(base+ '/login', usuarios.login);
    
    router.put(base + '/:id', variables.put);
    
    router.delete(base + '/:id', variables.destroy);
}