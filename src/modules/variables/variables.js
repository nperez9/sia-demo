const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse'); 

const mongoose = require('mongoose'); 
const objectID = mongoose.Types.ObjectId; 
const Variables = mongoose.model('variables');

function get (req, res) {
    var findCriteria = {};
    var documentsPerPage = 20; 
    var skip = (req.query.page - 1) * documentsPerPage;

    if (req.query.search != ''){
        findCriteria['campo_1'] = new RegExp(req.query.search, 'i'); 
        findCriteria['campo_2'] = new RegExp(req.query.search, 'i'); 
        findCriteria['campo_3'] = new RegExp(req.query.search, 'i'); 
        findCriteria['campo_4'] = new RegExp(req.query.search, 'i'); 
        findCriteria['campo_5'] = new RegExp(req.query.search, 'i'); 
    }

    findCriteria['empresa'] = req.params.empresa; 
    findCriteria['periodo'] = req.params.periodo; 

    Variables.find(findCriteria)
    .limit(documentsPerPage)
    .skip(skip)
    .sort({codigo: 1})
    .exec((err, items) => {
        return res.json(items);
    }); 
}

function getAll (req, res) {
    Variables.find({}, (err, docs) => {
        console.log(docs);
        res.json(docs);
    });
}

function post (req, res) {
    let variable = req.body;

    var where = {
        campo_1 : variable.campo_1,
        campo_2 : variable.campo_2,
        campo_3 : variable.campo_3,
        campo_4 : variable.campo_4,
        campo_5 : variable.campo_5
    }

    Variables.findOne(where,
        (err, r) => {
            if(r == null){
                Variables.create(variable, mongoResponse(res, 'Variable creada con exito')); 
            }else{
                return res.json({
                    success: false,
                    message: 'Variable existente'
                });
            }
    });
}

function getById (req, res) {
    let variableID = new objectID(req.params.id); 
    
    Variables.findOne({_id: variableID}, 
    (err, variable) => {
        return res.json(variable);
    });
}

// function getCount (find) {
//     if (typeof(find) ===  undefined)
//         find = {}; 

//     return Valores.count(find).exec((err, count) => {
//         return count; 
//     });
// }

function put (req, res) {
    var variable = req.body; 
    var variableID = new objectID(variable._id); 
    var message = 'Variable editada con exito'; 
    
    Variables.findByIdAndUpdate(variableID, variable, mongoResponse(res, message)); 
}

function destroy (req, res) {
    var varaibleID = new objectID(req.params.id); 
    let successMessage = 'Variable eliminada'; 

    Variables.delete({ _id: varaibleID }, mongoResponse(res, successMessage)); 
}

module.exports = {
    get,
    getAll,
    post,
    put,
    destroy,
    getById
}