const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const money = require('../../helpers/moneyHelper');
const moneyToSting = require('../../helpers/moneyToStringHelper');

const mongoose = require('mongoose');
const moment = require('moment');
var fs = require('fs');
var pdf = require('html-pdf');
const mustache = require('mustache');

const objectID = mongoose.Types.ObjectId;

const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');
const Empresas = mongoose.model('empresas');
const Periodos = mongoose.model('periodos');
const Valores = mongoose.model('valores');

function pad(n, length) {
  var n = n.toString();
  while (n.length < length)
    n = "0" + n;
  return n;
}

function horizontal(req, res) {
  const periodo = req.body.periodo;
  const empresa = req.body.empresa;
  const subempresas = req.body.subempresas;

  let periodoID = new objectID(periodo);
  let empresaID = new objectID(empresa);

  var view = {};
  var html = "";

  for (let a = 0; a < subempresas.length; a++) {
    subempresas[a] = new objectID(subempresas[a]);
  }
  
  var match = {
    'periodo._id': periodoID,
    'legajo.subempresa._id': { $in: subempresas },
    'legajo.subempresa.empresa._id': empresaID
  }
  
  var sort = {
    subempresa: 1
  }

  Liquidaciones.aggregate([
    { $match: match },
    { $unwind: '$legajo' },
    {
      $group: {
        _id: '$legajo.subempresa._id',
        periodo: { $first: '$periodo.descripcion' },
        subempresa: { $first: '$legajo.subempresa.descripcion' },
        legajos: { $push: '$legajo' },
        conceptos: { $push: '$conceptos' },
        remunerativo: { $push: '$remunerativo' },
        no_remunerativo: { $push: '$no_remunerativo' },
        deducciones: { $push: '$deducciones' },
        bruto: { $push: '$bruto' },
        neto: { $push: '$neto' }
      }
    },
    { $sort: sort }
  ]).exec((err, docs) => {

    Empresas.findById({ _id: empresaID }, (err, empresa) => {
      
      var recibos = {};
      var subempresas = [];

      for (var a = 0; a < docs.length; a++) {
        var view = {};
        var subtotal_titulares = {};
        var subtotal_suplentes = {};

        view.subempresa = docs[a].subempresa;
        view.periodo = docs[a].periodo;
        view.empresa = empresa.razon_social;
        view.registro = "-------------";
        view.direccion = empresa.direccion;
        view.localidad = empresa.localidad;
        view.telefono = empresa.telefono;

        view.legajos_titulares = [];
        view.legajos_suplentes = [];

        view.subtotal_titulares = {};
        view.subtotal_suplentes = {};

        view.subtotal_titulares.cargo_horas = 0;
        subtotal_titulares.col_1 = 0;
        subtotal_titulares.col_2 = 0;
        subtotal_titulares.col_3 = 0;
        subtotal_titulares.col_4 = 0;
        subtotal_titulares.col_5 = 0;
        subtotal_titulares.col_6 = 0;
        subtotal_titulares.col_7 = 0;
        subtotal_titulares.col_8 = 0;
        subtotal_titulares.remunerativo = 0;
        subtotal_titulares.col_10 = 0;
        subtotal_titulares.col_11 = 0;
        subtotal_titulares.col_12 = 0;
        subtotal_titulares.col_13 = 0;
        subtotal_titulares.deducciones = 0;
        subtotal_titulares.col_15 = 0;
        subtotal_titulares.col_16 = 0;
        subtotal_titulares.no_remunerativo = 0;
        subtotal_titulares.total_neto = 0;

        view.subtotal_suplentes.cargo_horas = 0;
        subtotal_suplentes.col_1 = 0;
        subtotal_suplentes.col_2 = 0;
        subtotal_suplentes.col_3 = 0;
        subtotal_suplentes.col_4 = 0;
        subtotal_suplentes.col_5 = 0;
        subtotal_suplentes.col_6 = 0;
        subtotal_suplentes.col_7 = 0;
        subtotal_suplentes.col_8 = 0;
        subtotal_suplentes.remunerativo = 0;
        subtotal_suplentes.col_10 = 0;
        subtotal_suplentes.col_11 = 0;
        subtotal_suplentes.col_12 = 0;
        subtotal_suplentes.col_13 = 0;
        subtotal_suplentes.deducciones = 0;
        subtotal_suplentes.col_15 = 0;
        subtotal_suplentes.col_16 = 0;
        subtotal_suplentes.no_remunerativo = 0;
        subtotal_suplentes.total_neto = 0;

        view.total_general = {};

        for (let i = 0; i < docs[a].legajos.length; i++) {
          var unLegajo = {};

          unLegajo.numero_legajo = docs[a].legajos[i].numero_legajo == undefined || docs[a].legajos[i].numero_legajo == "" ? docs[a].legajos[i].numero_sistema : docs[a].legajos[i].numero_legajo;
          unLegajo.persona = docs[a].legajos[i].persona.nombre;
          unLegajo.cargo_hrs = docs[a].legajos[i].horas;
          unLegajo.antiguedad = docs[a].legajos[i].antiguedad;

          unLegajo.subtotal_remunerativo = money(docs[a].remunerativo[i]);
          unLegajo.subtotal_deducciones = money(docs[a].deducciones[i]);
          unLegajo.subtotal_no_remunerativo = money(docs[a].no_remunerativo[i]);
          unLegajo.total_neto = money(docs[a].neto[i]);

          if (docs[a].legajos[i].caracter == "T") {
            subtotal_titulares.remunerativo = subtotal_titulares.remunerativo + docs[a].remunerativo[i];
            subtotal_titulares.deducciones = subtotal_titulares.deducciones + docs[a].deducciones[i];
            subtotal_titulares.no_remunerativo = subtotal_titulares.no_remunerativo + docs[a].no_remunerativo[i];
            subtotal_titulares.total_neto = subtotal_titulares.total_neto + docs[a].neto[i];
            view.subtotal_titulares.cargo_horas = view.subtotal_titulares.cargo_horas + parseInt(docs[a].legajos[i].horas);
          } else {
            subtotal_suplentes.remunerativo = subtotal_suplentes.remunerativo + docs[a].remunerativo[i];
            subtotal_suplentes.deducciones = subtotal_suplentes.deducciones + docs[a].deducciones[i];
            subtotal_suplentes.no_remunerativo = subtotal_suplentes.no_remunerativo + docs[a].no_remunerativo[i];
            subtotal_suplentes.total_neto = subtotal_suplentes.total_neto + docs[a].neto[i];
            view.subtotal_suplentes.cargo_horas = view.subtotal_suplentes.cargo_horas + parseInt(docs[a].legajos[i].horas);
          }

          for (let x = 0; x < docs[a].conceptos[i].length; x++) {

            switch (docs[a].conceptos[i][x].marcas.marca_1) {
              case 1:
                unLegajo.col_1 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_1 = subtotal_titulares.col_1 + unLegajo.col_1;
                } else {
                  subtotal_suplentes.col_1 = subtotal_suplentes.col_1 + unLegajo.col_1;
                }
                unLegajo.col_1 = money(unLegajo.col_1);
                break;
              case 2:
                unLegajo.col_2 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_2 = subtotal_titulares.col_2 + unLegajo.col_2;
                }
                else {
                  subtotal_suplentes.col_2 = subtotal_suplentes.col_2 + unLegajo.col_2;
                }
                unLegajo.col_2 = money(unLegajo.col_2);
                break;
              case 3:
                unLegajo.col_3 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_3 = subtotal_titulares.col_3 + unLegajo.col_3;
                }
                else {
                  subtotal_suplentes.col_3 = subtotal_suplentes.col_3 + unLegajo.col_3;
                }
                unLegajo.col_3 = money(unLegajo.col_3)
                break;
              case 4:
                unLegajo.col_4 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_4 = subtotal_titulares.col_4 + unLegajo.col_4;
                }
                else {
                  subtotal_suplentes.col_4 = subtotal_suplentes.col_4 + unLegajo.col_4;
                }
                unLegajo.col_4 = money(unLegajo.col_4);
                break;
              case 5:
                unLegajo.col_5 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_5 = subtotal_titulares.col_5 + unLegajo.col_5;
                }
                else {
                  subtotal_suplentes.col_5 = subtotal_suplentes.col_5 + unLegajo.col_5;
                }
                unLegajo.col_5 = money(unLegajo.col_5);
                break;
              case 6:
                unLegajo.col_6 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_6 = subtotal_titulares.col_6 + unLegajo.col_6;
                }
                else {
                  subtotal_suplentes.col_6 = subtotal_suplentes.col_6 + unLegajo.col_6;
                }
                unLegajo.col_6 = money(unLegajo.col_6);
                break;
              case 7:
                unLegajo.col_7 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_7 = subtotal_titulares.col_7 + unLegajo.col_7;
                }
                else {
                  subtotal_suplentes.col_7 = subtotal_suplentes.col_7 + unLegajo.col_7;
                }
                unLegajo.col_7 = money(unLegajo.col_7);
                break;
              case 8:
                unLegajo.col_8 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_8 = subtotal_titulares.col_8 + unLegajo.col_8;
                }
                else {
                  subtotal_suplentes.col_8 = subtotal_suplentes.col_8 + unLegajo.col_8;
                }
                unLegajo.col_8 = money(unLegajo.col_8);
                break;

              case 10:
                unLegajo.col_10 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_10 = subtotal_titulares.col_10 + unLegajo.col_10;
                }
                else {
                  subtotal_suplentes.col_10 = subtotal_suplentes.col_10 + unLegajo.col_10;
                }
                unLegajo.col_10 = money(unLegajo.col_10);
                break;
              case 11:
                unLegajo.col_11 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_11 = subtotal_titulares.col_11 + unLegajo.col_11;
                }
                else {
                  subtotal_suplentes.col_11 = subtotal_suplentes.col_11 + unLegajo.col_11;
                }
                unLegajo.col_11 = money(unLegajo.col_11);
                break;
              case 12:
                unLegajo.col_12 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_12 = subtotal_titulares.col_12 + unLegajo.col_12;
                }
                else {
                  subtotal_suplentes.col_12 = subtotal_suplentes.col_5 + unLegajo.col_12;
                }
                unLegajo.col_12 = money(unLegajo.col_12);
                break;
              case 13:
                unLegajo.col_13 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_13 = subtotal_titulares.col_13 + unLegajo.col_13;
                }
                else {
                  subtotal_suplentes.col_13 = subtotal_suplentes.col_13 + unLegajo.col_13;
                }
                unLegajo.col_13 = money(unLegajo.col_13);
                break;

              case 15:
                unLegajo.col_15 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_15 = subtotal_titulares.col_15 + unLegajo.col_15;
                }
                else {
                  subtotal_suplentes.col_15 = subtotal_suplentes.col_15 + unLegajo.col_15;
                }
                unLegajo.col_15 = money(unLegajo.col_15);
                break;
              case 16:
                unLegajo.col_16 = docs[a].conceptos[i][x].calculado;
                if (docs[a].legajos[i].caracter == "T") {
                  subtotal_titulares.col_16 = subtotal_titulares.col_16 + unLegajo.col_16;
                }
                else {
                  subtotal_suplentes.col_16 = subtotal_suplentes.col_16 + unLegajo.col_16;
                }
                unLegajo.col_16 = money(unLegajo.col_16);
                break;
            }

          }
          
          if (docs[a].legajos[i].caracter == "T") {
            view.legajos_titulares.push(unLegajo);
          } else {
            view.legajos_suplentes.push(unLegajo);
          }

          //check lineas subtotales
          if (view.legajos_titulares.length == 0) {
            view.linea_subtotal_titulares = false;
          } else {
            view.linea_subtotal_titulares = true;
          }
          if (view.legajos_suplentes.length == 0) {
            view.linea_subtotal_suplentes = false;
          } else {
            view.linea_subtotal_suplentes = true;
          }


          //sumatoria y conv. totales generales
          view.total_general.horas = view.subtotal_titulares.cargo_horas + view.subtotal_suplentes.cargo_horas;
          view.total_general.col_1 = money(subtotal_titulares.col_1 + subtotal_suplentes.col_1);
          view.total_general.col_2 = money(subtotal_titulares.col_2 + subtotal_suplentes.col_2);
          view.total_general.col_3 = money(subtotal_titulares.col_3 + subtotal_suplentes.col_3);
          view.total_general.col_4 = money(subtotal_titulares.col_4 + subtotal_suplentes.col_4);
          view.total_general.col_5 = money(subtotal_titulares.col_5 + subtotal_suplentes.col_5);
          view.total_general.col_6 = money(subtotal_titulares.col_6 + subtotal_suplentes.col_6);
          view.total_general.col_7 = money(subtotal_titulares.col_7 + subtotal_suplentes.col_7);
          view.total_general.col_8 = money(subtotal_titulares.col_8 + subtotal_suplentes.col_8);
          view.total_general.remunerativo = money(subtotal_titulares.remunerativo + subtotal_suplentes.remunerativo);

          view.total_general.col_10 = money(subtotal_titulares.col_10 + subtotal_suplentes.col_10);
          view.total_general.col_11 = money(subtotal_titulares.col_11 + subtotal_suplentes.col_11);
          view.total_general.col_12 = money(subtotal_titulares.col_12 + subtotal_suplentes.col_12);
          view.total_general.col_13 = money(subtotal_titulares.col_13 + subtotal_suplentes.col_13);

          view.total_general.deducciones = money(subtotal_titulares.deducciones + subtotal_suplentes.deducciones);

          view.total_general.col_15 = money(subtotal_titulares.col_15 + subtotal_suplentes.col_14);
          view.total_general.col_16 = money(subtotal_titulares.col_16 + subtotal_suplentes.col_15);

          view.total_general.no_remunerativo = money(subtotal_titulares.no_remunerativo + subtotal_suplentes.no_remunerativo)
          view.total_general.total_neto = money(subtotal_titulares.total_neto + subtotal_suplentes.total_neto);

          //conv. subtotal_titulares
          view.subtotal_titulares.col_1 = money(subtotal_titulares.col_1);
          view.subtotal_titulares.col_2 = money(subtotal_titulares.col_2);
          view.subtotal_titulares.col_3 = money(subtotal_titulares.col_3);
          view.subtotal_titulares.col_4 = money(subtotal_titulares.col_4);
          view.subtotal_titulares.col_5 = money(subtotal_titulares.col_5);
          view.subtotal_titulares.col_6 = money(subtotal_titulares.col_6);
          view.subtotal_titulares.col_7 = money(subtotal_titulares.col_7);
          view.subtotal_titulares.col_8 = money(subtotal_titulares.col_8);

          view.subtotal_titulares.col_10 = money(subtotal_titulares.col_10);
          view.subtotal_titulares.col_11 = money(subtotal_titulares.col_11);
          view.subtotal_titulares.col_12 = money(subtotal_titulares.col_12);
          view.subtotal_titulares.col_13 = money(subtotal_titulares.col_13);

          view.subtotal_titulares.col_15 = money(subtotal_titulares.col_15);
          view.subtotal_titulares.col_16 = money(subtotal_titulares.col_16);

          view.subtotal_titulares.remunerativo = money(subtotal_titulares.remunerativo);
          view.subtotal_titulares.deducciones = money(subtotal_titulares.deducciones);
          view.subtotal_titulares.no_remunerativo = money(subtotal_titulares.no_remunerativo);
          view.subtotal_titulares.total_neto = money(subtotal_titulares.total_neto);

          //conv. subtotal_suplentes
          view.subtotal_suplentes.col_1 = money(subtotal_suplentes.col_1);
          view.subtotal_suplentes.col_2 = money(subtotal_suplentes.col_2);
          view.subtotal_suplentes.col_3 = money(subtotal_suplentes.col_3);
          view.subtotal_suplentes.col_4 = money(subtotal_suplentes.col_4);
          view.subtotal_suplentes.col_5 = money(subtotal_suplentes.col_5);
          view.subtotal_suplentes.col_6 = money(subtotal_suplentes.col_6);
          view.subtotal_suplentes.col_7 = money(subtotal_suplentes.col_7);
          view.subtotal_suplentes.col_8 = money(subtotal_suplentes.col_8);

          view.subtotal_suplentes.col_10 = money(subtotal_suplentes.col_10);
          view.subtotal_suplentes.col_11 = money(subtotal_suplentes.col_11);
          view.subtotal_suplentes.col_12 = money(subtotal_suplentes.col_12);
          view.subtotal_suplentes.col_13 = money(subtotal_suplentes.col_13);

          view.subtotal_suplentes.col_15 = money(subtotal_suplentes.col_15);
          view.subtotal_suplentes.col_16 = money(subtotal_suplentes.col_16);

          view.subtotal_suplentes.remunerativo = money(subtotal_suplentes.remunerativo);
          view.subtotal_suplentes.deducciones = money(subtotal_suplentes.deducciones);
          view.subtotal_suplentes.no_remunerativo = money(subtotal_suplentes.no_remunerativo);
          view.subtotal_suplentes.total_neto = money(subtotal_suplentes.total_neto);

        }
        subempresas.push(view);
      }

      // Trae los valores 
      Valores.find({ 'codigo': 1 })
        .exec((err, items) => {
          try { 
            
            for (let i = 0; i < subempresas.length; i++) {
              subempresas[i].valor = items[0].valor
            }

            recibos.subempresas = subempresas;

            var content = fs.readFileSync("src/modules/recibos/recibo.oficio.tpl.html", 'utf8');
            html += mustache.render(content, recibos);
            var options = {
              format: 'Legal',
              orientation: "landscape"
            };

            pdf.create(html, options).toStream(function (err, stream) {
              stream.pipe(res);
            });
          } catch (e) {
            console.log(e);
          }
        });
    });
  });

}

module.exports = {
  horizontal
};