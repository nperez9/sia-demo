const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const money = require('../../helpers/moneyHelper');
const moneyToSting = require('../../helpers/moneyToStringHelper');

const mongoose = require('mongoose');
const moment = require('moment');
var fs = require('fs');
var pdf = require('html-pdf');
const mustache = require('mustache');

const objectID = mongoose.Types.ObjectId;

const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');
const Empresas = mongoose.model('empresas');
const Periodos = mongoose.model('periodos');

function pad(n, length) {
  var n = n.toString();
  while (n.length < length)
    n = "0" + n;
  return n;
}

function azul(req, res) {
  const periodo = req.body.periodo;
  const empresa = req.body.empresa;
  const subempresas = req.body.subempresas;
  const lugar_pago = req.body.lugar_pago;
  const fecha_pago = req.body.fecha_pago;
  const fecha_deposito = req.body.fecha_deposito;
  const orden = req.body.orden;

  let periodoID = new objectID(periodo);
  let empresaID = new objectID(empresa);

  var view = {};
  var html = "";

  if (subempresas.length != 0) {
    for (let a = 0; a < subempresas.length; a++) {
      subempresas[a] = new objectID(subempresas[a]);
    }
  }

  var match = {
    'periodo._id': periodoID,
    'legajo.subempresa._id': { $in: subempresas },
    'legajo.subempresa.empresa._id': empresaID
  }

  switch (orden) {
    case 1:
      var sort = {
        subempresa: 1,
        nombre: 1
      }
      break;
    case 2:
      var sort = {
        dni: 1
      }
      break;
    case 3:
      var sort = {
        nombre: 1
      }
      break;
  }
console.log(orden)
console.log(sort)
  Liquidaciones.aggregate([
    { $match: match },
    { $unwind: '$legajo' },
    {
      $group: {
        _id: '$_id',
        empresa: { $first: '$legajo.subempresa.empresa' },
        periodo: { $first: '$periodo.descripcion' },
        nombre: { $first: '$legajo.persona.nombre' },
        dni: { $first: '$legajo.persona.dni' },
        antiguedad: { $first: '$legajo.antiguedad' },
        numero_legajo: { $first: '$legajo.numero_legajo' },
        numero_sistema: { $first: '$legajo.numero_sistema' },
        subempresa: { $first: '$legajo.subempresa.descripcion' },
        categoria: { $first: '$legajo.subempresa.categoria' },
        fecha_alta: { $first: '$legajo.fecha_alta' },
        fecha_alta_instituto: { $first: '$legajo.persona.datos_administrativos.fecha_alta' },
        cargo: { $first: '$legajo.cargo.nombre' },
        cargo_editable: { $first: '$legajo.cargo_editable' },
        categoria_nombre: { $first: '$legajo.subempresa.categoria.nombre' },
        subvencion: { $first: '$legajo.suvencion' },
        titular: { $first: '$legajo.caracter' },
        cuil: { $first: '$legajo.persona.cuil' },
        conceptos: { $push: '$conceptos' },
        remunerativo : {$first: '$remunerativo'},
        no_remunerativo : {$first: '$no_remunerativo'},
        deducciones : {$first: '$deducciones'},
        neto : {$first: '$neto'}
      }
    },
    { $sort: sort }
  ]).exec((err, docs) => {

    var contador = 0;
    Empresas.findById({ _id: empresaID }, (err, empresa) => {

      for (var a = 0; a < docs.length; a++) {

        var subvencion = docs[a].suvencion == true ? "Si subv." : "No subv.";
        var titular = docs[a].titular == "T" ? "Titular" : "Suplente";
        var conceptos = [{}];

        for (var x = 0; x < docs[a].conceptos.length; x++) {
          for (let z = 0; z < docs[a].conceptos[x].length; z++) {
            
            var unConcepto = {};
            contador++;

            var reg = /\b(legajo->horas)\b/g
            if (reg.test(docs[a].conceptos[x].formula)) {
              unConcepto.cantidad = docs[a].legajos.horas;
            }else if (docs[a].conceptos[x].cantidad != undefined){
              unConcepto.cantidad = docs[a].conceptos[x].cantidad
            }else{
              unConcepto.cantidad = "1"
            }

            unConcepto.descripcion = docs[a].conceptos[x][z].descripcion ;
            monto = docs[a].conceptos[x][z].calculado == null ? 0 : docs[a].conceptos[x][z].calculado;

            switch (docs[a].conceptos[x][z].tipo_remuneracion) {
              case 1:
                unConcepto.remunerativo = money(monto);
                unConcepto.no_remunerativo = "";
                unConcepto.deducciones = "";
              break;

              case 2:
                unConcepto.remunerativo = money(monto);
                unConcepto.no_remunerativo = money(monto);
                unConcepto.deducciones = "";
              break;

              case 3:
                unConcepto.remunerativo = "";
                unConcepto.no_remunerativo = "";
                unConcepto.deducciones = money(monto);
              break;
            }

            conceptos.push(unConcepto);
          }
        }

        if(docs[a].categoria){
          var categoria = docs[a].categoria.codigo;
        }

        if(conceptos.length < 22){
          var relleno = [];
          for (let i = conceptos.length; i < 22; i++) {
            relleno.push({});
          }
        }

        var nro_recibo = pad(a+1, 7);
        var cargo = docs[a].cargo_editable == "" ? docs[a].cargo : docs[a].cargo_editable;
        var nro_legajo = docs[a].numero_legajo == undefined || docs[a].numero_legajo == "" ? docs[a].numero_sistema : docs[a].numero_legajo;

        view = {
          
          empresa: empresa.razon_social,
          cuit_empresa: empresa.cuit,
          direccion: empresa.direccion,
          localidad: empresa.localidad,
          persona: docs[a].nombre,
          antiguedad: docs[a].antiguedad,
          numero_legajo: nro_legajo,
          periodo: docs[a].periodo,
          categoria: categoria,
          subempresa: docs[a].subempresa,
          fecha_alta: moment(docs[a].fecha_alta).format('DD/MM/YYYY'),
          fecha_alta_instituto: moment(docs[a].fecha_alta_instituto).format('DD/MM/YYYY'),
          dni: docs[a].dni,
          cargo: cargo,
          categoria_nombre: docs[a].categoria_nombre,
          subvencion: subvencion,
          titular: titular,
          cuit_persona: docs[a].cuil,
          lugar_pago: lugar_pago,
          fecha_pago: moment(fecha_pago).format('DD/MM/YYYY'),
          fecha_deposito: moment(fecha_deposito).format('DD/MM/YYYY'),
          conceptos: conceptos,
          relleno: relleno,
          neto: money(docs[a].neto),
          total_remu: money(docs[a].remunerativo),
          total_no_remu: money(docs[a].no_remunerativo),
          total_deduccion: money(docs[a].deducciones),
          neto_string: moneyToSting(docs[a].neto),
          nro_recibo: nro_recibo
        }

        var content = fs.readFileSync("src/modules/recibos/recibo.a4.azul.tpl.html", 'utf8');
        html += mustache.render(content, view);
        
      }

      var options = {
        format: 'a4',
        orientation: "landscape"
      };

      pdf.create(html, options).toStream(function (err, stream) {
        stream.pipe(res);
      });

    });

  });

}

module.exports = {
  azul
};