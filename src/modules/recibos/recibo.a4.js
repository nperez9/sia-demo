const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const money = require('../../helpers/moneyHelper');
const moneyToSting = require('../../helpers/moneyToStringHelper');

const mongoose = require('mongoose');
const moment = require('moment');
var fs = require('fs');
var pdf = require('html-pdf');
const mustache = require('mustache');

const objectID = mongoose.Types.ObjectId;

const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');
const Empresas = mongoose.model('empresas');
const Periodos = mongoose.model('periodos');

function pad(n, length) {
  var n = n.toString();
  while (n.length < length)
    n = "0" + n;
  return n;
}

function vertical(req, res) {
  const periodo = req.body.periodo;
  const empresa = req.body.empresa;
  const subempresas = req.body.subempresas;
  const banco = req.body.banco;
  const fecha_pago = req.body.fecha_pago;
  const fecha_deposito = req.body.fecha_deposito;

  let periodoID = new objectID(periodo);
  let empresaID = new objectID(empresa);

  var html = "";

  if (subempresas.length != 0) {
    for (let a = 0; a < subempresas.length; a++) {
      subempresas[a] = new objectID(subempresas[a]);
    }
  }

  var match = {
    'periodo._id': periodoID,
    'legajo.subempresa._id': { $in: subempresas },
    'legajo.subempresa.empresa._id': empresaID
  }

  var sort = {
    nombre: 1,
    dni: 1
  }

  Liquidaciones.aggregate([
    { $match: match },
    { $unwind: '$legajo'},
    {
      $group: {
        _id: '$legajo.persona._id',
        legajos: {$push: '$legajo'},
        conceptos: {$push: '$conceptos'},
        remunerativo: {$push: '$remunerativo'},
        no_remunerativo: {$push: '$no_remunerativo'},
        deducciones: {$push: '$deducciones'},
        bruto: {$push: '$bruto'},
        neto: {$push: '$neto'},
        empresa: { $first: '$legajo.subempresa.empresa' },
        periodo: { $first: '$periodo.descripcion' },
        persona: { $first: '$legajo.persona.nombre' },
        antiguedad: { $first: '$legajo.antiguedad' },
        numero_legajo: { $first: '$legajo.numero_legajo' },
        fecha_alta: { $first: '$legajo.fecha_alta' },
        fecha_alta_instituto: { $first: '$legajo.persona.datos_administrativos.fecha_alta' },
        cuil: { $first: '$legajo.persona.cuil' },
        dni: { $first: '$legajo.persona.dni' },
        titular: { $first: '$legajo.caracter' },
       
      }
    },
    { $sort: sort }
  ]).exec((err, docs) => {
    if (typeof(docs) === 'undefined') 
      return res.status(500).json({'success': false, message:'Respuesta de consulta indefinida - contacte al daministrador'});
    if (docs.length < 1) 
      return res.status(504).json({success: false, message: 'No hay registros con el criterio seleccionado'});
      console.log()
    
      Empresas.findById({ _id: empresaID }, (err, empresa) => {
      for (var a = 0; a < docs.length; a++) {
        
        var legajos = [];
        var view = {};
        view.legajos = [];
        var nro_recibo = pad(a + 1, 7);

        var total_neto = 0;
        var total_bruto = 0;
        
        view.empresa = empresa.razon_social;
        view.cuit_empresa = empresa.cuit;
        view.direccion = empresa.direccion;
        view.localidad = empresa.localidad;
        view.persona = docs[a].persona;
        view.periodo = docs[a].periodo;
        view.dni = docs[a].dni;
        view.cuit_persona = docs[a].cuil;
        view.numero_legajo = docs[a].numero_legajo;
        view.banco = banco;
        view.fecha_pago = moment(fecha_pago).format('DD/MM/YY');
        view.fecha_deposito = moment(fecha_deposito).format('DD/MM/YY');
        view.fecha_alta = moment(docs[a].fecha_alta).format('DD/MM/YYYY');
        view.fecha_alta_instituto = moment(docs[a].fecha_alta_instituto).format('DD/MM/YYYY');
        view.nro_recibo = nro_recibo;
        view.antiguedad = docs[a].antiguedad;

        for (var x = 0; x < docs[a].conceptos.length; x++) {
          var unLegajo = {};
          unLegajo.conceptos = [];
          nro_legajo = docs[a].legajos[x].numero_legajo == undefined || docs[a].legajos[x].numero_legajo == "" ? docs[a].legajos[x].numero_sistema : docs[a].legajos[x].numero_legajo; 

          unLegajo.numero_legajo = nro_legajo;
          unLegajo.fecha_alta = moment(docs[a].legajos[x].fecha_alta).format('DD/MM/YYYY');
          unLegajo.antiguedad = docs[a].legajos[x].antigueda;
          unLegajo.seccion = docs[a].legajos[x].subempresa.descripcion;
          unLegajo.cargo = docs[a].legajos[x].cargo.nombre;
          unLegajo.titular = docs[a].titular == "T" ? "Titular" : "Suplente";
          // unLegajo.categoria = docs[a].legajos[x].subempresa.categoria.nombre;

          for (let z = 0; z < docs[a].conceptos[x].length; z++) {

            var unConcepto = {};
            var reg = /\b(legajo->horas)\b/g
            
            if (reg.test(docs[a].conceptos[x][z].formula)) {
              unConcepto.cantidad = docs[a].legajos[x].horas;
            }else if (docs[a].conceptos[x][z].cantidad != undefined){
              unConcepto.cantidad = docs[a].conceptos[x][z].cantidad
            }else{
              unConcepto.cantidad = "1"
            }

            unConcepto.descripcion = docs[a].conceptos[x][z].descripcion;
            monto = docs[a].conceptos[x][z].calculado == null ? 0 : docs[a].conceptos[x][z].calculado;

            switch (docs[a].conceptos[x][z].tipo_remuneracion) {
              case 1:
                unConcepto.remunerativo =  money(monto).substr(1);
                unConcepto.no_remunerativo = "";
                unConcepto.deducciones = "";
                break;

              case 2:
                unConcepto.remunerativo = "";
                unConcepto.no_remunerativo = money(monto).substr(1);
                unConcepto.deducciones = "";
                break;

              case 3:
                unConcepto.remunerativo = "";
                unConcepto.no_remunerativo = "";
                unConcepto.deducciones = money(monto).substr(1);
                break;
            }

            unLegajo.conceptos.push(unConcepto);
          }

          unLegajo.total_remu = money(docs[a].remunerativo[x]);
          unLegajo.total_no_remu = money(docs[a].no_remunerativo[x]);
          unLegajo.total_deduccion = money(docs[a].deducciones[x]);
          unLegajo.neto = money(docs[a].neto[x]);
          unLegajo.bruto = money(docs[a].bruto[x]);
          
          unLegajo.isfirst = false;
          unLegajo.islast = false;

          if (view.legajos.length == 0){
            unLegajo.isfirst = true;
          }

          if(view.legajos.length+1 == docs[a].legajos.length){
            unLegajo.islast = true;
          }

          unLegajo.num_pagina = view.legajos.length+1;

          if(docs[a].legajos[x].licencias.length == 0 || docs[a].legajos[x].licencias.fecha_fin > moment()){
            view.legajos.push(unLegajo);
          }

          total_neto = total_neto + docs[a].neto[x];
          total_bruto = total_bruto + docs[a].bruto[x];
        }

        view.total_neto = money(total_neto);
        view.neto_string = moneyToSting(total_neto),
        view.total_bruto = money(total_bruto);
        
        var content = fs.readFileSync("src/modules/recibos/recibo.a4.vertical.tpl.html", 'utf8');
        html += mustache.render(content, view);
      }

      var options = {
        format: 'a4'
      };
  
      pdf.create(html, options).toStream(function (err, stream) {
        stream.pipe(res);
      });

    });

  });

}

function horizontal(req, res) {
  const periodo = req.body.periodo;
  const empresa = req.body.empresa;
  const subempresas = req.body.subempresas;
  const lugar_pago = req.body.lugar_pago;
  const fecha_pago = req.body.fecha_pago;
  const fecha_deposito = req.body.fecha_deposito;
  const orden = req.body.orden;
  const imagen = req.body.fondo;
  const path = req.body.path;

  let periodoID = new objectID(periodo);
  let empresaID = new objectID(empresa);

  var view = {};
  var html = "";

  if (subempresas.length != 0) {
    for (let a = 0; a < subempresas.length; a++) {
      subempresas[a] = new objectID(subempresas[a]);
    }
  }

  var match = {
    'periodo._id': periodoID,
    'legajo.subempresa._id': { $in: subempresas },
    'legajo.subempresa.empresa._id': empresaID
  }

  let sort = {};
  switch (orden) {
    case 1:
      sort = {
        subempresa: 1,
        nombre: 1
      }
      break;
    case 2:
      sort = {
        dni: 1
      }
      break;
    case 3:
      sort = {
        nombre: 1
      }
      break;
    default: 
      sort = {
        nombre: 1
      }
      break;
  }

  Liquidaciones.aggregate([
    { $match: match },
    { $unwind: '$legajo' },
    {
      $group: {
        _id: '$_id',
        legajos: {$first: '$legajo'},
        empresa: { $first: '$legajo.subempresa.empresa' },
        periodo: { $first: '$periodo.descripcion' },
        nombre: { $first: '$legajo.persona.nombre' },
        dni: { $first: '$legajo.persona.dni' },
        antiguedad: { $first: '$legajo.antiguedad' },
        numero_legajo: { $first: '$legajo.numero_legajo' },
        subempresa: { $first: '$legajo.subempresa.descripcion' },
        categoria: { $first: '$legajo.subempresa.categoria' },
        fecha_alta: { $first: '$legajo.fecha_alta' },
        fecha_alta_instituto: { $first: '$legajo.persona.datos_administrativos.fecha_alta' },
        cargo: { $first: '$legajo.cargo.nombre' },
        cargo_editable: { $first: '$legajo.cargo_editable' },
        categoria_nombre: { $first: '$legajo.subempresa.categoria.nombre' },
        subvencion: { $first: '$legajo.suvencion' },
        titular: { $first: '$legajo.caracter' },
        cuil: { $first: '$legajo.persona.cuil' },
        conceptos: { $push: '$conceptos' },
        remunerativo : {$first: '$remunerativo'},
        no_remunerativo : {$first: '$no_remunerativo'},
        deducciones : {$first: '$deducciones'},
        neto : {$first: '$neto'}
      }
    },
    { $sort: sort }
  ]).exec((err, docs) => {

    if (typeof(docs) === 'undefined') 
      return res.status(500).json({'success': false, message:'Respuesta de consulta indefinida - contacte al daministrador'});
    if (docs.length < 1) 
      return res.status(504).json({success: false, message: 'No hay registros con el criterio seleccionado'});

    Empresas.findById({ _id: empresaID }, (err, empresa) => {

      for (var a = 0; a < docs.length; a++) {

        var subvencion = docs[a].suvencion == true ? "Si subv." : "No subv.";
        var titular = docs[a].titular == "T" ? "Titular" : "Suplente";
        var conceptos = [{}];
        var auxCount = 0;

        for (var x = 0; x < docs[a].conceptos.length; x++) {
          for (let z = 0; z < docs[a].conceptos[x].length; z++) {
            
            var unConcepto = {};

            var reg = /\b(legajo->horas)\b/g
            if (reg.test(docs[a].conceptos[x][z].formula)) {
              unConcepto.cantidad = docs[a].legajos.horas
            }else if (docs[a].conceptos[x][z].cantidad != undefined){
              unConcepto.cantidad = docs[a].conceptos[x][z].cantidad
            }else{
              unConcepto.cantidad = "1"
            }

            // unConcepto.cantidad = docs[a].conceptos[x][z].cantidad == undefined ? docs[a].legajos.horas : docs[a].conceptos[x][z].cantidad;
            unConcepto.descripcion = docs[a].conceptos[x][z].descripcion ;
            monto = docs[a].conceptos[x][z].calculado == null ? 0 : docs[a].conceptos[x][z].calculado;

            switch (docs[a].conceptos[x][z].tipo_remuneracion) {
              case 1:
                unConcepto.remunerativo = money(monto).substr(1);
                unConcepto.no_remunerativo = "";
                unConcepto.deducciones = "";
              break;

              case 2:
                unConcepto.remunerativo = "";
                unConcepto.no_remunerativo = money(monto).substr(1);
                unConcepto.deducciones = "";
              break;

              case 3:
                unConcepto.remunerativo = "";
                unConcepto.no_remunerativo = "";
                unConcepto.deducciones = money(monto).substr(1);
              break;
            }

            conceptos.push(unConcepto);
          }
        }

        if(docs[a].categoria){
          var categoria = docs[a].categoria.codigo;
        }

        if(conceptos.length < 21){
          var relleno = [];
          for (let i = conceptos.length; i < 21; i++) {
            relleno.push({});
          }
        }

        var nro_recibo = pad(a+1, 7);
        var cargo = docs[a].cargo_editable == "" ? docs[a].cargo : docs[a].cargo_editable;
        
          if(docs[a].fecha_alta_instituto == null){
            fecha_alta_instituto = "S/A";
          }else{
            fecha_alta_instituto = moment(docs[a].fecha_alta_instituto).format('DD/MM/YYYY')
          }

          nro_legajo = docs[a].legajos.numero_legajo == undefined || docs[a].legajos.numero_legajo == "" ? docs[a].legajos.numero_sistema : docs[a].legajos.numero_legajo; 

        view = {
          
          empresa: empresa.razon_social,
          cuit_empresa: empresa.cuit,
          direccion: empresa.direccion,
          localidad: empresa.localidad,
          persona: docs[a].nombre,
          antiguedad: docs[a].antiguedad,
          numero_legajo: nro_legajo,
          periodo: docs[a].periodo,
          categoria: categoria,
          subempresa: docs[a].subempresa,
          fecha_alta: moment(docs[a].fecha_alta).format('DD/MM/YYYY'),
          fecha_alta_instituto: fecha_alta_instituto,
          dni: docs[a].dni,
          cargo: cargo,
          categoria_nombre: docs[a].categoria_nombre,
          subvencion: subvencion,
          titular: titular,
          cuit_persona: docs[a].cuil,
          lugar_pago: lugar_pago,
          fecha_pago: moment(fecha_pago).format('DD/MM/YYYY'),
          fecha_deposito: moment(fecha_deposito).format('DD/MM/YYYY'),
          conceptos: conceptos,
          relleno: relleno,
          neto: money(docs[a].neto),
          total_remu: money(docs[a].remunerativo),
          total_no_remu: money(docs[a].no_remunerativo),
          total_deduccion: money(docs[a].deducciones),
          neto_string: moneyToSting(docs[a].neto),
          nro_recibo: nro_recibo,
          path: path,
          imagen: imagen
        }
        
        if(docs[a].legajos.licencias.length == 0 || docs[a].legajos.licencias.fecha_fin > moment()){
          var content = fs.readFileSync("src/modules/recibos/recibo.a4.horizontal.tpl.html", 'utf8');
          html += mustache.render(content, view);
        }
        
        // res.write(html)
        
      }
      var options = {
        format: 'a4',
        orientation: "landscape"
      };

      pdf.create(html, options).toStream(function (err, stream) {
        stream.pipe(res);
      });

    });

  });

}

module.exports = {
  vertical,
  horizontal
};