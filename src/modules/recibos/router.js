const base = '/recibos';
const recibosA4 = require('./recibo.a4');
const recibosOficio = require('./recibo.oficio');
const azul = require('./recibo.azul');

module.exports = function (router) {
    router.post(base + '/a4/vertical', recibosA4.vertical);
    router.post(base + '/a4/horizontal', recibosA4.horizontal);
    router.post(base + '/oficio', recibosOficio.horizontal);
    router.post(base + '/a4/azul', azul.azul);
}