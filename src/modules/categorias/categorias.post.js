const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse'); 

const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId; 
const Categorias = mongoose.model('categorias');

module.exports = function (req, res) {
    let categoria = req.body;

    Categorias.create(categoria, mongoResponse(res, 'Categoria creada con exito'));
}