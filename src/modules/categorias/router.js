const base = '/categorias';
var categorias = {};
categorias = require('./categorias.get');
categorias.post = require('./categorias.post');
const isLoggued = require('../../auth/auth_middleware.js');

module.exports = function (router) {
	router.get(base, isLoggued, categorias.get);
	router.get(base + '/all', categorias.getAll);
	router.get(base + '/:id', categorias.getOne);

	router.put(base + '/:id', categorias.put);
	
	router.post(base, categorias.post);

	router.delete(base + '/:id', categorias.destroy);
}