const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse'); 

const mongoose = require('mongoose'); 
const objectID = mongoose.Types.ObjectId; 
const Categorias = mongoose.model('categorias'); 

function getCount (find) {
    if (typeof(find) ===  undefined)
        find = {}; 

    return Categorias.count(find).exec((err, count) => {
        return count;
    });
}


function get (req, res) {
    var findCriteria = {};
    var documentsPerPage = 20;
    var skip = (req.query.page - 1) * documentsPerPage;

    if (req.query.search != '') {
        findCriteria['nombre'] = new RegExp(req.query.search, 'i');
    }

    Categorias.find(findCriteria)
    .limit(documentsPerPage)
    .skip(skip)
    .exec((err, items) => {
        getCount(findCriteria).then(total => {
            return res.json({
                items,
                total
            });
        }); 
    }); 
}

function getAll (req, res) {
    Categorias.find({}, (err, docs) => {
        res.json(docs);
    });
}


function getOne (req, res) {
    var categoriasObjId = new objectID(req.params.id);
    
    Categorias.findOne({_id: categoriasObjId}, (err, doc) => {
        if (err) console.log(err);
        return res.json(doc);
    });
}


function put (req, res) {
    var categoria = req.body;
    var categoriasObjId = new objectID(categoria._id);

    Categorias.findOneAndUpdate(
        {_id: categoriasObjId},
        {$set: categoria},
        mongoResponse(res, 'Categoria Editada con exito')
    );
}


function destroy (req, res) {
    var categoriasObjId = new objectID(req.params.id);

    Categorias.delete({_id: categoriasObjId}, mongoResponse(res, 'Categoria eliminada'));
}

module.exports = {
    get,
    getAll,
    getOne,
    put,
    destroy
}