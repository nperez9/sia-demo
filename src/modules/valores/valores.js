const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');

const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId;
const Valores = mongoose.model('valores');
const moment = require('moment');

function get(req, res) {
    var findCriteria = {};
    var documentsPerPage = 20;
    var skip = (req.query.page - 1) * documentsPerPage;

    // if(req.cookies.tipo != "admin"){
    //     return res.status(403).json({success: false, message: 'Acceso denegado'});
    // }

    if (req.query.search != '')
        findCriteria['descripcion'] = new RegExp(req.query.search, 'i');

    Valores.find(findCriteria)
        .limit(documentsPerPage)
        .skip(skip)
        .sort({ codigo: 1 })
        .exec((err, items) => {
            getCount(findCriteria).then(total => {
                return res.json({
                    items,
                    total
                });
            });
        });
}

function getAll(req, res) {
    Valores.find({}, (err, docs) => {
        res.json(docs);
    });
}

function getValores(req, res) {

    Valores.find({fecha_fin : null}, (err, docs) => {
        if (err) console.log(err);
        return res.json(docs);
    });
}

function post(req, res) {
    let concepto = req.body;
    let successMessage = 'Valor creado con exito'

    Valores.findOne({ codigo: concepto.codigo, fecha_fin: null },
        (err, doc) => {

            if (doc != null) {
                let conceptoID = new objectID(doc._id);
                Valores.update({ _id: conceptoID}, { "fecha_fin": moment().format('MM/DD/YYYY') }).exec((err, searches) => {
                    if (err) return res.status(500).json({ 'success': false, message: "Error" });
                    Valores.create(concepto, mongoResponse(res, successMessage));
                });
            }else{
                Valores.create(concepto, mongoResponse(res, successMessage));
            }

        });
}

function getById(req, res) {
    let conceptoID = new objectID(req.params.id);

    Valores.findOne({ _id: conceptoID },
        (err, concepto) => {
            return res.json(concepto);
        });
}

function getCount(find) {
    if (typeof (find) === undefined)
        find = {};

    return Valores.count(find).exec((err, count) => {
        return count;
    });
}

function put(req, res) {
    var valor = req.body;
    var valorID = new objectID(valor._id);
    var message = 'Valor editado con exito';

    Valores.findByIdAndUpdate(valorID, valor, mongoResponse(res, message));
}

function destroy(req, res) {
    var conceptoID = new objectID(req.params.id);
    let successMessage = 'Valor eliminado';

    Valores.delete({ _id: conceptoID }, mongoResponse(res, successMessage));
}

function check_code(req, res) {
    var code = req.params.code;

    Valores.findOne({ codigo: code, fecha_fin: null },
        (err, doc) => {
            return res.json(doc)
        });
}

module.exports = {
    get,
    getAll,
    getValores,
    post,
    put,
    destroy,
    getById,
    check_code
}