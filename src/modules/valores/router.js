const base = '/valores'; 
const valores = require('./valores');
const isLoggued = require('../../auth/auth_middleware.js');
//const inserter = require('./insertvalores');

module.exports = function (router) {
    router.get(base + '/activos', valores.getValores);
    router.get(base, isLoggued, valores.get);
    router.get(base + '/all', valores.getAll); 
    router.get(base + '/:id', valores.getById); 
    router.get(base + '/check/:code', valores.check_code);
    
    router.post(base, valores.post);
    
    router.put(base + '/:id', valores.put); 
    
    router.delete(base + '/:id', valores.destroy);
}