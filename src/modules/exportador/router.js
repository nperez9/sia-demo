const base = '/exportar';
const fs = require('fs');
const glob = require('glob');
const mongoose = require('mongoose');
const Conceptos = mongoose.model('conceptos');

module.exports = function (router) {
    // Carga todos los json de conceptos de la carpetas conceptos
    router.get(base + '/conceptos', (req, res) => {
      var conceptos = glob.sync('./export/conceptos/*.json');
      
      console.log(conceptos);
      var jsonConceptos = [];
      var insertConceptos = [];

      if (conceptos.length <= 0) throw new Error('Theres no modules, check you routes');
      conceptos.forEach(function (fileRoute) {
        var readedFile =  fs.readFileSync(fileRoute);
        console.log(fileRoute);
        jsonConceptos = jsonConceptos.concat(JSON.parse(readedFile));
      });

      jsonConceptos.forEach(con => {
        if (con['CODIGO'] !== undefined) {
          let formatedConcepto = {
            tipo_remuneracion: parseInt(con['NRO_COD'].toString().substr(0,1)),
            codigo: con['CODIGO'],
            descripcion: con['DESCRIP'],
            formula: con['FORMULA'],
            indice: (!Number.isNaN(Number(con['INDICE'].replace(',', '.')))) ? Number(con['INDICE'].replace(',', '.')) : 0,
            fijo: (con['EVENTUAL'] == '1' || con['EVENTUAL'] == 'VERDADERO') ? true : false, 
            orden:  (con['ORDEN']),
            clase: (!Number.isNaN(Number(con['CLASE']))) ? Number(con['CLASE']) : 0,
            marcas: {
              marca: (!Number.isNaN(Number(con['MARCA']))) ? Number(con['MARCA']) : 0,
              marca_1: (!Number.isNaN(Number(con['MARCA1']))) ? Number(con['MARCA1']) : 0,
              marca_2: (!Number.isNaN(Number(con['MARCA2']))) ? Number(con['MARCA2']) : 0,
              marca_3: (!Number.isNaN(Number(con['MARCA3']))) ? Number(con['MARCA3']) : 0,
              marca_4: (!Number.isNaN(Number(con['MARCA4']))) ? Number(con['MARCA4']) : 0,
              marca_5: (!Number.isNaN(Number(con['MARCA5']))) ? Number(con['MARCA5']) : 0
            },
            imp_recibo: (con['IMPRIME'] == '1'|| con['IMPRIME'] == 'VERDADERO') ? true : false,
            ganancias: (con['GANANCIAS'] == '1'|| con['GANANCIAS'] == 'VERDADERO') ? true : false,
          };

          if (con['SAC'] !== undefined) 
            formatedConcepto.sac = (con['SAC'] == '1' || con['SAC'] == 'SI' || con['SAC'] == 'VERDADERO') ? true : false;
          else if (con['S.A.C.'] !== undefined)
            formatedConcepto.sac = (con['IMP. REC.'] == '1' || con['IMP. REC.'] == 'SI' || con['IMP. REC.'] == 'VERDADERO' )
             ? true : false;

          if (con['CAJA COMPLEMENTARIA'] !== undefined) 
            formatedConcepto.caja_complementaria = (con['CAJA COMPLEMENTARIA'] == '1' || con['CAJA COMPLEMENTARIA'] == 'SI' || con['CAJA COMPLEMENTARIA'] == 'VERDADERO') 
            ? true : false;
          else if (con['C. COMPL.'] !== undefined)
            formatedConcepto.caja_complementaria = (con['C. COMPL.'] == '1' || con['C. COMPL.'] == 'SI' || con['C. COMPL.'] == 'VERDADERO') ? true : false;

          //if (formatedConcepto.orden == NaN) return res.json(formatedConcepto);
          insertConceptos.push(formatedConcepto);
        }
      });
      console.log(insertConceptos.length);

      try {
        Conceptos.insertMany(insertConceptos).then(function (err, docs) {
          if (err) console.log(err);
          return res.send('Good process');
        });
      } catch (e) {
        console.log(e);
        console.log('------------------------------------');
        console.log(insertConceptos);
        console.log('------------------------------------');
        process.exit(1);
      }
      
    });
};
