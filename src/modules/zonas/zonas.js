const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const mongoose = require('mongoose'); 
const objectID = mongoose.Types.ObjectId; 
const Zonas = mongoose.model('zonas');

function get (req, res) {
    Zonas.find({}, (err, docs) => {
        res.json(docs); 
    });
}

module.exports = {
    get
}