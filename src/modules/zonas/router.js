const base = '/zonas';
const zonas = require('./zonas');

module.exports = function (router) {
    router.get(base, zonas.get);
}
