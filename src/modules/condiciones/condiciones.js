const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const mongoose = require('mongoose'); 
const objectID = mongoose.Types.ObjectId; 
const Condiciones = mongoose.model('condiciones');

function get (req, res) {
    Condiciones.find({}, (err, docs) => {
        res.json(docs); 
    });
}

module.exports = {
    get
}