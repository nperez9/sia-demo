const base = '/condiciones';
const condiciones = require('./condiciones');

module.exports = function (router) {
    router.get(base, condiciones.get);
}