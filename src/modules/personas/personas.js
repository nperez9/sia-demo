const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');

const mongoose = require('mongoose');
var csvjson = require('csvjson');
const moment = require('moment');
const fs = require('fs');
const objectID = mongoose.Types.ObjectId;

const Personas = mongoose.model('personas');
const Legajos = mongoose.model('legajos');
const Liquidaciones = mongoose.model('liquidaciones');
const ObrasSociales = mongoose.model('obras_sociales');
const Actividades = mongoose.model('actividades');
const Condiciones = mongoose.model('condiciones');
const Modalidades = mongoose.model('modalidades');
const Zonas = mongoose.model('zonas');
const Situaciones = mongoose.model('situaciones');


/**
 * Endpoint GET personas con soporte a queryString
 * @param {objectId} empresa
 * @param {objectId} periodo
 * @param {string} ?search
 * @return {object} response
 */
function get(req, res) {
    var findCriteria = {};
    var documentsPerPage = 20;
    var skip = (req.query.page - 1) * documentsPerPage;

    findCriteria['empresa._id'] = new objectID(req.params.empresa);
    findCriteria['periodo_id'] = new objectID(req.params.periodo);

    if (req.query.search != '')
        findCriteria['$or'] = [
            { nombre: new RegExp(req.query.search, 'i') },
            { cuil: new RegExp(req.query.search, 'i') }
        ];

    Personas.find(findCriteria)
        .limit(documentsPerPage)
        .skip(skip)
        .sort({ nombre: 1 })
        .exec((err, items) => {
            getCount(findCriteria).then(total => {
                return res.json({
                    items,
                    total
                });
            });
        });
}


/**
 * Endpoint GET de personas desde liquidaciones para
 * poder filtrar por periodo sin necesidad de modificar
 * la coleccion de personas
 * @param {Number} ?page
 * @param {ObjectId} &empresaId
 * @param {ObjectId} &periodoId
 * @param {String} &seach (Nombre de persona)
 */
function getPersonasByLiquidacion(req, res) {
    var findCriteria = {};
    var documentsPerPage = 20;
    var skip = (req.query.page - 1) * documentsPerPage;

    if (req.query.search != '' && typeof (req.query.search) !== 'undefined')
        findCriteria['legajo.persona.nombre'] = new RegExp(req.query.search, 'i');
    if (req.query.empresaId != '')
        findCriteria['legajo.subempresa.empresa._id'] = new objectID(req.query.empresaId);
    if (req.query.periodoId != '')
        findCriteria['periodo._id'] = new objectID(req.query.periodoId);


    Liquidaciones.aggregate([
        {
            $match: findCriteria
        },
        {
            $group: {
                _id: "$legajo.persona._id",
                legajo: { $first: "$legajo" },
                periodo: { $first: "$periodo" },
                ganancias: { $first: "$ganancias" }
            }
        }
    ])
        .exec((err, items) => {
            getCount(findCriteria, Liquidaciones).then(total => {
                return res.json({
                    items,
                    total
                });
            });
        });
}


function getCount(find, model) {
    if (typeof (find) === undefined)
        find = {};
    if (typeof (model) === undefined)
        model = Personas;

    return Personas.count(find).exec((err, count) => {
        return count;
    });
}


function getOne(req, res) {
    var personaID = new objectID(req.params.id);

    Personas.findOne({ _id: personaID }, (err, doc) => {
        res.json(doc);
    });
}

function getOnePersonaId(req, res) {
    var personaID = new objectID(req.params.id);

    Personas.findOne({ persona_id: personaID }, (err, doc) => {
        res.json(doc);
    });
}


function post(req, res) {
    var persona = req.body;
    const empresaID = new objectID(persona.empresa._id);

    Personas.findOne({ dni: persona.dni, 'empresa._id': empresaID }, (err, doc) => {
        if (doc != null)
            return res.status(500).json({ success: false, message: 'Ya existe el documento en el padron' });

        //persona.persona_id = new objectID();
        Personas.create(persona, mongoResponse(res, 'Persona insertada con exito'));
    });

}

function put(req, res) {
    const persona = req.body;
    const personaID = new objectID(req.params.id);
    const periodoObjID = new objectID(persona.periodo_id);
    const persona_idObj = new objectID(persona.persona_id);

    Personas.update(
        { _id: personaID },
        { $set: persona },
        (err, doc) => {
            Liquidaciones.update({
                'legajo.persona.persona_id': persona_idObj,
                'periodo._id': periodoObjID
            },
                { $set: { 'legajo.persona': persona } },
                { multi: true },
                (err, doc) => {
                    console.log(doc);
                    Legajos.update(
                        {
                            'persona.persona_id': persona_idObj
                        },
                        {
                            $set: { persona: persona }
                        },
                        { multi: true },
                        mongoResponse(res, 'Persona actualizada')
                    );
                });
        });
}


/**
 * Delete de personas
 * @param {objectid} :id
 */
function destroy(req, res) {
    var personaID = new objectID(req.params.id);
    var sMessage = 'Persona eliminada con exito';
    var periodo = req.params.periodo;

    Personas.delete(
        { _id: personaID },
        (err, doc) => {
            Legajos.delete(
                { 'persona._id': personaID },
                { multi: true },
                (err, doc) => {
                    Liquidaciones.delete(
                        {
                            'legajo.persona._id': personaID,
                            'periodo._id': periodo
                        },
                        { multi: true },
                        mongoResponse(res, sMessage)
                    )
                });
        });
}

function csv(req, res) {
    var periodo = req.body.periodo;
    var empresa = req.body.empresa;
    var csv = req.body.csv;

    var options = {
        delimiter: ';'
    };

    var os = ObrasSociales.find({}, (err, obrasSociales) => { });
    var actividades = Actividades.find({}, (err, actividades) => { });
    var condiciones = Condiciones.find({}, (err, condiciones) => { });
    var modalidades = Modalidades.find({}, (err, modalidades) => { });
    var zonas = Zonas.find({}, (err, zones) => { });
    var situaciones = Situaciones.find({}, (err, situaciones) => { });

    var personas = csvjson.toObject(csv, options);

    Promise.all([os, actividades, condiciones, modalidades, zonas, situaciones]).then(resultados => {
        obras_sociales = resultados[0];
        actividades = resultados[1];
        condiciones = resultados[2];
        modalidades = resultados[3];
        zonas = resultados[4];
        situaciones = resultados[5];
        var repetidos = [];
        var nuevasPersonas = [];

        for (let i = 0; i < personas.length; i++) {

            obra_social = obras_sociales.find(item => item.codigo == personas[i].obra_social);
            actividad = actividades.find(item => item.codigo == personas[i].c_actividad);
            condicion = condiciones.find(item => item.codigo == personas[i].condicion);
            modalidad = modalidades.find(item => item.codigo == personas[i].modalidad);
            zona = zonas.find(item => item.codigo == personas[i].zona);
            situacion = situaciones.find(item => item.codigo == personas[i].situacion);
            
            var newPersona = {
                "nombre": personas[i].nombre,
                "cuil": personas[i].cuil,
                "dni": personas[i].dni,
                "tipo_documento": personas[i].tipo_documento,
                "sexo": personas[i].sexo,
                "nacionalidad": personas[i].nacionalidad,
                "conyugue": personas[i].conyugue,
                "cantidad_hijos": personas[i].cantidad_hijos,
                "periodo_id": periodo._id,
                "datos_administrativos": {
                    "obra_social": obra_social,
                    "c_actividad": actividad,
                    "condicion": condicion,
                    "modalidad": modalidad,
                    "zona": zona,
                    "situacion": situacion,
                    "sv": personas[i].sv,
                    "cc": personas[i].cc,
                    "adherentes": personas[i].adherentes,
                },
                "empresa": empresa,
                "__v": 0,
                "domicilio": {
                    "calle": personas[i].calle,
                    "numero": personas[i].numero,
                    "codigo_postal": personas[i].codigo_postal,
                    "localidad": personas[i].localidad,
                    "provincia": personas[i].provincia
                }
            };

            if(personas[i].fecha_nacimiento != ""){
                var peronsa_fecha_nacimiento = personas[i].fecha_nacimiento.split(/\//);
                var newPersonona_fecha_nacimiento = [peronsa_fecha_nacimiento[1], peronsa_fecha_nacimiento[0], peronsa_fecha_nacimiento[2]].join('/');
                newPersona.datos_administrativos.fecha_alta = newPersonona_fecha_nacimiento;
            }
            if(personas[i].fecha_alta != ""){
                var peronsa_fecha_alta = personas[i].fecha_alta.split(/\//);
                var newPersonona_fecha_alta = [peronsa_fecha_alta[1], peronsa_fecha_alta[0], peronsa_fecha_alta[2]].join('/');
                newPersona.datos_administrativos.fecha_alta = newPersonona_fecha_alta;
            }
            if(personas[i].cuit != "" && personas[i].nombre != ""){
                nuevasPersonas.push(newPersona);
            }
        }

        Personas.insertMany(nuevasPersonas, (err, doc) => {
            if (err != null) {
                return res.status(500).json({
                    success: false,
                    message: 'Hay personas fuera del formato correspondiente.'
                });
            } else {
                return res.json({
                    success: true,
                    message: 'Personas agregadas correctamente'
                });
            }
        });

    });
}

function descargarCSV(req, res) {
    res.download("public/importar_padron.csv");
}

function adherentes(req, res) {

    var persona = new objectID(req.params.persona);
    var adherentes = req.body;

    Personas.update(
        { _id: persona },
        { $set: {"datos_administrativos.adherentes" : adherentes}},
        {upsert : true},
        (err, doc) => {
            if (err != null) {
                console.log(err)
                return res.status(500).json({
                    success: false,
                    message: 'Error en el servidor.'
                });
            } else {
                return res.json({
                    success: true,
                    message: 'Adherentes agregados correctamente.'
                });
            }
        });
}

module.exports = {
    get,
    getOnePersonaId,
    getPersonasByLiquidacion,
    getOne,
    post,
    put,
    destroy,
    csv,
    descargarCSV,
    adherentes
}
