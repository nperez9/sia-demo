const base = '/personas';
const personas = require('./personas');
//const inserter = require('./insertpersonas');

module.exports = function (router) {
    router.get(base + '/persona_id/:id', personas.getOnePersonaId);
    router.get(base + '/liquidacion/', personas.getPersonasByLiquidacion);
    router.get(base + '/one/:id', personas.getOne);
    router.get(base + '/:empresa/:periodo', personas.get);
    
    router.post(base, personas.post);
    router.post(base + "/csv", personas.csv);
    router.post(base + '/csvejemplo', personas.descargarCSV);
    router.post(base + '/adherentes/:persona', personas.adherentes);

    router.put(base + '/:id/:periodo', personas.put);

    router.delete(base + '/:id/:periodo', personas.destroy);
}
