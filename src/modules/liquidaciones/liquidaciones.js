const validator = require('../../helpers/validationHelper');
const mongoResponse = require('../responses/mongoResponse');
const mongoose = require('mongoose');
const objectID = mongoose.Types.ObjectId;

const Liquidaciones = mongoose.model('liquidaciones');
const Legajos = mongoose.model('legajos');
const Periodos = mongoose.model('periodos');
const fs = require('fs'); 
const http = require('http');
var pdf = require('html-pdf');
const mustache = require('mustache');
const moment = require('moment');

function getListadoLegajos (req, res) {

    if(!req.cookies.token){
        return res.status(403).json({success: false, message: 'Acceso denegado'});
    }

    var filter = {};
    filter['legajo.subempresa.empresa._id'] = new objectID(req.query.empresaId);
    filter['periodo._id'] = new objectID(req.query.periodoId);

    if (typeof(req.query.subempresaId) !== 'undefined')
        filter['legajo.subempresa._id'] = new objectID(req.query.subempresaId);

    if (typeof(req.query.estado) !== 'undefined') {
        let estados = req.query.estado.split(',');
        filter['$or'] = [];
        estados.forEach(element => {
            filter['$or'].push({estado: element});
        });
    }

    if (typeof(req.query.nombre) !== 'undefined')
        filter['legajo.persona.nombre'] = req.query.nombre;

    console.log(filter);
    
    Liquidaciones.find(filter)
    .sort({'legajo.persona.nombre':1})
    .exec((err, items) => {
        return res.json(items);
    });
}


function getOne (req, res) {
    var liquidacionObjId = new objectID(req.params.id);

    Liquidaciones.findOne({_id: liquidacionObjId}, (err, liquidacion) => {
        return res.json(liquidacion);
    });
}


function setTotales (req, res) {
    var liqID = new objectID(req.params.id)
    var insertData = req.body.totales;
    insertData['conceptos'] = req.body.conceptos;

    Liquidaciones.findOneAndUpdate(
    {_id: liqID},
    {$set: insertData},
    (err, liquidacion) => {
        res.json({'success': true});
    });
}


function getAll (req, res) {
    Empresas.find({}, (err, docs) => {
        res.json(docs);
    });
}


function getData (req, res) {
    let empresaID = new objectID(req.params.empresaid);
    var periodo = new objectID(req.params.periodo);

    SubEmpresas.find({ 'empresa._id': empresaID, periodo : periodo }, (err, subempresas) => {
        Cargos.find({}, (err, cargos) => {
            Personas.find({}, (err, personas) => {
                res.json({
                    subempresas,
                    cargos,
                    personas
                });
            });
        });
    });
}


function getByLegajoAndPeriodo (req, res) {
    var filter = {
        'legajo._id': new objectID(req.params.legajoid),
        'periodo._id': new objectID(req.params.periodoid)
    };
    console.log('------------------------------------');
    console.log(filter);
    console.log('------------------------------------');
    Liquidaciones.findOne(filter, (err, doc) => {
        if (doc != null)
            return res.json(doc);
        else {
            Legajos.findOne({_id: filter['legajo._id']}, (err, legajo) => {
            Periodos.findOne({_id: filter['periodo._id']}, (err, periodo) => {
                Liquidaciones.create({legajo, periodo}, (err, liq) => {
                    return res.json(liq);
                });
            });
            });
        }
    });
}


function post (req, res) {
    let legajo = req.body;
    Liquidaciones.create(legajo,
    (err, newLegajo) => {
        return res.json(newLegajo);
    });
}


function getById (req, res) {
    let empresaID = new objectID(req.params.id);

    Empresas.findOne({_id: empresaID},
    (err, empresa) => {
        return res.json(empresa);
    });
}


function setFijo (req, res) {
    var liquidacionID = new objectID(req.params.id);
    var liquidacionConceptoID = new objectID(req.body.liquidacion_id);
    var fijo = req.body.fijo;
    var activo = req.body.activo;

    Liquidaciones.findOneAndUpdate(
        {'conceptos.liquidacion_id': liquidacionConceptoID},
        {
            $set: {
                'conceptos.$.fijo': fijo,
                'conceptos.$.activo': activo
            }
        },
        (err, doc) => {
            return res.json({success: true});
        }
    );
}

function editConcepto (req, res) {
    // var concepto = req.body;
    var concepto = req.body.concepto || req.body;

    var liquidacionConceptoID = new objectID(concepto.liquidacion_id);

    Liquidaciones.findOneAndUpdate(
        {'conceptos.liquidacion_id': liquidacionConceptoID},
        {'conceptos.$': concepto},
        (err, doc) => {
            return res.json({success: true});
        }
    );
}

function addLinea (req, res) {
    var conceptos = req.body;
    var liquidacionID = new objectID(req.params.id);

    Liquidaciones.findOne({_id: liquidacionID}, (err, liquidacion) => {
        var check = liquidacion.conceptos.filter( (concepto) => concepto._id == conceptos._id)
        if (check.length > 0)
            return res.status(410).json({success: false, message: 'El concepto ya esta agregado'});
        Liquidaciones.findOneAndUpdate(
            {_id: liquidacionID},
            {$push: {conceptos: conceptos}},
            (err, doc) => {
                return res.json({
                    success:true,
                    message: 'Concepto agregado con exito'
                });
            });
    });
}

function deleteLinea (req, res) {
    var liquidacionID = new objectID(req.params.id);
    var liquidacionConceptoID = new objectID(req.params.liquidacionconceptoid);

    var message = 'Fila eliminada';

    Liquidaciones.findOneAndUpdate(
        {_id: liquidacionID},
        {$pull: {conceptos: {liquidacion_id: liquidacionConceptoID}}},
        (err, doc) => {
            return res.json({
                success: true,
                message: 'Concepto eliminado de la liquidacion'
            });
        });
}

function editMemo (req, res) {
    var memo = req.body.memo;
    var id = new objectID(req.params.id);

    Liquidaciones.findOneAndUpdate(
        {_id: id},
        {$set: {memo: memo}},
        (err, doc) => {
            return res.json(
              {
                success: true,
                message: 'Memo modificado'
              });
        }
    );
}

function getMemos(req,res){
    var periodo = new objectID(req.params.periodo);
    var empresa = new objectID(req.params.empresa);

    Liquidaciones.find(
        { 'legajo.subempresa.empresa._id': empresa, 'periodo._id' : periodo }, 
        (err, liquidaciones) => {
            
            var memos = [];

            for (let a = 0; a < liquidaciones.length; a++) {
                if (memos.indexOf(liquidaciones[a].memo) < 0 && liquidaciones[a].memo != "") {
                    var unMemo = {};

                    unMemo.nombre = liquidaciones[a].legajo.persona.nombre;
                    unMemo.legajo = liquidaciones[a].legajo.numero_legajo == undefined ? liquidaciones[a].legajo.numero_sistema : liquidaciones[a].legajo.numero_legajo;
                    unMemo.memo = liquidaciones[a].memo;
                    memos.push(unMemo);
                }
            }

            var view = {
                memos: memos,
                fecha_actual: moment().format('DD/MM/YYYY')
            }

            var content = fs.readFileSync("src/modules/liquidaciones/reporte.memo.tpl.html", 'utf8');
            html = mustache.render(content, view);
                
            var options = {
                format: 'a4'
              };
        
              pdf.create(html, options).toStream(function (err, stream) {
                stream.pipe(res);
              });
    });
}

function legajosActivos (req, res){
    var periodo = new objectID(req.params.periodo);
    var empresa = new objectID(req.params.empresa);

    var match = {
        'periodo._id': periodo,
        'legajo.subempresa.empresa._id': empresa
      }
    
      Liquidaciones.aggregate([
        { $match: match },
        { $unwind: '$legajo'},
        {
          $group: {
            _id: '$legajo.subempresa._id',
            subempresa: {$first: '$legajo.subempresa.descripcion'},
            legajos: {$push: '$legajo'},
          }
        },
        // { $sort: sort }
      ]).exec((err, docs) => {

        var subempresas = {
            subempresa : [],
            total_legajos : 0,
            total_empleados : 0
        }

        for (let i = 0; i < docs.length; i++) {

            var subempresa = {
                nombre : docs[i].subempresa,
                legajos_activos : 0,
                empleados : []
            }

            for (let e = 0; e < docs[i].legajos.length; e++) {
                if(docs[i].legajos[e].estado == "normal"){
                    subempresa.legajos_activos++;
                }

                if(subempresa.empleados.indexOf(docs[i].legajos[e].persona._id) == -1){
                    subempresa.empleados.push(docs[i].legajos[e].persona._id)
                }

            }
            
            subempresa.empleados = subempresa.empleados.length;

            subempresas.total_empleados = subempresa.empleados + subempresas.total_empleados;
            subempresas.total_legajos = subempresa.legajos_activos + subempresas.total_legajos;

            subempresas.subempresa.push(subempresa);
        }

        return res.json(subempresas)
      });


}



module.exports = {
    getListadoLegajos,
    getOne,
    getByLegajoAndPeriodo,
    getAll,
    getData,
    post,
    setFijo,
    editConcepto,
    addLinea,
    deleteLinea,
    setTotales,
    editMemo,
    getMemos,
    legajosActivos
}
