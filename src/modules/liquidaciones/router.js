const base = '/liquidaciones';
const liquidaciones = require('./liquidaciones');
const isLoggued = require('../../auth/auth_middleware.js');

module.exports = function (router) {
    router.get(base + '/legajos/', isLoggued, liquidaciones.getListadoLegajos);
    router.get(base + '/:id', liquidaciones.getOne);
    router.get(base + '/:legajoid/:periodoid', liquidaciones.getByLegajoAndPeriodo);
    router.get(base + '/legajos/activos/:periodo/:empresa', liquidaciones.legajosActivos);
    
    router.put(base + '/fijo/:id', liquidaciones.setFijo);
    router.put(base + '/totales/:id', liquidaciones.setTotales);
    router.put(base + '/concepto/:id', liquidaciones.editConcepto);
    router.put(base + '/memo/:id', liquidaciones.editMemo);
    
    router.post(base + '/memos/:periodo/:empresa', liquidaciones.getMemos);
    router.post(base + '/conceptos/:id', liquidaciones.addLinea);

    router.delete(base + '/:id/conceptos/:liquidacionconceptoid', liquidaciones.deleteLinea);
}
