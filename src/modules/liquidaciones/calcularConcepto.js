function calcularFormulaConcepto (formula, liquidacion) {
    if (formula === undefined || formula == '')
        return false;

    if (liquidacion !== undefined) {
        formula = formula.replace(/\b(legajo->horas)\b/g, $scope.legajoDetalle.horas);
        formula = formula.replace(/\b(legajo->importe1)\b/g,  $scope.legajoDetalle.importe1);
        formula = formula.replace(/\b(legajo->importe2)\b/g,  $scope.legajoDetalle.importe2);
        formula = formula.replace(/\b(legajo->importe3)\b/g,  $scope.legajoDetalle.importe3);
        formula = formula.replace(/\b(legajo->subsidio)\b/g,  $scope.legajoDetalle.subsidio);
        formula = formula.replace(/\b[Q]\b/g, liquidacion.cantidad);
        formula = formula.replace(/\b[I]\b/g, liquidacion.importe);

        if (typeof(liquidacion.importe) !== 'undefined' && liquidacion.importe != 0) {
            formula = '(' + formula + ')*' + liquidacion.cantidad;
        }
    }
    // si se cagotea aca devuelve 1
    try {
        
        if (anioPat.test(formula)) {
            formula = formula.replace(anioPat, calculaAntiguedad($scope.legajoDetalle.antiguedad.anios, $scope.legajoDetalle.subempresa.categoria._id));
            console.log('con antiguedad:', formula);
        }
        //reemplazar vl por valores
        while (remPat.test(formula)) {
            formula = formula.replace(remPat, calcularRemunerativo());
        }

        while (patt.test(formula)){
            var rx = /(Vl+)\w+/g;
            var arr = rx.exec(formula);
            var l = arr[0].length;
            var sp = formula.indexOf("Vl");
            var val = formula.substring(sp+2, sp+l);
            val = parseInt(val);

            var valor = $filter('filter')($rootScope.valor, { codigo: val }, true)[0];
            formula = formula.replaceBetween(sp, sp+l, valor.valor.toString());
        }

        //reemplazar ! por su propio indice
        while (patt2.test(formula)) {
            var rx = /(![A-Z])\w+/g;
            var arr = rx.exec(formula);
            var l = arr[0].length;
            var sp = formula.indexOf("!");
            var val = formula.substring(sp+1, sp+l);
            var valor = $filter('filter')($rootScope.conceptos, { codigo: val }, true)[0];

            if (typeof(valor) === "undefined") {
                Notification.error('El Concepto: '+ val + ' no esta cargado. Requerido por el concepto: ' + liquidacion.codigo );
                formula = formula.replaceBetween(sp, sp+l, '0');
            }
            else if (valor.indice != 0) {
                formula = formula.replaceBetween(sp, sp+l, valor.indice.toString());
            }
            else {
                var myLiquidacion = $filter('filter')($scope.liquidacion, { codigo: val }, true)[0];
                if (myLiquidacion !== undefined)
                    formula = formula.replaceBetween(sp, sp+l, $scope.formularFuc(myLiquidacion.formula, myLiquidacion));
                else
                    formula = formula.replaceBetween(sp, sp+l, '0');
            }
        }

        //reemplazar functions de excels
        while (patt3.test(formula)) {
            var rx = /([IF])\w+/g;
            var arr = rx.exec(formula);
            var l = arr[0].length;
            var sp = formula.indexOf("IF"); //start point
            var text = formula.substring(sp);

            var temp = 0;
            var temp2 = 0;
            var first = false;
            for (var i=0; i<text.length; i++){
                if(text.charAt(i) == '('){
                    temp++;
                    first = true;
                }
                if(text.charAt(i) == ')'){
                    temp2 = i;
                    temp--;
                }
                if(temp == 0 && first == true){
                    break;
                }
            }
            text = text.substring(0,temp2+1);
            text = text.replace(/(#[A-Z])\w+/g, "1");

            var formattedFormula = excelFormulaUtilities.formula2JavaScript(text);
            var x = eval(formattedFormula);
            formula = formula.replaceBetween(sp, sp + temp2+1, x.toString());
            //                 text = text.substring(0,temp2+1);
            //                 text = text.replace(/(#[A-Z])\w+/g, "1");

            //var formattedFormula = excelFormulaUtilities.formula2JavaScript(text);
            //var x = eval(formattedFormula);
            //formula = formula.replaceBetween(sp, sp + temp2+1, 1);
        }

        while (patt4.test(formula)) {
            var rx = /(#[A-Z])\w+/g;
            var arr = rx.exec(formula);
            var l = arr[0].length;
            var sp = formula.indexOf("#");
            var val = formula.substring(sp+1, sp+l);
            formula = formula.replaceBetween(sp, sp+l, calculaNumerales(val));
        }
        return formula;
    }
    catch (e) {

        console.log(e);
        console.log(formula);
        return 9;
    }
}

module.exports = calcularFormulaConcepto;