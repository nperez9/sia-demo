const mongoose = require('mongoose');
const Usuarios = mongoose.model('usuarios');
const crypt = require('../helpers/cryptHelper');

/**
 * Callback for LocalStrategy
 * @param {string} email 
 * @param {string} password 
 * @param {function} done 
 */

var loginStrategy = (username, password, done) => {
	//let myPassword = crypt.encrypt(password);
	Usuarios.findOne({ email: username, password: password },
		(err, logUser) => {

			if (err) {
				console.log(err);
			}

			if (logUser === null) {
				return done(null, false, {
					message: 'No existe el usuario'
				});
			}

			return done(null, logUser);
		});
}

module.exports = loginStrategy; 