var jwt = require('jsonwebtoken');

// middleware para autenticacion
function isLoggedIn(req, res, next) {

	var token = req.cookies.token;

	if (token) {
		jwt.verify(token, 'siasueldo-secret', function (err, decoded) {
			if (err) {
				return res.status(403).json({
					message: "Error de validacion de credenciales"
				});
			}
			else {
				return next();
			}
		});
	} else {
		return res.status(403).send({
			success: false,
			message: 'Sesion no inicializada.'
		});
	}
}

module.exports = isLoggedIn;