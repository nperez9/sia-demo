const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;
const categoriaSchema = require('./categoriaSchema');

var valorSchema = new Schema({
    valor: Number,
    codigo: Number,
    descripcion: String,
    fecha_inicio: { default: Date.now, type: Date },
    fecha_fin: { default: null, type: Date },
    provincia: Number
});

var conceptoSchema = new Schema({
    fecha_alta: { default: Date.now, type: Date },
    descripcion: String,
    empresa: {
        _id: Schema.Types.ObjectId,
        razon_social: String
    },
    categoria_codigo: String,
    valores: valorSchema,
    formula: String,
    ganancias: Boolean,
    indice: {type: Number, default: 0},
    columna_haberes: Boolean,
    orden: String,
    fijo: Boolean,
    imp_recibo: Boolean,
    ganancias: Boolean, 
    imprime: Boolean,
    tipo_afip: Number,
    tipo_remuneracion: Number,
    codigo: String,
    fijo: Boolean,
    sac: Boolean,
    caja_complementaria: Boolean,
    clase: Number,
    marcas: {
        marca:   {type:Number, default:0},
        marca_1: {type:Number, default:0}, 
        marca_2: {type:Number, default:0}, 
        marca_3: {type:Number, default:0}, 
        marca_4: {type:Number, default:0}, 
        marca_5: {type:Number, default:0}
    }
});

conceptoSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
valorSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });

Mongoose.model('valores', valorSchema, 'valores');
Mongoose.model('conceptos', conceptoSchema, 'conceptos');

module.exports = valorSchema;
