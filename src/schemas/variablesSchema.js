const Mongoose = require('mongoose'); 
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var variablesSchema = new Mongoose.Schema({
    campo_1: {type:String, default:""},
    campo_2: {type:String, default:""},
    campo_3: {type:String, default:""},
    campo_4: {type:String, default:""},
    campo_5: {type:String, default:""},
    valor : Number,
    empresa: Schema.Types.ObjectId,
    periodo: Schema.Types.ObjectId
}); 

variablesSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('variables', variablesSchema, 'variables');

module.exports = variablesSchema;
