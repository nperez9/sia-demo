const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete'); 
const Schema = Mongoose.Schema; 

var categoriaSchema = new Schema({
    nombre: String,
    codigo: String
}); 

categoriaSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('categorias', categoriaSchema, 'categorias');

module.exports = categoriaSchema; 
