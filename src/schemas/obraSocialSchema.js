const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var obraSocialSchema = new Schema({
    codigo: Number,
    nombre: String
});

obraSocialSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('obras_sociales', obraSocialSchema, 'obras_sociales');
module.exports = obraSocialSchema;