const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var cargoSchema = new Schema({
    fecha_alta: {default: Date.now, type: Date},
    nombre: String,
    cargo: Number,
    nomenclatura: String,
    indice: Number,
    categoria_codigo: String,
    conceptos_template:[{
        codigo: String,
        descripcion: String, 
        _id: Schema.Types.ObjectId   
    }]
});

cargoSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });

Mongoose.model('cargos', cargoSchema, 'cargos');

module.exports = cargoSchema;