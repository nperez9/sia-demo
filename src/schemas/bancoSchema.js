const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var bancoSchema = new Schema({
    codigo: Number,
    nombre: String,
    cuenta: Boolean,
    l_cuenta: Number,
    sucursal: Boolean,
    l_sucursal: Number,
    tipo: Boolean,
    l_tipo: Number,
    cbu: String,
    l_cbu: String
});

bancoSchema.plugin(mongooseDelete, { overrideMethods: Boolean, deletedAt: Boolean });
Mongoose.model('bancos', bancoSchema, 'bancos');

module.exports = bancoSchema;
