const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;
const conceptoSchema = require('./conceptoSchema');

var cajaComplementariaConfigSchema = new Schema({
    empresa: Schema.Types.ObjectId,
    conceptos: [{
        codigo : String,
        descripcion : String
    }],
});

cajaComplementariaConfigSchema.plugin(mongooseDelete, { overrideMethods: Boolean, deletedAt: Boolean });
Mongoose.model('caja_config', cajaComplementariaConfigSchema, 'caja_config');

module.exports = cajaComplementariaConfigSchema;
