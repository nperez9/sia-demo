const Mongoose = require('mongoose'); 
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var usuarioSchema = new Mongoose.Schema({
    nombre: String, 
    email: String,
    password: String,
    key: String,
    tipo: String,
    permiso_empresas: [{
        id: Schema.Types.ObjectId,
        razon_social : String
    }],
    activado: {type: Number, default: 0}
}); 

usuarioSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('usuarios', usuarioSchema, 'usuarios');

module.exports = usuarioSchema;
