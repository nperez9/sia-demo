const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;
const categoriaSchema = require('./categoriaSchema');

var antiguedadSchema = new Schema({
    antiguedad : Number,
    porcentaje : Number,
    categoria : categoriaSchema,
    importe : Number,
    observaciones : String,
    empresa: {
        _id: Schema.Types.ObjectId,
        razon_social: String
    },
    fecha_alta: {type:Date, default: Date.now}
});

antiguedadSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('antiguedad', antiguedadSchema, 'antiguedad');

module.exports = antiguedadSchema;
