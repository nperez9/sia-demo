const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;
const empresaSchema = require('./empresaSchema');

var subEmpresaSchema = new Schema({
    descripcion: String,
    categoria_codigo: String,
    categoria: {
        _id: Schema.Types.ObjectId,
        nombre: String,
        codigo: String
    },
    subempresa_id: {
        type: Schema.Types.ObjectId, 
        required: true
    },
    caracteristicas : String,
    subvencion : {type: Number, default : 0},
    afip : Boolean,
    banco : Boolean,
    libro_sueldo : Boolean,
    nombre : String,
    domicilio : String,
    listado: String,
    codigo: Number,
    empresa: empresaSchema,
    periodo_id: {
        type: Schema.Types.ObjectId,
        required: true
    },
    fecha_alta: {
        type:Date, 
        default: Date.now
    }
});

subEmpresaSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('sub_empresas', subEmpresaSchema, 'sub_empresas');

module.exports = subEmpresaSchema;
