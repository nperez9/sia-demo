const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

// Call all the Schemas
const valorSchema = require('./conceptoSchema');
const legajoSchema = require('./legajoSchema');
const periodoSchema = require('./periodoSchema');
const gananciaSchema = require('./gananciaSchema');

var liquidacionSchema = new Schema({
    legajo: legajoSchema,
    conceptos: [{
        fecha_alta: { default: Date.now, type: Date },
        descripcion: String,
        empresa: {
            default: null,
            type: {
                _id: Schema.Types.ObjectId,
                nombre: String
            }
        },
        categoria_codigo: String,
        valores: valorSchema,
        formula: String,
        ganancias: Boolean,
        indice: Number,
        columna_haberes: Boolean,
        orden: Number,
        fijo: {type: Boolean, default: false},
        fijo_mensual: {type: Number, default: null},
        imp_recibo: Boolean,
        tipo_afip: Number,
        tipo_remuneracion: Number,
        codigo: String,
        cantidad: Number,
        importe: Number,
        clase: Number,
        descipcion_custom: String,
        liquidacion_id: {
            type: Schema.Types.ObjectId,
            auto: true,
            required:true
        }, 
        sac: Boolean,
        caja_complementaria: Boolean,
        calculado: Number,
        activo: {
            type: Boolean, 
            default: true
        },
        marcas: {
            marca:   {type:Number, default:0},
            marca_1: {type:Number, default:0}, 
            marca_2: {type:Number, default:0}, 
            marca_3: {type:Number, default:0}, 
            marca_4: {type:Number, default:0}, 
            marca_5: {type:Number, default:0}
        }
    }],
    remunerativo: Number,
    remunerativo_sac: {type:Number, default: 0},
    no_remunerativo: Number,
    deducciones: Number,
    bruto: {type: Number , default:0},
    neto: {type: Number , default:0},
    total_persona: {
        bruto: Number,
        neto: Number
    },
    periodo: periodoSchema,
    ganancias: gananciaSchema,
    ganancias_calculada: {
        alicuota: {type: Number, default: 0},
        mensual: {type: Number, default: 0},
        anual: {type: Number, default: 0}
    },
    neto_con_ganancias: {type: Number, default: 0},
    memo : {type: String, default:""}
});

liquidacionSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('liquidaciones', liquidacionSchema, 'liquidaciones');
module.exports = liquidacionSchema;
