const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var periodoSchema = new Schema({
    fecha_inicio: Date,
    fecha_fin: Date,
    mes: Number,
    anio: Number,
    sac: {type: Boolean, default: false},
    semestre: Number,
    tipo: String,
    descripcion: String,
    empresa: {
        type: Schema.Types.ObjectId, 
        required: true
    },
    cerrado: {type: Boolean, default: false}
});

periodoSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });

Mongoose.model('periodos', periodoSchema, 'periodos');

module.exports = periodoSchema;