const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var modalidadSchema = new Schema({
    codigo: Number,
    descripcion: String
});

modalidadSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('modalidades', modalidadSchema, 'modalidades');
module.exports = modalidadSchema;