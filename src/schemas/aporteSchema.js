const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var aporteSchema = new Schema({
    nombre: String
});

cargoSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });

Mongoose.model('aportes', aporteSchema, 'aportes');

module.exports = aporteSchema; 