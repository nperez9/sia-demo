const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var empresaSchema = new Schema({
    nombre: String,
    direccion: String,
    localidad: String,
    codigo_postal: String,
    cuit: String,
    razon_social: String,
    tipo: String,
    provincia: String,
    email: String,
    actividad: String,
    telefono: String,
    registro: String,
    distrito: String,
    bancos: Schema.Types.Mixed,
    activo: {default: true, type: Boolean},
    asignacion_familiar: {default:false, type:Boolean},
    quincenal: {default:false, type:Boolean},
    no_remunerativo_familiar: {default:false, type:Boolean},
    fecha_alta: {default: Date.now, type: Date},
    recibos: String,
    datos_listado: {
        razon_social: String,
        domicilio: String,
        telefonos: String,
        localidad: String,
        codigo_postal: String,
        cuit: String,
        ingresos_brutos: String,
        registro: String,
        region: String,
        distrito: String
    }
});

empresaSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('empresas', empresaSchema, 'empresas');

module.exports = empresaSchema;
