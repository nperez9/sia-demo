const Mongoose = require('mongoose'); 
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;
// const empresaSchema = require('./empresaSchema');

var zonaSchema = new Mongoose.Schema({
    codigo: Number, 
    descripcion: String,
    valor: Number
}); 

zonaSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('zonas', zonaSchema, 'zonas_afip');

module.exports = zonaSchema;
