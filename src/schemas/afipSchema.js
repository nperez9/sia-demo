const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var tempAportesSchema = new Schema({
    empresa: Schema.Types.ObjectId,
    periodo: Schema.Types.ObjectId,
    nombre: String,
    nro_legajo: Number,
    categoria: String,
    ordenador: String,
    caracter: String,
    situacion: Number,
    estado: String,
    provincia: String,
    jubilacion: String,
    seguro_vida: Boolean,
    convenio_colectivo: Boolean,
    alta: Date,
    baja: Date,
    dias_trabajados: Number,
    dni: Number,
    cuil: String,
    codigo_actividad: Number,
    condicion: Number,
    modalidad: Number,
    adherentes: Number,
    codigo_os: Number,
    conyuge: Boolean,
    cantidad_hijos: Number,
    codigo_zona: Number,
    localidad: String,
    tipo: Number,
    campo_1: Number,
    campo_2: Number,
    campo_3: Number,
    campo_4: Number,
    campo_5: Number,
    campo_6: Number,
    campo_7: Number,
    campo_8: Number,
    campo_9: Number,
    campo_10: Number,
    campo_11: Number,
    campo_12: Number,
    campo_13: Number,
    campo_14: Number,
    campo_15: Number,
    campo_16: Number,
    campo_17: Number,
    campo_18: Number,
    campo_19: Number,
    campo_20: Number,
    campo_21: Number,
    campo_22: Number,
    campo_23: Number,
    campo_24: Number,
    campo_25: Number,
    campo_26: Number,
    campo_27: Number,
    campo_28: Number,
    campo_29: Number,
    campo_30: Number,
    campo_31: Number,
    campo_32: Number,
    campo_33: Number,
    campo_34: Number,
    campo_35: Number,
    campo_36: Number,
    campo_37: Number,
    campo_38: Number,
    campo_39: Number,
    campo_40: Number,
    campo_41: Number,
    campo_42: Number,
    campo_43: Number,
    campo_44: Number,
    campo_45: Number,
    campo_46: Number,
    campo_47: Number,
    campo_48: Number,
    campo_49: Number,
    campo_50: Number,
    campo_51: Number,
    campo_52: Number,
    campo_53: Number,
    campo_54: Number,
    campo_55: Number,
    campo_56: Number,
    campo_57: Number,
    campo_58: Number,
    campo_59: Number,
    campo_60: Number
});

tempAportesSchema.plugin(mongooseDelete, { overrideMethods: Boolean, deletedAt: Boolean });
Mongoose.model('tempAportes', tempAportesSchema, 'temp_aportes');

var aportesSchema = new Schema({
    empresa: Schema.Types.ObjectId,
    periodo: Schema.Types.ObjectId,
    col01: String,
    col02: String,
    col03: String,
    col04: String,
    col05: String,
    col06: String,
    col07: String,
    col08: String,
    col09: Number,
    col10: String,
    col11: String,
    col12: String,
    col13: Number,
    col14: Number,
    col15: Number,
    col16: Number,
    col17: Number,
    col18: Number,
    col19: Number,
    col20: String,
    col21: Number,
    col22: Number,
    col23: Number,
    col24: String,
    col25: String,
    col26: Number,
    col27: String,
    col28: Number,
    col29: String,
    col30: String,
    col31: String,
    col32: String,
    col33: String,
    col34: String,
    col35: String,
    col36: Number,
    col37: Number,
    col38: Number,
    col39: Number,
    col40: Number,
    col41: Number,
    col42: Number,
    col43: String,
    col44: Number,
    col45: String,
    col46: Number,
    col47: Number,
    col48: Number,
    col49: Number,
    col50: String,
    col51: Number,
    col52: Number,
    col53: Number,
    col54: Number,
    col55: Number,
    col56: String,
    col57: String,
    col58: String,
    col59: Number,
    col60: Number,
    col61: Number,
    col62: Number,
    col63: Number,
    col64: Number,
    col65: Number
});

aportesSchema.plugin(mongooseDelete, { overrideMethods: Boolean, deletedAt: Boolean });
Mongoose.model('aportes', aportesSchema, 'aportes');
Mongoose.model('aportes1', aportesSchema, 'aportes1');

module.exports = tempAportesSchema;
