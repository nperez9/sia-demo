const Mongoose = require('mongoose'); 
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var reporteBancoSchema = new Mongoose.Schema({
    banco: {
        _id : Schema.Types.ObjectId,
        nombre: String
     },
    head:[{
        columna: Number,
        ancho: { type: String, default: "" },
        alinea: { type: String, default: "" },
        relleno: { type: String, default: "" },
        literal: { type: String, default: "" },
        formula: { type: String, default: "" }    
    }],
    body:[{
        columna: Number,
        ancho: { type: String, default: "" },
        alinea: { type: String, default: "" },
        relleno: { type: String, default: "" },
        literal: { type: String, default: "" },
        formula: { type: String, default: "" }    
    }],
    footer:[{
        columna: Number,
        ancho: { type: String, default: "" },
        alinea: { type: String, default: "" },
        relleno: { type: String, default: "" },
        literal: { type: String, default: "" },
        formula: { type: String, default: "" }    
    }],
    
}); 

reporteBancoSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('reporte_banco', reporteBancoSchema, 'reporte_banco');

module.exports = reporteBancoSchema;
