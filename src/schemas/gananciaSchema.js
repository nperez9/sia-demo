const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var gananciaSchema = new Schema({
    descripcion: String,
    fecha_alta: {default: Date.now, type: Date},
    presentacion: Schema.Types.Mixed,
    nombre_archivo: String,
    keys: {
        anio: {type: Number},
        persona_id: {type: Schema.Types.ObjectId},
        empresa_id: {type: Schema.Types.ObjectId}
    },
    datos_ganancias: [{
        _id:{
            type: Schema.Types.ObjectId,
            auto: true,
            required:true
        },
        periodo_mes: Number,
        conyugue: {type: Boolean, default: false},
        hijos: {type: Number, default: 0},
        o_cargos: {type: Number, default: 0},
        prepaga: {type: Number, default: 0},
        seguro_vida: {type: Number, default: 0},
        hipoteca: {type: Number, default: 0},
        donacion: {type: Number, default: 0},
        servicio_domestico: {type: Number, default: 0},
        gastos_medicos: {type: Number, default: 0},
        sepelio: {type: Number, default: 0},
        desc_sac: {type: Number, default: 0},
        alquiler: {type: Number, default: 0},
        material_didactico: {type: Number, default: 0},
        horas_extra: {type: Number, default: 0},
        pago_cta: {type: Number, default: 0}
    }],
    otros_sueldos: [{
        _id:{
            type: Schema.Types.ObjectId,
            auto: true,
            required:true
        },
        periodo_mes: Number,
        cuit: String,
        razon_social: String,
        domicilio: String,
        neto: {type: Number, default: 0}
    }]

});

gananciaSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });

Mongoose.model('ganancias', gananciaSchema, 'ganancias');

module.exports = gananciaSchema;