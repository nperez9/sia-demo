const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var actividadSchema = new Schema({
    codigo: Number,
    descripcion: String
});

actividadSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('actividades', actividadSchema, 'actividades');
module.exports = actividadSchema;