const mainSchema = require('./src/schemas/liquidacionSchema');
const tempAportesSchema = require('./src/schemas/afipSchema');
const Usuarios = require('./src/schemas/usuarioSchema');
const Variables = require('./src/schemas/variablesSchema');
const caja_configSchema = require('./src/schemas/cajaComplementariaConfigSchema');
const tabla_subsidios = require('./src/schemas/tablaSubsidiosSchema');
const reporte_bancos = require('./src/schemas/reporteBancoSchema');