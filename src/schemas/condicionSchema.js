const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var condicionSchema = new Schema({
    codigo: Number,
    descripcion: String
});

condicionSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('condiciones', condicionSchema, 'condiciones');
module.exports = condicionSchema;