const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;
const subEmpresaSchema = require('./subEmpresaSchema');

var tablaSubsidiosSchema = new Schema({
    empresa: Schema.Types.ObjectId,
    periodo: Schema.Types.ObjectId,
    subempresa: subEmpresaSchema,
    porcentaje: Number,
    afip: {type : Boolean , default : false},
    banco: {type : Boolean , default : false}
});

tablaSubsidiosSchema.plugin(mongooseDelete, { overrideMethods: Boolean, deletedAt: Boolean });
Mongoose.model('tabla_subsidios', tablaSubsidiosSchema, 'tabla_subsidios');

module.exports = tablaSubsidiosSchema;
