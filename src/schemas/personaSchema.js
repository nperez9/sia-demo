const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;
const bancoSchema = require('./bancoSchema');
const obraSocialSchema = require('./obraSocialSchema');
const actividadSchema = require('./actividadSchema');
const modalidadSchema = require('./modalidadSchema');
const condicionSchema = require('./condicionSchema');
const situacionSchema = require('./situacionSchema');
const zonaSchema = require('./zonaSchema');

var personaSchema = new Schema({
    nombre: String,
    cuil: String,
    fecha_nacimiento: Date,
    tipo_documento: String,
    dni: { type: Number, required: true },
    domicilio: {
        depto: String,
        calle: String,
        numero: Number,
        piso: Number,
        localidad: String,
        provincia: String,
        codigo_postal: String
    },
    empresa: {
        _id: Schema.Types.ObjectId,
        razon_social: String
    },
    sexo: String,
    nacionalidad: String,
    estado_civil: Number,
    cantidad_hijos: { type: Number, default: 0 },
    conyugue: { type: Boolean, default: false },
    persona_id: {
        type: Schema.Types.ObjectId,
        required: true,
        auto: true
    },
    periodo_id: Schema.Types.ObjectId,
    datos_administrativos: {
        banco: bancoSchema,
        agente_retencion: {
            type: Boolean,
            default: true
        },
        numero_cuenta: String,
        cbu: Number,
        sucursal: String,
        tipo_cuenta: String,
        fecha_alta: { type: Date, default: Date.now },
        adherentes: {
            cantidad: { type: Number, default: 0 },
            adherentes: [Schema.Types.Mixed]
        },
        cc: { type: Boolean, default: false },
        sv: { type: Boolean, default: false },
        obra_social: obraSocialSchema,
        c_actividad: actividadSchema,
        modalidad: modalidadSchema,
        condicion: condicionSchema,
        zona: zonaSchema,
        opc_jubilatoria: String,
        situacion: situacionSchema,
        inicio: String
    },
});

personaSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });

Mongoose.model('personas', personaSchema, 'personas');

module.exports = personaSchema;
