const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var situacionSchema = new Schema({
    codigo: Number,
    descripcion: String
});

situacionSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('situaciones', situacionSchema, 'situaciones');
module.exports = situacionSchema;