const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const Schema = Mongoose.Schema;

var afipTempAportes = new Schema({
    nombre : Number,
    numero_legajo : Number,
    categoria : categoriaSchema,
    ordenador : Number,
    caracter : String,
    situacion : String,
    estado : String,
    estado : String,
    provincia : String,
    jubilacion : String,
    seguro_vida : String,
    convenio_colectivo : Boolean,
    alta : date,
    baja : date,
    fecha_alta: {type:Date, default: Date.now}
});

antiguedadSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
Mongoose.model('antiguedad', antiguedadSchema, 'antiguedad');

module.exports = antiguedadSchema;
