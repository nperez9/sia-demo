const Mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const mongooseSequence = require('mongoose-sequence') (Mongoose);

const Schema = Mongoose.Schema;
const personaSchema = require('./personaSchema');
const subempresaSchema = require('./subEmpresaSchema');
const cargoSchema = require('./cargoSchema');
const antiguedadSchema = require('./antiguedadSchema');
const periodoSchema = require('./periodoSchema');
const categoriaSchema = require('./categoriaSchema');

var legajoSchema = new Schema({
    cargo: cargoSchema,
    cargo_editable: String,
    numero_legajo: Number,
    numero_sistema: Number, // como el numero de legajo, pero es una secuencia automatica del sistema x Empresa
    horas: {
        type:Number,
        default: 0
    },
    cantidad: Number,
    antiguedad: {
        anios: {
            type:Number,
            default: 0
        },
        meses: {
            type:Number,
            default: 0
        }
    },
    licencias: [{
        goce_sueldo: Boolean,
        articulo: String,
        fecha_inicio: Date,
        fecha_fin: Date
    }],
    suplencias: [{
        legajo: Number,
        cuil: String,
        titular: String,
        fecha_desde: Date,
        fecha_hasta: Date,
        motivo: String,
        dias: Number
    }],
    importe1: {
        type:Number,
        default:0
    },
    importe2: {
        type:Number,
        default:0
    },
    importe3: {
        type:Number,
        default:0
    },
    subsidio: {
        type:Number,
        default:0
    },
    persona: personaSchema,
    subempresa: {
        _id:{ type: Schema.Types.ObjectId, required: true},
        descripcion: String,
        empresa: {
            _id: Schema.Types.ObjectId,
            razon_social: String,
            datos_listado: Schema.Types.Mixed,
            asignacion_familiar: Boolean,
            quincenal: Boolean,
            no_remunerativo_familiar: Boolean,
            cuit: String,
            tipo: String
        },
        categoria: {
            _id: Schema.Types.ObjectId,
            nombre: String,
            codigo: String
        },
        subvencion: Number,
        afip: Boolean,
        banco: Boolean,
        libro_sueldo: Boolean,
        codigo: String
    },
    suvencion: Boolean,
    fecha_alta: Date,
    fecha_baja: Date,
    registro_alta: {type:Date, default: Date.now},
    registro_baja: Date,
    caracter: String,
    ordenador: String,
    observaciones : String,
    provincia: String,
    estado: String
});

legajoSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });

Mongoose.model('legajos', legajoSchema, 'legajos');


module.exports = legajoSchema;