const config = require('../../config/config');
const nodeMailerModule = require('nodemailer'); 

var nodeMailer = nodeMailerModule.createTransport(config.nodeMailerConfig);

/**
 * Builds the html email from text Object
 * @param {Object} text 
 * @return {String} template
 */
function htmlBuilder (text) {
    var space = '<div><font color="#444444"><br></font></div>'; 
    var content = '';
    
    text.content.forEach(function(element) {
        content += '<div><font color="#444444">'+element+'</font></div>';
        content += space; 
    }, this);

    var template = 
    '<div>' + 
        '<div><font color="#444444">Hola '+ text.name +',</font></div>' 
        + space + space +
        content 
        + space +
        '<div><font color="#444444">Gracias,</font></div>'
        + space +
        '<div><b style="color:rgb(68,68,68)">SIA</b><br></div>'
        + space +
    '</div>';

    return template; 
}

/**
 * Builds the plain Text email from text Object
 * @param {Object} text 
 * @return {String} textPlane 
 */
function planeTextBuilder (text) {
    var content = ''
    
    text.content.forEach(function(element) {
        content +=  element; 
        content += '  '; 
    }, this);

    var textPlane = 'Hello' + text.name + '  ' + content; 

}

module.exports = 
/**
 * recive los siguientes parametros para el envio de emails
 * {to: String, from: String, Subject: String, text: String}
 * @param {Object} emailOptions
 * @return {Void}
 */
function (emailOptions) {
    if (!emailOptions.hasOwnProperty('to'))
        throw new Error('You must enter an Email');

    if (!emailOptions.hasOwnProperty('from'))
        emailOptions.from = '"SIA" <info@sia.com>'

    if (!emailOptions.hasOwnProperty('subject'))
        emailOptions.subject = ''; 
        
    if (!emailOptions.hasOwnProperty('text'))
        emailOptions.text = ''; 

    var html = htmlBuilder(emailOptions.text); 
    var text = planeTextBuilder(emailOptions.text); 

    let mailOptions = {
        from: emailOptions.from,
        to: emailOptions.to, 
        subject: emailOptions.subject, // Subject line
        text: text,
        html: html
    };

    // send mail with defined transport object
    nodeMailer.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
}
