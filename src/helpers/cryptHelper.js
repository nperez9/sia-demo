module.exports = {
	crypto: require('crypto'),
	algorithm: 'aes-256-ctr',
	password: 'lloraPepelui',

	/**
	* Crypt a string into sha256 with the module password (for users passwords)
	* @param {string} Text to encrypt
	* @return {string} Encrypted text  
	*/
	encrypt: (text) => {
		const cipher = this.crypto.createCipher(this.algorithm, this.password);
		let crypted = cipher.update(text, 'utf8', 'hex');
		crypted += cipher.final('hex');
		return crypted;
	},

	/**
	* Decrypt a string sha256 with the module password (for users passwords)
	* @param {string} Text to desencrypt
	* @return {string} Desencrypted text
	*/
	decrypt: (text) => {
		const decipher = this.crypto.createDecipher(this.algorithm, this.password)
		let dec = decipher.update(text, 'hex', 'utf8')
		dec += decipher.final('utf8');
		return dec;
	}
}