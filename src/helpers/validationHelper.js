/**
* Module for common sistem validation
*/
module.exports = {
    /**
    * Rustic Validation of queryString
    * @param {Object} QueryString
    * @return {Object} Validated QueryString
    */
    validationSearch: (params) => {
        let filterAux = {}; 
        let defaultValue = /.*/; //valor por defecto para los textos 
        //let searchRegex = 
    
        if (params.hasOwnProperty('search') && params.search != '' && params.search.length < 5000) {
            filterAux.search = params.search;  
        } else {   
            filterAux.search = '';
        }

        if (params.hasOwnProperty('articleType') && params.articleType != '') {
            filterAux.articleType = params.articleType; 
        } else {
            filterAux.articleType = defaultValue; 
        }

        if (params.hasOwnProperty('lenguage') && params.articleType != '') {
            filterAux.lenguage = params.lenguage; 
        } else {
            filterAux.lenguage = defaultValue; 
        }

        if (params.hasOwnProperty('limit') && isNaN(params.limit)) {
            filterAux.limit = parseInt(params.limit); 
        } else {
            filterAux.limit = 20; 
        }

        if (params.hasOwnProperty('page') && !isNaN(params.page)) {
            filterAux.page = parseInt(params.page); 
            filterAux.skip = filterAux.page * filterAux.limit; 
        } else {
            filterAux.page = 0; 
            filterAux.skip = 0; 
        }

        if (params.hasOwnProperty('orderBy')) {
            switch (params.orderBy) {
                case 'recent':
                    filterAux.order = {
                        "MedlineCitation.Article.ArticleDate.Year": -1, 
                        "MedlineCitation.Article.ArticleDate.Month": -1, 
                        "MedlineCitation.Article.ArticleDate.Day": -1
                    }; 
                    break;
                case 'oldest':
                    filterAux.order = {
                        "MedlineCitation.Article.ArticleDate.Year": 1, 
                        "MedlineCitation.Article.ArticleDate.Month": 1, 
                        "MedlineCitation.Article.ArticleDate.Day": 1
                    }; 
                    break;
                case 'more-view':
                    filterAux.order = {
                        visits:-1
                    }; 
                    break; 
                case 'less-view':
                    filterAux.order = {
                        visits:1
                    }; 
                    break; 
                default:
                    filterAux.order = {
                        '_id': -1
                    }; 
                    break;
            }
        }
        else {
            filterAux.order = {
                '_id': -1
            }; 
        }
        
        filterAux.find = {}; 
        if (filterAux.search != '')
            filterAux.find["$text"] = {"$search": filterAux.search, "$caseSensitive": false}; 
        filterAux.find['MedlineCitation.Article.PublicationTypeList.PublicationType'] = filterAux.articleType; 
		filterAux.find['MedlineCitation.Article.Language'] = filterAux.lenguage; 
        
        return filterAux; 
    }, 

    /**
    * Validate the singUp data
    */
    validationSignUp: function (params) {
        var error = ''; 
        var mailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    
        if (params.name == '' || params.name.length >= 200) 
            error += 'Wrong name'; 

        if (params.email == '' || !mailRegex.test(params.email)) 
            error += 'Wrong Email'; 

        error += this.password(params.password, params.passwordConfirm); 

        if (error != '') {
            console.log(error); 
            return false; 
        }

        return {
            name: params.name, 
            email: params.email,
            password: params.password
        }; 
    }, 

    password: (password, passwordConfirm) => {
        let ret = ''; 
        
        if (password == '' || passwordConfirm == '' || 
            password != passwordConfirm)
            ret += 'Wrong Pass'; 

        return ret; 
    },

    email: (email) => {
        let ret = ''; 
        var mailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        
        if (email == '' || email === undefined || !mailRegex.test(email))     
            ret = 'Wrong Mail';

        return ret;
    } ,
    
    /**
     * Validacion de las liquidaciones 
     * @param {Object} postData 
     */
    liquidacion: (postData) => {
        let liquidacion = {
            codigoConcepto: postData.codigoConcepto, 
            legajoId: postData.legajoId, 
            conceptoId: postData.conceptoId
        }; 

        if (postData.hasOwnProperty('cantidad') && postData.cantidad != '') {
            liquidacion.cantidad = postData.cantidad; 
        }
        if (postData.hasOwnProperty('importe') && postData.importe != '') {
            liquidacion.importe = postData.importe; 
        }
        if (postData.hasOwnProperty('descripcionCustom') && postData.descripcionCustom != '') {
            liquidacion.descripcionCustom = postData.descripcionCustom; 
        }

        return liquidacion; 
    }

}